<?php

use app\components\ProfileMenu;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Тикет №' . Yii::$app->request->get('id');
?>
<div class="profile-wrapper row">
    <?php echo ProfileMenu::widget(); ?>
    <div class="profile-content col-lg-9 support">
        <h1><?php echo $this->title . ((!empty($ticket) && $ticket->closed) ? ' (тикет закрыт)' : ''); ?></h1>
        <?php if (Yii::$app->session->getFlash('error')) {?>
            <div class="bg-danger message-error"><?php echo Yii::$app->session->getFlash('error'); ?></div>
        <?php } ?>
            <div class="chat-window">
                <div class="object">
                    <h3>Тема: <?php echo $ticket['theme']; ?></h3>
                </div>
                    <div class="chat" >
                        <?php Pjax::begin([
                            'id' => 'pjax-support',
                            'timeout' => false,
                            'scrollTo' => false
                        ]); ?>
                        <?php if (!empty($ticket)) { ?>
                        <?php if (!empty($ticket->messages)) { ?>
                        <?php foreach ($ticket->messages as $message) { 
                            if (!empty($message['user_message'])) { ?>
                                <div class="message bg-success">
                                    <div class="name"><?php echo 'От: ' . Yii::$app->user->identity->name . ' - ' . Yii::$app->formatter->asDate($message['time'], 'dd.MM.yyyy H:m:s'); ?></div>
                                    <div class="text"><?php echo $message['user_message']; ?></div>
                                    <?php if (!empty($message['user_file'])) {?>
                                        <?php $fileName = explode('/', $message['user_file']); ?>
                                        <div class="file">
                                            <a data-pjax=0 href="/<?php echo $message['user_file']; ?>"><span class="glyphicon glyphicon-file"></span><?php echo array_pop($fileName); ?></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php }
                                if (!empty($message['user_img'])) {?>
                                    <div class="user-img img-thumbnail">
                                        <div class="name"><?php echo 'От: ' . Yii::$app->user->identity->name . ' - ' . Yii::$app->formatter->asDate($message['time'], 'dd.MM.yyyy H:m:s'); ?></div>
                                        <a data-fancybox="gallery" href="/<?php echo $message['user_img']; ?>"><img src="/<?php echo $message['user_img']; ?>"></a>
                                    </div>
                                <?php } 
                            if (!empty($message['support_message'])) { ?>
                            <div class="message bg-info">
<!--                                <div class="name">--><?php //echo 'От: ' . $ticket->admin->name . ' - ' . Yii::$app->formatter->asDate($message['time'], 'dd.MM.yyyy H:m:s'); ?><!--</div>-->
                                <div class="name"><?php echo 'От: Помощник Монокля - ' . Yii::$app->formatter->asDate($message['time'], 'dd.MM.yyyy H:m:s'); ?></div>
                                <div class="text"><?php echo $message['support_message']; ?></div>
                            </div>
                            <?php }
                                if (!empty($message['support_img'])) { ?>
                                <div class="support-img img-thumbnail">
                                    <div class="name"><?php echo 'От: Помощник Монокля - ' . Yii::$app->formatter->asDate($message['time'], 'dd.MM.yyyy H:m:s'); ?></div>
                                    <a data-fancybox="gallery" href="/<?php echo $message['support_img']; ?>"><img src="/<?php echo $message['support_img']; ?>"></a>
                                </div>
                            <?php } ?>
                         <?php } ?>
                        <a id="supp" href="<?php echo Url::current(['id' => Yii::$app->request->get('id')]); ?>"></a>
                        <?php Pjax::end(); ?>
                    </div>
                        <?php } ?>
                        <?php } ?>
            </div>
        <div class="form">
            <?php $form = ActiveForm::begin([
                'id' => 'message',
                'enableClientValidation' => true,
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <fieldset class="form-group">
                <?php echo $form->field($model, 'message')->textarea([
                    'rows' => 5,
                    'disabled' => (!empty($ticket) ? (!empty($ticket) && $ticket->closed) ? true : false : true)
                ]); ?>
                <div class="upload-file">
                    <?php echo $form->field($model, 'file', [
                    'template' => '<span class="glyphicon glyphicon-download-alt"></span>{label}{input}{error}'
                    ])->fileInput(); ?>
                </div>
            </fieldset>
            <button type="submit" class="btn btn-raised btn-primary" <?php echo (!empty($ticket) ? (!empty($ticket) && $ticket->closed) ? 'disabled' : 'enabled' : 'disabled'); ?>>Отправить</button>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php if (!(!empty($ticket) && $ticket->closed)) {?>
<script type="text/javascript">
    $(document).ready(function () {
        setInterval(function () {
            $('#supp').click();
        }, 5000);
    });
</script>
<?php } ?>

