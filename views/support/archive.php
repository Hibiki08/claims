<?php

use app\components\ProfileMenu;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Архив тикетов';
?>
<div class="profile-wrapper row">
    <?php echo ProfileMenu::widget(); ?>
    <div class="profile-content col-lg-9 notifications">
        <h1><?php echo $this->title; ?></h1>
        <?php echo GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
//            'rowOptions' => function ($model, $key, $index, $grid){
//                return [
//                    'data-key' => $model['id']
//                ];
//            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Номер тикета',
                    'attribute' => 'id',
                    'value' => function ($model, $key, $index, $column) {
                        return $model['id'];
                    },
                    'format' => 'html',
                ],
                [
                    'label' => 'Тема',
                    'value' => function ($model, $key, $index, $column) {
                        return $model['theme'];
                    },
                    'format' => 'raw',
                ],
                [
                    'label' => 'Статус',
                    'value' => function ($model, $key, $index, $column) {
                        return $model['closed'] ? 'Закрыт' : 'Открыт';
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'contentOptions' => ['class' => 'buttons'],
                    'buttons' => [
                        'view' => function($model, $key, $index) {
//                            $link = unserialize($key['request']);
//                            $link['search-notice'] = $key['id'];
//                            array_unshift($link, 'request/index');
                            return Html::a(
                                '<span class="glyphicon glyphicon-eye-open">',
                                Url::to(['/profile/support/conversation', 'id' => $key['id']])
                                , ['title' => 'Посмотреть']);
                        },
                    ]
                ]
            ],
        ]) ?>
    </div>
</div>
