<?php

use app\components\ProfileMenu;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Поддержка';
?>
<div class="profile-wrapper row">
    <?php echo ProfileMenu::widget(); ?>
    <div class="profile-content col-lg-9 support">
        <h1><?php echo $this->title; ?></h1>
        <blockquote class="warning">
            <p>Спросите мнение Monocle о сходстве знаков, результатах проведенного поиска, юридических рисках, функциях поисковой системы.</p>
        </blockquote>
        <?php $form = ActiveForm::begin([
            'id' => 'support',
            'enableClientValidation' => true,
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>
        <fieldset class="form-group">
            <?php echo $form->field($model, 'theme')->input('text'); ?>
            <?php echo $form->field($model, 'message')->textarea(['rows' => 5]); ?>
            <?php if (empty(Yii::$app->user->identity->name)) {
                echo $form->field($model, 'name')->input('text');
            } else {
                echo $form->field($model, 'name', [
                    'template' => '{input}'
                ])->hiddenInput(['value' => Yii::$app->user->identity->name])->label(false);
             } ?>
            <div class="upload-file">
                <?php echo $form->field($model, 'file', [
                    'template' => '<span class="glyphicon glyphicon-download-alt"></span>{label}{input}{error}'
                ])->fileInput(); ?>
            </div>
        </fieldset>
        <button type="submit" class="btn btn-raised btn-primary">Отправить</button>
        <?php ActiveForm::end(); ?>
    </div>
</div>
