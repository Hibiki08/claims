<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\Requests;
use app\models\Trademarks;
use app\components\TrackingMenu;

/* @var $this yii\web\View */
/* @var $searchRequests yii\data\ActiveDataProvider */

$this->title = 'Отслеживание';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tracking">
    <h1><?= Html::encode($this->title) ?></h1>
    <blockquote>
        <p>Уведомления о новых запросах приходят в строку уведомлений <i class="glyphicon glyphicon-bell"></i>, на почту и в телеграм.
            <i class="small">
                <a data-pjax=0 href="<?php echo Url::to(['/profile/notifications']); ?>">Редактировать настройки</a>
            </i>
            <i class="small">
                <a data-pjax=0 href="<?php echo Url::to(['/tracking/instruction']); ?>">Как пользоваться отслеживанием</a>
            </i>
        </p>
    </blockquote>
    <div class="profile-wrapper row requests-index">
        <?php echo TrackingMenu::widget(); ?>
        <div class="profile-content col-lg-9">
            <?php Pjax::begin(['id' => 'pjax-grid', 'timeout' => false]); ?>
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'form-horizontal ',
                            'data-pjax' => 1
                        ],
                        'fieldConfig' => [
                        ],
                        'action' => [''],
                    ]); ?>
                    <div class="add">Ручное добавление:</div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-10">
                            <?= $form->field($tracking, 'search_request')->textInput([
                                'placeholder' => 'Вставьте сюда адрес страницы с результатами выдачи'
                            ])->label(false) ?>
                        </div>
                        <div class="col-lg-2">
                            <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                    <?php if(Yii::$app->session->getFlash('success')) {?>
                        <div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
                            <span><?php echo Yii::$app->session->getFlash('success'); ?></span>
                        </div>
                    <?php } ?>
                    <?= GridView::widget([
                        'dataProvider' => $searchRequests,
                        'summary' => '',
                        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                        'rowOptions' => function ($model) {
                            if ($model['notice']) {
                                return ['class' => 'success', 'data-id' => $model['id']];
                            } else {
                                return ['data-id' => $model['id']];
                            }
                        },
                        'columns' => [
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{active}',
                                'contentOptions' => ['class' => 'buttons'],
                                'buttons' => [
                                    'active' => function($model, $key, $index) {
                                        if ($key['active']) {
                                            return '<label title="Деактивировать" class="switch"><input type="checkbox" data-model="' . $key->tableName() . '" checked><span class="slider round"></span></label>';
                                        }
                                        return '<label title="Активировать" class="switch"><input type="checkbox" data-model="' . $key->tableName() . '"><span class="slider round"></span></label>';
                                    },

                                ]
                            ],
                            ['class' => 'yii\grid\SerialColumn'],
                            'id:text:ID',
                            [
                                'label' => 'Запрос',
                                'attribute' => 'request',
                                'value' => function ($model, $key, $index, $column) {
                                    $string = '<div><h4 class="title" data-model="' . $model->tableName() . '">' . $model['title'] . '</h4><span title="Редактировать" class="glyphicon glyphicon-pencil edit"></span>';
                                    if (isset($model->history)) {
                                        $string .= '<ul>';
                                        foreach ($model->history as $history) {
                                            $itemsIds = '';
                                            if (!empty($history->requests_diff_ids)) {
                                                $reqIds = unserialize($history->requests_diff_ids);
                                                $requestsNumber = Requests::getReqNumbers($reqIds);
                                                if (!empty($requestsNumber)) {
                                                    $itemsIds .= '<ul>Заявки:';
                                                    foreach ($requestsNumber as $diff) {
                                                        $itemsIds .= '<li><a href="' . Url::to(['request/index', 'string' => $diff['request_number'], 'type' => 'requests']) . '">' . $diff['request_number'] . '</a></li>';
                                                    }
                                                    $itemsIds .= '</ul>';
                                                }
                                            }
                                            if (!empty($history->trademarks_diff_ids)) {
                                                $tradeIds = unserialize($history->trademarks_diff_ids);
                                                $trademarksNumber = Trademarks::getReqNumbers($tradeIds);
                                                if (!empty($trademarksNumber)) {
                                                    $itemsIds .= '<ul>Товарные знаки:';
                                                    foreach ($trademarksNumber as $diff) {
                                                        $itemsIds .= '<li><a href="' . Url::to(['request/index', 'string' => $diff['request_number'], 'type' => 'trademarks' ]) . '">' . $diff['request_number'] . '</a></li>';
                                                    }
                                                    $itemsIds .= '</ul>';
                                                }
                                            }
                                            $string .= '<li><span><b>' . date('d.m.Y', strtotime($history->date)) . '</b></span><span> ' . $history->event . (!empty($history->count) ? ' на ' . $history->count . $itemsIds : '') . '</span></li>';
                                        }
                                        $string .= '</ul></div>';
                                    }
                                    return $string;
                                },
                                'headerOptions' => ['class' => 'tracking-width'],
                                'contentOptions' => ['class' => 'tracking-width'],
                                'format' => 'raw',
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view} {delete}',
                                'contentOptions' => ['class' => 'buttons'],
                                'buttons' => [
                                    'view' => function($model, $key, $index) {
                                        $link = unserialize($key['request']);
                                        $link['search-notice'] = $key['id'];
                                        array_unshift($link, 'request/index');
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-eye-open">',
                                            Url::to($link),
                                            [
                                                'title' => 'Посмотреть',
                                                'data-pjax' => 0
                                            ]);
                                    },
                                    'delete' => function($model, $key, $index) {
                                        return '<span title="Удалить" class="glyphicon glyphicon-trash delete" data-model="' . $key->tableName() . '"></span>';
                                    }
                                ]
                            ]
                        ],
                    ]); ?>
        </div>
    </div>
    <script>
        $( function() {
            $( "#tabs" ).tabs();
        } );
    </script>

    <?php Pjax::end(); ?>
</div>