<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\Requests;
use app\models\Trademarks;
use app\components\TrackingMenu;

/* @var $this yii\web\View */
/* @var $searchRequests yii\data\ActiveDataProvider */

$this->title = 'Отслеживание';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tracking">
    <h1><?= Html::encode($this->title) ?></h1>
    <blockquote>
        <p>Уведомления о новых запросах приходят в строку уведомлений <i class="glyphicon glyphicon-bell"></i>, на почту и в телеграм.
            <i class="small">
                <a data-pjax=0 href="<?php echo Url::to(['/profile/notifications']); ?>">Редактировать настройки</a>
            </i>
            <i class="small">
                <a data-pjax=0 href="<?php echo Url::to(['/tracking/instruction']); ?>">Как пользоваться отслеживанием</a>
            </i>
        </p>
    </blockquote>
    <div class="profile-wrapper row requests-index">
        <?php echo TrackingMenu::widget(); ?>
        <div class="profile-content col-lg-9">
            <?php Pjax::begin(['id' => 'pjax-grid', 'timeout' => false]); ?>
                <?= GridView::widget([
                    'dataProvider' => $trademarks,
                    'summary' => '',
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'rowOptions' => function ($model) {
                        if ($model['notice']) {
                            return ['class' => 'success', 'data-id' => $model['id']];
                        } else {
                            return ['data-id' => $model['id']];
                        }
                    },
                    'columns' => [
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{active}',
                            'contentOptions' => ['class' => 'buttons'],
                            'buttons' => [
                                'active' => function($model, $key, $index) {
                                    if ($key['active']) {
                                        return '<label title="Деактивировать" class="switch"><input type="checkbox" data-model="' . $key->tableName() . '" checked><span class="slider round"></span></label>';
                                    }
                                    return '<label title="Активировать" class="switch"><input type="checkbox" data-model="' . $key->tableName() . '"><span class="slider round"></span></label>';
                                },

                            ]
                        ],
                        ['class' => 'yii\grid\SerialColumn'],
                        'id:text:ID',
                        [
                            'label' => 'Превью',
                            'value' => function ($model, $key, $index, $column) {
                                $img = $model->trademark->thumb;
                                return $model->trademark->thumb ? '<img src="/' . $model->trademark->thumb . '">' : '';
                            },
                            'contentOptions' => ['class' => 'thumb'],
                            'format' => 'raw',
                        ],
                        [
                            'label' => 'Товарный знак',
                            'attribute' => 'title',
                            'value' => function ($model, $key, $index, $column) {
                                $string = '<div><h4 class="title" data-model="' . $model->tableName() . '">' . $model['title'] . '</h4><span title="Редактировать" class="glyphicon glyphicon-pencil edit"></span>';
                                if (isset($model->history)) {
                                    $string .= '<ul>';
                                    foreach ($model->history as $history) {
                                        $string .= '<li><span><b>' . date('d.m.Y', strtotime($history->date)) . '</b></span><span> ' . $history->event . '</span></li>';
                                    }
                                    $string .= '</ul></div>';
                                }
                                return $string;
                            },
                            'headerOptions' => ['class' => 'tracking-width'],
                            'contentOptions' => ['class' => 'tracking-width'],
                            'format' => 'raw',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {delete}',
                            'contentOptions' => ['class' => 'buttons'],
                            'buttons' => [
                                'view' => function($model, $key, $index) {
                                    $link['string'] = $key->trademark->reg_number;
                                    $link['type'] = 'trademarks';
                                    $link['trademark-notice'] = $key['id'];
                                    array_unshift($link, 'request/index');
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-open">',
                                        Url::to($link),
                                        [
                                            'title' => 'Посмотреть',
                                            'data-pjax' => 0
                                        ]);
                                },
                                'delete' => function($model, $key, $index) {
                                    return '<span title="Удалить" class="glyphicon glyphicon-trash delete" data-model="' . $key->tableName() . '"></span>';
                                }
                            ]
                        ]
                    ],
                ]); ?>
        </div>
    </div>
    <script>
        $( function() {
            $( "#tabs" ).tabs();
        } );
    </script>

    <?php Pjax::end(); ?>
</div>