<?php if (!empty($request)) { ?>
    <?php $this->title = 'Заявка на товарный знак №' . $request->request_number . ' в системе Monocle'; ?>
    <div class="single">
        <?php if (!empty($request->img)) { ?>
            <img class="preview" src="/<?php echo $request->img; ?>" height="50"/>
        <?php } ?>
        <h1>Заявка на товарный знак №<?php echo $request->request_number; ?></h1>
        <p><i>Номер заявки:</i> <b><?php echo $request->request_number; ?></b></p>
        <p><i>Номер регистрации:</i> <b><?php echo $request->reg_number; ?></b></p>
        <p><i>Дата государственной регистрации:</i> <b><?php echo $request->reg_date; ?></b></p>
        <p><i>Дата поступления заявки:</i> <b><?php echo $request->request_date; ?></b></p>
        <p><i>Заявитель:</i> <b><?php echo $request->applicant; ?></b></p>
        <p><i>Статус:</i> <b><?php echo $request->status->title; ?></b></p>
        <p><i>Номер первой заявки:</i> <b><?php echo $request->first_request_number; ?></b></p>
        <p><i>Дата подачи первой заявки:</i> <b><?php echo $request->first_request_date; ?></b></p>
        <p><i>Код страны или международной организации, куда была подана первая заявка:</i> <b><?php echo $request->first_request_country; ?></b></p>
        <p><i>Адрес для переписки:</i> <b><?php echo $request->address; ?></b></p>
        <p><i>Факсимильные изображения:</i><br>
            <?php echo $request->facsimile; ?>
        </p>
        <p><i>Классы МКТУ:</i><br>
            <?php if (!empty($classes)) { ?>
        <p><?php foreach ($classes as $key => $description) { ?>
                <b><?php echo $key; ?></b> - <?php echo $description; ?><br>
            <?php } ?>
        </p>
    <?php } ?>
        </p>
    </div>
<?php } ?>