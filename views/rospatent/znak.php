<?php if (!empty($trademark)) { ?>
<div class="single">
    <?php if (!empty($trademark->img)) { ?>
        <img src="/<?php echo $trademark->img; ?>" height="50"/>
    <?php } ?>
    <?php $this->title = 'Товарный знак №' . $trademark->reg_number . ' в системе Monocle'; ?>
    <h1>Товарный знак №<?php echo $trademark->reg_number; ?></h1>
    <p><i>Номер заявки:</i> <b><?php echo $trademark->request_number; ?></b></p>
    <p><i>Номер регистрации:</i> <b><?php echo $trademark->reg_number; ?></b></p>
    <p><i>Дата государственной регистрации:</i> <b><?php echo $trademark->reg_date ?></b></p>
    <p><i>Дата поступления заявки:</i> <b><?php echo $trademark->request_date ?></b></p>
    <p><i>Дата публикации:</i> <b><?php echo $trademark->publish_date ?></b></p>
    <p><i>Дата истечения срока действия регистрации:</i> <b><?php echo $trademark->reg_expiry_date ?></b></p>
    <p><i>Дата прекращения правовой охраны товарного знака:</i> <b><?php echo $trademark->legal_protection_end_date ?></b></p>
    <p><i>Статус:</i> <b><?php echo $trademark->status->title ?></b></p>
    <p><i>Последнее изменение статуса:</i> <b><?php echo $trademark->status_date ?></b></p>
    <p><i>Приоритет:</i> <b><?php echo $trademark->priority ?></b></p>
    <p><i>Указание цвета или цветового сочетания:</i> <b><?php echo $trademark->colors ?></b></p>
    <p><i>Номер первой заявки:</i> <b><?php echo $trademark->first_request_number ?></b></p>
    <p><i>Дата подачи первой заявки:</i> <b><?php echo $trademark->first_request_date ?></b></p>
    <p><i>Код страны или международной организации, куда была подана первая заявка:</i> <b><?php echo $trademark->first_request_country ?></b></p>
    <p><i>Наименование лицензиата:</i> <b><?php echo $trademark->licensee ?></b></p>
    <p><i>Дата и номер регистрации договора:</i> <b><?php echo $trademark->contract_date ?>&nbsp;<?php echo $trademark->contract_number ?></b></p>
    <p><i>Указание условий и/или ограничений лицензии:</i> <b><?php echo $trademark->license_terms ?></b></p>
    <p><i>Правообладатель:</i> <b><?php echo $trademark->rightholder ?></b></p>
    <p><i>Адрес для переписки:</i> <b><?php echo $trademark->address ?></b></p>
    <p><i>Классы МКТУ:</i><br>
    <?php if (!empty($classes)) { ?>
        <p><?php foreach ($classes as $key => $description) { ?>
                <b><?php echo $key; ?></b> - <?php echo $description; ?><br>
            <?php } ?>
        </p>
    <?php } ?>
    </p>
</div>
<?php } ?>
