<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\models\RequestTerms;
use app\models\TrademarkTerms;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $terms array */

$this->title = 'Поиск по заявкам';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requests-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <? //= Html::a('Create Requests', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'pjax-requests', 'timeout' => false]); ?>
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'method' => 'get',
        'fieldConfig' => [
            'template' => "{input}",
            'options' => [
                'tag' => false,
            ],
        ],
        'action' => [''],
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="form-group">
        <div class="col-lg-6">
            <?= $form->field($searchModel, 'searchString')->input('search') ?>
        </div>
        <div class="col-lg-6">
            <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-5">
            <label class="range">Точность поиска:
                <input type="range" name="RequestSearch[similarity]" min="0.01" max="1" step="0.01" value="<?=$searchModel->similarity?>" id="similarity">
            </label>
        </div>
        <div class="col-lg-1 js_similarity"><?=$searchModel->similarity?></div>
    </div>

    <div class="form-group">
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'database')->radioList(['requests' => 'По заявкам', 'trademarks' => 'По товарным знакам']) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'searchArea')->radioList(['all' => 'По всему', 'applicant' => 'По заявителю/правообладателю', 'address' => 'По адресу для переписки', 'term' => 'По тегам изображений']) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6">
            <p>Классы:</p>
            <p>
                <button class="btn btn-info js-search-form-classes_selectall">Все</button>
                <button class="btn btn-info js-search-form-classes_deselectall">Ни одного</button>
            </p>
            <?php for ($i = 1; $i <= 45; ++$i): ?>
                <label class="checkbox-inline">
                    <input class="js-search-form-classes_checkbox" type="checkbox"
                           name="RequestSearch[searchClasses][<?= $i ?>]" <?= empty($searchModel->searchClasses[$i]) ? '' : 'checked' ?>> <?= $i ?>
                </label>
            <?php endfor; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'summary' => "Всего найдено: " . ($dataProvider->totalCount == 1000 ? 'более ' : '') . $dataProvider->totalCount . " за {$dataProvider->totalCount['time']} сек.",
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'rowOptions' => function ($model) {
            switch ($model->status_id) {
                case 7:
                case 8:
                case 14:
                    $cls = 'success';
                    break;

                case 4:
                case 5:
                case 6:
                case 13:
                case 15:
                    $cls = 'danger';
                    break;

                default:
                    $cls = '';
            }

            return ['class' => $cls];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label' => 'Изображение',
                'value' => function ($model, $key, $index, $column) {
                    //var_dump($model); die;
                    $result = Html::a(Html::img("/{$model->thumb}"), ['/' . $model->img], ['target' => '_blank', 'data-pjax' => '0', 'title' => $model->tableName()]);
                    switch($model->tableName()) {
                        case 'requests':
                            $terms = RequestTerms::find()
                                ->with('term')
                                ->where(['request_id' => $model->id]);
                            break;
                        case 'trademarks':
                            $terms = TrademarkTerms::find()
                                ->with('term')
                                ->where(['trademark_id' => $model->id]);
                            break;
                    }
                    $terms = $terms->all();

                    if (!empty($terms)) {
                        foreach ($terms as $term) {
                            $result .= "<p>{$term->term->term}</p>";
                        }
                    }

                    $result .= Html::a('Неверно!', ["/parser/wrong-tags?id={$model->id}&model={$model->tableName()}"], ['class' => 'js-wrong_tags', 'data-pjax' => '0', 'data-container' => $model->tableName()]);

                    return $result;
                    //return Html::img("/{$model->img}", ['style' => 'max-width: 200px']);
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Классы',
                'value' => function ($model, $key, $index, $column) {
                    $classes = unserialize($model->classes);
                    $arr = [];
                    if (!empty($classes)) {
                        foreach ($classes as $key => $value) {
                            //$result .= empty($result) ? $key : ", $key";
                            $arr[] = '<a tabindex="0" role="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="'.$value.'">'.$key.'</a>';
                        }
                    }
                    $result = implode('<br>',$arr);
                    return $result;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Номер и дата заявки',
                'value' => function ($model, $key, $index, $column) {
                    $date = empty($model->request_date) ? '' : date('d.m.Y', strtotime($model->request_date));
                    if (empty($model->request_number)) {
                        return $date;
                    }
                    return
                        Html::a("{$model->request_number} <small class=\"glyphicon glyphicon-new-window\"></small>", "http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTMAP&rn=6932&DocNumber={$model->request_number}&TypeFile=html", ['target' => '_blank']) . "<br>" .
                        $date . '<br><br><small>' .
                        Html::a("Делопроизводство <small class=\"glyphicon glyphicon-new-window\"></small>", "http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTMAP&DocNumber={$model->request_number}&TypeFile=html&Delo=1", ['target' => '_blank']) . '</small>';
                },
                'format' => 'raw',
            ],
            [
                'header' => 'Номер регистрации<br>и дата публикации',
                'value' => function ($model, $key, $index, $column) {
                    $date = empty($model->reg_date) ? '' : date('d.m.Y', strtotime($model->reg_date));
                    if (empty($model->reg_number)) {
                        return $date;
                    }
                    return Html::a("{$model->reg_number} <small class=\"glyphicon glyphicon-new-window\"></small>", "http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTM&rn=4386&DocNumber={$model->reg_number}", ['target' => '_blank']) . "<br>" . $date;
                },
                'format' => 'raw',
            ],
            [
                'header' => 'Заявитель<br>Адрес для переписки',
                'value' => function ($model, $key, $index, $column) {
                    switch($model->tableName()) {
                        case 'requests':
                            return $model->applicant . '<hr>' . $model->address;
                        case 'trademarks':
                            return $model->rightholder . '<hr>' . $model->address;
                    }
                },
                'format' => 'html',
            ],
            'status.title:text:Статус',
        ],
        'emptyText' => 'Ничего не найдено. Пора срочно подавать заявку!'
    ]); ?>
    <?php Pjax::end(); ?></div>
