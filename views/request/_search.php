<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="requests-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'request_number') ?>

    <?= $form->field($model, 'reg_number') ?>

    <?= $form->field($model, 'request_date') ?>

    <?= $form->field($model, 'reg_date') ?>

    <?php // echo $form->field($model, 'applicant') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'first_request_number') ?>

    <?php // echo $form->field($model, 'first_request_date') ?>

    <?php // echo $form->field($model, 'first_request_country') ?>

    <?php // echo $form->field($model, 'classes') ?>

    <?php // echo $form->field($model, 'img') ?>

    <?php // echo $form->field($model, 'facsimile') ?>

    <?php // echo $form->field($model, 'status_id') ?>

    <?php // echo $form->field($model, 'update_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
