<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\models\RequestTerms;
use app\models\TrademarkTerms;
use app\models\TmForeignTerm;
use app\models\RequestSearch;
use yii\helpers\Url;
use app\components\Parser;
use kartik\typeahead\Typeahead;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $terms array */

$this->title = 'Товарные знаки и заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requests-index">
    <div class="row">
        <div class="col-lg-7">
            <h1><?php echo Html::encode($this->title) ?></h1>
        </div>
        <div class="col-lg-5">
            <code class="bg-alert pull-right">
                <span>Дата обновления из базы ФИПС: <?php echo ($lastUpdateRequests > $lastUpdateTrademarks) ? $lastUpdateRequests : $lastUpdateTrademarks; ?></span><br>
                <span>Заявки: <?php echo $countRequests; ?> от <?php echo $lastUpdateRequests; ?></span><br>
                <span>Товарные знаки: <?php echo $countTrademarks; ?> от <?php echo $lastUpdateTrademarks; ?></span><br>

            </code>
        </div>
    </div>
    <div class="clear"></div>

    

    <?php Pjax::begin(['id' => 'pjax-grid', 'timeout' => false]); ?>
    <?php if(Yii::$app->session->getFlash('success')) {?>
        <blockquote class="request-success col-lg-6">
            <p>Запрос добавлен в отслеживаемые. При изменении выдачи вы получите уведомление на почту и в телеграм.<br>
                <a href="<?php echo Url::to(['profile/notifications/']); ?>"><i>Настройки отслеживания</i></a>
            </p>
        </blockquote>
    <?php } ?>
    <?php if(Yii::$app->session->getFlash('request_empty')) {?>
        <div class="alert alert-danger fade in alert-dismissable col-lg-6">
            <span><?php echo Yii::$app->session->getFlash('request_empty'); ?></span>
        </div>
    <?php } ?>

    <div class="collapse-search">
        <span class="collapse-search__text"></span>
        <span class="collapse-search__icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="15" viewBox="0 0 16 15"><defs><path id="ipr8a" d="M289.85 255.1l-5.73 7v6.23c0 .23-.12.45-.3.57a.63.63 0 0 1-.64.03l-2.94-1.52a.67.67 0 0 1-.36-.6v-4.72l-5.73-7a.69.69 0 0 1-.09-.7c.1-.24.33-.39.59-.39h14.7c.26 0 .48.15.59.39.1.23.07.5-.1.7zm-1.89.23h-11.92l4.98 6.09c.1.12.16.27.16.43v4.55l1.64.85v-5.4c0-.16.06-.31.16-.43z"/></defs><g><g transform="translate(-274 -254)"><use id="iconCollapse" fill="#b5b5b5" xlink:href="#ipr8a"/></g></g></svg>
        </span>
    </div>

    <div class="common-form-wrapper">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'request-form',
        'method' => 'get',
        'fieldConfig' => [
            'template' => "{input}",
            'options' => [
                'tag' => false,
            ],
        ],
        'action' => [''],
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>


    <div class="common-row">

        <div class="form-group">
            <div class="col-lg-12 form-group-wrapper">
                <?= $form->field($searchModel, 'string')->textInput(['name' => 'string', 'class' => 'form-control', 'value' => $searchModel->string])
                    ->widget(Typeahead::classname(), [
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'value',
                                'remote' => [
                                    'url'=> Url::to(['/request/query-list']) . '?string=QRY',
                                    'wildcard' => 'QRY',
                                ],
                                'limit' => 10
                            ]
                        ],
                        'pluginOptions' => ['highlight' => true, 'minLength' => 3],
                        'options' => ['placeholder' => 'Введите запрос ...', 'name' => 'string']
                    ]);
                ?>
                <div class="buttons">
                    <?= Html::submitButton('Искать', ['class' => 'btn btn-raised btn-primary btn-violet']) ?>
                    <?php if (Yii::$app->user->can('clientExtendedPermission')) {?>
                        <div id="add-to-track">
                            <button class="btn btn-raised btn-info btn-lime-green">Отслеживать</button>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php if (!empty($queryCorrected)) {?>
            <?php $params = Yii::$app->request->getQueryParams(); ?>
            <?php unset($params['RequestSearch']);?>
            <?php $params[0] = '/request';?>
            <div>Возможно, вы имели в виду: <a href="<?php echo Url::to($params) . '&string=' . urlencode($queryCorrected); ?>"><?php echo $queryCorrected; ?></a></div><br>
        <?php } ?>
        <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="row">
            <div class="col-lg-7">
                <a class="collapse-btn" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><span class="search_operators">Операторы поиска </span><span class="glyphicon glyphicon-menu-down"></span></a>
                <div class="collapse" id="collapseExample">
                    <div class="well operators">
                        <p>Оператор И всегда активен. Значит, что запрос «котенок песик» найдет документы, содержащие слова «котенок» и «песик».</p>
                        <p><span>|</span> &mdash; ИЛИ. Пример использования:  «котенок | песик».</p>
                        <p><span>- или !</span> &mdash; НЕ. Пример использования:  «котенок -песик (котенок !песик)».</p>
                        <!--<p><span>MAYBE</span> работает подобно оператору ИЛИ. Пример использования:  «котенок MAYBE песик».</p>-->
                        <p><span>=</span> &mdash; ищет точное соотвествие слову. Пример использования:  «котенок =песик».</p>
                        <p><span>""</span> &mdash; фраза целиком. Пример использования:  «"котенок песик"» найдет документы, где фраза встречается в указанном виде.</p>
                        <p><span><<</span> &mdash; порядок слов. Выводит документы, если слова идут в указанном порядке. Пример использования:  котенок << песик &mdash; выведет документ "котенок побежал за песиком", но не выведет "песик побежал за котенком".</p>
                        <p><span>*</span> &mdash; любое выражение. Пример использования:  котенок * песик &mdash; значит между котенком и песиком может быть любое слово.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <?php if (Yii::$app->user->can('clientExtendedPermission')) { ?>
                    <a class="instr" href="<?php echo Url::to(['/tracking/instruction']); ?>"><span class="svg-qst"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13" viewBox="0 0 13 13"><defs><path id="d3dia" d="M36.81 507.69v.3c0 .26-.2.47-.47.47a.47.47 0 0 1-.46-.46v-.31c0-.26.2-.47.46-.47s.47.21.47.47zm-6.81-3.2a6.5 6.5 0 1 1 13.01.02A6.5 6.5 0 0 1 30 504.5zm.93 0a5.57 5.57 0 1 0 11.15 0 5.57 5.57 0 0 0-11.15 0zm3.09-1.6c0-.07.02-.6.3-1.17.28-.56.86-1.21 2.08-1.21 1.09 0 1.66.45 1.95.82a2 2 0 0 1 .32 1.77c-.18.7-.68 1.21-1.13 1.66-.36.36-.69.7-.69.98a.47.47 0 1 1-.93 0c0-.66.49-1.15.95-1.63.38-.39.78-.79.9-1.24.1-.38.04-.72-.16-.97-.23-.3-.65-.46-1.22-.46-.6 0-1.02.24-1.25.7-.19.37-.2.74-.2.74 0 .26-.2.45-.46.45a.45.45 0 0 1-.46-.45z"/></defs><g><g transform="translate(-30 -498)"><use fill="#b5b5b5" xlink:href="#d3dia"/></g></g></svg></span>Как пользоваться отслеживанием</a>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="common-row">
        <div class="row">
            <div class="col-lg-12">
                <div class="common_title">По чему ищем</div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="type-list">
                    <?php if (!empty($searchModel->type)) { ?>
                        <?php $types = explode('.', $searchModel->type); ?>
                        <label>
                            <input class="checkbox-true" type="checkbox" value="requests" <?php echo in_array('requests', $types) ? 'checked': ''; ?>>
                            <span class="checkbox-custom"></span>
                            <span class="label">По заявкам Роспатента</span>
                        </label>
                        <label>
                            <input class="checkbox-true" type="checkbox" value="trademarks" <?php echo in_array('trademarks', $types) ? 'checked': ''; ?>>
                            <span class="checkbox-custom"></span>
                            <span class="label">По знакам Роспатента</span>
                        </label>
                        <label>
                            <input class="checkbox-true" type="checkbox" value="wipo" <?php echo in_array('wipo', $types) ? 'checked': ''; ?>>
                            <span class="checkbox-custom"></span>
                            <span class="label"> По знакам Мадридской системы (бета)</span>
                        </label>
                    <?php } else { ?>
                        <label><input type="checkbox" value="requests" checked>По заявкам Роспатента</label>
                        <label><input type="checkbox" value="trademarks">По знакам Роспатента</label>
                        <label><input type="checkbox" value="wipo"> По знакам Мадридской системы (бета)</label>
                    <?php } ?>
                </div>
                <?php echo $form->field($searchModel, 'type')->hiddenInput(['value' => (!empty($searchModel->type) ? $searchModel->type : 'requests'), 'name' => 'type'])->label(false); ?>
            </div>
        </div>
    </div>
    <div class="common-row">
        <div class="row">
            <div class="col-lg-12">
                <div class="common_title">Где ищем</div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo $form->field($searchModel, 'area')->radioList($searchArea,[
                    'item' => function($index, $label, $name, $checked, $value) {
                        $area = 'term';
                        if (isset(Yii::$app->request->queryParams['area'])) {
                            $area = Yii::$app->request->queryParams['area'];
                        }
                        $return = '<div class="radio">';
                        $return .= '<label class="label-common">';
                        $return .= '<input class="radio-true" type="radio" name="area" value="' . $value . '" ' . ($area == $value ? 'checked' : '') . '>';
                        $return .= '<span class="radio-custom"></span>';
                        $return .= '<span class="label">' . $label . '</span>';
                        $return .= '</label>';
                        $return .= '</div>';

                        return $return;
                    },
                    'unselect' => null
                ]); ?>
            </div>
        </div>
    </div>

    <div class="common-row row-status">
        <div class="row">
            <div class="col-lg-12">
                <div class="common_title">Статус</div>
            </div>
        </div>

        <div class="form-group status">
            <div class="col-lg-12">
                <!--<span class="label-status">Статус: </span>-->
                <?php echo $form->field($searchModel, 'status')->radioList([
                    'all' => 'Все',
                    'active' => 'Действующие',
                    'invalid' => 'Недействующие',
                ],[
                    'item' => function($index, $label, $name, $checked, $value) {
                        $status = 'all';
                        if (isset(Yii::$app->request->queryParams['status'])) {
                            $status = Yii::$app->request->queryParams['status'];
                        }
                        $return = '<div class="radio">';
                        $return .= '<label>';
                        $return .= '<input class="radio-true" type="radio" name="status" value="' . $value . '" ' . ($status == $value ? 'checked' : '') . '>';
                        $return .= '<span class="radio-custom"></span>';
                        $return .= '<span class="label">' . $label . '</span>';
                        $return .= '</label>';
                        $return .= '</div>';

                        return $return;
                    },
                    'unselect' => null
                ]); ?>
            </div>
        </div>
    </div>

    <div class="rest_options">

        <div class="common-row">
            <div class="form-group precision">
                <div class="col-lg-5 col-sm-12">
                    <label class="range">
                        <span class="range-caption">Точность поиска: <span class="js_similarity"><?=$searchModel->similarity?></span></span>
                        <input type="range" name="similarity" min="0.1" max="1" step="0.1" value="<?=$searchModel->similarity?>" id="similarity">
                    </label>
                </div>
                <div class="col-lg-4 col-lg-offset-3 col-sm-12">
                    <p class="add_info_small">Влияет на схожие слова и опечатки. Чем меньше коэффициент, тем ниже точность.</p>
                </div>
            </div>
        </div>
        
        <div class="common-row">
            <div class="row">
                <div class="col-lg-12">
                    <div class="common_title">Классы</div>
                </div>
            </div>
            <div class="class-list">
                <div class="row">
                    <div class="col-lg-8 col-sm-12 class-sel-caption">
                        <div class="classes">
                            <?php if (isset($searchModel->classes)) {?>
                                <?php $classesArr = explode('.', $searchModel->classes);
                                $count = count($classesArr);
                                if ($count > 3 && $count != $classesCount) {
                                    $classValue = 'Выбрано ' . $count . ' из ' . $classesCount;
                                } else {
                                    $classValue = str_replace('.', ', ', $searchModel->classes);
                                    if ($count == $classesCount) {
                                        $classValue = 'Все классы';
                                    }
                                } ?>
                                <div class="value" id="classValue"><?php echo $classValue; ?></div>
                            <?php } else { ?>
                                <div class="value" id="classValue">Все классы</div>
                            <?php } ?>
                            <div class="checkbox-list">
                                <ul>
                                    <li>
                                        <label>
                                            <input class="checkbox-true" type="checkbox" value="all" <?php echo !isset($searchModel->classes) || (count($classesArr) == $classesCount) ? 'checked' : ''; ?>>
                                            <span class="checkbox-custom"></span>
                                            <span class="label">Все</span>
                                        </label>
                                    </li>
                                    <?php foreach ($itemClasses as $itemClass) { ?>
                                        <?php if (!isset($searchModel->classes)) { ?>
                                            <?php $classesArr[] = $itemClass->id; ?>
                                        <?php } ?>
                                        <li>
                                            <label>
                                                <input class="checkbox-true" type="checkbox" value="<?php echo $itemClass->id; ?>" <?php echo in_array($itemClass->id, $classesArr) ? 'checked': ''; ?>>
                                                <span class="checkbox-custom"></span><span class="label"><?php echo $itemClass->id . ' - ' . $itemClass->description; ?></span>
                                            </label>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php echo $form->field($searchModel, 'classes')->hiddenInput(['value' => (isset($searchModel->classes) ? $searchModel->classes : implode('.', $classesArr)), 'name' => 'classes'])->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (Yii::$app->user->can('clientExtendedPermission')) { ?>
        <div class="common-row">
            <div class="row">
                <div class="col-lg-12">
                    <div class="common_title">Цвета</div>
                </div>
            </div>

            <div class="row">
                    <div class="colors-list">
                        <div class="col-lg-4 col-sm-12 colors-sel-caption">
                            <div class="colors">
                                <?php if (isset($searchModel->colors)) {?>
                                    <?php $colorsArr = explode('.', $searchModel->colors);
                                    $count = count($colorsArr);
                                    if ($count > 5 && $count != $colorsCount) {
                                        $colorValue = 'Выбрано ' . $count . ' из ' . $colorsCount;
                                    } else {
                                        $colorValue = '';
                                        foreach ($colors as $colorItem) {
                                            if (in_array($colorItem['id'], $colorsArr)) {
                                                $colorValue .= ', <span class="color-circle" style="background: ' . $colorItem['hex_bg'] . '"></span>';
                                            }
                                        }
                                        $colorValue = substr($colorValue, 2);
                                        if ($count == $colorsCount) {
                                            $colorValue = 'Все цвета';
                                        }
                                    } ?>
                                    <div class="value" id="colorValue"><?php echo $colorValue; ?></div>
                                <?php } else { ?>
                                    <div class="value" id="colorValue">Все цвета</div>
                                <?php } ?>
                                <div class="checkbox-list">
                                    <ul>
                                        <li>
                                            <label>
                                                <input class="checkbox-true" type="checkbox" value="all" <?php echo !isset($searchModel->colors) || (count($colorsArr) == $colorsCount) ? 'checked' : ''; ?>>
                                                <span class="checkbox-custom"></span>
                                                <span class="label">Все</span>
                                            </label>
                                        </li>
                                        <?php foreach($colors as $color) { ?>
                                            <?php if (!isset($searchModel->colors)) { ?>
                                                <?php $colorsArr[] = $color['id']; ?>
                                            <?php } ?>
                                            <li>
                                                <label>
                                                    <input class="checkbox-true" type="checkbox" value="<?php echo $color['id']; ?>" <?php echo (!isset($searchModel->colors) || in_array($color['id'], $colorsArr) ? 'checked' : ''); ?>>
                                                    <span class="checkbox-custom"></span><span class="label"><span class="color-circle" style="background: <?php echo $color['hex_bg']; ?>"></span><?php echo $color['name']; ?></span>
                                                </label>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php echo $form->field($searchModel, 'colors')->hiddenInput(['value' => (isset($searchModel->colors) ? $searchModel->colors : implode('.', $colorsArr)), 'name' => 'colors'])->label(false); ?>
                            </div>
                        </div>
                    </div>

                <?php if (Yii::$app->user->can('clientExtendedPermission')) { ?>

                    <div class="col-lg-8 col-sm-12">
                        <?php  echo $form->field($searchModel, 'colorCondition')->radioList([
                            'without' => 'Цвет не имеет значения',
                            'any' => 'По любому из цветов',
                            'all' => 'По всем цветам',
                        ],[
                            'item' => function($index, $label, $name, $checked, $value) {
                                $colorCondition = 'without';
                                if (isset(Yii::$app->request->queryParams['colorCondition'])) {
                                    $colorCondition = Yii::$app->request->queryParams['colorCondition'];
                                }
                                $return = '<div class="radio">';
                                $return .= '<label class="label-common">';
                                $return .= '<input class="radio-true" type="radio" name="colorCondition" value="' . $value . '" ' . ($colorCondition == $value ? 'checked' : '') . '>';
                                $return .= '<span class="radio-custom"></span>';
                                $return .= '<span class="label">' . $label . '</span>';
                                $return .= '</label>';
                                $return .= '</div>';

                                return $return;
                            },
                            'unselect' => null
                        ]) ?>
                    </div>

                <?php } ?>
            </div>
        </div>
        <?php } ?>

        <div class="common-row">
            <div class="form-group years">
                <div class="col-lg-5">
                    <span class="range-caption">Фильтр по годам: <span class="range-years"><?php echo Yii::$app->formatter->asDate(time(), 'yyyy'); ?></span></span>
                    <div id="range-years" ></div>
                </div>
                <?php echo $form->field($searchModel, 'yearMin')->hiddenInput(['value' => (isset($searchModel->yearMin) ? $searchModel->yearMin : 1924 . '-01-01'), 'name' => 'yearMin'])->label(false); ?>
                <?php echo $form->field($searchModel, 'yearMax')->hiddenInput(['value' => (isset($searchModel->yearMax) ? $searchModel->yearMax : date('Y') . '-01-01'), 'name' => 'yearMax'])->label(false); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="row_options">
        <div class="row">
            <div class="col-lg-12">
                <span class="more_options"><span class="more_options-text">Показать дополнительные опции</span></span><!-- <span class="glyphicon glyphicon-menu-down"></span> -->
            </div>
        </div>
    </div>
    </div>
    <?= Html::submitButton('Искать', ['class' => 'btn btn-raised btn-primary btn-violet mobile-btn']) ?>

<!--    <a href="http://claims.ru" target="_blank" class="btn_assess_risks">-->
<!--        Оценить риски и провести аналитику конкурентов помогут специалисты <img src="/images/claims.png" alt="claims">-->
<!--    </a>-->


    <!--    --><?php //$exportLink = Yii::$app->request->queryParams; $exportLink[0] = 'request/export'; ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{summary}{items}<div class="table-footer">{pager}<div class="page-size-limit">
        <span>По</span>
        <div class="options">
        <ul>
        <li ' . (Yii::$app->request->get('per-page') == 20 || empty(Yii::$app->request->get('per-page')) ? 'class="active"' : '') . '><a href="' .  Url::current(['per-page' => 20]) . '"'  . '>20</a></li>
         <li ' . (Yii::$app->request->get('per-page') == 50 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 50]) . '"'  . '>50</a></option>
         <li ' . (Yii::$app->request->get('per-page') == 100 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 100]) . '"' . '>100</a></option>
         <li ' . (Yii::$app->request->get('per-page') == 300 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 300]) . '"' . '>300</a></option>
        </ul></div>на странице
    </div></div>',
        //'filterModel' => $searchModel,
        'summary' => "<div class='top_title_wrapper'><span class='all_found_btn'> Всего найдено: " . ($dataProvider->totalCount == 1000000 ? 'более ' : '') . RequestSearch::$count . "</span>" . (Yii::$app->user->can('clientExtendedPermission') ? "<span class='export_btn'>Экспорт</span>" : "") . "</div>"/* . " за {$dataProvider->totalCount['time']} сек."*/,
        /* $export  .= '<div class="export_btn">Экспорт</div>',
         'export' => $export, */
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'rowOptions' => function ($model) {
            if ($model->tableName() == 'tm_foreign') {
                if (in_array($model->status_id, RequestSearch::$foreign_statuses_active)) {
                    $cls = 'success';
                } elseif (in_array($model->status_id, RequestSearch::$foreign_statuses_invalid)) {
                    $cls = 'danger';
                } else {
                    $cls = '';
                }
            } else {
                if (in_array($model->status_id, RequestSearch::$statuses_active)) {
                    $cls = 'success';
                } elseif (in_array($model->status_id, RequestSearch::$statuses_invalid)) {
                    $cls = 'danger';
                } else {
                    $cls = '';
                }
            }


            if (isset(Yii::$app->session['exp'])) {
                $exportItems = Yii::$app->session['exp'];

                if ($model->tableName() == 'requests') {
                    if (isset($exportItems['requests'])) {
                        if (in_array((string)$model->id, $exportItems['requests'])) {
                            $cls .= ' active';
                        }
                    }
                }
                if ($model->tableName() == 'trademarks') {
                    if (isset($exportItems['trademarks'])) {
                        if (in_array((string)$model->id, $exportItems['trademarks'])) {
                            $cls .= ' active';
                        }
                    }
                }
                if ($model->tableName() == 'tm_foreign') {
                    if (isset($exportItems['tm_foreign'])) {
                        if (in_array((string)$model->id, $exportItems['tm_foreign'])) {
                            $cls .= ' active';
                        }
                    }
                }
            }

            return ['class' => $cls, 'data-id' => $model['id'], 'data-model' => $model->tableName()];
        },
        'columns' => [
            [
                'header' => '<label class="exp-check"><input class="checkbox-true" type="checkbox"><span class="checkbox-custom"></span></label>',
                'value' => function($model, $key, $index, $column) {
                    $checked = false;
                    if (isset(Yii::$app->session['exp'])) {
                        $exportItems = Yii::$app->session['exp'];

                        if ($model->tableName() == 'requests') {
                            if (isset($exportItems['requests'])) {
                                if (in_array((string)$model->id, $exportItems['requests'])) {
                                    $checked = true;
                                }
                            }
                        }
                        if ($model->tableName() == 'trademarks') {
                            if (isset($exportItems['trademarks'])) {
                                if (in_array((string)$model->id, $exportItems['trademarks'])) {
                                    $checked = true;
                                }
                            }
                        }
                        if ($model->tableName() == 'tm_foreign') {
                            if (isset($exportItems['tm_foreign'])) {
                                if (in_array((string)$model->id, $exportItems['tm_foreign'])) {
                                    $checked = true;
                                }
                            }
                        }
                    }
                    $checkbox = '<label><input class="checkbox-true" type="checkbox" ' . ($checked ? 'checked': '') . '><span class="checkbox-custom"></span></label>';
                    return $checkbox;
                },
                'headerOptions' => ['class' => 'export-amount'],
                'contentOptions' => ['class' => 'export'],
                'format' => 'raw',
            ],
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Изображение',
                'value' => function ($model, $key, $index, $column) {
                    switch($model->tableName()) {
                        case 'requests':
                            $terms = RequestTerms::find()
                                ->with('term')
                                ->where(['request_id' => $model->id])
                                ->all();
                            $restype = 'Заявка';
                            break;
                        case 'trademarks':
                            $terms = TrademarkTerms::find()
                                ->with('term')
                                ->where(['trademark_id' => $model->id])
                                ->all();
                            $restype = 'Товарный знак';
                            break;
                        case 'tm_foreign':
                            $terms = TmForeignTerm::find()
                                ->with('term')
                                ->where(['tm_id' => $model->id])
                                ->all();
                            $restype = 'WIPO';
                            break;
                    }

                    $result = '<pre class="text-center text-uppercase bg-warning">'.$restype.'</pre>';

                    if ($model->tableName() == 'tm_foreign') {
                        $result .= Html::a(Html::img('/images/load.gif', ['class' => 'lazyload', 'data-src' => '/' . $model->thumb]), ['/' . $model->img], ['class' => 'rez-logo', 'target' => '_blank', 'data-pjax' => '0', 'title' => $model->name]);
                    } else {
                        $result .= Html::a(Html::img('/images/load.gif', ['class' => 'lazyload', 'data-src' => '/' . $model->thumb]), ['/' . $model->img], ['class' => 'rez-logo', 'target' => '_blank', 'data-pjax' => '0', 'title' => $model->rank]);
                    }

                    if (Yii::$app->user->can('claimsPermission')) {
                        $result .= '<div class="tags-wrap">';
                        if (!empty($terms)) {
                            foreach ($terms as $term) {
                                $result .= "<p><small>{$term->term->term} <i class=\"fa fa-times delete-term\" aria-hidden=\"true\" data-term_id=\"" . $term->term_id . "\"></i></small></p>";
                            }
                        }
                        $result .='<i class="fa fa-plus" aria-hidden="true"></i></div>';
                        /*$result .= Html::a('<small>Перераспознать</small>', ["/parser/wrong-tags?id={$model->id}&model={$model->tableName()}"], ['class' => 'btn btn-default js-wrong_tags', 'data-pjax' => '0', 'data-container' => $model->tableName()]);*/
                    }

                    return $result;
                },
                'contentOptions' => ['class' => 'application-image'],
                'format' => 'raw',
            ],
            [
                'label' => 'Классы',
                'value' => function ($model, $key, $index, $column) {
                    if ($model->tableName() == 'tm_foreign') {
                        if (!empty($model->classes)) {
                            foreach ($model->classes as $value) {
                                $arr[] = '<a tabindex="0" role="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="'.$value->class->description.'">'.$value->class->id.'</a>';
                            }
                        }
                    } else {
                        $classes = unserialize($model->classes);
                        $arr = [];
                        if (!empty($classes)) {
                            foreach ($classes as $key => $value) {
                                $arr[] = '<a tabindex="0" role="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="'.$value.'">'.$key.'</a>';
                            }
                        }
                    }

                    $result = '<div class="classes-wrap js-classes-wrap">' . implode('<br>',$arr) . '</div>';
                    return $result;
                },
                'contentOptions' => ['class' => 'table-classes'],
                'format' => 'raw',
            ],
            [
                'label' => 'Номер и дата',
                'value' => function ($model, $key, $index, $column) {
                    if ($model->tableName() == 'tm_foreign') {
                        $date = empty($model->base_reg_date) ? '' : date('d.m.Y', strtotime($model->base_reg_date));
                        if (empty($model->base_reg_number)) {
                            return $date;
                        }

                        return
                            '<div class="trademark-item__number"><span class="request-number-link">' . $model->base_reg_number . '</span><br><span class="request-number-date">' .
                            $date . '</span></div>';
//                            Html::a('Подробнее', '/rospatent/zayavka/?number=' . $model->request_number, ['class' => 'more-info-link']) . '<br><br>' .
//                            '<strong>WIPO:</strong><br><span class="flip-link-wrapper">' .
//                            Html::a('Заявка', Parser::TM_FOREIGN_URL . $model->st13, ['target' => '_blank']);
                    } else {
                        $date = empty($model->request_date) ? '' : date('d.m.Y', strtotime($model->request_date));
                        if (empty($model->request_number)) {
                            return $date;
                        }
                        return
//                        Html::a("{$model->request_number} <small class=\"glyphicon glyphicon-new-window\"></small>", Parser::REQUEST_URL . $model->request_number, ['target' => '_blank']) . "<br>" .
                            '<div class="trademark-item__number"><span class="request-number-link">' . $model->request_number . '</span><br><span class="request-number-date">' .
                            $date . '</span></div>' .
                            Html::a('Подробнее', '/rospatent/zayavka/?number=' . $model->request_number, ['class' => 'more-info-link']) . '<br><br>' .
                            '<strong>ФИПС:</strong><br><span class="flip-link-wrapper">' .
                            Html::a('Заявка', Parser::REQUEST_URL . $model->request_number, ['target' => '_blank']) . '<br>' .
                            Html::a('Делопроизводство', Parser::REQUEST_URL . $model->request_number . '&Delo=1', ['target' => '_blank']) . '</span></small>';
                    }
                },
                'contentOptions' => ['class' => 'number number-main'],
                'format' => 'raw',
            ],
            [
                'header' => 'Номер<br>и дата регистрации',
                'value' => function ($model, $key, $index, $column) {
                    if ($model->tableName() == 'tm_foreign') {
                        $date = empty($model->base_reg_date) ? '' : date('d.m.Y', strtotime($model->base_reg_date));
                        if (empty($model->reg_number)) {
                            return $date;
                        }

                        return
                            $model->reg_number . '<br>' .
                            $date . '<br><br>' .
//                            Html::a('Подробнее', '/rospatent/znak/?number=' . $model->reg_number) . '<br><br>' .
                            '<strong>WIPO:</strong><br><span class="flip-link-wrapper">' .
                        Html::a('Товарный знак', Parser::TM_FOREIGN_URL . $model->st13, ['target' => '_blank']);
                    } else {
                        $date = empty($model->reg_date) ? '' : date('d.m.Y', strtotime($model->reg_date));
                        if (empty($model->reg_number)) {
                            return $date;
                        }
                        return
                            $model->reg_number . '<br>' .
                            $date . '<br>' .
                            Html::a('Подробнее', '/rospatent/znak/?number=' . $model->reg_number) . '<br><br>' .
                            '<strong>ФИПС:</strong><br>' .
                            Html::a('Товарный знак', Parser::TRADEMARK_URL . $model->reg_number, ['target' => '_blank']);
                    }
                },
                'contentOptions' => ['class' => 'number number-second'],
                'format' => 'raw',
            ],
            [
                'header' => 'Заявитель / Правообладатель<br>Адрес для переписки',
                'value' => function ($model, $key, $index, $column) {
                    switch($model->tableName()) {
                        case 'requests':
                            return  '<a href="' . Url::to([Yii::$app->controller->id . '/index', 'string' => (str_replace(['«','»','"','/'],'', htmlspecialchars_decode($model->applicant))), 'type' => 'requests.trademarks.wipo', 'area' => 'applicant', 'yearMin' => (isset(Yii::$app->request->queryParams['yearMin']) ? Yii::$app->request->queryParams['yearMin'] : 1924 . '-01-01'), 'yearMax' => (isset(Yii::$app->request->queryParams['yearMax']) ? Yii::$app->request->queryParams['yearMax'] : date('Y') . '-01-01')]) . '">' . $model->applicant . '</a><br/>' . $model->address;
                        case 'trademarks':
                            return '<a href="' . Url::to([Yii::$app->controller->id . '/index', 'string' => (str_replace(['«','»','"','/'],'', htmlspecialchars_decode(stripslashes($model->rightholder)))), 'type' => 'requests.trademarks.wipo', 'area' => 'applicant', 'yearMin' => (isset(Yii::$app->request->queryParams['yearMin']) ? Yii::$app->request->queryParams['yearMin'] : 1924 . '-01-01'), 'yearMax' => (isset(Yii::$app->request->queryParams['yearMax']) ? Yii::$app->request->queryParams['yearMax'] : date('Y') . '-01-01')]) . '">' . stripslashes($model->rightholder) . '</a><br/><br/>' . stripslashes($model->address);
                        case 'tm_foreign':
                            return '<a href="' . Url::to([Yii::$app->controller->id . '/index', 'string' => (str_replace(['«','»','"','/'],'', htmlspecialchars_decode(stripslashes($model->owner)))), 'type' => 'requests.trademarks.wipo', 'area' => 'applicant', 'yearMin' => (isset(Yii::$app->request->queryParams['yearMin']) ? Yii::$app->request->queryParams['yearMin'] : 1924 . '-01-01'), 'yearMax' => (isset(Yii::$app->request->queryParams['yearMax']) ? Yii::$app->request->queryParams['yearMax'] : date('Y') . '-01-01')]) . '">' . stripslashes($model->owner) . '</a><br/><br/>' . stripslashes($model->address);
                        default:
                            return '';
                    }
                },
                'contentOptions' => ['class' => 'applicant-address'],
                'format' => 'html',
            ],
            [
                'label' => 'Статус',
                'value' => function ($model, $key, $index, $column) {
                    $status = '';
                    $tracking = '';
                    $recognize = '';
                    if (!empty($model->status)) {
                        $status = '<span>' . $model->status['title'] . '</span>';
                    }
                    if (Yii::$app->user->can('clientExtendedPermission')) {
                        $tracking = '<button class="btn btn-warning add-item track" data-id="' . $model->id . '" data-model="' . $model->tableName() . '">Отслеживать</button>';
                        $tracking .= '<div class="helper"><p></p></div>';
                        $recognize = Html::a('<small>Перераспознать</small>', ["/parser/wrong-tags?id={$model->id}&model={$model->tableName()}"], ['class' => 'btn btn-default js-wrong_tags', 'data-pjax' => '0', 'data-container' => $model->tableName()]);
                    }
                    $div = '<div class="actions">' . $tracking . $recognize . '</div>';
                    return $status . $div;
                },
                'contentOptions' => ['class' => 'status-main'],
                'format' => 'raw',
            ],
            /*[
                'value' => function($model, $key, $index, $column) {
                    $button = '<button class="btn btn-warning add-item track" data-id="' . $model->id . '" data-model="' . $model->tableName() . '">Добавить в<br>отслеживаемые</button>';
                    $button .= '<div class="helper"><p></p></div>';
                    return $button;
                },
                'visible' => $hasAccess,
                'format' => 'raw',
            ],*/
            /*[
                'header' => '<div class="export-links"><a data-pjax=0 target="_blank" class="export-item" data-export="excel" href="' . Url::to($exportLink) . '&exportType=excel' . '">Excel</a><a data-pjax=0 target="_blank" class="export-item" data-export="pdf" href="' . Url::to($exportLink) . '&exportType=pdf' . '">PDF</a><a data-pjax=0 target="_blank" class="export-item" data-export="docx" href="' . Url::to($exportLink) . '&exportType=docx' . '">Docx</a><br><label class="exp-check">Все <input type="checkbox" checked></label></div>',
                'value' => function($model, $key, $index, $column) {
                    return '';
                },
                'contentOptions' => ['class' => 'export'],
                'headerOptions' => ['class' => 'export'],
                'visible' => Yii::$app->user->can('clientExtended') ? 1 : 0,
                'format' => 'raw',
            ],*/
        ],
        'pager' => [
            'prevPageLabel' => '<img src="/images/left.png" alt="left">',
            'nextPageLabel' => '<img src="/images/right.png" alt="right">',
            'firstPageLabel' => false,
            'lastPageLabel'  => false,
//            'options' => [
//                'tag' => 'div',
//                'class' => 'pager-wrapper',
//                'id' => 'pager-container',
//            ],
        ],
        'emptyText' => 'Ничего не найдено :('
    ]); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            /*$.material.init({
             "radio": ''
             });*/
            $('.class-sel-caption .classes .checkbox-list label').click({classCount: <?php echo $classesCount; ?>}, classesSearch);
            $('.colors-sel-caption .colors .checkbox-list label').click({colorCount: <?php echo $colorsCount; ?>}, colorsSearch);

            $('#range-years').slider({
                range: true,
                min: 1924,
                max: <?php echo Yii::$app->formatter->asDate(time(), 'yyyy'); ?>,
                values: [<?php if (!empty($searchModel->yearMin)) {
                    if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $searchModel->yearMin)) {
                        echo Yii::$app->formatter->asDate($searchModel->yearMin, 'yyyy');
                    } else {
                        echo 1924;
                    }
                } else {
                    echo 1924;
                } ?>,
                    <?php if (!empty($searchModel->yearMax)) {
                    if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $searchModel->yearMax)) {
                        echo Yii::$app->formatter->asDate($searchModel->yearMax, 'yyyy');
                    } else {
                        echo Yii::$app->formatter->asDate(time(), 'yyyy');
                    }
                } else {
                    echo Yii::$app->formatter->asDate(time(), 'yyyy');
                } ?>],
                slide: function( event, ui ) {
                    $('.years .range-years').text(ui.values[0] + " - " + ui.values[ 1 ]);
                    if (ui.values[0] + '-01-01' == ui.values[1] + '-01-01') {
                        $('.years input[name=yearMax]').val(ui.values[1] + '-12-31');
                    } else {
                        $('.years input[name=yearMax]').val(ui.values[1] + '-01-01');
                    }
                    $('.years input[name=yearMin]').val(ui.values[0] + '-01-01');
                }
            });
            $('.years .range-years').text($('#range-years').slider( "values", 0) +
                " - " + $('#range-years').slider( "values", 1));
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (document.documentElement.clientWidth < 500) {

                $('.grid-view table').css('display', 'none');
                $('.grid-view .export_btn').css('display', 'none');
                $('<div class="trademark-block"></div>').insertBefore( $( ".grid-view .table-footer" ) );

                var resultTable = $('.grid-view table tbody tr');
                $('.table-bordered .actions').css('display', 'block');

                if($(".empty").length>0) {
                    $('.trademark-block').append('<div class="empty-block">Ничего не найдено.</div>');
                    $('.table-footer').css('display', 'none');
                } else {

                    $.each(resultTable, function() {

                        var trClass = $(this).attr('class');
                        var applicationImage = $(this).find('.application-image').html();
                        var trademarkLink = $(this).find('.trademark-item__number .request-number-link').html();
                        var trademarkDates = $(this).find('.trademark-item__number .request-number-date').html();
                        var trademarkStatus = $(this).find('.status-main span').html();
                        var moreInfoLinkHref = $(this).find('.more-info-link').attr('href');
                        var numberSecond = $(this).find('.number-second a:last-child').wrapAll('<div>').parent().html();
                        var trackButton = $(this).find('.actions').html();
                        var numberMain = $(this).find('.number-main .flip-link-wrapper').html();
                        var applicantAddress = $(this).find('.applicant-address').html();
                        var tableClasses = $(this).find('.table-classes').html();

                        if(numberSecond){
                            $('.trademark-block').append('<div class="trademark-item"><div class="trademark-view trademark-view-left"><div class="track-button">'+ trackButton +'</div><div class="fips"><div class="fips-title">ФИПС:</div>' + numberMain +'<br>' + numberSecond + '</div></div><div class="trademark-view-left-btn"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19" height="17" viewBox="0 0 19 17"><defs><path id="ftkfa" d="M284 22.54V21h19v1.54zm0 7.73v-1.54h19v1.54zm0 7.73v-1.55h19V38z"/></defs><g><g transform="translate(-284 -21)"><use fill="#957da2" xlink:href="#ftkfa"/></g></g></svg></div><div class="trademark-item-wrapper"><div class="trademark-view trademark-view-center"><div class="trademark-item__application">' + applicationImage + '</div><div class="trademark-item__dates"><div class="trademark-inner-title">Номер и дата:</div><a class="link-wrapper" target="_blank" href="' + moreInfoLinkHref + '">' + trademarkLink + '</a><br>'+ trademarkDates +'</div><div class="trademark-inner-title">Статус:</div><div class="trademark-item__status">' + trademarkStatus +'</div></div><div class="trademark-view trademark-view-right"><div class="address-title">Заявитель / Правообладатель <span class="address-title__add">Адрес для переписки</span></div>'+ applicantAddress +'<div class="wrapper-classes"><div class="trademark-inner-title">Классы:</div>'+ tableClasses +'</div></div></div></div>');
                        } else {
                            $('.trademark-block').append('<div class="trademark-item"><div class="trademark-view trademark-view-left"><div class="track-button">'+ trackButton +'</div><div class="fips"><div class="fips-title">ФИПС:</div>' + numberMain + '</div></div><div class="trademark-view-left-btn"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19" height="17" viewBox="0 0 19 17"><defs><path id="ftkfa" d="M284 22.54V21h19v1.54zm0 7.73v-1.54h19v1.54zm0 7.73v-1.55h19V38z"/></defs><g><g transform="translate(-284 -21)"><use fill="#957da2" xlink:href="#ftkfa"/></g></g></svg></div><div class="trademark-item-wrapper"><div class="trademark-view trademark-view-center"><div class="trademark-item__application">' + applicationImage + '</div><div class="trademark-item__dates"><div class="trademark-inner-title">Номер и дата:</div><a class="link-wrapper" target="_blank" href="' + moreInfoLinkHref + '">' + trademarkLink + '</a><br>'+ trademarkDates +'</div><div class="trademark-inner-title">Статус:</div><div class="trademark-item__status">' + trademarkStatus +'</div></div><div class="trademark-view trademark-view-right"><div class="address-title">Заявитель / Правообладатель <span class="address-title__add">Адрес для переписки</span></div>'+ applicantAddress +'<div class="wrapper-classes"><div class="trademark-inner-title">Классы:</div>'+ tableClasses +'</div></div></div></div>');
                        }

                        $('.trademark-item__status').addClass(trClass);


                    });
                }

                $('.trademark-item-wrapper').slick({
                    dots: true,
                    arrows: false,
                    infinite: false
                });

                $('.trademark-view-left-btn').on('click', function() {

                    var findParent = $(this).parent().find('.trademark-view-left');
                    findParent.toggleClass('trademark-view-left-active');
                });

                var $tablePagination = $(".table-footer .pagination li");
                var indCurrentPage = $(".table-footer .pagination li.active").index();
                $.each($tablePagination, function(i) {
                    if( i < indCurrentPage-2 || i > indCurrentPage+2) {
                        $(this).not($('.table-footer .pagination li.next')).not($('.table-footer .pagination li.prev')).remove();
                    }
                });

                $('.trademark-view-right a').attr('data-placement', 'top');
            }
        });
    </script>

    <script type="text/javascript">
        var currentExportAmount = <?php echo $amountExportItems; ?>;
        var exportItems = {};
        exportItems.requests = [<?php echo isset($currentItems['requests']) ? implode(',', $currentItems['requests']) : ''; ?>];
        exportItems.trademarks = [<?php echo isset($currentItems['trademarks']) ? implode(', ', $currentItems['trademarks']) : ''; ?>];
            
        <?php if (isset(Yii::$app->session['exp'])) { ?>
        $(document).ready(function () {
            setTimeout(function(){
                $('.export_btn').trigger('click');
            }, 400);
        });
        <?php } ?>
    </script>
    <?php Pjax::end(); ?>
    <script type="text/javascript">
        $(document).on('click', '.table-striped tbody tr', function () {
            console.log($(this));

            if ($('.export_btn').hasClass('active')) {
                var $this = $(this);
                checkItems(currentExportAmount,exportItems, $this);
            }
            })
            .on('click', '.exp-check', function () {
                if ($('.export_btn').hasClass('active')) {
                    checkItems(currentExportAmount, exportItems);
                }
            })
            .on('click', '.export_btn', function() {
            if ($(this).hasClass('active')) {
                exportItems.requests = [];
                exportItems.trademarks = [];
            }
        });
    </script>
    
    <div class="export-links">
        <div class="container">
            <span class="export-links-text">Экспортировать <span class="export-links-text-bold"><?php echo $amountExportItems; ?> позиций</span> в формате:</span>
            <a data-pjax=0 target="_blank" class="export-item" data-export="excel" href="<?php echo Url::to(['/request/export', 'export_type' => 'excel']); ?>">Excel</a>
            <a data-pjax=0 target="_blank" class="export-item" data-export="pdf" href="<?php echo Url::to(['/request/export', 'export_type' => 'pdf']); ?>">PDF</a>
            <a data-pjax=0 target="_blank" class="export-item" data-export="docx" href="<?php echo Url::to(['/request/export', 'export_type' => 'docx']); ?>">Docx</a>
        </div>
    </div>
    </div></div>