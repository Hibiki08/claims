<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\models\TrademarkTerms;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $terms array */

$this->title = 'Поиск по заявкам';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requests-trademarks">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <? //= Html::a('Create Requests', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'pjax-trademarks', 'timeout' => false]); ?>
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'method' => 'get',
        'fieldConfig' => [
            'template' => "{input}",
            'options' => [
                'tag' => false,
            ],
        ],
        'action' => [''],
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="form-group">
        <div class="col-lg-6">
            <?= $form->field($searchModel, 'searchString')->input('search') ?>
        </div>
        <div class="col-lg-6">
            <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7">
            <a class="collapse-btn" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Операторы поиска <span class="glyphicon glyphicon-menu-down"></span></a>
            <div class="collapse" id="collapseExample">
                <div class="well operators">
                  <p>Оператор И всегда активен. Значит, что запрос «котенок песик» найдет документы, содержащие слова «котенок и «песик».</p>
                    <p><span>|</span> &mdash; ИЛИ. Пример использования:  «котенок | песик».</p>
                    <p><span>- или !</span> &mdash; НЕ. Пример использования:  «котенок -песик (котенок !песик)».</p>
                    <!--<p><span>MAYBE</span> работает подобно оператору ИЛИ. Пример использования:  «котенок MAYBE песик».</p>-->
                    <p><span>=</span> &mdash; ищет точное соотвествие слову. Пример использования:  «котенок =песик».</p>
                    <p><span>""</span> &mdash; фраза целиком. Пример использования:  «"котенок песик"» найдет документы, где фраза встречается в указанном виде.</p>
                    <p><span><<</span> &mdash; порядок слов. Выводит документы, если слова идут в указанном порядке. Пример использования:  котенок << песик &mdash; выведет документ "котенок побежал за песиком", но не выведет "песик побежал за котенком".</p>
                    <p><span>*</span> &mdash; любое выражение. Пример использования:  котенок * песик &mdash; значит между котенком и песиком может быть любое слово.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'database')->radioList(['requests' => 'По заявкам', 'trademarks' => 'По товарным знакам']) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'searchArea')->radioList(['all' => 'По всему', 'applicant' => 'По заявителю/правообладателю', 'address' => 'По адресу для переписки', 'term' => 'По тегам изображений']) ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-6">
            <div class="class-sel-caption">Классы:</div>
            <select class="classes-sel js-classes-sel" multiple="multiple">
            <?php for ($i = 1; $i <= 45; ++$i): ?>
                <option value="RequestSearch[searchClasses][<?= $i ?>]" <?= empty($searchModel->searchClasses[$i]) ? '' : 'selected="selected"' ?>><?= $i ?></option>
            <?php endfor; ?>
            </select>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'summary' => "Всего найдено: " . ($dataProvider->totalCount == 1000 ? 'более ' : '') . $dataProvider->totalCount . " за {$dataProvider->totalCount['time']} сек.",
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'rowOptions' => function ($model) {
            switch ($model->status_id) {
                case 14:
                    $cls = 'success';
                    break;

                case 15:
                case 13:
                    $cls = 'danger';
                    break;

                default:
                    $cls = '';
            }

            return ['class' => $cls];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label' => 'Изображение',
                'value' => function ($model, $key, $index, $column) {
                    //var_dump($model); die;
                    $result = Html::a(Html::img("/{$model->thumb}"), ['/' . $model->img], ['class' => 'rez-logo', 'target' => '_blank', 'data-pjax' => '0']);
                    $terms = TrademarkTerms::find()
                        ->with('term')
                        ->where(['trademark_id' => $model->id])
                        ->all();

                    $result .= '<div class="tags-wrap">';
                    if (!empty($terms)) {
                        foreach ($terms as $term) {
                            if ($term->trademark_id == $model->id) {
                                $result .= "<p>{$term->term->term}</p>";
                            }
                        }
                    }
                    $result .='</div>';

                    $result .= Html::a('<small>Перераспознать</small>', ["/parser/wrong-tags?id={$model->id}&model={$model->tableName()}"], ['class' => 'btn btn-default js-wrong_tags', 'data-pjax' => '0', 'data-container' => $model->tableName()]);

                    return $result;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Классы',
                'value' => function ($model, $key, $index, $column) {
                    $classes = unserialize($model->classes);
                    $arr = [];
                    if (!empty($classes)) {
                        foreach ($classes as $key => $value) {
                            //$result .= empty($result) ? $key : ", $key";
                            $arr[] = '<a tabindex="0" role="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="'.$value.'">'.$key.'</a>';
                        }
                    }
                    $result = '<div class="classes-wrap js-classes-wrap">'. implode('<br>',$arr) . '</div>';
                    return $result;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Номер и дата заявки',
                'value' => function ($model, $key, $index, $column) {
                    $date = empty($model->request_date) ? '' : date('d.m.Y', strtotime($model->request_date));
                    if (empty($model->request_number)) {
                        return $date;
                    }
                    return
                        Html::a("{$model->request_number} <small class=\"glyphicon glyphicon-new-window\"></small>", "http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTMAP&DocNumber={$model->request_number}&TypeFile=html", ['target' => '_blank', 'rel'=>'noreferrer']) . "<br>" .
                        $date . '<br><br><small>' .
                        Html::a("Делопроизводство <small class=\"glyphicon glyphicon-new-window\"></small>", "http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTMAP&DocNumber={$model->request_number}&TypeFile=html&Delo=1", ['target' => '_blank', 'rel'=>'noreferrer']) . '</small>';
                },
                'format' => 'raw',
            ],
            [
                'header' => 'Номер регистрации<br>и дата публикации',
                'value' => function ($model, $key, $index, $column) {
                    $date = empty($model->reg_date) ? '' : date('d.m.Y', strtotime($model->reg_date));
                    if (empty($model->reg_number)) {
                        return $date;
                    }
                    return Html::a("{$model->reg_number} <small class=\"glyphicon glyphicon-new-window\"></small>", "http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTM&rn=4386&DocNumber={$model->reg_number}", ['target' => '_blank']) . "<br>" . $date;
                },
                'format' => 'raw',
            ],
            [
                'header' => 'Правообладатель<br>Адрес для переписки',
                'value' => function ($model, $key, $index, $column) {
                    return <hr>' . stripslashes($model->address);
                },
                'format' => 'html',
            ],
            //'request_number',
            //'request_date',
            //'reg_number',
            //'reg_date',
            //'applicant:ntext',
            'status.title:text:Статус',
            //'address:ntext',
            //'classes:ntext',
            // 'first_request_number:ntext',
            // 'first_request_date',
            // 'first_request_country:ntext',
            // 'img:ntext',
            // 'facsimile:ntext',
            // 'status_id',
            // 'update_time',

            //['class' => 'yii\grid\ActionColumn'],
        ],
        'emptyText' => 'Ничего не найдено. Пора срочно подавать заявку!'
    ]); ?>
    <?php Pjax::end(); ?></div>
