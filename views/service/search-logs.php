<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Users;
use yii\helpers\Url;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История поиска';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="searchlogs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'filterModel' => $searchModel,
        'summary' => 'Всего найдено: ' . $count,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'header' => 'Пользователь',
                'attribute' => 'user_id',
                'value' => function ($model, $key, $index, $column) {
                    if ($model->user_id) {
                        $user = Users::findIdentity($model->user_id);
                        return $user->login;
                    }
                    return 'Гость';

                },
                'filter' => $users,
                'format' => 'raw',
            ],
            'query:ntext',
            'area:ntext',
            'ip:ntext',
            [
                'label' => 'Дата',
                'attribute' => 'date',
                'filter' => '<div class="form-group">' . DatePicker::widget([
                    'language' => 'ru',
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'dateFormat' => 'dd-MM-yyyy',
                        'options' => [
                            'class' => 'form-control'
                        ]
                ]). '</div>',
                'format' =>  ['date', 'dd-MM-Y HH:mm:ss']
            ],
        ],
    ]); ?>
    <div class="page-size-limit">
        <span>Показывать по: </span>
        <a href="<?php echo Url::current(['per-page' => 20]); ?>" <?php echo (Yii::$app->request->get('per-page') == 20 || empty(Yii::$app->request->get('per-page')) ? 'class="active"' : ''); ?>>20</a>
        <a href="<?php echo Url::current(['per-page' => 50]); ?>" <?php echo (Yii::$app->request->get('per-page') == 50 ? 'class="active"' : ''); ?>>50</a>
        <a href="<?php echo Url::current(['per-page' => 100]); ?>" <?php echo (Yii::$app->request->get('per-page') == 100 ? 'class="active"' : ''); ?>>100</a>
        <a href="<?php echo Url::current(['per-page' => 300]); ?>" <?php echo (Yii::$app->request->get('per-page') == 300 ? 'class="active"' : ''); ?>>300</a>
    </div>
</div>
