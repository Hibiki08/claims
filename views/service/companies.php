<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Users;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Компании';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tm-companies">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3>Товарные знаки</h3>

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'tm-companies',
        'method' => 'get',
        'fieldConfig' => [
            'template' => "{input}",
            'options' => [
                'tag' => false,
            ],
        ],
        'action' => [''],
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="form-group">
        <div class="col-lg-6">
            <?= $form->field($searchModel, 'string')->textInput(['name' => 'string', 'class' => 'form-control', 'value' => $searchModel->string]) ?>
        </div>
        <div class="col-lg-2">
            <?= Html::submitButton('Искать', ['class' => 'btn btn-raised btn-primary']) ?>
        </div>
    </div>

    <div class="class-list">
        <div class="col-lg-5 class-sel-caption">
            <div>Классы:</div>
            <div class="classes">
                <?php if (isset($searchModel->classes)) {?>
                    <?php $classesArr = explode('.', $searchModel->classes);
                    $count = count($classesArr);
                    if ($count > 3 && $count != $classesCount) {
                        $classValue = 'Выбрано ' . $count . ' из ' . $classesCount;
                    } else {
                        $classValue = str_replace('.', ', ', $searchModel->classes);
                        if ($count == $classesCount) {
                            $classValue = 'Все классы';
                        }
                    } ?>
                    <div class="value" id="classValue"><?php echo $classValue; ?></div>
                <?php } else { ?>
                    <div class="value" id="classValue">Все классы</div>
                <?php } ?>
                <div class="checkbox-list">
                    <ul>
                        <li>
                            <label>
                                <input type="checkbox" value="all" <?php echo !isset($searchModel->classes) || (count($classesArr) == $classesCount) ? 'checked' : ''; ?>>
                                <span>Все</span>
                            </label>
                        </li>
                        <?php foreach ($classes as $class) { ?>
                            <?php if (!isset($searchModel->classes)) { ?>
                                <?php $classesArr[] = $class->id; ?>
                            <?php } ?>
                            <li>
                                <label>
                                    <input type="checkbox" value="<?php echo $class->id; ?>" <?php echo in_array($class->id, $classesArr) ? 'checked': ''; ?>>
                                    <span><?php echo $class->id . ' - ' . $class->description; ?></span>
                                </label>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <?php echo $form->field($searchModel, 'classes')->hiddenInput(['value' => (isset($searchModel->classes) ? $searchModel->classes : implode('.', $classesArr)), 'name' => 'classes'])->label(false); ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $trademarkDataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'header' => 'Компания',
                'attribute' => 'rightholder',
                'value' => function ($model, $key, $index, $column) {
                    return '<a href="' . Url::to(['/', 'string' => (str_replace(['«','»','"','/'],'', htmlspecialchars_decode($model['rightholder']))), 'type' => 'trademarks', 'area' => 'rightholder', 'yearMin' => 1924 . '-01-01', 'yearMax' => date('Y') . '-01-01']) . '">' . $model['rightholder'] . '</a>';
                },
                'filterInputOptions' => ['name' => 'rightholder', 'class' => 'form-control'],
//                'filter' => $users,
                'format' => 'raw',
            ],
            [
                'label' => 'Количество ТЗ',
                'attribute' => 'count_rightholder',
                'value' => function ($model, $key, $index, $column) {
                    return $model['count_rightholder'];
                },
                'format' => 'html',
            ]
        ],
    ]); ?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.class-sel-caption .classes .checkbox-list label').click({classCount: <?php echo $classesCount; ?>}, classesSearch);
    });
</script>
