<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\components\Parser;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проблемные заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requests-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['id' => 'pjax-grid']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => "Всего найдено: " . ($meta['total'] == 1000 ? 'более ' : '') . $meta['total'] . " за {$meta['time']} сек.",
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label' => 'Изображение',
                'value' => function ($model, $key, $index, $column) {
                    //var_dump($model); die;
                    return Html::a(Html::img("/{$model->getThumb()}"), ['/' . $model->img], ['target' => '_blank', 'data-pjax' => '0']);
                    //return Html::img("/{$model->img}", ['style' => 'max-width: 200px']);
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Классы',
                'value' => function ($model, $key, $index, $column) {
                    $classes = unserialize($model->classes);
                    $arr = [];
                    if (!empty($classes)) {
                        foreach ($classes as $key => $value) {
                            //$result .= empty($result) ? $key : ", $key";
                            $arr[] = '<a tabindex="0" role="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="'.$value.'">'.$key.'</a>';
                        }
                    }
                    $result = implode('<br>',$arr);
                    return $result;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Номер и дата заявки',
                'value' => function ($model, $key, $index, $column) {
                    return
                        Html::a("{$model->request_number} <small class=\"glyphicon glyphicon-new-window\"></small>", Parser::REQUEST_URL . $model->request_number, ['target' => '_blank']) . "<br>" .
                        date('d.m.Y', strtotime($model->request_date)) . '<br><br><small>' .
                        Html::a("Делопроизводство <small class=\"glyphicon glyphicon-new-window\"></small>", Parser::REQUEST_URL . $model->request_number . '&Delo=1', ['target' => '_blank']) . '</small>';
                },
                'format' => 'raw',
            ],
            [
                'header' => 'Номер регистрации<br>и дата публикации',
                'value' => function ($model, $key, $index, $column) {
                    $date = empty($model->reg_date) ? '' : date('d.m.Y', strtotime($model->reg_date));
                    if (empty($date) || empty($model->reg_number)) {
                        return "{$model->reg_number}<br>$date";
                    }
                    return Html::a("{$model->reg_number} <small class=\"glyphicon glyphicon-new-window\"></small>", Parser::TRADEMARK_URL . $model->reg_number, ['target' => '_blank']) . "<br>" . $date;
                },
                'format' => 'raw',
            ],
            [
                'header' => 'Заявитель<br>Адрес для переписки',
                'value' => function ($model, $key, $index, $column) {
                    return $model->applicant . '<hr>' . $model->address;
                },
                'format' => 'html',
            ],
            //'request_number',
            //'request_date',
            //'reg_number',
            //'reg_date',
            //'applicant:ntext',
            //'cstatus.title:text:Статус',
            [
                'label' => 'Статус',
                //'value' => 'status.name'
                'attribute' => 'cstatus_id',
                'filter' => ArrayHelper::map(app\models\Statuses::find()->where(['in', 'id', [16,17,18]])->all(), 'id', 'title'),
                'value' => function ($model, $key, $index, $column) {
                    return Html::activeDropDownList($model, 'cstatus_id', ArrayHelper::map(app\models\Statuses::find()->where(['in', 'id', [16,17,18]])->all(), 'id', 'title'), [
                        'class' => 'form-control js-err_request_update',
                        'data-id' => $model->id
                    ]);
                },
                'format' => 'raw',
            ],
            //'address:ntext',
            //'classes:ntext',
            // 'first_request_number:ntext',
            // 'first_request_date',
            // 'first_request_country:ntext',
            // 'img:ntext',
            // 'facsimile:ntext',
            // 'status_id',
            // 'update_time',
        ],
        'emptyText' => 'Ничего не найдено.'
    ]); ?>
    <?php Pjax::end(); ?></div>
