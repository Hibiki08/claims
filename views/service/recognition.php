<?php

use app\components\RecognitionMenu;

$this->title = 'Распознавание';
?>
<div class="recogntion-wrapper row">
    <?php echo RecognitionMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <div>
            <h2>Заявки</h2>
            <p>Нераспознанные: <?php echo $requestCount; ?></p>
            <p>Пустые: <?php echo $requestCountEmpty; ?></p>
        </div>
        <div>
            <h2>Товарные знаки</h2>
            <p>Нераспознанные: <?php echo $trademarkCount; ?></p>
            <p>Пустые: <?php echo $trademarkCountEmpty; ?></p>
        </div>
        <div>
            <h2>ТЗ WIPO</h2>
            <p>Нераспознанные: <?php echo $tmForeignCount; ?></p>
            <p>Пустые: <?php echo $tmForeignCountEmpty; ?></p>
        </div>
    </div>
</div>
