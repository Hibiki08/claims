<?php
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<?php Pjax::begin(['id' => 'pjax-grid', 'timeout' => false]); ?>
<div class="colors-wrapper">
    <div class="colors-base">
        <ul class="sortable connectedSortable">
            <?php $colorsBaseArr = []; ?>
        <?php foreach($colorsBase as $colors) { ?>
            <?php $colorsBaseArr[$colors->id] = $colors->name; ?>
            <li data-color_id="<?php echo $colors->id; ?>" class="btn btn-sm draggable" style="background: <?php echo !empty($colors->hex_bg) ? $colors->hex_bg : '#fff'; ?>; color: <?php echo !empty($colors->hex_text) ? $colors->hex_text : '#333'; ?>;"><?php echo $colors->name; ?></li>
        <?php } ?>
        </ul>
    </div>
    <div class="type">
        <?php $type = Yii::$app->request->getQueryParam('type'); ?>
        <a href="<?php echo Url::to(['service/colors']); ?>" <?php echo $type ? '' : 'class="active"'; ?>>Все</a>
        <a href="<?php echo Url::to(['service/colors', 'type' => 'unsorted']); ?>" <?php echo $type == 'unsorted' ? 'class="active"' : ''; ?>>Нераспределённые цвета</a>
    </div>
    <div class="progress progress-striped active sort-progress">
        <div class="progress-bar progress-bar-info" style="width: 100%"></div>
    </div>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'rowOptions'=>function ($model, $key, $index, $grid){
            return [
                'data-key' => $model['id']
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => 'Название цвета',
                'value' => function ($model, $key, $index, $column) {
                    return $model['name'];
                },
                'format' => 'html',
            ],
            [
                'label' => 'Выбранные цвета',
                'value' => function ($model, $key, $index, $column) {
                    $colors = '<ul class="sortable2 connectedSortable">';
                    if (isset($model->colorShade)) {
                        foreach ($model->colorShade as $color) {
                            $colors .= '<li data-color_id="' . $color->color['id'] . '" class="btn btn-sm" style="background: ' . (!empty($color->color['hex_bg']) ? $color->color['hex_bg'] : '#fff') . '; color: ' . (!empty($color->color['hex_text']) ? $color->color['hex_text'] : '#333') . '">' . $color->color['name'] . '<span class="glyphicon glyphicon-remove"></span></li>';
                        }
                    }
                    return $colors . '</ul>';

                },
                'format' => 'raw',
            ],
        ],
    ]) ?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.sortable, .sortable2').sortable({
        }).disableSelection();
        $('.sortable').sortable({
            connectWith: ".connectedSortable",
            beforeStop: function (event, ui) {
                currentList = $('.sortable2 .ui-sortable-placeholder').parent();
            },
            remove: function (event, ui) {
                $('body').fadeTo(100, 0.3);
                var item = ui.item.clone().append('<span class="glyphicon glyphicon-remove"></span>').appendTo(currentList);
                $(this).sortable('cancel');
                var shade_color_id = currentList.parents('tr').data('key');
                var color_ids = [];
                currentList.find('li').each(function(index, elem) {
                    if ($.inArray($(elem).data('color_id'), color_ids) == -1) {
                        color_ids.push($(elem).data('color_id'));
                    }
                });

                $.ajax({
                    url: '/service/colors-assign',
                    type: 'post',
                    data: {
                        shade_color_id: shade_color_id,
                        color_ids: color_ids,
                        _csrf: yii.getCsrfToken()
                    },
                    timeout: 600000,
                    success: function (data) {
                        console.log('ok');
                        console.log(data);
                        if (data.status > -1) {
                            $('body').fadeTo(100, 1);
                        }
                    },
                    error: function (data) {
                        console.log('bad');
                        console.log(data);
                    }
                });
            }
        }).disableSelection();
    });
</script>
<?php Pjax::end(); ?>