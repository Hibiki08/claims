<?php

if (is_string($result)): ?>
    <p><?= $result; ?></p>
<?php else: ?>
    <p>
        <a href="<?= $url ?>" target="_blank">Заявка</a>
        <button type="button" onclick="window.location.href = window.location.origin + window.location.pathname + '?id=<?php echo $nextID ?><? if (!empty($proxy)) echo "&proxy=$proxy"; ?><? if (!empty($endID)) echo "&end_id=$endID"; ?><? if (!empty($TMLog)) echo "&log=$TMLog"; ?><? if (!empty($slash)) echo "&slash=$slash"; echo !empty($timeout) ? "&timeout=$timeout" : ''; echo !empty($missing) ? "&missing=$missing" : ''; echo !empty($wellknown) ? "&wellknown=$wellknown" : ''; ?>'">Далее</button>
    </p>
    <p>
        <h3>Номер регистрации</h3>
        <?= $result->reg_number ?>
    </p>
    <p>
        <h3>Номер заявки</h3>
        <?= $result->request_number ?>
    </p>
    <p>
        <h3>Дата государственной регистрации</h3>
        <?= $result->reg_date ?>
    </p>
    <p>
        <h3>Дата поступления заявки</h3>
        <?= $result->request_date ?>
    </p>
    <p>
        <h3>Дата публикации</h3>
        <a title="Официальная публикация в формате PDF" href="<?= $result->publication_pdf ?>" target="_blank"><?= $result->publish_date ?></a>
    </p>
    <p>
        <h3>Дата истечения срока действия регистрации</h3>
        <?= $result->reg_expiry_date ?>
    </p>
    <p>
    <h3>Дата прекращения правовой охраны товарного знака</h3>
    <?= $result->legal_protection_end_date ?>
    </p>
    <p>
        <h3>Статус</h3>
        <?= $result->status->title ?>
    </p>
    <p>
        <h3>Последнее изменение статуса</h3>
        <?= $result->status_date ?>
    </p>
    <p>
        <h3>Приоритет</h3>
        <?= $result->priority ?>
    </p>
    <p>
        <h3>Указание цвета или цветового сочетания</h3>
        <?= $result->colors ?>
    </p>
    <p>
        <h3>Номер первой заявки</h3>
        <?= $result->first_request_number ?>
    </p>
    <p>
        <h3>Дата подачи первой заявки</h3>
        <?= $result->first_request_date ?>
    </p>
    <p>
        <h3>Код страны или международной организации, куда была подана первая заявка</h3>
        <?= $result->first_request_country ?>
    </p>
    <p>
        <h3>Наименование лицензиата</h3>
        <?= $result->licensee ?>
    </p>
    <p>
        <h3>Дата и номер регистрации договора</h3>
        <?= $result->contract_date ?>&nbsp;<?= $result->contract_number ?>
    </p>
    <p>
        <h3>Указание условий и/или ограничений лицензии</h3>
        <?= $result->license_terms ?>
    </p>
    <p>
        <h3>Изображение</h3>
        <img src="/<?= $result->img ?>" height="50"/>
    </p>
    <p>
        <h3>Правообладатель</h3>
        <?= $result->rightholder ?>
    </p>
    <p>
        <h3>Адрес для переписки</h3>
        <?= $result->address ?>
    </p>
    <p>
        <h3>Классы МКТУ</h3>
        <pre>
            <? print_r($result->classes) ?>
        </pre>
    </p>
<?php endif; ?>
<?php //var_dump(!isset($end)); ?>
<?php if (!isset($end) || (isset($end) && !$end)) {?>
    <script>
        setTimeout ( function(){
            window.location.href = window.location.origin + window.location.pathname + '?id=<?php echo $nextID; echo !empty($proxy) ? "&proxy=$proxy" : ''; echo !empty($endID) ? "&end_id=$endID" : ''; echo !empty($TMLog) ? "&log=$TMLog" : ''; echo !empty($slash) ? "&slash=$slash" : ''; echo !empty($timeout) ? "&timeout=$timeout" : ''; echo !empty($error) ? "&error=$error" : ''; echo !empty($missing) ? "&missing=$missing" : ''; echo !empty($wellknown) ? "&wellknown=$wellknown" : ''; ?>';
        }, <?= $timeout ?>);
    </script>
<?php } ?>