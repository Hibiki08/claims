<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\components\StatisticsMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Подача заявок';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <?php echo StatisticsMenu::widget(); ?>
    <div class="stat-content col-lg-9">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <h3>Наиболее популярные дни недели подачи</h3>

        <?= GridView::widget([
            'dataProvider' => $dayname,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'dayname:text:День недели',
                'count_dayname:raw:Количество подач',
            ],
        ]) ?>

        <h3>Наиболее популярные дни месяца подачи</h3>

        <?= GridView::widget([
            'dataProvider' => $day,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'day:text:День',
                'count_day:raw:Количество подач',
            ],
        ]) ?>

        <h3>Наиболее популярные месяца подачи</h3>

        <?= GridView::widget([
            'dataProvider' => $month,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'label' => 'month',
                    'header' => 'Месяц',
                    'value' => function ($model, $key, $index, $column) {
                        $monthNum  = $model['month'];
                        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                        return $dateObj->format('F');
                    }
                ],
                'count_month:raw:Количество подач',
            ],
        ]) ?>

        <h3>Наиболее популярные года подачи</h3>

        <?= GridView::widget([
            'dataProvider' => $year,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'year:text:Год',
                'count_year:raw:Количество подач',
            ],
        ]) ?>
    </div>
</div>
