<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\StatisticsMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'По классам';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="requests-trademarks row">
    <?php echo StatisticsMenu::widget(); ?>
    <div class="stat-content col-lg-9">
        <h1><?= Html::encode($this->title) ?></h1>

        <h3>Наиболее популярные классы среди товарных знаков</h3>

        <?= GridView::widget([
            'dataProvider' => $tmClasses,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'class:text:№ класса',
                'count:raw:Количество использований',
            ],
        ]) ?>

        <h3>Наиболее популярные классы среди заявок</h3>

        <?= GridView::widget([
            'dataProvider' => $rClasses,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'class:text:№ класса',
                'count:raw:Количество использований',
            ],
        ]) ?>
    </div>
</div>