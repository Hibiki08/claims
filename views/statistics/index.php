<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\components\StatisticsMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <?php echo StatisticsMenu::widget(); ?>
    <div class="stat-content col-lg-9">
        <h1><?php echo Html::encode($this->title); ?></h1>
        <p>Здесь собирается занимательная статистика.</p>
        <p>← Выберите подстраницу слева</p>
    </div>
</div>
