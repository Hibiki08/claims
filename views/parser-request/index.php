<?php
if (is_null($result)) { ?>
    <p>Ошибка чтения документа</p>
<?php } else {
if (is_string($result)) { ?>
    <p><?php echo $result; ?></p>
<?php } else { ?>
    <p>
        <a href="<?php echo $url; ?>" target="_blank">Заявка </a>
        <a class="btn btn-default" href="<?php echo '/parser-request/update?id=' . $nextID; echo !empty($proxy) ? '&proxy=' . $proxy : ''; echo !empty($endID) ? '&end_id=' . $endID : ''; echo !empty($TMLog) ? '&log=' . $TMLog : ''; echo !empty($timeout) ? '&timeout=' . $timeout : '';  echo !empty($update) ? "&update=$update" : '';  echo !empty($infinity) ? "&infinity=$infinity" : ''; ?>">Далее</a>
    </p>
    <p>
    <h3>Номер регистрации</h3>
    <?php echo $result->reg_number; ?>
    </p>
    <p>
    <h3>Номер заявки</h3>
    <?php echo $result->request_number ?>
    </p>
    <p>
    <h3>Дата государственной регистрации</h3>
    <?php echo $result->reg_date; ?>
    </p>
    <p>
    <h3>Дата поступления заявки</h3>
    <?php echo $result->request_date; ?>
    </p>
    <p>
    <h3>Заявитель</h3>
    <?php echo $result->applicant; ?>
    </p>
    <p>
    <h3>Статус</h3>
    <?php echo $result->status->title; ?>
    </p>
    <p>
    <h3>Номер первой заявки</h3>
    <?php echo $result->first_request_number; ?>
    </p>
    <p>
    <h3>Дата подачи первой заявки</h3>
    <?php echo $result->first_request_date; ?>
    </p>
    <p>
    <h3>Код страны или международной организации, куда была подана первая заявка</h3>
    <?php echo $result->first_request_country; ?>
    </p>
    <p>
    <h3>Изображение</h3>
    <img src="/<?php echo $result->img; ?>" height="50"/>
    </p>
    <p>
    <h3>Адрес для переписки</h3>
    <?php echo $result->address; ?>
    </p>
    <p>
    <h3>Факсимильные изображения</h3>
    <?php echo $result->facsimile; ?>
    </p>
    <p>
    <h3>Классы МКТУ</h3>
    <pre>
            <?php print_r($result->classes); ?>
        </pre>
    </p>
<?php } ?>
<?php if (!isset($end) || (isset($end) && !$end)) {?>
    <script>
        setTimeout ( function(){
            window.location.href = window.location.origin + window.location.pathname + '?id=<?php echo $nextID; echo !empty($proxy) ? "&proxy=$proxy" : ''; echo !empty($endID) ? "&end_id=$endID" : ''; echo !empty($TMLog) ? "&log=$TMLog" : '';echo !empty($timeout) ? "&timeout=$timeout" : ''; echo !empty($error) ? "&error=$error" : '';  echo !empty($update) ? "&update=$update" : '';  echo !empty($infinity) ? "&infinity=$infinity" : ''; ?>';
        }, <?php echo $timeout ?>);
    </script>
<?php }
}
