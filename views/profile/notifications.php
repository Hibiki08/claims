<?php

use app\components\ProfileMenu;
use yii\helpers\Url;

$this->title = 'Уведомления';
?>
<div class="profile-wrapper row">
    <?php echo ProfileMenu::widget(); ?>
    <div class="profile-content col-lg-9 notifications">
        <h1><?php echo $this->title; ?></h1>
<!--        --><?php //if (Yii::$app->user->can('claimsPermission')) {?>
            <blockquote>
                <p>Чтобы настроить уведомления в телеграмме, отправьте боту команду:</p>
                <?php if (!empty($user['telegram_key'])) {?>
                    <code class="bg-alert ">
                        <span>/key <?php echo $user['telegram_key']; ?></span>
                    </code>
                <?php } else { ?>
                    <code>
                        <span>Для получения ключа свяжитесь с администратором</span>
                    </code>
                <?php } ?>
            </blockquote>
            <div>
                <h5>Отслеживание:</h5><br>
                <span>Бот: @MonocleMentionBot</span>
                <?php if (!empty($user['telegram_key'])) {?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="telegram-notice" <?php echo $user['telegram_notice'] ? 'checked' : ''; ?>> Присылать уведомления в телеграмм
                        </label>
                    </div>
                <?php } ?>
                <span><a href="<?php echo Url::to(['/tracking/instruction']); ?>">Инструкция по отслеживанию</a></span>
            </div>
            <hr>
        <?php if (Yii::$app->user->can('support')) { ?>
            <div>
                <h5>Техническая поддержка:</h5><br>
                <span>Бот: @MonocleSupportTestBot</span><br>
                <span><a href="https://docs.google.com/document/d/1FnNwXzYYs1Bogyg4JwLyHcRzZxuDLkcoWu5eSBEqNIc/edit" target="_blank">Инструкция ведения тикета для тех. поддержки</a></span>
            </div>
        <?php } ?>
    </div>
</div>
