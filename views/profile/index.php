<?php

use app\components\ProfileMenu;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Профиль';
?>
<div class="profile-wrapper row">
    <?php echo ProfileMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <h1><?php echo $this->title; ?></h1>
        <div class="form">
            <?php if (Yii::$app->session->get('success')) {?>
                <p class="success">Пароль успешно изменён!</p>
                <?php Yii::$app->session->remove('success');} ?>
            <?php if (Yii::$app->session->get('profile_success')) {?>
                <p class="success">Изменения сохранены!</p>
                <?php Yii::$app->session->remove('profile_success');} ?>
            <div class="form-group">
                <label class="control-label">Электронная почта</label>
                <input type="text" class="form-control" value="<?php echo $user->login; ?>" disabled>
            </div>
            <?php $profileForm = ActiveForm::begin([
                'id' => 'profile-settings',
                'enableClientValidation' => false,
            ]); ?>
            <fieldset class="form-group">
                <legend>Настройки профиля</legend>
                <p>Текущий тариф: <?php
                    if (Yii::$app->user->can('clientExtendedPermission')) {
                        echo 'Premium' . ' до ' . date('m.d.Y', strtotime($user->subscription_date)) . '<a href="/subscription" class="renew-subscription">Продлить подписку</a></p>';
                    } else {
                    echo 'Basic' . '</p>'; ?>
                <p><a href="<?php echo Url::to(['profile/subscription']); ?>">Сменить тариф</a></p>
                <?php } ?>
                <?php echo $profileForm->field($profileModel, 'name')->input('text',[
                    'value' => $user->name
                ]); ?>
            </fieldset>
            <button type="submit" class="btn btn-raised btn-primary">Сохранить</button>
            <?php ActiveForm::end(); ?>
            <?php $form = ActiveForm::begin([
                'id' => 'pass-reset',
                'enableClientValidation' => false,
            ]); ?>
            <fieldset class="form-group">
                <legend>Сбросить пароль</legend>
                <?php echo $form->field($model, 'password')->input('password'); ?>
                <?php echo $form->field($model, 'email', [
                    'template' => '{input}'
                ])->hiddenInput(['value' => $user->login])->label(false); ?>
                <?php echo $form->field($model, 'new_pass')->input('password'); ?>
                <?php echo $form->field($model, 'repeat_pass')->input('password'); ?>
            </fieldset>
            <button type="submit" class="btn btn-raised btn-primary">Изменить пароль</button>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
