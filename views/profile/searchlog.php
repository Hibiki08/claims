<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\ProfileMenu;

$this->title = 'История поиска';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="profile-wrapper row">
    <?php echo ProfileMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <div class="searchlogs-index">

            <h1><?= Html::encode($this->title) ?></h1>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'query:ntext',
                    'area:ntext',
                    ['attribute' => 'date', 'format' =>  ['date', 'dd.MM.Y HH:mm:ss']]
                ],
            ]); ?>
        </div>
    </div>
</div>
