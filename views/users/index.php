<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\models\Users;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\AuthItem;
use app\models\AuthAssignment;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $users yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
//var_dump(key(Yii::$app->authManager->getRolesByUser(10)));
?>

<div class="users">
    <h1><?= Html::encode($this->title) ?></h1>
    <blockquote>
        <p>В этом разделе можно настроить уровень доступа для пользователей.</p>
        <code class="bg-alert ">
            <span><b>claims</b> - максимальный доступ, кроме отдельного функционала</span><br>
            <span><b>claimsPartners</b> - максимальный доступ</span><br>
            <span><b>client</b> - только ограниченный функционал поиска по знакам и заявкам</span><br>
            <span><b>clientExtended</b> - полный функционал поиска по всем разделам, чат тех. поддержки, Telegram уведомлятор</span><br>
            <span><b>superAdmin</b> - максимальный доступ, в том числе доступ к функционалу в разработке.</span><br>
            <span><b>none</b> - доступ к сайту закрыт</span>
        </code>
        <br>
        <br>
        <p>Поле "Тех. поддержка" отвечает за назначение пользователю роли "Техническая поддержка".</p>
    </blockquote>
    <?= GridView::widget([
        'summary' => '',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'rowOptions' => function ($model) {
            return ['data-id' => $model['id']];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id:text:ID',
            [
                'attribute' => 'login',
                'label' => 'Логин',
                'value' => function ($model, $key, $index, $column) {
                    return $model['login'];
                },
                'format' => 'html',
            ],
            [
                'label' => 'Уровень прав',
                'attribute' => 'role',
                'filter' => ArrayHelper::map($roles, 'name', 'name'),
                'value' => function ($model, $key, $index, $column) {
                    $user = Users::getUserWithRole($model['id']);
                    if (!empty($user->role)) {
                        $select = Html::dropDownList('roles' . $model['id'], $user->role->item_name, ArrayHelper::map(AuthItem::getRoles(), 'name', 'name'), [
                            'class' => 'select-role form-control js-err_request_update',
                            'data-id' => $model['id'],
                            'data-role' => $user->role->item_name
                        ]);
                        return '<div class="form-group">' . $select . '</div>';
                    }
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Тех. поддержка',
                'value' => function ($model, $key, $index, $column) {
                    $res = Users::hasSupportRole($model['id']);
                    return '<div class="checkbox"><label>' . Html::checkbox('support' . $model['id'], $res) . '</label></div>';
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Дата окончания подписки',
                'value' => function ($model, $key, $index, $column) {
                    return DatePicker::widget([
                        'language' => 'ru',
                        'model' => $model,
                        'attribute' => 'subscription_date',
                        'dateFormat' => 'dd.MM.yyyy',
                        'options' => [
                            'id' => 'dd' . $model->id,
                            'class' => 'datepicker'
                        ]
                    ]);
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{active} {delete}',
                'contentOptions' => ['class' => 'buttons'],
                'buttons' => [
                    'active' => function($model, $key, $index) {
                        if ($key['status']) {
                            return '<label title="Деактивировать" class="switch"><input type="checkbox" checked><span class="slider round"></span></label>';
                        }
                        return '<label title="Активировать" class="switch"><input type="checkbox"><span class="slider round"></span></label>';
                    },
                    'delete' => function($model, $key, $index) {
                        return '<span title="Удалить" class="glyphicon glyphicon-trash delete"></span>';
                    }
                ]
            ],
        ],
    ]) ?>
</div>