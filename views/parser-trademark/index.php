<?php
if (is_null($result)) { ?>
    <?php if (!$missing) {?>
    <p>Ошибка чтения документа</p>
        <?php } else { ?>
        <p><?php echo $nextID; ?></p>
        <?php } ?>
<?php } else {
if (is_string($result)) { ?>
    <p><?php echo $result; ?></p>
<?php } else { ?>
    <p>
        <a href="<?php echo $url; ?>" target="_blank">Товарный знак </a>
        <a class="btn btn-default" href="<?php echo '/parser-trademark/update?id=' . $nextID; echo !empty($proxy) ? '&proxy=' . $proxy : ''; echo !empty($endID) ? '&end_id=' . $endID : ''; echo !empty($TMLog) ? '&log=' . $TMLog : ''; echo !empty($slash) ? '&slash=' . $slash : ''; echo !empty($timeout) ? '&timeout=' . $timeout : ''; echo !empty($missing) ? '&missing=' . $missing : ''; echo !empty($wellknown) ? "&wellknown=$wellknown" : ''; echo !empty($update) ? "&update=$update" : ''; echo !empty($infinity) ? "&infinity=$infinity" : ''; ?>">Далее</a>
    </p>
    <p>
        <h3>Номер регистрации</h3>
        <?php echo $result->reg_number; ?>
    </p>
    <p>
        <h3>Номер заявки</h3>
        <?php echo $result->request_number; ?>
    </p>
    <p>
        <h3>Дата государственной регистрации</h3>
        <?php echo $result->reg_date; ?>
    </p>
    <p>
        <h3>Дата поступления заявки</h3>
        <?php echo $result->request_date; ?>
    </p>
    <p>
        <h3>Дата публикации</h3>
        <a title="Официальная публикация в формате PDF" href="<?php echo $result->publication_pdf; ?>" target="_blank"><?php echo $result->publish_date; ?></a>
    </p>
    <p>
        <h3>Дата истечения срока действия регистрации</h3>
        <?php echo $result->reg_expiry_date; ?>
    </p>
    <p>
        <h3>Дата прекращения правовой охраны товарного знака</h3>
        <?php echo $result->legal_protection_end_date; ?>
    </p>
    <p>
        <h3>Статус</h3>
        <?php echo $result->status->title; ?>
    </p>
    <p>
        <h3>Последнее изменение статуса</h3>
        <?php echo $result->status_date; ?>
    </p>
    <p>
        <h3>Приоритет</h3>
        <?php echo $result->priority; ?>
    </p>
    <p>
        <h3>Указание цвета или цветового сочетания</h3>
        <?php echo $result->colors; ?>
    </p>
    <p>
        <h3>Номер первой заявки</h3>
        <?php echo $result->first_request_number; ?>
    </p>
    <p>
        <h3>Дата подачи первой заявки</h3>
        <?php echo $result->first_request_date; ?>
    </p>
    <p>
        <h3>Код страны или международной организации, куда была подана первая заявка</h3>
        <?php echo $result->first_request_country; ?>
    </p>
    <p>
        <h3>Наименование лицензиата</h3>
        <?php echo $result->licensee; ?>
    </p>
    <p>
        <h3>Дата и номер регистрации договора</h3>
        <?php echo $result->contract_date; ?>&nbsp;<?php echo $result->contract_number; ?>
    </p>
    <p>
        <h3>Указание условий и/или ограничений лицензии</h3>
        <?php echo $result->license_terms; ?>
    </p>
    <p>
        <h3>Изображение</h3>
        <img src="/<?php echo $result->img; ?>" height="50"/>
    </p>
    <p>
        <h3>Правообладатель</h3>
        <?php echo $result->rightholder; ?>
    </p>
    <p>
        <h3>Адрес для переписки</h3>
        <?php echo $result->address; ?>
    </p>
    <p>
        <h3>Классы МКТУ</h3>
        <pre>
            <?php print_r($result->classes); ?>
        </pre>
    </p>
<?php } ?>
<?php } ?>
<?php if (!isset($end) || (isset($end) && !$end)) {?>
    <script>
        setTimeout ( function(){
            window.location.href = window.location.origin + window.location.pathname + '?id=<?php echo $nextID; echo !empty($proxy) ? "&proxy=$proxy" : ''; echo !empty($endID) ? "&end_id=$endID" : ''; echo !empty($TMLog) ? "&log=$TMLog" : ''; echo !empty($slash) ? "&slash=$slash" : ''; echo !empty($timeout) ? "&timeout=$timeout" : ''; echo !empty($error) ? "&error=$error" : ''; echo !empty($missing) ? '&missing=' . $missing: ''; echo !empty($wellknown) ? "&wellknown=$wellknown" : ''; echo !empty($update) ? "&update=$update" : '';  echo !empty($infinity) ? "&infinity=$infinity" : ''; ?>';
        }, <?= $timeout ?>);
    </script>
<?php }
//} ?>