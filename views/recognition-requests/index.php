<?php

use app\components\RecognitionMenu;
use yii\grid\GridView;
use yii\helpers\Html;
use app\components\Parser;

$this->title = 'Нераспознанные знаки';
?>
<div class="recogntion-wrapper row">
    <?php echo RecognitionMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <h1>Заявки > Нераспознанные</h1>
        <div id="request-recognize">
            <div class="info">
                <span>Не распознано всего: <?php echo $countUnrecognized; ?></span><br>
                <span class="current-count">На данный момент: <span>0</span></span>
            </div>
            <button class="btn btn-info">Распознать</button>
            <i class="glyphicon glyphicon-refresh spinner js-spinner"></i>
        </div>
        <?php if (!empty($unrecognized)) {?>
            <?php echo GridView::widget([
                'dataProvider' => $unrecognized,
                'summary' => '',
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'rowOptions' => function ($model, $key, $index, $grid){
                    return [
                        'data-key' => $model['id']
                    ];
                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'request_number',
                        'label' => 'Номер',
                        'value' => function ($model, $key, $index, $column) {
                            return Html::a($model->request_number . ' <small class="glyphicon glyphicon-new-window"></small>', Parser::REQUEST_URL . $model->request_number, ['target' => '_blank']);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'img',
                        'label' => 'Картинка',
                        'value' => function ($model, $key, $index, $column) {
                            return '<img src="/' . $model['img'] . '">';
                        },
                        'contentOptions' => ['class' => 'img'],
                        'format' => 'html',
                    ],
                ],
            ]) ?>
        <?php } ?>
    </div>
</div>
