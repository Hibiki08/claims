<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\IndexAsset;
//use yii\helpers\Url;

IndexAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="wide wow-animation smoothscroll scrollTo">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?php echo $content; ?>

<footer>
    <div class="container">
        <div class="footer">

            <div class="logo wrapper-box__item">
                <img src="/images/logo-violet.png" alt="Monocle">
            </div>
            <div class="inspired footer-item wrapper-box__item">Inspired by <a data-hover="Claims" target="_blank" href="http://claims.ru/" class="">Claims</a></div>

            <div class="user-agreement footer-item"><a data-hover="Пользовательское соглашение" id="demo01" href="#modal-01" class="">Пользовательское соглашение</a></div>
            <div class="requisites footer-item"><a data-hover="Реквизиты юридического лица" id="demo02" href="#modal-02" class="">Реквизиты юридического лица</a></div>
            <div class="developer footer-item">Разработано в <a data-hover="«Актив Медиа»" target="_blank" href="https://activemedia.pro/" class="active-media">«Актив Медиа»</a> в 2017 году</div>
        </div>
    </div>
</footer>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47896811 = new Ya.Metrika({
                    id:47896811,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47896811" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
