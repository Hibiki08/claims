<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\components\Parser;
use app\models\prom\forms\PatentForm;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $terms array */

$this->title = 'Промышленные образцы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patents-index">
    <code class="bg-alert pull-right">
        <!--        <span>Дата обновления из базы ФИПС: --><?php //echo $lastUpdatePatents; ?><!--</span><br>-->
        <span>Патенты: <?php echo $countPatents; ?> от <?php echo $lastUpdatePatents; ?></span><br>
    </code>
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php Pjax::begin(['id' => 'pjax-grid', 'timeout' => false]); ?>

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'patent-form',
        'method' => 'get',
        'fieldConfig' => [
            'template' => "{input}",
            'options' => [
                'tag' => false,
            ],
        ],
        'action' => [''],
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="common-row">
        <div class="form-group">
            <div class="col-lg-12">
                <?= $form->field($patentForm, 'string')->textInput(['placeholder' => 'Введите запрос ...', 'name' => 'string', 'class' => 'form-control', 'value' => $patentForm->string]) ?>
                <div class="search-btn">
                    <?= Html::submitButton('Искать', ['class' => 'btn btn-raised btn-primary btn-violet']) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7">
                <a class="collapse-btn" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Операторы поиска <span class="glyphicon glyphicon-menu-down"></span></a>
                <div class="collapse" id="collapseExample">
                    <div class="well operators">
                        <p>Оператор И всегда активен. Значит, что запрос «котенок песик» найдет документы, содержащие слова «котенок» и «песик».</p>
                        <p><span>|</span> &mdash; ИЛИ. Пример использования:  «котенок | песик».</p>
                        <p><span>- или !</span> &mdash; НЕ. Пример использования:  «котенок -песик (котенок !песик)».</p>
                        <!--<p><span>MAYBE</span> работает подобно оператору ИЛИ. Пример использования:  «котенок MAYBE песик».</p>-->
                        <p><span>=</span> &mdash; ищет точное соотвествие слову. Пример использования:  «котенок =песик».</p>
                        <p><span>""</span> &mdash; фраза целиком. Пример использования:  «"котенок песик"» найдет документы, где фраза встречается в указанном виде.</p>
                        <p><span><<</span> &mdash; порядок слов. Выводит документы, если слова идут в указанном порядке. Пример использования:  котенок << песик &mdash; выведет документ "котенок побежал за песиком", но не выведет "песик побежал за котенком".</p>
                        <p><span>*</span> &mdash; любое выражение. Пример использования:  котенок * песик &mdash; значит между котенком и песиком может быть любое слово.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="common-row">
        <div class="row">
            <div class="col-lg-12">
                <div class="common_title">Где ищем</div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php  echo $form->field($patentForm, 'area')->radioList([
                    'all' => 'По всему',
                    'name' => 'По названию',
                    'owner' => 'По владельцу',
                    'address' => 'По адресу для переписки',
                    'img_description' => 'По описаниям изображений'
                ],[
                    'item' => function($index, $label, $name, $checked, $value) {
                        $area = 'all';
                        if (isset(Yii::$app->request->queryParams['area'])) {
                            $area = Yii::$app->request->queryParams['area'];
                        }
                        $return = '<div class="radio">';
                        $return .= '<label class="label-common">';
                        $return .= '<input class="radio-true" type="radio" name="area" value="' . $value . '" ' . ($area == $value ? 'checked' : '') . '>';
                        $return .= '<span class="radio-custom"></span>';
//                        $return .= '<span class="circle"></span>';
//                        $return .= '<span class="check"></span>';
//                        $return .= '<span class="radio_index">' . $label . '</span>';
                        $return .= '<span class="label">' . $label . '</span>';
                        $return .= '</label>';
                        $return .= '</div>';

                        return $return;
                    },
                    'unselect' => null
                ]) ?>
            </div>
        </div>
    </div>
    <div class="common-row">
        <div class="row">
            <div class="col-lg-12">
                <div class="common_title">Классы</div>
            </div>
        </div>
        <div class="class-list">
            <div class="row">
                <div class="col-lg-8 col-sm-12 class-sel-caption">
                    <div class="classes">
                        <?php if (!empty($patentForm->patentClasses)) {?>
                            <?php $classesArr = explode('.', $patentForm->patentClasses);
                            $count = count($classesArr);
                            if ($count > 3 && $count != $classesCount) {
                                $classValue = 'Выбрано ' . $count . ' из ' . $classesCount;
                            } else {
                                $classValue = str_replace('.', ', ', $patentForm->patentClasses);
                                if ($count == $classesCount) {
                                    $classValue = 'Все классы';
                                }
                            } ?>
                            <div class="value" id="classValue"><?php echo $classValue; ?></div>
                        <?php } else { ?>
                            <div class="value" id="classValue">Все классы</div>
                        <?php } ?>
                        <div class="checkbox-list">
                            <ul>
                                <li>
                                    <label>
                                        <input class="checkbox-true" type="checkbox" value="all" <?php echo empty($patentForm->classes) || (count($classesArr) == $classesCount) ? 'checked' : ''; ?>>
                                        <span class="checkbox-custom"></span>
                                        <span class="label">Все</span>
                                    </label>
                                </li>
                                <?php foreach ($classes as $class) { ?>
                                    <?php if (empty($patentForm->patentClasses)) {
                                        if (!empty($class->parent_id) || (empty($class->parent_id) && $class->class_key == 31) || (empty($class->parent_id) && $class->class_key == 32)) {
                                            $classesArr[] = $class->id;
                                        }
                                    } ?>
                                    <li <?php echo empty($class->parentClass) ? 'class="parent"' : ''; ?>>
                                        <label>
                                            <?php if (!empty($class->parentClass) || (empty($class->parentClass) && $class->class_key == 31) || (empty($class->parentClass) && $class->class_key == 32)) {?>
                                                <input class="checkbox-true" type="checkbox" value="<?php echo $class->id; ?>" <?php echo in_array($class->id, $classesArr) ? 'checked': ''; ?>>
                                                <span class="checkbox-custom"></span>
                                            <?php } ?>
                                            <span class="label"><?php echo $class->class_key . ' - ' . $class->description; ?></span>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php echo $form->field($patentForm, 'classes')->hiddenInput(['value' => (!empty($patentForm->classes) ? $patentForm->classes : implode('.', $classesArr)), 'name' => 'patentClasses'])->label(false); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="common-row">
        <div class="row">
            <div class="col-lg-12">
                <div class="common_title">Статус</div>
            </div>
        </div>
        <div class="row status">
            <div class="col-lg-12">
                <?php echo $form->field($patentForm, 'status')->radioList([
                    'all' => 'Все',
                    'active' => 'Действующие',
                    'invalid' => 'Недействующие',
                ],[
                    'item' => function($index, $label, $name, $checked, $value) {
                        $status = 'all';
                        if (isset(Yii::$app->request->queryParams['stat'])) {
                            $status = Yii::$app->request->queryParams['stat'];
                        }
                        $return = '<div class="radio">';
                        $return .= '<label class="label-common">';
                        $return .= '<input class="radio-true" type="radio" name="stat" value="' . $value . '" ' . ($status == $value ? 'checked' : '') . '>';
//                            $return .= '<span class="circle"></span>';
//                            $return .= '<span class="check"></span>';
                        $return .= '<span class="radio-custom"></span>';
                        $return .= '<span class="label">' . $label . '</span>';
//                            $return .= '<span class="radio_index">' . $label . '</span>';
                        $return .= '</label>';
                        $return .= '</div>';

                        return $return;
                    },
                    'unselect' => null
                ]); ?>
            </div>
        </div>
    </div>

    <div class="common-row">
        <div class="form-group precision">
            <div class="col-lg-5 col-sm-12">
                <label class="range">
                    <span class="range-caption">Точность поиска: <span class="js_similarity"><?php echo $patentForm->similarity; ?></span></span>
                    <input type="range" name="similarity" min="0.1" max="1" step="0.1" value="<?php echo $patentForm->similarity; ?>" id="similarity">
                </label>
            </div>
            <div class="col-lg-4 col-lg-offset-3 col-sm-12">
                <p class="add_info_small">Влияет на схожие слова и опечатки. Чем меньше коэффициент, тем ниже точность.</p>
            </div>
        </div>
    </div>

    <div class="common-row">
        <div class="form-group years">
            <div class="col-lg-5">
                <span class="range-caption">Фильтр по годам: <span class="range-years"><?php echo Yii::$app->formatter->asDate(time(), 'yyyy'); ?></span></span>
                <div id="range-years" ></div>
            </div>
            <?php echo $form->field($patentForm, 'yearMin')->hiddenInput(['value' => (isset($patentForm->yearMin) ? $patentForm->yearMin : 1990 . '-01-01'), 'name' => 'yearMin'])->label(false); ?>
            <?php echo $form->field($patentForm, 'yearMax')->hiddenInput(['value' => (isset($patentForm->yearMax) ? $patentForm->yearMax : date('Y') . '-01-01'), 'name' => 'yearMax'])->label(false); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if (!isset(Yii::$app->request->queryParams['view']) || Yii::$app->request->queryParams['view'] == 'list') {?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{summary}{items}<div class="table-footer">{pager}<div class="page-size-limit">
        <span>По</span>
        <div class="options">
        <ul>
        <li ' . (Yii::$app->request->get('per-page') == 20 || empty(Yii::$app->request->get('per-page')) ? 'class="active"' : '') . '><a href="' .  Url::current(['per-page' => 20]) . '"'  . '>20</a></li>
         <li ' . (Yii::$app->request->get('per-page') == 50 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 50]) . '"'  . '>50</a></option>
         <li ' . (Yii::$app->request->get('per-page') == 100 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 100]) . '"' . '>100</a></option>
         <li ' . (Yii::$app->request->get('per-page') == 300 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 300]) . '"' . '>300</a></option>
        </ul></div>на странице
    </div></div>',
            'summary' => "<div class='top_title_wrapper'><span class='all_found_btn'>Всего найдено: " . ($dataProvider->totalCount == 1000000 ? 'более ' : '') . PatentForm::$count . '</span><span class="views">Вид: <a href="' . Url::current($viewList) . '">списком</a>/<a href="' . Url::current($viewTile) . '">плиткой</a></span></div>',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'rowOptions' => function ($model) {
                switch ($model->status_id) {
                    case PatentForm::STATUS_ACTIVE:
                        $cls = 'success';
                        break;

                    case PatentForm::STATUS_INVALID:
                        $cls = 'danger';
                        break;

                    default:
                        $cls = '';
                }

                return ['class' => $cls];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'header' => 'Название',
                    'value' => function ($model, $key, $index, $column) {
                        return  $model->name;
                    },
                    'format' => 'html',
                ],
                [
                    'label' => 'Изображения',
                    'value' => function ($model, $key, $index, $column) {
                        if (!empty($model->images)) {
                            $result = [];
                            foreach ($model->images as $image) {
                                $result[] = '<a data-fancybox="group-' . $key . '" href="/' . $image['path'] . '"><img class="lazyload" data-src="/' .  $image['path'] . '" src="/images/load.gif"></a>';
                            }
                            return '<div class="images-gallery">' . implode('', $result) . '</div>';
                        }
                    },
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'images']
                ],
                [
                    'label' => 'Классы',
                    'value' => function ($model, $key, $index, $column) {
                        if (!empty($model->classes)) {
                            $result = [];
                            foreach ($model->classes as $class) { ?>
                                <?php $result[] = '<a tabindex="0" role="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="' . $class->class['description'] . '">' . $class->class->parentClass['class_key'] . '-' . $class->class['class_key'] . '</a>'; ?>
                            <?php }
                            return '<div class="classes-wrap js-classes-wrap">' . implode('<br>', $result) . '</div>';;
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'header' => 'Номер<br>и дата подачи заявки',
                    'value' => function ($model, $key, $index, $column) {
                        $date = empty($model->request_date) ? '' : date('d.m.Y', strtotime($model->request_date));
                        if (empty($model->number)) {
                            return $date;
                        }
                        return Html::a($model->number, Parser::PATENT_URL . $model->number, ['target' => '_blank']) . '<br>' . $date;
                    },
                    'format' => 'raw',
                ],
                [
                    'header' => 'Правообладатель<br>Адрес для переписки',
                    'value' => function ($model, $key, $index, $column) {
                        return  '<a href="' . Url::current(['string' => (str_replace(['«','»','"','/', '\\'], '', htmlspecialchars_decode($model->owner))), 'area' => 'owner', 'yearMin' => (isset(Yii::$app->request->queryParams['yearMin']) ? Yii::$app->request->queryParams['yearMin'] : 1990 . '-01-01'), 'yearMax' => (isset(Yii::$app->request->queryParams['yearMax']) ? Yii::$app->request->queryParams['yearMax'] : date('Y') . '-01-01')]) . '">' . str_replace('\\', '', $model->owner) . '</a><hr>' . $model->address;
                    },
                    'format' => 'html',
                ],
                'status.title:text:Статус',
            ],
            'pager' => [
                'prevPageLabel' => '<img src="/images/left.png" alt="left">',
                'nextPageLabel' => '<img src="/images/right.png" alt="right">',
                'firstPageLabel' => false,
                'lastPageLabel'  => false,
            ],
            'emptyText' => 'Ничего не найдено :('
        ]); ?>
    <?php } else {
        if (Yii::$app->request->queryParams['view'] == 'tile') {
            $patents = $dataProvider->getModels();
            if (!empty($patents)) { ?>
                <div class="patents-tile">
                    <div class='top_title_wrapper'><span class='all_found_btn'>Всего найдено: <?php echo ($dataProvider->totalCount == 1000000 ? 'более ' : '') . PatentForm::$count; ?></span><span class="views">Вид: <a href="<?php echo Url::current($viewList); ?>">списком</a>/<a href="<?php echo Url::current($viewTile); ?>">плиткой</a></span></div>
                    <!--                    <p>Всего найдено: --><?php //echo ($dataProvider->totalCount == 1000000 ? 'более ' : '') . PatentForm::$count; ?><!--<span class="views">Вид: <a href="--><?php //echo Url::current($viewList); ?><!--">списком</a>/<a href="--><?php //echo Url::current($viewTile); ?><!--">плиткой</a></span></p>-->
                    <div class="images">
                        <?php foreach ($patents as $patent) {
                            if (!empty($patent->images)) { ?>
                                <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12 img img-thumbnail">
                                    <div class="name">
                                        <?php echo Html::a($patent->number, Parser::PATENT_URL . $patent->number, ['target' => '_blank']); ?>
                                        <span tabindex="0" role="button" data-container="body" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="<?php echo str_replace(['«','»','"','/', '\\'], '', htmlspecialchars_decode($patent->name)); ?>"><?php echo strip_tags(StringHelper::truncate(str_replace(['«','»','"','/', '\\'], '', htmlspecialchars_decode($patent->name)), 80, '...')); ?></span>
                                    </div>
                                    <a data-caption="Номер: <a target='__blank' href='<?php echo Parser::PATENT_URL . $patent->number; ?>'><?php echo $patent->number; ?></a><br>Название: <?php echo str_replace(['«','»','"','/', '\\'], '', htmlspecialchars_decode($patent->name)); ?><br>Статус: <?php echo $patent->status->title; ?>" data-fancybox="gallery" href="/<?php echo $patent->images[0]['path']; ?>"><img src="/<?php echo $patent->images[0]['path']; ?>"></a>
                                </div>
                            <?php }
                        } ?>
                    </div>
                    <?php echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => '<div class="table-footer">{pager}<div class="page-size-limit">
        <span>По</span>
        <div class="options">
        <ul>
        <li ' . (Yii::$app->request->get('per-page') == 20 || empty(Yii::$app->request->get('per-page')) ? 'class="active"' : '') . '><a href="' .  Url::current(['per-page' => 20]) . '"'  . '>20</a></li>
         <li ' . (Yii::$app->request->get('per-page') == 50 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 50]) . '"'  . '>50</a></option>
         <li ' . (Yii::$app->request->get('per-page') == 100 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 100]) . '"' . '>100</a></option>
         <li ' . (Yii::$app->request->get('per-page') == 300 ? 'class="active"' : '') . '><a href="' . Url::current(['per-page' => 300]) . '"' . '>300</a></option>
        </ul></div>на странице
    </div></div>',
                        'pager' => [
                            'prevPageLabel' => '<img src="/images/left.png" alt="left">',
                            'nextPageLabel' => '<img src="/images/right.png" alt="right">',
                            'firstPageLabel' => false,
                            'lastPageLabel'  => false,
                        ],
                        'emptyText' => 'Ничего не найдено :('
                    ]); ?>
                </div>
            <?php }
        }
    } ?>
    <script type="text/javascript">
        $(document).ready(function () {
//            $.material.init({
//                "radio": ''
//            });
            $('.class-sel-caption .classes .checkbox-list label').click({classCount: <?php echo $classesCount; ?>}, classesSearch);

            $('#range-years').slider({
                range: true,
                min: 1990,
                max: <?php echo Yii::$app->formatter->asDate(time(), 'yyyy'); ?>,
                values: [<?php if (!empty($patentForm->yearMin)) {

                    if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $patentForm->yearMin)) {
                        echo Yii::$app->formatter->asDate($patentForm->yearMin, 'yyyy');
                    } else {
                        echo 1990;
                    }
                } else {
                    echo 1990;
                } ?>,
                    <?php if (!empty($patentForm->yearMax)) {
                    if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $patentForm->yearMax)) {
                        echo Yii::$app->formatter->asDate($patentForm->yearMax, 'yyyy');
                    } else {
                        echo Yii::$app->formatter->asDate(time(), 'yyyy');
                    }
                } else {
                    echo Yii::$app->formatter->asDate(time(), 'yyyy');
                } ?>],
                slide: function( event, ui ) {
                    $('.years .range-years').text(ui.values[0] + " - " + ui.values[ 1 ]);
                    if (ui.values[0] + '-01-01' == ui.values[1] + '-01-01') {
                        $('.years input[name=yearMax]').val(ui.values[1] + '-12-31');
                    } else {
                        $('.years input[name=yearMax]').val(ui.values[1] + '-01-01');
                    }
                    $('.years input[name=yearMin]').val(ui.values[0] + '-01-01');
                }
            });
            $('.years .range-years').text($('#range-years').slider( "values", 0) +
                " - " + $('#range-years').slider( "values", 1));
        });
    </script>
    <?php Pjax::end(); ?></div>
