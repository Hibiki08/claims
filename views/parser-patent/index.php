<?php
if (is_null($result)) { ?>
    <p>Ошибка чтения документа</p>
<?php } else {
    if (is_string($result)) { ?>
        <p><?php echo $result; ?></p>
    <?php } else { ?>
        <p>
            <a href="<?php echo $url; ?>" target="_blank">Посмотреть патент</a>
            <a class="btn btn-default" href="<?php echo '/parser-patent/update?id=' . $nextID; echo !empty($proxy) ? '&proxy=' . $proxy : ''; echo !empty($timeout) ? '&timeout=' . $timeout : ''; ?>">Далее</a>
        </p>
        <p>
        <h3>Название патента</h3>
        <?php echo $result->name; ?>
        </p>
        <p>
        <h3>Номер документа</h3>
        <?php echo $result->number; ?>
        </p>
        <p>
        <h3>Дата регистрации</h3>
        <?php echo $result->reg_date; ?>
        </p>
        <p>
        <h3>Номер заявки</h3>
        <?php echo $result->request_number; ?>
        </p>
        <p>
        <h3>Дата подачи заявки</h3>
        <?php echo $result->request_date; ?>
        </p>
        <p>
        <h3>Дата начала действия патента</h3>
        <?php echo $result->actual_at; ?>
        </p>
        <p>
        <h3>Дата прекращения действия патента</h3>
        <?php echo $result->not_valid_date; ?>
        </p>
        <p>
        <h3>Дата публикации</h3>
        <?php echo $result->publish_date; ?>
        </p>
        <p>
        <h3>Статус</h3>
        <?php echo $result->status->title; ?>
        </p>
        <p>
        <h3>Последнее изменение статуса</h3>
        <?php echo $result->last_update_status; ?>
        </p>
        <p>
        <h3>Патентообладатель</h3>
        <?php echo $result->owner; ?>
        </p>
        <p>
        <h3>Адрес для переписки</h3>
        <?php echo $result->address; ?>
        </p>
        <p>
        <h3>Изображения</h3>
        <?php if (!empty($result->images)) {
            foreach ($result->images as $image) { ?>
                <img src="<?php echo '/' . $image['path']; ?>">
            <?php }
        } ?>
        </p>
        <p>
        <h3>Классы</h3>
        <?php if (!empty($result->classes)) {
            foreach ($result->classes as $class) { ?>
                <?php echo $class->class->parentClass['class_key'] . '-' . $class->class['class_key']; ?>
            <?php }
        } ?>
        </p>
    <?php } ?>
    <?php if (!isset($end) || (isset($end) && !$end)) {?>
        <script>
            setTimeout ( function(){
                window.location.href = window.location.origin + window.location.pathname + '?id=<?php echo $nextID; echo !empty($proxy) ? "&proxy=$proxy" : ''; echo !empty($timeout) ? "&timeout=$timeout" : ''; echo !empty($error) ? "&error=$error" : ''; ?>';
            }, <?php echo $timeout ?>);
        </script>
    <?php }
}
