<?php

use app\components\ProfileMenu;
use yii\grid\GridView;

$this->title = 'История платежей';
?>
<div class="profile-wrapper row">
    <?php echo ProfileMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <h1><?php echo $this->title; ?></h1>
        
        <?php echo GridView::widget([
            'summary' => '',
            'dataProvider' => $dataProvider,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id:text:ID',
                'description:text:Описание',
                [
                    'attribute' => 'pay_status',
                    'label' => 'Статус платежа',
                    'value' => function ($model, $key, $index, $column) {
                        return $model['pay_status'] ? 'Успешно' : 'Ошибка';
                    },
                    'format' => 'html',
                ],
                [
                    'attribute' => 'date',
                    'format' =>  ['date', 'dd.MM.Y HH:mm:ss']
                ]
            ],
        ]); ?>
    </div>
</div>
