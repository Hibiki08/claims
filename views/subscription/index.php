<?php

use app\components\ProfileMenu;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Подписка';
?>
<div class="profile-wrapper row">
    <?php echo ProfileMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <h1><?php echo $this->title; ?></h1>
        <blockquote class="warning">
            <p>Подписка позволяет использовать расширенный поиск товарных знаков и заявок, а также поиск по патентам.</p>
        </blockquote>
        <div class="subscription-list">
            <div data-val="<?php echo $day; ?>" class="subscription-list__block subscription-list__block1 wow flipInX" data-wow-duration="3s" style="visibility: visible; animation-duration: 3s; animation-name: flipInX;">
                <div class="subscription-list__text">День</div>
                <div class="subscription-list__amount subscription-list__amount-old">1 400 руб.</div>
                <div class="subscription-list__amount subscription-list__amount-new">125 руб.</div>
                <div class="subscription-list__line"></div>
            </div>
            <div data-val="<?php echo $month; ?>" class="subscription-list__block subscription-list__block2 wow flipInX selected" data-wow-duration="3s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 3s; animation-delay: 0.5s; animation-name: flipInX;">
                <div class="subscription-list__text">Месяц</div>
                <div class="subscription-list__amount subscription-list__amount-old">18 000 руб.</div>
                <div class="subscription-list__amount subscription-list__amount-new">5 000 руб.</div>
                <div class="subscription-list__line"></div>
                <div class="subscription-list__hit"></div>
                <span class="hit-text">хит</span>
            </div>
            <div data-val="<?php echo $year; ?>" class="subscription-list__block subscription-list__block3 wow flipInX" data-wow-duration="3s" data-wow-delay="1s" style="visibility: visible; animation-duration: 3s; animation-delay: 1s; animation-name: flipInX;">
                <div class="subscription-list__text">Год</div>
                <div class="subscription-list__amount subscription-list__amount-old">180 000 руб.</div>
                <div class="subscription-list__amount subscription-list__amount-new">45 000 руб.</div>
                <div class="subscription-list__line"></div>
            </div>
        </div>
        <blockquote class="warning">
            <p><strong>Скидки в связи с тестированием интерфейса!</strong><br/>Цены действительны до 2 сентября 2018 года.</p>
        </blockquote>
        <?php
//        echo Html::radioList('period', $selected, $periodCost, ['class' => 'radio']);
        foreach ($periodCost as $key => $period) {
            echo Html::beginForm(Yii::$app->walletone->apiUrl, 'post', ['csrf' => false,  'accept-charset' => 'UTF-8', 'class' => 'form-subscription', 'id' => 'subscription-' . $key]);
            $formData = Yii::$app->walletone->getFields([
                'WMI_PAYMENT_AMOUNT' => $key,
                'WMI_CUSTOMER_EMAIL' => Yii::$app->user->identity->login,
                'WMI_DESCRIPTION' => '',
                'WMI_PAYMENT_NO' => mt_rand(0, 100000),
                'user_id' => Yii::$app->user->id
            ]);
            foreach ($formData as $k => $value) {
                echo Html::hiddenInput($k, $value);
            }
            echo Html::submitButton('Оплатить', ['class' => 'btn btn-success']);
            echo Html::endForm();
        }
        ?>
    </div>
</div>
