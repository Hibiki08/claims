<?php

use app\components\RecognitionMenu;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Пустые';
?>
<div class="recogntion-wrapper row">
    <?php echo RecognitionMenu::widget(); ?>
    <div class="profile-content col-lg-9">
        <h1>TЗ WIPO > <?php echo Html::encode($this->title); ?></h1>
        <?php Pjax::begin(['id' => 'pjax-grid', 'timeout' => false]); ?>
        <?php if (!empty($empties)) {?>
            <?php echo GridView::widget([
                'dataProvider' => $empties,
                'summary' => '',
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'rowOptions'=>function ($model, $key, $index, $grid){
                    return [
                        'data-key' => $model['id']
                    ];
                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'label' => 'ID',
                    ],
                    [
                        'attribute' => 'img',
                        'label' => 'Картинка',
                        'value' => function ($model, $key, $index, $column) {
                            return '<img src="/' . $model['img'] . '">';
                        },
                        'contentOptions' => ['class' => 'img'],
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'captcha_id',
                        'label' => 'CAPTCHA ID',
                        'format' => 'html',
                    ],
                    [
                        'label' => '',
                        'value' => function ($model, $key, $index, $column) {
                            return '<a href="/parser/wrong-tags?id=' . $model['id'] . '&amp;model=tmForeign" class="btn btn-default js-wrong_tags">Перераспознать</a>';
                        },
                        'format' => 'raw',
                    ],
                ],
            ]) ?>
        <?php } ?>
        <?php Pjax::end(); ?>
    </div>
</div>
