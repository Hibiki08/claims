<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Сброс пароля';
?>
<div class="site-request-password-reset">
    <h1><?php echo $this->title; ?></h1>
    <?php if (Yii::$app->session->has('success')) { ?>
        <p><?php echo Yii::$app->session['success']; ?></p>
        <?php Yii::$app->session->remove('success'); ?>
    <?php }
    elseif (Yii::$app->session->has('error')) { ?>
        <p><?php echo Yii::$app->session['error']; ?></p>
        <?php Yii::$app->session->remove('error'); ?>
        <?php } else { ?>

    <p data-name="reset-pass-2">Пожалуйста, укажите адрес электронной почты. Вам будет выслана ссылка для сброса пароля</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', [
                        'class' => 'btn btn-primary',
                        'data-name' => 'reset-pass-3'
                    ]) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <?php } ?>
</div>
