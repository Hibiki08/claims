<?php

use yii\helpers\Url;

?>
<!-- Page Head-->
<header class="page-head slider-menu-position">
    <!-- RD Navbar Transparent-->
    <div class="rd-navbar-wrap">
        <nav data-auto-height="false" data-md-auto-height="false" data-md-device-layout="rd-navbar-fixed" data-anchor-offset="20" data-lg-device-layout="rd-navbar-static" data-lg-auto-height="false" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-stick-up="true" class="rd-navbar rd-navbar-default">
            <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                    <!-- RD Navbar Toggle-->
                    <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                    <!-- Navbar Brand-->
                    <div class="rd-navbar-brand"><a href="/"><img width="208" height="44" src="/images/monocle-logo.png" alt=""></a></div>
                </div>
                <div class="rd-navbar-menu-wrap">
                    <div class="rd-navbar-nav-wrap">
                        <div class="rd-navbar-mobile-scroll">
                            <!-- Navbar Brand Mobile-->
                            <div class="rd-navbar-mobile-brand"><a href="/"><img width="208" height="44" src="/images/monocle-logo.png" alt=""></a></div>
                            <!-- RD Navbar Nav-->
                            <ul class="rd-navbar-nav">
                                <li class="active"><a href="#about">О сервисе</a></li>
                                <li><a href="#features">Что умеет Monocle?</a></li>
<!--                                <li><a href="#reviews">Отзывы</a></li>-->
                                <li><a href="#pages">Подписка и цены</a></li>
                                <li><a href="<?php echo Url::to(['/login']); ?>" class="btn btn-primary btn-icon btn-icon-left btn-sm"><span class="icon mdi mdi-login"></span>Войти или зарегистрироваться</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
<!-- Page Contents-->
<main class="page-content">
    <div id="home" data-type="anchor">
        <section>
            <div data-on="false" data-md-on="true" class="rd-parallax bg-white">
                <div data-speed="0.15" data-type="media" data-url="images/bg-01-1920x1000.jpg" class="rd-parallax-layer background-custom-position"></div>
                <div data-speed="0" data-type="html" data-md-fade="true" class="rd-parallax-layer">
                    <div class="shell padding-bottom-percent">
                        <div class="offset-top-140 offset-xs-top-0 offset-md-top-165">
                            <div class="range">
                                <div class="range section-cover range-xs-middle">
                                    <div class="cell-xs-12">
                                        <h2 class="text-uppercase text-bold text-darkest">Аналитическая система контроля за брендами и конкурентами</h2>
                                        <div class="offset-top-15">
                                            <h6 class="text-dark text-regular">Сервис поиска по товарным знакам и промышленным образцам</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
    </div>
    <div>
        <section class="section-relative section-65 section-md-top-0 section-md-bottom-100">
            <h4 style=" margin-top: -19%;">Бесплатный поиск без регистрации <i class="mdi mdi-arrow-down-bold"></i></h4>
            <div class="cell-md-4 cell-lg-3 offset-top-50 offset-lg-top-0 try">
                <div class="box-planning-body">
                    <a href="<?php echo Url::to(['/request']); ?>" class="btn btn-block btn-default btn-sm">Попробовать бесплатно</a>
                </div>
            </div>
            <iframe src="https://monocle.report/request/index" width="1067" height="604" style="border: 1px solid #eee;  box-shadow: 2px #ccc; height: 604px; max-width: 75.5729%" class="veil reveal-md-inline-block"></iframe>
            <div class="shell offset-md-top-60 clearfix" id="about" data-type="anchor">
                <h4>О сервисе</h4>
                <div class="about">
                    <p>Мы придумали Monocle для создателей новых продуктов и брендов, маркетологов, юристов и предпринимателей, заинтересованных в проверке и управлении товарными знаками и дизайнами.</p>
                    <p>Monocle анализирует товарные знаки, заявки и промышленные образцы, зарегистрированные в общедоступных реестрах Роспатента.</p>
                </div>
            </div>
            <div class="shell offset-md-top-60" id="features" data-type="anchor">
                <h2>Что умеет Monocle бесплатно</h2>
                <h6>Бесплатный поиск по товарным знакам и заявкам, зарегистрированным в Роспатенте.<br>Доступно для всех зарегистрированных пользователей</h6>
                <div class="range range-xs-center">
                    <div class="cell-xs-10 cell-sm-6 cell-md-4"><span class="icon icon-lg icon-circle mdi mdi-keyboard icon-primary"></span>
                        <h4>Поиск с учетом опечаток</h4>
                        <p class="inset-sm-left-15 inset-sm-right-15">Можно задать точность поиска, и Monocle найдет похожие слова</p>
                    </div>
                    <div class="cell-xs-10 cell-sm-6 cell-md-4 offset-top-45 offset-sm-top-0"><span class="icon icon-lg icon-circle mdi mdi-refresh icon-primary"></span>
                        <h4>Актуальная база</h4>
                        <p class="inset-sm-left-15 inset-sm-right-15">Ежедневно обновляем базы товарных знаков и промышленных образцов</p>
                    </div>
                    <div class="cell-xs-10 cell-sm-6 cell-md-4 offset-top-45 offset-md-top-0"><span class="icon icon-lg icon-circle mdi mdi-playlist-plus icon-primary"></span>
                        <h4>Операторы поиска</h4>
                        <p class="inset-sm-left-15 inset-sm-right-15">Ищите точные вхождения словоформ, указанный порядок слов</p>
                    </div>
                    <div class="cell-xs-10 cell-sm-6 cell-md-4 offset-top-45"><span class="icon icon-lg icon-circle mdi mdi-file-find icon-primary"></span>
                        <h4>Поиск по тексту на изображениях</h4>
                        <p class="inset-sm-left-15 inset-sm-right-15">Логотипы, надписи, картинки распознаны вручную для более детального поиска</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-90 section-md-120">
            <div class="shell">
                <h2>Преимущества платной подписки</h2>
                <h6>Полный функционал доступен всем пользователям с действующей подпиской.<br>Стоимость подписки указана в отношении функционала системы, действующего на момент начала использования в первый день соответствующего периода.</h6>
                <div class="range range-md-middle offset-top-0 offset-md-top-50 range-xs-center">
                    <div class="cell-sm-12 cell-md-6 cell-md-push-1"><img src="/images/home-02-443x631.png" alt="" width="443" height="631" class="center-block img-responsive"></div>
                    <div class="cell-sm-5 cell-md-3 text-sm-left offset-top-50 offset-md-top-0">
                        <div class="offset-m-md-right--35">
                            <div class="unit unit-md unit-md-horizontal">
                                <div class="unit-left"><span class="icon icon-xxs mdi mdi-package-variant icon-primary"></span></div>
                                <div class="unit-body">
                                    <div>
                                        <h4 class="text-bold">Поиск по патентам на промышленные образцы</h4>
                                    </div>
                                    <p class="offset-top-10"></p>
                                </div>
                            </div>
                            <div class="unit unit-md unit-md-horizontal offset-top-30 offset-md-top-60">
                                <div class="unit-left"><span class="icon icon-xxs mdi mdi-bell-outline icon-primary"></span></div>
                                <div class="unit-body">
                                    <div>
                                        <h4 class="text-bold">Отслеживание изменений по запросу, товарному знаку или заявке</h4>
                                    </div>
                                    <p class="offset-top-10"></p>
                                </div>
                            </div>
                            <div class="unit unit-md unit-md-horizontal offset-top-30 offset-md-top-60">
                                <div class="unit-left"><span class="icon icon-xxs mdi icon-primary mdi-message-text-outline"></span></div>
                                <div class="unit-body">
                                    <div>
                                        <h4 class="text-bold">Уведомления на сайте, в Телеграмме и почте</h4>
                                    </div>
                                    <p class="offset-top-10"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell-sm-5 cell-md-3 text-sm-left cell-md-push-1 offset-top-50 offset-md-top-0">
                        <div class="offset-m-md-left--35">
                            <div class="unit unit-md unit-md-horizontal">
                                <div class="unit-left"><span class="icon icon-xxs mdi icon-primary mdi-help-circle"></span></div>
                                <div class="unit-body">
                                    <div>
                                        <h4 class="text-bold">Неограниченные онлайн-консультации поверенного по поиску и защите товарных знаков</h4>
                                    </div>
                                    <p class="offset-top-10"></p>
                                </div>
                            </div>
                            <div class="unit unit-md unit-md-horizontal offset-top-30 offset-md-top-60">
                                <div class="unit-left"><span class="icon icon-xxs mdi icon-primary mdi-format-color-fill"></span></div>
                                <div class="unit-body">
                                    <div>
                                        <h4 class="text-bold">Поиск по цветам в изображениях товарных знаков и заявок</h4>
                                    </div>
                                    <p class="offset-top-10"></p>
                                </div>
                            </div>
                            <div class="unit unit-md unit-md-horizontal offset-top-30 offset-md-top-60">
                                <div class="unit-left"><span class="icon icon-xxs mdi icon-primary mdi-poll-box"></span></div>
                                <div class="unit-body">
                                    <div>
                                        <h4 class="text-bold">Аналитика и статистика по товарным знакам</h4>
                                    </div>
                                    <p class="offset-top-10">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--<div id="reviews" data-type="anchor">
        <section class="context-dark">
            <div data-on="false" data-md-on="true" class="rd-parallax">
                <div data-speed="0.35" data-type="media" data-url="images/bg-02-1920x730.jpg" class="rd-parallax-layer"></div>
                <div data-speed="0" data-type="html" class="rd-parallax-layer">
                    <div class="shell section-90">
                        <h2>Отзывы клиентов</h2>
                        <div class="range range-xs-center offset-top-66">
                            <div class="cell-sm-8">
                                <div data-items="1" data-nav="true" data-dots="false" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]" class="owl-carousel owl-carousel-testimonials-2">
                                    <div>
                                        <blockquote class="quote quote-slider-2"><img width="80" height="80" src="/images/user-alisa-milano-80x80.jpg" alt="" class="quote-img img-circle"/>
                                            <h6 class="quote-author">
                                                <cite class="text-normal">Василий Карасев</cite>
                                            </h6>
                                            <p class="quote-desc">Ведущий юрист компании «РосРыба»</p>
                                            <h6 class="quote-body">Наконец-то появился сервис, в котором можно искать товарные знаки по логотипам.<br>Купили подписку сразу на год!</h6>
                                        </blockquote>
                                    </div>
                                    <div>
                                        <blockquote class="quote quote-slider-2"><img width="80" height="80" src="/images/user-alisa-milano-80x80.jpg" alt="" class="quote-img img-circle"/>
                                            <h6 class="quote-author">
                                                <cite class="text-normal">Сергей Возняков</cite>
                                            </h6>
                                            <p class="quote-desc">CEO в Cockroach StartUP</p>
                                            <h6 class="quote-body">Мы с командой сами нашли все похожие товарные знаки, надавили на компании конкурентов и подали свои заявки.<br>Спасибо сервису Monocle!</h6>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>-->
    <div id="pages" data-type="anchor">
        <section class="section-90">
            <div class="shell">
                <h2>Подписка</h2>
                <h6>Полный функционал доступен на всех тарифах</h6>
                <div class="range range-condensed range-lg-center range-xs-bottom">
                    <div class="cell-md-4 cell-lg-3">
                        <!-- Planning Box type 3-->
                        <div class="box-planning box-planning-type-3">
                            <div class="box-planning-header bg-shark context-dark" style="background-color: #3F51B5;">
                                <h3>День</h3>
                                <p class="h2 plan-price">1400 ₽</p>
                            </div>
                        </div>
                    </div>
                    <div class="cell-md-4 cell-lg-3 offset-top-50 offset-lg-top-0">
                        <!-- Planning Box type 3-->
                        <div class="box-planning box-planning-type-3 active">
                            <div class="box-planning-header bg-primary context-dark" style="background-color: #111A4D;">
                                <h3>Месяц</h3>
                                <p class="h2 plan-price">18 000 ₽</p>
                            </div>
                        </div>
                    </div>
                    <div class="cell-md-4 cell-lg-3 offset-top-50 offset-lg-top-0">
                        <!-- Planning Box type 3-->
                        <div class="box-planning box-planning-type-3">
                            <div class="box-planning-header bg-shark context-dark" style="background-color: #3F51B5;">
                                <h3>Год</h3>
                                <p class="h2 plan-price">180 000 ₽</p>
                            </div>
                        </div>
                    </div>
                    <div class="range range-condensed range-lg-center range-xs-bottom">

                        <div class="cell-md-4 cell-lg-3 offset-top-50 offset-lg-top-0">
                            <div class="box-planning-body">
                                <a href="<?php echo Url::to(['/login']); ?>" class="btn btn-block btn-default btn-sm">Зарегистрироваться</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <div id="contacts" data-type="anchor">
        <section class="context-dark bg-gray-base">
            <div data-on="false" data-md-on="true" class="rd-parallax">
                <div data-speed="0.35" data-type="media" data-url="images/bg-03-1920x546.jpg" class="rd-parallax-layer"></div>
                <div data-speed="0" data-type="html" class="rd-parallax-layer">
                    <div class="shell section-90 section-md-110">
                        <div class="range range-xs-center">
                            <div class="cell-sm-8 cell-md-6">
                                <h2>Рассылка обновлений</h2>
                                <p>Monocle постоянно развивается и модернизируется.<br>Оставьте свою почту, и мы будем присылать новости о новом функционале.</p>
                                <div class="offset-top-40">
                                    <form data-form-output="form-output-global" data-form-type="contact" method="post" action="https://cp.unisender.com/ru/subscribe?hash=6m934wkzinwme5bin9heujtcyg98cma3qc64btapy3h34xq18ynpy" class="">
                                        <input name="charset" type="hidden" value="UTF-8">
                                        <input name="default_list_id" type="hidden" value="12776009">
                                        <input name="overwrite" type="hidden" value="2">
                                        <input name="is_v5" type="hidden" value="1">
                                        <div class="range range-narrow">
                                            <div class="cell-xs-7 cell-sm-8">
                                                <div class="form-group">
                                                    <label for="contact-email" class="form-label">Электронная почта</label>
                                                    <input id="contact-email" type="email" name="email" data-constraints="@Required @Email" class="form-control">
                                                </div>
                                            </div>
                                            <div class="cell-xs-5 cell-sm-4 offset-top-15 offset-xs-top-0">
                                                <button type="submit" class="btn btn-primary">Подписаться</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- Default footer-->
<footer class="section-relative section-50 section-sm-top-30 section-sm-bottom-34 page-footer bg-gray-base context-dark">
    <div class="shell offset-top-35">
        <p class="small text-lighter">Inspired by <a target="_blank" href="http://claims.ru/">Claims</a>.</p>
        <p class="text-lighter"><a href="" data-toggle="modal" data-target="#modal-agreement">Пользовательское соглашение</a></p>
        <p class="text-lighter"><a href="" data-toggle="modal" data-target="#modal-details">Реквизиты юридического лица</a></p>
    </div>
    <div class="shell offset-top-35">
        <p class="small text-darker">Сделано в «<a target="_blank" href="https://activemedia.pro/">Актив Медиа</a>» в 2018 году.
        </p>
    </div>
</footer>