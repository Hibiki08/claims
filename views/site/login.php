<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
        echo "<div class=\"col-lg-6\"><p class=\"bg-$key\">$message</p></div><div class=\"col-lg-offset-6 col-lg-6\"></div>";
    } ?>

    <a href="<?php echo Url::to(['/request']); ?>" class="btn btn-raised btn-primary">Попробовать без регистрации</a>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'login')->textInput(['autofocus' => true])->label('Логин') ?>

    <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>

    <?= $form->field($model, 'rememberMe')->checkbox([
        'template' => '<div class="col-lg-offset-1 col-lg-2">{input} {label}</div><a href="' . Url::to(['/request-password-reset']) . '" class="col-lg-2 forget">Забыли пароль?</a><div class="col-lg-7">{error}</div>',
    ])->label('Запомнить меня') ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-1">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>