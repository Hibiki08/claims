<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Searchlogs */

$this->title = 'Create Searchlogs';
$this->params['breadcrumbs'][] = ['label' => 'Searchlogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="searchlogs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
