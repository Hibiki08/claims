<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\components\LogMenu;

/* @var $this yii\web\View */
/* @var $tmClasses yii\data\ActiveDataProvider */
/* @var $rClasses yii\data\ActiveDataProvider */

$this->title = 'Логирование';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stat row">
    <?php echo LogMenu::widget(); ?>
    <div class="stat-content col-lg-9">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <h3>Обновление последних 100 знаков</h3>

        <?= GridView::widget([
            'dataProvider' => $requestLogs,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id:text:ID',
                'message:text:Лог',
                'created_at:text:Дата',
            ],
        ]) ?>

        <br><br>
        <h3>Обновление всех заявок</h3>

        <?= GridView::widget([
            'dataProvider' => $requestsCycleLogs,
            'summary' => '',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id:text:ID',
                'message:text:Лог',
                'created_at:text:Дата',
            ],
        ]) ?>
    </div>
</div>
