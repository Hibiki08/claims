<?php
if (is_null($res)) { ?>
    <p>Ошибка</p>
<?php } else { ?>
<p>
    <a href="<?php echo $url; ?>" target="_blank">Посмотреть ТМ</a>
    <a class="btn btn-default" href="<?php echo '/parser-tm-foreign/update?id=' . $nextID; echo !empty($timeout) ? '&timeout=' . $timeout : ''; echo !empty($page) ? '&page=' . $page : ''; ?>">Далее</a>
</p>
<p>
<h3>Name</h3>
<?php echo $res->name; ?>
</p>

<p>
<h3>ST13</h3>
<?php echo $res->st13; ?>
</p>

<p>
<h3>Application Number</h3>
<?php echo $res->app_number; ?>
</p>

<p>
<h3>Application Language</h3>
<?php echo $res->app_language; ?>
</p>

<p>
<h3>Application date</h3>
<?php echo $res->app_date; ?>
</p>

<p>
<h3>Registration office</h3>
<?php echo $res->reg_office; ?>
</p>

<p>
<h3>Registration number</h3>
<?php echo $res->reg_number; ?>
</p>

<p>
<h3>Registration date</h3>
<?php echo $res->reg_date; ?>
</p>

<p>
<h3>Expiry date</h3>
<?php echo $res->expiry_date; ?>
</p>

<p>
<h3>Basic application number</h3>
<?php echo $res->base_app_number; ?>
</p>

<p>
<h3>Basic registration number</h3>
<?php echo $res->base_reg_number; ?>
</p>

<p>
<h3>Basic application date</h3>
<?php echo $res->base_app_date; ?>
</p>

<p>
<h3>Basic registration date</h3>
<?php echo $res->base_reg_date; ?>
</p>

<p>
<h3>Owner</h3>
<?php echo $res->owner; ?>
</p>

<p>
<h3>Trade mark type</h3>
<?php echo $res->type->name; ?>
</p>

<p>
<h3>Kind of mark</h3>
<?php echo $res->kind->name; ?>
</p>

<p>
<h3>Status</h3>
<?php echo $res->status->title; ?>
</p>

<p>
<h3>Image</h3>
<?php if (!empty($res->img)) { ?>
        <img src="<?php echo '/' .$res->img; ?>">
<?php } ?>
</p>

<p>
<h3>Classes</h3>
<?php if (!empty($res->classes)) {
        $classes = [];
        foreach ($res->classes as $class) {
            $classes[] = $class->class_id;
        }
        echo implode(', ', $classes);
} ?>
</p>

<p>
<h3>Countries</h3>
<?php if (!empty($res->countries)) {
        $countries = [];
        foreach ($res->countries as $country) {
            $countries[] = $country->country->abbr;
        }
        echo implode(', ', $countries);
} ?>
</p>
<?php } ?>

<?php if (!isset($end) || (isset($end) && !$end)) {?>
<script>
    setTimeout ( function(){
        window.location.href = window.location.origin + window.location.pathname + '?id=<?php echo $nextID; echo !empty($timeout) ? "&timeout=$timeout" : ''; echo !empty($error) ? "&error=$error" : ''; echo !empty($page) ? "&page=$page" : ''; ?>';
    }, <?php echo $timeout ?>);
</script>
<?php } ?>

