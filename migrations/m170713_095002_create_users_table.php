<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170713_095002_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'password' => $this->string(100)->notNull(),
            'password_reset_token' => $this->string(100),
            'auth_key' => $this->string(100),
            'access_token' => $this->string(100),
            'company' => $this->string(100),
            'domain' => $this->string(255),
            'status' => $this->boolean()->notNull()->defaultValue(0),
            'login' => $this->string(100)->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
