<?php

namespace app\controllers;

use Yii;
use app\models\SignupForm;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;
use yii\web\Controller;
use app\models\LoginForm;
use Telegram\Bot\Exceptions\TelegramResponseException;


class SignupController extends Controller {

    public function actionIndex() {

        $form = new SignupForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            if ($user = $form->signup()) {
                Yii::$app->session->setFlash('success-signup', 'Для окончания регистрации необходимо подтвердить вашу почту. Письмо с дальнейшими указаниями отправлено вам на электронный ящик.');
                $lasId = $user->id;
                $userRole = Yii::$app->authManager->getRole('client');
                Yii::$app->authManager->assign($userRole, $lasId);

                return Yii::$app->getResponse()->redirect(Url::toRoute(['/signup']));
//                return $this->goHome();
            }
        }

        return $this->render('index', [
            'model' => $form,
        ]);
    }

    public function actionConfirmEmail($email, $token) {
        $user = Users::findOne(['login' => $email]);
        if ($user && $user->validateAuthKey($token)) {
            $user->status = Users::STATUS_ACTIVE;
            $user->auth_key = null;
            if ($user->save()) {
                $supportUsers = Users::getUsersWithSupportRole();
                if (!empty($supportUsers)) {
                    $telegram = Yii::$app->MonocleSupportBot;
                    foreach ($supportUsers as $supportUser) {
                        if (!empty($supportUser->telegram_id)) {
                            try {
                                $text = '<b>Зарегистрирован новый пользователь:</b> ' . $user->login;
                                $telegram->api->sendMessage([
                                    'chat_id' => $supportUser->telegram_id,
                                    'text' => $text,
                                    'parse_mode' => 'html'
                                ]);
                            } catch (TelegramResponseException $e) {
                                continue;
                            }
                        }
                    }
                }
                Yii::$app->session->setFlash('success-confirm');
                Yii::$app->getUser()->login($user);
            }

        } else {
            Yii::$app->session->setFlash('confirm-error', 'Что-то пошло не так.');
        }
        return $this->render('success');
    }

}