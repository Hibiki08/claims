<?php

namespace app\controllers;

use app\models\prom\Classes;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\prom\forms\PatentForm;
use app\models\prom\Patent;


class PatentController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['clientExtendedPermission'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = new PatentForm(['scenario' => 'patent']);
        $lastUpdatePatents = Patent::getLastUpdate();
        $countPatents = Patent::getCount();
        $classesCount = Classes::getCount() + 2;
        $classes = Classes::getAllClasses();
        $queryParams = Yii::$app->request->queryParams;
        $viewTile = $viewList = $queryParams;
        $viewTile['view'] = 'tile';
        $viewList['view'] = 'list';
        $dataProvider = $model->search($queryParams);

        return $this->render('index', [
            'patentForm' => $model,
            'lastUpdatePatents' => $lastUpdatePatents,
            'countPatents' => $countPatents,
            'dataProvider' => $dataProvider,
            'classesCount' => $classesCount,
            'viewTile' => $viewTile,
            'viewList' => $viewList,
            'classes' => $classes
        ]);
    }

}