<?php

namespace app\controllers;

use app\models\TmForeign;
use app\models\TmForeignTerm;
use Yii;
use yii\web\Controller;
use app\components\Parser;
use app\models\Terms;
use \Wkhooy\ObsceneCensorRus;
use yii\web\Response;


class ParserTmForeignController extends Controller {

    public function actionUpdate($page = 1) {
        $key = '';
        for ($i = 1; $i <= 13; ++$i)  {
            $key .= rand(0,9);
        }
        $url_page = Parser::TM_FOREIGN_PAGE_URL . $page . '&nd=' . $key;


        $url_tm = null;
        $res = null;
        $nextID = null;
        $end = false;
        $output = null;
        $parser = new Parser();
        $data = [];
        $numbers = [];

        $error = empty(Yii::$app->request->get('error')) ? 0 : Yii::$app->request->get('error');
        $timeout = empty(Yii::$app->request->get('timeout')) ? Parser::$timeout : Yii::$app->request->get('timeout');
        $id = empty(Yii::$app->request->get('id')) ? null : Yii::$app->request->get('id');

        $response = exec(dirname(__DIR__) . '/bin/phantomjs/bin/phantomjs ' . dirname(__DIR__) . '/web/js/phantom.js \'' . $url_page . '\' 2000 \'' . $browsers = Parser::$browsers[mt_rand(0, 14)] . '\' 2>&1');
        if (!empty($response)) {
            $data = (array)json_decode(strip_tags($response));
            if (!empty($data)) {
                if (isset($data['rows'])) {
                    foreach ($data['rows'] as $num) {
                        $numbers[] = $num->ST13;
                    }
                }
                if (!empty($numbers)) {
                    if (!$id) {
                        $id = $numbers[0];
                    }

                    $key = array_search($id, $numbers);
                    if ($key !== false && !is_null($key)) {
                        $nextKey = $key + 1;
                        if (isset($numbers[$nextKey])) {
                            $nextID = $numbers[$nextKey];
                        } else {
                            $nextID = null;
                            $page = $page + 1;
                            $error = $error + 1;
                        }
                    }

                    $tm_foreign = TmForeign::findOne(['st13' => $id]);
                    if (empty($tm_foreign)) {
                        $tm_foreign = new TmForeign();
                    }

                    $url_tm = Parser::TM_FOREIGN_URL . $id;
                    exec(dirname(__DIR__) . '/bin/phantomjs/bin/phantomjs ' . dirname(__DIR__) . '/web/js/phantom.js \'' . $url_tm . '\' 2000 \'' . $browsers = Parser::$browsers[mt_rand(0, 14)] . '\' 2>&1', $response2);
                    $result = implode($response2);

                    $reg_number = Parser::extractForeignString($result, '(111)</span>Registration number</td>');
                    if (empty($reg_number)) {
                        $error = $error + 1;
                        $nextID = $id;
                    } else {
                        $name = Parser::extractForeignString($result, '<span class="trademarkName">');
                        $app_lang = Parser::extractForeignString($result, '(270)</span>Application language</td>');
                        $app_date = Parser::extractForeignDate($result, '(220)</span>Application date</td>');
                        $reg_office = Parser::extractForeignString($result, '(190)</span>Registration office</td>');
                        $app_number = Parser::extractForeignString($result, '(210)/(260)</span>Application number</td>');
                        $reg_date = Parser::extractForeignDate($result, '(151)</span>Registration date</td>');
                        $expiry_date = Parser::extractForeignDate($result, '(141)</span>Expiry date</td>');
                        $base_app_number = Parser::extractForeignString($result, 'Basic application number</td>');
                        $base_app_date = Parser::extractForeignDate($result, 'Basic application date</td>');
                        $base_reg_number = Parser::extractForeignString($result, 'Basic registration number</td>');
                        $base_reg_date = Parser::extractForeignDate($result, 'Basic registration date</td>');
                        $type_id = Parser::extractTypeId($result, '(550)</span>Trade mark type</td>');
                        $kind_id = Parser::extractKindId($result, '(551)</span>Kind of mark</td>');
                        $status = Parser::extractForeignStatusId($result, 'Current trade mark status</td>');
                        $img = Parser::extractForeignImg($result, '<span class="trademarkName">');
                        $classes = Parser::extractForeignClasses($result, '(511)</span>Nice classification</td>');

                        // countries
                        $protocol1 = Parser::extractCountries($result, '(832)</span>Designation(s) under Madrid Protocol</td>');
                        $protocol2 = Parser::extractCountries($result, '(834)</span>Designation(s) under Madrid Protocol (Article 9-6)</td>');
                        $protocol3 = Parser::extractCountries($result, '(831)</span>Designation(s) under Madrid Agreement</td>');
                        $int_office = Parser::extractCountries($result, '(527)</span>Use intent office(s)</td>');
                        $countries = array_merge($protocol1, $protocol2, $protocol3, $int_office);

                        //owner
                        $ownerAr['id'] = Parser::extractForeignString($result, 'Applicant identifier</td>');
                        $ownerAr['name'] = Parser::extractForeignString($result, '<span class="FreeFormatNameLine">');
                        $ownerAr['nation'] = Parser::extractForeignString($result, '(811)</span>Entitlement nationality</td>');
                        $ownerAr['country'] = Parser::extractForeignString($result, 'Address country</td>');
                        $ownerAr['address'] = Parser::extractForeignString($result, '<td class="size25 bold">Address</td>');
                        $owner = implode(' ', $ownerAr);

                        $address = Parser::extractForeignAddress($result, 'Correspondence address');

                        $tm_foreign->name = $name;
                        $tm_foreign->st13 = $id;
                        $tm_foreign->app_number = $app_number;
                        $tm_foreign->app_language = $app_lang;
                        $tm_foreign->app_date = $app_date;
                        $tm_foreign->reg_office = $reg_office;
                        $tm_foreign->reg_number = $reg_number;
                        $tm_foreign->reg_date = $reg_date;
                        $tm_foreign->expiry_date = $expiry_date;
                        $tm_foreign->base_app_number = $base_app_number;
                        $tm_foreign->base_reg_number = !empty($base_reg_number) ? (int)str_replace(' ', '', $base_reg_number) : null;;
                        $tm_foreign->base_app_date = $base_app_date;
                        $tm_foreign->base_reg_date = $base_reg_date;
                        $tm_foreign->owner = $owner;
                        $tm_foreign->address = $address;
                        $tm_foreign->type_id = $type_id;
                        $tm_foreign->kind_id = $kind_id;
                        $tm_foreign->status_id = $status;
                        $tm_foreign->img = Parser::saveForeignImage($img, $id);
                        if ($tm_foreign->save()) {
                            Parser::saveForeignClasses($classes, $tm_foreign->id);
                            Parser::saveForeignCountries($countries, $tm_foreign->id);
                            $res = $tm_foreign;
                            $errorlog = 'OK';
                        } else {
                            $errorlog = '';
                            foreach ($tm_foreign->getErrors() as $error) {
                                $errorlog .= 'ERROR: ' . $error . ' ';
                            }
                        }

                        $log = date('Y-m-d H:i:s') . ' ' . $id . ' ' . $errorlog . "\n";
                        $parser->writeLog(Parser::LOG_PARSER_TM_FOREIGN, $log);
                        $error = 0;
                    }
                }
            }
        }

        if (is_null($res)) {
            $nextID = $id;
            $error = $error + 1;
        }

        if ($error >= Parser::ERROR_LIMIT) {
            $end = true;
        }

        return $this->render('update', [
            'res' => $res,
            'timeout' => $timeout,
            'nextID' => $nextID,
            'url' => $url_tm,
            'page' => $page,
            'error' => $error,
            'end' => $end
        ]);
    }

    public function actionTagUpdate() {
        if (Yii::$app->request->isAjax) {
            $res = true;
            $tmForeign = TmForeign::getUnrecognized(1000);
            if (!empty($tmForeign)) {
                $captchaIds = [];
                foreach ($tmForeign as $trademark) {
                    if (!empty($trademark->thumb)) {
                        $log = date("Y-m-d H:i:s") . ' ' . $trademark['id'];
                        $path = $trademark->thumb;
                        $captchaId = Yii::$app->captcha->run($path);
                        if (!empty($captchaId)) {
                            $captchaIds[$trademark['id']] = $captchaId;
                        } else {
                            $error = Yii::$app->captcha->error();
                            file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_TM_FOREIGN_TAG, $log . ', Error: ' . $error . PHP_EOL, FILE_APPEND);
                        }
                        sleep(4);
                    }
                }
                if (!empty($captchaIds)) {
                    foreach ($captchaIds as $trademark_id => $captcha_id) {
                        $log = date("Y-m-d H:i:s") . ' ' . $trademark_id;

                        if (TmForeign::updateAll(['captcha_id' => $captcha_id], ['id' => $trademark_id]) !== false) {
                            $result = Yii::$app->captcha->getAnswer($captcha_id);
                            if (!empty($result)) {
                                $words = explode(' ', $result);
                                if (!empty($words)) {
                                    foreach ($words as $word) {
                                        $word = mb_strtolower($word);
                                        if (!empty($word)) {
                                            $term = Terms::findOne(['term' => $word]);
                                            if (empty($term)) {
                                                $allowed = ObsceneCensorRus::isAllowed($term);
                                                if ($allowed) {
                                                    $term = new Terms();
                                                    $term->term = $word;
                                                    if ($term->save() === false) {
                                                        $res = false;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!empty($term)) {
                                                $termId = $term->id;
                                                $tmForeignTerm = TmForeignTerm::findOne(['tm_id' => $trademark_id, 'term_id' => $termId]);
                                                if (empty($tmForeignTerm)) {
                                                    $tmForeignTerm = new TmForeignTerm();
                                                    $tmForeignTerm->tm_id = $trademark_id;
                                                    $tmForeignTerm->term_id = $termId;
                                                    if ($tmForeignTerm->save() === false) {
                                                        $res = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_TM_FOREIGN_TAG, $log . ', captchaID: ' . $captcha_id . ' Response: ' . $result . PHP_EOL, FILE_APPEND );
                                }
                            } elseif (empty(Yii::$app->captcha->error())) {
                                $error = 'CAPCHA_NOT_READY';
                            } else {
                                $error = Yii::$app->captcha->error();
                            }
                            if (isset($error)) {
                                file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_TM_FOREIGN_TAG, $log . ', captchaID: ' . $captcha_id . ' Error: ' . $error . PHP_EOL, FILE_APPEND );
                            }
                        } else {
                            $res = false;
                            break;
                        }
                    }
                }
                if ($res) {
                    return Yii::createObject([
                        'class' => 'yii\web\Response',
                        'format' => Response::FORMAT_JSON,
                        'data' => [
                            'next' => true,
                            'count' => TmForeign::getCountUnrecognized()
                        ],
                    ]);
                }
            }
            return Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => Response::FORMAT_JSON,
                'data' => [
                    'next' => false,
                ],
            ]);
        }
    }

}