<?php

namespace app\controllers;

use app\models\RequestClasses;
use app\models\Requests;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;
use app\models\SignupForm;
use yii\web\Response;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
//            'verify' => [
//                'class' => 'app\components\Verify',
//                'actions' => [
//                    'classes',
//                    'error',
//                ]
//            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(Url::toRoute(['/request']));
        }
        $this->layout = 'index';
        $this->view->title = 'Monocle — Сервис бесплатного поиска по товарным знакам и промышленным образцам';
        return $this->render('index');
    }

    public function actionOffer() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $res = 0;
            $offer = Yii::$app->request->post('offer');

            $sent = Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
                ->setTo('yura@chetverikov.org')
                ->setSubject('Предложение от посетителя с главной')
                ->setHtmlBody($offer)
                ->send();

            if ($sent) {
                $res = 1;
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $res,
            ];
        }
        return Yii::$app->getResponse()->redirect(Url::toRoute(['/']));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(Url::toRoute(['/request']));
        }

        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->login()) {
//            return $this->goBack();
            return Yii::$app->getResponse()->redirect(Url::toRoute(['/request']));
        }

        $signUpForm = new SignupForm();
        if ($signUpForm->load(Yii::$app->request->post()) && $signUpForm->validate()) {
            if ($user = $signUpForm->signup()) {
                Yii::$app->session->setFlash('success-signup', 'Для окончания регистрации необходимо подтвердить вашу почту. Письмо с дальнейшими указаниями отправлено вам на электронный ящик.');
                $lasId = $user->id;
                $userRole = Yii::$app->authManager->getRole('client');
                Yii::$app->authManager->assign($userRole, $lasId);
                return Yii::$app->getResponse()->redirect(Url::current());
            }
        }
        
        return $this->render('login', [
            'model' => $form,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        if (Yii::$app->user->logout()) {
            return $this->goHome();
        }
    }

    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте свой почтовый ящик и следуйте дальнейшим инструкциям.');
            } else {
                Yii::$app->session->setFlash('error', 'Извините, сброс пароля по предоставленному электронному адресу не возможен.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Пароль успешно изменён.');
            Yii::$app->getResponse()->redirect(Url::toRoute(['/request-password-reset']));
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Displays about page.
     *
     * @return string
     */
//    public function actionAbout()
//    {
//        return $this->render('about');
//    }
    
}
