<?php

namespace app\controllers;

use app\controllers\metric\User;
use app\models\AuthAssignment;
use Yii;
use yii\web\Controller;
use yii\base\ErrorException;
use app\models\Payment;
use app\models\Users;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use Telegram\Bot\Exceptions\TelegramResponseException;


class SubscriptionController  extends Controller {

    const PRICE_DAY = 125;
    const PRICE_MONTH = 5000;
    const PRICE_YEAR = 45000;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['success', 'fail', 'payment-check'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['payments', 'index'],
                        'allow' => true,
                        'roles' => ['clientPermission'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (in_array($action->id, ['payment-check'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex() {
        $this->view->registerCssFile('/css/style.css');

        $periodCost = [
            self::PRICE_DAY => 'На день (' .  self::PRICE_DAY . ' р)',
            self::PRICE_MONTH => 'На месяц (' .  self::PRICE_MONTH . ' р)',
            self::PRICE_YEAR => 'На год (' .  self::PRICE_YEAR . ' р)'
        ];

        return $this->render('index', [
            'periodCost' => $periodCost,
            'selected' => self::PRICE_MONTH,
            'day' => self::PRICE_DAY,
            'month' => self::PRICE_MONTH,
            'year' => self::PRICE_YEAR
        ]);
    }

    public function actionPaymentCheck() {

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            if ($post['WMI_PAYMENT_AMOUNT'] == number_format(self::PRICE_DAY, 2, '.', '') ||
                $post['WMI_PAYMENT_AMOUNT'] == number_format(self::PRICE_MONTH, 2, '.', '') ||
                $post['WMI_PAYMENT_AMOUNT'] == number_format(self::PRICE_YEAR, 2, '.', '')) {
                $walletone = Yii::$app->walletone;

                try{
                    if($walletone->checkPayment($post)) {
                        $res = 'OK: '. print_r($post, true);
                        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/payments.log', date("d.m.Y H:i:s") . ' ' . $res . "\n", FILE_APPEND);

                        if (!Users::hasPremiumRole($post['user_id'])) {
                            $userRole = Yii::$app->authManager->getRole('premium');
                            Yii::$app->authManager->assign($userRole, $post['user_id']);
                        }

                        $payment = new Payment();
                        $payment->description = $post['WMI_DESCRIPTION'];
                        $payment->pay_status = 1;
                        $payment->user_id = $post['user_id'];
                        $payment->wallet_data = serialize($post);
                        if ($payment->save()) {
                            $user = Users::findOne(['id' => $post['user_id']]);
                            if (!empty($user)) {
                                if (!empty($user->subscription_date)) {
                                    if ($user->subscription_date > date('Y-m-d H:i:s')) {
                                        $date = new \DateTime($user->subscription_date);
                                    } else {
                                        $date = new \DateTime(date('Y-m-d H:i:s'));
                                    }
                                } else {
                                    $date = new \DateTime(date('Y-m-d H:i:s'));
                                }

                                if ($post['WMI_PAYMENT_AMOUNT'] == number_format(self::PRICE_DAY, 2, '.', '')) {
                                    $user->subscription_date = $date->modify('+1 day')->format('Y-m-d H:i:s');
                                }
                                elseif ($post['WMI_PAYMENT_AMOUNT'] == number_format(self::PRICE_MONTH, 2, '.', '')) {
                                    $user->subscription_date = $date->modify('+1 month')->format('Y-m-d H:i:s');
                                }
                                elseif ($post['WMI_PAYMENT_AMOUNT'] == number_format(self::PRICE_YEAR, 2, '.', '')) {
                                    $user->subscription_date = $date->modify('+1 year')->format('Y-m-d H:i:s');
                                }
                                $user->sub_expired_sent = 0;
                                $user->left_few_days_sent = 0;
                                if ($user->update() !== false) {

                                    Yii::$app->mailer
                                        ->compose()
                                        ->setFrom(['mention@monocle.report' => Yii::$app->name . ' robot'])
                                        ->setTo($user->login)
                                        ->setHtmlBody('Вы оформили подписку на Monocle до ' . date('d.m.Y', strtotime($user->subscription_date)))
                                        ->setSubject('Подписка оформлена')
                                        ->send();

                                    $admins = Users::getUsersWithAdminRole();

                                    if (!empty($admins)) {
                                        $telegram = Yii::$app->MonocleSupportBot;
                                        foreach ($admins as $admin) {
                                            if (!empty($admin->telegram_id)) {
                                                try {
                                                    $text = '<b>Произведена оплата #' . $payment->id . ':</b> ' . $user->login;
                                                    $telegram->api->sendMessage([
                                                        'chat_id' => $admin->telegram_id,
                                                        'text' => $text,
                                                        'parse_mode' => 'html'
                                                    ]);
                                                } catch (TelegramResponseException $e) {
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        return 'WMI_RESULT=OK';
                    }
                } catch (ErrorException $c) {
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/payments.log', date("d.m.Y H:i:s"). ' '. $c->getMessage() . "\n", FILE_APPEND);
                    $payment = new Payment();
                    $payment->description = $post['WMI_DESCRIPTION'];
                    $payment->user_id = $post['user_id'];
                    $payment->wallet_data = serialize($post);
                    $payment->save();
                    return 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.$c->getMessage();
                }
            }
            exit;
        }
        else {
            return Yii::$app->getResponse()->redirect(Url::toRoute(['/request']));
        }
    }

    public function actionSuccess() {
        return $this->render('success');
    }

    public function actionFail() {
        return $this->render('fail');
    }

    public function actionPayments() {
        $query = Payment::getHistory(true);

        $dataProvider = new ActiveDataProvider([
            'query' => $query->orderBy(['id' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('payments', [
            'dataProvider' => $dataProvider
        ]);
    }

}