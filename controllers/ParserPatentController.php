<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\Parser;
use app\models\prom\Patent;


class ParserPatentController extends Controller {

    public function actionUpdate($id = 43086) {
        $contentCorrect = false;
        $result = null;
        $nextID = $id;
        $end = false;
        $output = null;
        $parser = new Parser();
        $url = Parser::PATENT_URL . $id;

        $error = empty(Yii::$app->request->get('error')) ? 0 : Yii::$app->request->get('error');
        $timeout = empty(Yii::$app->request->get('timeout')) ? Parser::$timeout : Yii::$app->request->get('timeout');

        if (empty(Yii::$app->request->get('proxy'))) {
            $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
            if (!empty($proxyList)) {
                foreach ($proxyList as $val) {
                    $proxy = trim($val);
                    if (!empty($proxy)) {
                        $output = Parser::extract($url, $proxy);
                        if (($contentCorrect = $parser->contentCorrect($output)) || $parser->getErrorCode() == Parser::ERROR_DOC_NOT_EXISTS) {
                            break;
                        }
                    }
                }
            }
        } else {
            $proxy = Yii::$app->request->get('proxy');
            $output = Parser::extract($url, $proxy);
            $contentCorrect = $parser->contentCorrect($output);
        }

        if ($contentCorrect) {
            $patent = Patent::findOne(['number' => $id]);
            if (empty($patent)) {
                $patent = new Patent();
            } else {
                $date = new \DateTime($patent->reg_date);
                $interval = $date->diff(new \DateTime('now'));

                if ($interval->m > 2) {
                    $result = $patent->reg_date . ' ' . $interval->m . ' ' . $id;
                    $nextID = $id + 1;
                }
            }

            $statusString = Parser::extractPatentStatusString($output);
            $status = !is_null($statusString) ? Parser::getPatentStatusID($statusString) : 1;
            $images = Parser::extractPatentImages($output);
            $classes = Parser::extractPatentClasses($output);
            $owner = Parser::extractString($output, 'Патентообладатель(и):');
            $name = mb_strtolower(Parser::extractString($output, '(54)'));
            $name = mb_strtoupper(mb_substr($name, 0, 1)) . mb_substr($name, 1);
            if (is_null($owner)) {
                $owner = Parser::extractString($output, 'Патентообладатель:');
            }

            $patent->number = $id;
            $patent->request_number = Parser::extractNumber($output, 'Номер заявки:');
            $patent->actual_at = Parser::extractDate($output, 'Дата начала отчета срока <br>действия патента:');
            $patent->not_valid_date = Parser::extractDate($output, 'Дата прекращения действия патента:');
            $patent->publish_date = Parser::extractDate($output, 'Дата публикации:');
            $patent->owner = $owner;
            $patent->name = $name;
            $patent->reg_date = Parser::extractDate($output, 'Дата регистрации:');
            $patent->request_date = Parser::extractDate($output, 'Дата подачи заявки:');
            $patent->address = Parser::extractLastString($output, 'Адрес для переписки:');
            $patent->status_id = $status;
            $patent->last_update_status = Parser::extractDate($output, '(последнее изменение статуса:');

            if ($patent->save(false)) {
                $result = $patent;
                $nextID = $id + 1;
                $error = 0;
                $errorlog = 'OK';

                if (!empty($classes)) {
                    Parser::savePatentClasses($classes, $patent->id);
                }
                if (!empty($images)) {
                    Parser::savePatentImages($images, $patent->id);
                }
            } else {
                $errorlog = '';
                foreach ($patent->getErrors() as $error) {
                    $errorlog .= 'ERROR: ' . $error . ' ';
                }
            }
        } else {
            $parserError = $parser->getErrorCode();
            if ($parserError == Parser::ERROR_FAST_SEEN || $parserError == Parser::ERROR_BAD_REQUEST
                || $parserError == Parser::ERROR_DOC_NOT_EXISTS || $parserError == Parser::ERROR_DOCUMENT
                || $parserError == Parser::ERROR_DOC_NOT_FOUND || Parser::ERROR_BAN) {
                $result = $parser->getErrorText() . ' ' . $id;
            }
            if ($parserError == Parser::ERROR_BAN ||
                $parserError == Parser::ERROR_BAD_REQUEST) {
                $proxy = 0;
            }
            if ($parserError == Parser::ERROR_UNKNOWN) {
                $result = $parser->getErrorText() . ' ' . $id;
                $end = true;
            }
            if ($parserError == Parser::ERROR_LIMIT) {
                $result = 'Забанили ip-адрес до конца дня ' . $id;
                $proxy = 0;
            }
            if ($parserError == Parser::ERROR_DOCUMENT || $parserError == Parser::ERROR_FAST_SEEN) {
                $error = $error + 1;
            }
            if ($parserError == Parser::ERROR_FAST_SEEN) {
                $end = ($error > Parser::LIMIT_ERROR) ? true : false;
            }
            if ($parserError == Parser::ERROR_DOCUMENT) {
                if ($error > Parser::LIMIT_ERROR) {
                    $proxy = 0;
                    $error = 0;
                }
            }
            if ($parserError == Parser::ERROR_DOC_NOT_EXISTS ||
                $parserError == Parser::ERROR_DOC_NOT_FOUND) {
                $nextID = $id + 1;
                $error = $error + 1;
                $end = ($error > Parser::LIMIT_ERROR) ? true : false;
            }
            $errorlog = 'ERROR: ' . $parser->getErrorText();
        }

        $log = date('Y-m-d H:i:s') . ' ' . $id . ' ' . $errorlog . "\n";
        $parser->writeLog(Parser::LOG_PARSER_PATENT, $log);

        return $this->render('index', [
            'result' => $result,
            'timeout' => $timeout,
            'nextID' => $nextID,
            'url' => $url,
            'proxy' => $proxy,
            'error' => $error,
            'end' => $end
        ]);
    }
    
}
