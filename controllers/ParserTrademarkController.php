<?php

namespace app\controllers;

use app\models\Log;
use app\models\LogType;
use Yii;
use yii\db\Query;
use yii\web\Controller;
use app\components\Parser;
use app\models\Trademarks;
use yii\web\Response;
use app\models\Terms;
use app\models\TrademarkTerms;
use yii\helpers\Url;
use yii\db\Expression;
use app\models\TrademarkColors;
use \Wkhooy\ObsceneCensorRus;


class ParserTrademarkController extends Controller {

    const MAX_SLASH_NUMBER = 5;
    const MAX_UPDATE_NUMBER = 200;

    public function actionUpdate($id = 82) {
        $missingNext = null;
        $contentCorrect = false;
        $result = null;
        $nextID = $id;
        $end = false;
        $output = null;
        $parser = new Parser();
        $slashExists = false;
        $newItem = true;
        $message = '';

        $error = empty(Yii::$app->request->get('error')) ? 0 : Yii::$app->request->get('error');
        $timeout = empty(Yii::$app->request->get('timeout')) ? Parser::$timeout : Yii::$app->request->get('timeout');
        $slash = empty(Yii::$app->request->get('slash')) ? 0 : Yii::$app->request->get('slash');
        $missing = empty(Yii::$app->request->get('missing')) ? 0 : Yii::$app->request->get('missing');
        $wellknown = empty(Yii::$app->request->get('wellknown')) ? 0 : Yii::$app->request->get('wellknown');
        $update = empty(Yii::$app->request->get('update')) ? 0 : Yii::$app->request->get('update');
        $infinity = empty(Yii::$app->request->get('infinity')) ? 0 : Yii::$app->request->get('infinity');

        if ($update) {
            if (empty($id)) {
                $nextTm = Trademarks::find()
                    ->orderBy(['id' => SORT_DESC])
                    ->limit(1)
                    ->one();
                $id = $nextTm->reg_number;
            }
        } else {
            if ($infinity) {
                if (empty($id)) {
                    $nextTm = Trademarks::find()
                        ->orderBy(['id' => SORT_ASC])
                        ->limit(1)
                        ->one();

                    $id = $nextTm->reg_number;

                    $logId = LogType::TM_ALL_UPDATE_ID;
                    $log = new Log();
                    $log->message = 'Начало цикла';
                    $log->number_from = $nextTm->reg_number;
                    $log->log_type_id = $logId;
                    $log->save();
                }
            }
        }



        if ($wellknown) {
            $url = Parser::WELLKNOWN_TM_URL . $id;
        } else {
            $url = Parser::TRADEMARK_URL . ($slash ? ($id . '/' . $slash) : $id);
        }

        if ($missing) {
            $lastId = null;
            $trademarks = (new Query())
                ->select('cast(reg_number as UNSIGNED) as reg_num')
                ->from(Trademarks::tableName())
                ->orderBy('reg_num')
                ->limit(1000)
                ->where(['and', ['>=', 'cast(reg_number as UNSIGNED)', $missing], ['not like', 'reg_number', '/']])
                ->all();

            if (!empty($trademarks)) {
                $tmArr = [];
                foreach ($trademarks as $tm) {
                    $tmArr[] = (int)$tm['reg_num'];
                }

                $lastNumber = max($tmArr); //1010

                $range = range($id , $lastNumber);
                $missingNumbers = array_diff($range, $tmArr); //1005, 1006
                $missingNext = $lastNumber; //1010

                if (!empty($missingNumbers)) {

                    if (count($missingNumbers) > 1) {
                        $missingNext = next($missingNumbers); //1006
                        prev($missingNumbers);
                    } else {
                        if ($id != $missingNumbers[key($missingNumbers)]) {
                            $missingNext = $missingNumbers[key($missingNumbers)];
                        } else {
                            return $this->render('index', [
                                'error' => $error,
                                'timeout' => $timeout,
                                'result' => $result,
                                'nextID' => $missingNext,
                                'wellknown' => $wellknown,
                                'slash' => $slash,
                                'missing' => $lastNumber,
                                'url' => $url,
                                'proxy' => Yii::$app->request->get('proxy'),
                                'end' => $end
                            ]);
                        }
                    }
                } else {
                    return $this->render('index', [
                        'error' => $error,
                        'timeout' => $timeout,
                        'result' => $result,
                        'nextID' => $missingNext,
                        'wellknown' => $wellknown,
                        'slash' => $slash,
                        'missing' => $lastNumber,
                        'url' => $url,
                        'proxy' => Yii::$app->request->get('proxy'),
                        'end' => $end
                    ]);
                }
            }
        }


        $regNumber = $slash ? $id . '/' . $slash : $id;

        if (empty(Yii::$app->request->get('proxy'))) {
            $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
            if (!empty($proxyList)) {
                foreach ($proxyList as $val) {
                    $proxy = trim($val);
                    if (!empty($proxy)) {
                        $output = Parser::extract($url, $proxy);
                        if (($contentCorrect = $parser->contentCorrect($output)) || $parser->getErrorCode() == Parser::ERROR_DOC_NOT_EXISTS) {
                            break;
                        }
                    }
                }
            }
        } else {
            $proxy = Yii::$app->request->get('proxy');
            $output = Parser::extract($url, $proxy);
            $contentCorrect = $parser->contentCorrect($output);
        }

        if ($contentCorrect) {
            if ($slash) {
                $trademark = Trademarks::find()
                    ->where(['reg_number' => ($id . '/' . $slash)])
                    ->one();
            } else {
                $trademark = Trademarks::find()
                    ->where(['reg_number' => $id])
                    ->one();
            }

            if (empty($trademark)) {
                $trademark = new Trademarks();
            } else {
                $date = new \DateTime($trademark->reg_date);
                $interval = $date->diff(new \DateTime('now'));

                if (!$slash) {
                    for ($i = 1; $i <= self::MAX_SLASH_NUMBER; $i++) {
                        if (strpos($trademark->rightholder, $trademark->reg_number . '/' . $i) !== false) {
                            $slashExists = true;
                            $slash = 1;
                            break;
                        }
                    }
                }

//                if ($interval->y > 0 || ($interval->y == 0 && $interval->m > 2) || (!empty($trademark->img) && !empty($trademark->rightholder) && !empty($trademark->classes))) {
//                    $newItem = false;
//                    $result = $slash ? $trademark->reg_date . ' ' . $interval->m . ' ' . $id . '/' . $slash : $trademark->reg_date . ' ' . $interval->m . ' ' . $id;
//                    $nextID = ($missingNext ? $missingNext : $id + 1);
//                    $slash = (!$slashExists && $slash) ? $slash < self::MAX_SLASH_NUMBER ? $slash + 1 : 0 : $slash;
//                }
            }

            if ($newItem) {
                $rightholder = Parser::extractLastString($output, 'Правообладатель:');
                if (is_null($rightholder)) {
                    $rightholder = Parser::extractLastString($output, 'Имя правообладателя:');
                    if (is_null($rightholder)) {
                        $rightholder = Parser::extractLastString($output, 'Правообладатель общеизвестного знака:');
                    }
                }
                $reg_date = Parser::extractDate($output, 'Дата государственной регистрации:');
                if (is_null($reg_date)) {
                    $reg_date = Parser::extractDate($output, 'Дата регистрации:');
                }

                $reg_expiry_date = Parser::extractDate($output, 'Дата истечения срока действия регистрации:');
                if (is_null($reg_expiry_date)) {
                    $reg_expiry_date = Parser::extractDate($output, 'Дата истечения срока действия исключительного права:');
                }
                if (is_null($reg_expiry_date)) {
                    $reg_expiry_date = Parser::extractDate($output, 'Дата, до которой продлен срок действия регистрации:', true);
                }
                if (is_null($reg_expiry_date)) {
                    $reg_expiry_date = Parser::extractDate($output, 'Дата, до которой продлен срок действия исключительного права:', true);
                }

                $legal_protection_end_date = Parser::extractDate($output, 'Дата прекращения правовой охраны товарного знака:');
                if (is_null($legal_protection_end_date)) {
                    $legal_protection_end_date = Parser::extractDate($output, 'Дата прекращения правовой охраны:');
                }
                $priority = Parser::extractDate($output, 'Приоритет:');
                if (is_null($priority)) {
                    $priority = Parser::extractDate($output, 'Дата приоритета:');
                }
                $imgPath = Parser::extractImg($output, 'Изображение (воспроизведение) товарного знака, знака обслуживания');
                if (is_null($imgPath)) {
                    $imgPath = Parser::extractImg($output, 'Изображение товарного знака, знака обслуживания');
                    if (is_null($imgPath)) {
                        $imgPath = Parser::extractImg($output, '(540)');
                        if (is_null($imgPath)) {
                            $imgPath = Parser::extractImg($output, 'Изображение общеизвестного товарного знака');
                        }
                    }
                }
                $classes = Parser::extractTMClasses($output, 'Классы МКТУ и перечень товаров и/или услуг:');
                if (is_null($classes)) {
                    $classes = Parser::extractTMClasses($output, 'Классы МКТУ и перечень товаров и/или услуг, в отношении которых товарный знак признан общеизвестным:');
                }
                $publish_date = Parser::extractDate($output, 'Дата публикации:');
                if (is_null($publish_date)) {
                    $publish_date = Parser::extractDate($output, '(450) Дата публикации:');
                }

                if ($wellknown && is_null($reg_date)) {
                    $reg_date = $publish_date;
                }

                $trademark->reg_number = $regNumber;
                $trademark->request_number = Parser::extractNumber($output, 'Номер заявки:');
                $trademark->reg_date = $reg_date;
                $trademark->request_date = Parser::extractDate($output, 'Дата подачи заявки:');
                $trademark->publish_date = $publish_date;
                $trademark->reg_expiry_date = $reg_expiry_date;
                $trademark->publication_pdf = Parser::extractPublication($output, 'Дата публикации:');
                $trademark->legal_protection_end_date = $legal_protection_end_date;
                $trademark->priority = $priority;
                $trademark->rightholder = $rightholder;
                $trademark->address = Parser::extractString($output, 'Адрес для переписки:', true);
                $trademark->first_request_number = Parser::extractString($output, 'Номер первой заявки:');
                $trademark->first_request_date = Parser::extractDate($output, 'Дата подачи первой заявки:');
                $trademark->first_request_country = Parser::extractString($output, 'Код страны или международной организации, куда была подана первая заявка: ');
                $trademark->licensee = Parser::extractString($output, 'Наименование лицензиата:');
                $trademark->contract_date = Parser::extractContractDate($output, 'Дата и номер регистрации договора:');
                $trademark->contract_number = Parser::extractContractNumber($output, 'Дата и номер регистрации договора:');
                $trademark->license_terms = Parser::extractString($output, 'Указание условий и/или ограничений лицензии:');
                $trademark->colors = Parser::extractString($output, 'Указание цвета или цветового сочетания:');
                $trademark->classes = serialize($classes);
                $trademark->img = Parser::saveTrademarkImage($imgPath, $regNumber);

                if ($wellknown) {
                    $trademark->wellknown = 1;
                    $trademark->status_id = 14;
                } else {
                    $statusString = explode('(', Parser::extractStatusString($output, 'Статус:'));
                    $trademark->status_id = Parser::getStatusID($statusString[0]);
                    if (!empty($statusString[1])) {
                        $trademark->status_date = Parser::extractStatusDate($statusString[1]);
                    }
                }


                if ($trademark->save(false)) {
                    Parser::assignColorById($trademark->id);
                    $result = $trademark;
                    if ($update) {
                        if ($update <= self::MAX_UPDATE_NUMBER) {
                            $nextTm = Trademarks::find()
                                ->where(['<', 'id', $trademark->id])
                                ->orderBy(['id' => SORT_DESC])
                                ->limit(1)
                                ->one();

                            if ($update == self::MAX_UPDATE_NUMBER || $update == 1) {
                                $log = new Log();
                                $from = '';

                                if ($update == 1) {
                                    $message = 'Начало цикла';
                                    $log->number_from = $id;
                                }
                                if ($update == self::MAX_UPDATE_NUMBER) {
                                    $update = 0;
                                    $nextTm = Trademarks::find()
                                        ->orderBy(['id' => SORT_DESC])
                                        ->limit(1)
                                        ->one();
                                    $log->number_to = $trademark->reg_number;

                                    $logFrom = Log::find()
                                        ->where(['is not', 'number_from', null])
                                        ->andWhere(['log_type_id' => LogType::TM_100_UPDATE_ID])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->limit(1)
                                        ->one();

                                    $message = 'Цикл закончен';
                                    if (!empty($logFrom)) {
                                        $from = '. Обновлено с ' . $logFrom->number_from . ' по ' . $log->number_to . ' номер';
                                    }
                                    $end = true;
                                }
                                $logId = LogType::TM_100_UPDATE_ID;

                                $log->message = $message . $from;
                                $log->log_type_id = $logId;
                                $log->save();
                            }
                            $update = $update + 1;
                        } else {
                            $update = 1;
                            $nextTm = Trademarks::find()
                                ->orderBy(['id' => SORT_DESC])
                                ->limit(1)
                                ->one();
                        }
                        $nextID = $nextTm->reg_number;
                    } else {
                        if ($infinity) {
                            $nextTm = Trademarks::find()
                                ->orderBy(['id' => SORT_ASC])
                                ->where(['>', 'id', $trademark->id])
                                ->limit(1)
                                ->one();

                            if (empty($nextTm)) {
                                $logId = LogType::TM_ALL_UPDATE_ID;
                                $from = '';
                                $logFrom = Log::find()
                                    ->where(['is not', 'number_from', null])
                                    ->andWhere(['log_type_id' => $logId])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->limit(1)
                                    ->one();

                                if (!empty($logFrom)) {
                                    $from = '. Обновлено с ' . $logFrom->number_from . ' по ' . $id . ' номер';
                                }
                                $log = new Log();
                                $log->message = 'Цикл закончен' . $from;
                                $log->number_to = $id;
                                $log->log_type_id = $logId;
                                $log->save();

                                $nextID = '';
                            } else {
                                $nextID = $nextTm->reg_number;
                            }

                        } else {
                            $nextID = (($slash && $slash < self::MAX_SLASH_NUMBER) ? $id : ($missingNext ? $missingNext : $id + 1));
                        }
                    }
                    $error = 0;
                    $errorlog = 'OK';
                    if ($missing) {
                        $errorlog .= $errorlog . ' missing';
                    }
                    $slash = ($slash && $slash < self::MAX_SLASH_NUMBER) ? $slash + 1 : 0;

                    if (!$slash) {
                        for ($i = 1; $i <= self::MAX_SLASH_NUMBER; $i++) {
                            if (strpos($rightholder, $regNumber . '/' . $i) !== false) {
                                $nextID = $id;
                                $slash = 1;
                            }
                        }
                    }

                    if (!empty($classes)) {
                        Parser::saveTMClasses($classes, $trademark->id);
                    }
                } else {
                    $errorlog = '';
                    foreach ($trademark->getErrors() as $error) {
                        $errorlog .= 'ERROR: ' . $error . ' ';
                    }
                    if ($update || $infinity) {
                        if ($update) {
                            $logId = LogType::TM_100_UPDATE_ID;
                        }
                        if ($infinity) {
                            $logId = LogType::TM_ALL_UPDATE_ID;
                        }

                        $log = new Log();
                        $log->message = $errorlog;
                        $log->is_error = 1;
                        $log->log_type_id = $logId;
                        $log->save();
                    }
                }
            } else {
                $errorlog = 'OK';
            }
        } else {
            $parserError = $parser->getErrorCode();
            if ($parserError == Parser::ERROR_FAST_SEEN || $parserError == Parser::ERROR_BAD_REQUEST
                || $parserError == Parser::ERROR_DOC_NOT_EXISTS || $parserError == Parser::ERROR_DOCUMENT
                || $parserError == Parser::ERROR_DOC_NOT_FOUND || Parser::ERROR_BAN) {
                $result = $parser->getErrorText() . ' ' . $regNumber;
            }
            if ($parserError == Parser::ERROR_BAN ||
                $parserError == Parser::ERROR_BAD_REQUEST) {
                $proxy = 0;
            }
            if ($parserError == Parser::ERROR_UNKNOWN) {
                $result = $parser->getErrorText() . ' ' . $regNumber;
                $end = true;
                if ($infinity) {
                    $end = false;
                }
            }
            if ($parserError == Parser::ERROR_LIMIT) {
                $result = 'Забанили ip-адрес до конца дня ' . $regNumber;
                $proxy = 0;
            }
            if ($parserError == Parser::ERROR_DOCUMENT || $parserError == Parser::ERROR_FAST_SEEN) {
                $error = $error + 1;
            }
            if ($parserError == Parser::ERROR_FAST_SEEN) {
                $end = ($error > Parser::LIMIT_ERROR) ? true : false;
                if ($infinity) {
                    $end = false;
                }
            }
            if ($parserError == Parser::ERROR_DOCUMENT) {
                if ($error > Parser::LIMIT_ERROR) {
                    $proxy = 0;
                    $error = 0;
                }
            }
            if ($parserError == Parser::ERROR_DOC_NOT_EXISTS ||
                $parserError == Parser::ERROR_DOC_NOT_FOUND) {

                $slash = ($slash && $slash < self::MAX_SLASH_NUMBER) ? $slash + 1 : 0;
                $error = $error + 1;
                $end = ($error > Parser::LIMIT_ERROR) ? true : false;
                if ($missing) {
                    $nextID = $missingNext;
                    $end = false;
                    $error = 0;
                } else {
                    if ($update || $infinity) {
                        $nextID = $id;
                        if ($infinity) {
                            $end = false;
                        }
                    } else {
                        $nextID = ($slash && $slash < self::MAX_SLASH_NUMBER) ? $id : ($missingNext ? $missingNext : $id + 1);
                    }
                }
            }
            $errorlog = 'ERROR: ' . $parser->getErrorText();

            if ($update || $infinity) {
                if ($update) {
                    $logId = LogType::TM_100_UPDATE_ID;
                }
                if ($infinity) {
                    $logId = LogType::TM_ALL_UPDATE_ID;
                }

                $log = new Log();
                $log->message = $errorlog;
                $log->is_error = 1;
                $log->log_type_id = $logId;
                $log->save();
            }
        }

        $log = date('Y-m-d H:i:s') . ' ' . $regNumber . ' ' . $errorlog . "\n";
        if (!$missing) {
            $parser->writeLog(Parser::LOG_PARSER_TRADEMARK, $log);
        } else {
            $parser->writeLog(Parser::LOG_PARSER_TRADEMARK_MISSING, $log);
        }


        return $this->render('index', [
            'result' => $result,
            'timeout' => $timeout,
            'nextID' => $nextID,
            'update' => $update,
            'infinity' => $infinity,
            'slash' => $slash,
            'url' => $url,
            'missing' => $missing,
            'proxy' => $proxy,
            'wellknown' => $wellknown,
            'error' => $error,
            'end' => $end
        ]);
    }

    public function actionTagUpdate() {
        if (Yii::$app->request->isAjax) {
            $res = true;
            $trademarks = Trademarks::getUnrecognized(1000);
            if (!empty($trademarks)) {
                $captchaIds = [];
                foreach ($trademarks as $trademark) {
                    if (!empty($trademark->thumb)) {
                        $log = date("Y-m-d H:i:s") . ' ' . $trademark['id'];
                        $path = $trademark->thumb;
                        $captchaId = Yii::$app->captcha->run($path);
                        if (!empty($captchaId)) {
                            $captchaIds[$trademark['id']] = $captchaId;
                        } else {
                            $error = Yii::$app->captcha->error();
                            file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_TRADEMARK_TAG, $log . ', Error: ' . $error . PHP_EOL, FILE_APPEND);
                        }
                        sleep(4);
                    }
                }
                if (!empty($captchaIds)) {
                    foreach ($captchaIds as $trademark_id => $captcha_id) {
                        $log = date("Y-m-d H:i:s") . ' ' . $trademark_id;

                        if (Trademarks::updateAll(['captcha_id' => $captcha_id], ['id' => $trademark_id]) !== false) {
                            $result = Yii::$app->captcha->getAnswer($captcha_id);
                            if (!empty($result)) {
                                $words = explode(' ', $result);
                                if (!empty($words)) {
                                    foreach ($words as $word) {
                                        if (!empty($word)) {
                                            $word = mb_strtolower($word);
                                            $term = Terms::findOne(['term' => $word]);
                                            if (empty($term)) {
                                                $allowed = ObsceneCensorRus::isAllowed($term);
                                                if ($allowed) {
                                                    $term = new Terms();
                                                    $term->term = $word;
                                                    if ($term->save() === false) {
                                                        $res = false;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!empty($term)) {
                                                $termId = $term->id;
                                                $trademarkTerm = TrademarkTerms::findOne(['trademark_id' => $trademark_id, 'term_id' => $termId]);
                                                if (empty($trademarkTerm)) {
                                                    $trademarkTerm = new TrademarkTerms();
                                                    $trademarkTerm->trademark_id = $trademark_id;
                                                    $trademarkTerm->term_id = $termId;
                                                    if ($trademarkTerm->save() === false) {
                                                        $res = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_TRADEMARK_TAG, $log . ', captchaID: ' . $captcha_id . ' Response: ' . $result . PHP_EOL, FILE_APPEND );
                                }
                            } elseif (empty(Yii::$app->captcha->error())) {
                                $error = 'CAPCHA_NOT_READY';
                            } else {
                                $error = Yii::$app->captcha->error();
                            }
                            if (isset($error)) {
                                file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_TRADEMARK_TAG, $log . ', captchaID: ' . $captcha_id . ' Error: ' . $error . PHP_EOL, FILE_APPEND );
                            }
                        } else {
                            $res = false;
                            break;
                        }
                    }
                }
                if ($res) {
                    return Yii::createObject([
                        'class' => 'yii\web\Response',
                        'format' => Response::FORMAT_JSON,
                        'data' => [
                            'next' => true,
                            'count' => Trademarks::getCountUnrecognized()
                        ],
                    ]);
                }
            }
            return Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => Response::FORMAT_JSON,
                'data' => [
                    'next' => false,
                ],
            ]);
        }
    }

}