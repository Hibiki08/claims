<?php

namespace app\controllers;

use app\models\Dictionary;
use app\models\RequestClasses;
use app\models\Requests;
use app\models\RequestTerms;
use app\models\Statuses;
use app\models\Terms;
use app\models\TmForeignTerm;
use app\models\TrademarkClasses;
use app\models\Trademarks;
use app\models\TrademarkTerms;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use DateTime;
use yii\web\Response;
use app\components\Parser;
use Zend\Http\PhpEnvironment\Request;


class ParserController extends Controller
{
    static $ua_all = [
        'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0',
        'Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko',
        'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)',
        'Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.9.0.16) Gecko/2009122206 Firefox/3.0.16 Flock/2.5.6',
        'Mozilla/5.0 (X11; U; Linux x86_64; es-AR; rv:1.9.0.2) Gecko/2008091920 Firefox/3.0.2 Flock/2.0b3',
        'Mozilla/5.0 (compatible; Konqueror/4.2) KHTML/4.2.4 (like Gecko) Fedora/4.2.4-2.fc11',
        'Mozilla/5.0 (X11) KHTML/4.9.1 (like Gecko) Konqueror/4.9',
        'Mozilla/4.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0)',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; chromeframe/12.0.742.100)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36'
    ];
    static $proxy = '139.59.72.192:3128';
    static $timeout = 4500;
    static $maxTimeout = 180;
    const MAX_SLASH_NUMBER = 5;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post']
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Ищем пустые заявки и дополняем информацию в них
     * @return string
     */
    public function actionIndex()
    {
        set_time_limit(60 * 60);
        $requests = Requests::find()
            ->where(['or', [
                'applicant' => NULL,
                'classes' => NULL,
                'img' => NULL
            ]])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1000)
            ->all();

        $last = null;
        foreach ($requests as $request) {

            while(true) {
                $url = 'http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTMAP&rn=6932&DocNumber=' . $request->request_number . '&TypeFile=html';
                $output = $this->parse($url, static::$proxy);
                if (strstr($output, 'Слишком быстрый просмотр документов.')) {
                    sleep(5);
                    continue;
                }
                if (strstr($output, 'Вы заблокированы до')) {
                    die("Забанили ip-адрес {$request->request_number}");
                }
                if (strstr($output, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
                    die("Забанили ip-адрес до конца дня {$request->request_number}");
                }
                break;
            }
            //var_dump($output);die;

            $request->reg_number = $this->extractNumber($output, 'Номер регистрации');
            $request->request_date = $this->extractDate($output, 'Дата поступления заявки');
            $request->reg_date = $this->extractDate($output, 'Дата публикации');
            $request->applicant = $this->extractString($output, 'Заявитель:');
            $request->address = $this->extractString($output, 'Адрес для переписки:');
            $request->first_request_number = $this->extractString($output, 'Номер первой заявки:');
            $request->first_request_date = $this->extractDate($output, 'Дата подачи первой заявки:');
            $request->first_request_country = $this->extractString($output, 'Код страны подачи первой заявки:');
            $request->classes = $this->extractClasses($output, 'Классы МКТУ и перечень товаров и/или услуг:');
            $request->img = $this->extractImg($output, 'Изображение заявляемого обозначения');
            $request->facsimile = $this->extractFax($output, 'Факсимильные изображения');
            $request->status_id = $this->extractStatus($output);

            $errorlog = 'OK';
            if (!$request->save(false)) {
                $errorlog = '';
                foreach ($request->getErrors() as $error) {
                    $errorlog .= "ERROR: $error\n";
                }
            }

            $log = date("Y-m-d H:i:s")." $request->request_number:\n$errorlog\n";
            file_put_contents(Yii::$app->basePath . '/runtime/logs/parser.log',$log, FILE_APPEND );
            $last = $request->request_number;
            sleep(5);
        }

        return $this->render('index', [
            'last' => $last
        ]);
    }

    public function actionTradeMarks($id = -1)
    {
        $TMLog = 'parser_trademarks_ext';
        if (!empty(Yii::$app->request->get('log'))) {
            $TMLog = Yii::$app->request->get('log');
        }

        $endID = empty(Yii::$app->request->get('end_id')) ? 625463 : Yii::$app->request->get('end_id');
        if ($id > $endID) {
            return $this->render('trademark', [
                'result' => "Done! {$id}",
                'end' => true
            ]);
        }
        $url = 'http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTM&DocNumber=' . $id . '/1&TypeFile=html';
        $proxy = empty(Yii::$app->request->get('proxy')) ? static::$proxy : Yii::$app->request->get('proxy');
        $output = $this->parse($url, $proxy);

        if (strstr($output, 'Слишком быстрый просмотр документов.')) {
            return $this->render('trademark', [
                'result' => "Слишком быстрый просмотр документов. {$id}",
                'timeout' => static::$timeout,
                'proxy' => $proxy,
                'nextID' => $id,
                'endID' => $endID,
                'TMLog' => $TMLog,
                'end' => true
            ]);
        }
        if (strstr($output, 'Вы заблокированы до')) {
            return $this->render('trademark', [
                'result' => "Забанили ip-адрес {$id}",
                'end' => true
            ]);
        }
        if (strstr($output, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
            return $this->render('trademark', [
                'result' => "Забанили ip-адрес до конца дня {$id}",
                'end' => true
            ]);
        }
        if (strstr($output, 'Документ с данным номером отсутствует')) {
            return $this->render('trademark', [
                'result' => "Документ с данным номером отсутствует {$id}",
                'timeout' => static::$timeout,
                'proxy' => $proxy,
                'nextID' => $id + 1,
                'endID' => $endID,
                'TMLog' => $TMLog
            ]);
        }

        $trademark = Trademarks::find()
            ->where(['reg_number' => ($id . '/1')])
            ->limit(1)
            ->one();
        if (empty($trademark)) {
            $trademark = new Trademarks();
        }

        $reg_number = $this->extractNumber($output, 'Номер государственной регистрации:');
        if ($reg_number == null) {
            $reg_number = $this->extractNumber($output, 'Номер регистрации:');
        }
        if ($reg_number == null) {
            return $this->render('trademark', [
                'result' => $output,
                'end' => true
            ]);
        }
        $trademark->reg_number = $reg_number;
        $trademark->request_number = $this->extractNumber($output, 'Номер заявки:');
        $trademark->reg_date = $this->extractDate($output, 'Дата государственной регистрации:');
        if ($trademark->reg_date == null) {
            $trademark->reg_date = $this->extractDate($output, 'Дата регистрации:');
        }
        $trademark->request_date = $this->extractDate($output, 'Дата подачи заявки:');
        $trademark->publish_date = $this->extractDate($output, 'Дата публикации:');
        $trademark->reg_expiry_date = $this->extractDate($output, 'Дата истечения срока действия регистрации:');
        if ($trademark->reg_expiry_date == null) {
            $trademark->reg_expiry_date = $this->extractDate($output, 'Дата истечения срока действия исключительного права:');
        }
        if (($temp = $this->extractLastDate($output, 'Дата, до которой продлен срок действия регистрации:')) != null) {
            $trademark->reg_expiry_date = $temp;
        }
        if (($temp = $this->extractLastDate($output, 'Дата, до которой продлен срок действия исключительного права:')) != null) {
            $trademark->reg_expiry_date = $temp;
        }
        $trademark->publication_pdf = $this->extractPublication($output, 'Дата публикации:');
        $trademark->legal_protection_end_date = $this->extractDate($output, 'Дата прекращения правовой охраны товарного знака:');
        if ($trademark->legal_protection_end_date == null) {
            $trademark->legal_protection_end_date = $this->extractDate($output, 'Дата прекращения правовой охраны:');
        }
        $trademark->priority = $this->extractDate($output, 'Приоритет:');
        if ($trademark->priority == null) {
            $trademark->priority = $this->extractDate($output, 'Дата приоритета:');
        }
        $trademark->rightholder = $this->extractLastString($output, 'Правообладатель:');
        if ($trademark->rightholder == null) {
            $trademark->rightholder = $this->extractLastString($output, 'Имя правообладателя:');
        }
        $trademark->address = $this->extractLastString($output, 'Адрес для переписки:');
        $trademark->first_request_number = $this->extractString($output, 'Номер первой заявки:');
        $trademark->first_request_date = $this->extractDate($output, 'Дата подачи первой заявки:');
        $trademark->first_request_country = $this->extractString($output, 'Код страны или международной организации, куда была подана первая заявка: ');
        $trademark->licensee = $this->extractString($output, 'Наименование лицензиата:');
        $this->extractContract($trademark, $output, 'Дата и номер регистрации договора:');
        $trademark->license_terms = $this->extractString($output, 'Указание условий и/или ограничений лицензии:');
        $trademark->colors = $this->extractString($output, 'Указание цвета или цветового сочетания:');
        $trademark->classes = $this->extractTMClasses($output, 'Классы МКТУ и перечень товаров и/или услуг:');
        $trademark->img = $this->extractTMImg($id, $output, 'Изображение (воспроизведение) товарного знака, знака обслуживания', true);
        if ($trademark->img == null) {
            $trademark->img = $this->extractTMImg($id, $output, '(540)', true);
        }
        $status = explode('(', $this->extractStatusString($output, 'Статус:'));
        $trademark->status_id = $this->getStatusID($status[0]);
        if (!empty($status[1])) {
            $trademark->status_date = $this->extractStatusDate($status[1]);
        }

        $errorlog = 'OK';
        if (!$trademark->save(false)) {
            $errorlog = '';
            foreach ($trademark->getErrors() as $error) {
                $errorlog .= "ERROR: $error ";
            }
        }

        if (!empty($trademark->classes)) {
            $this->saveTMClasses(unserialize($trademark->classes), $trademark->id);
        }

        $log = date("Y-m-d H:i:s")." $id $errorlog\n";
        file_put_contents(Yii::$app->basePath . "/runtime/logs/$TMLog.log",$log, FILE_APPEND );

        return $this->render('trademark', [
            'result' => $trademark,
            'timeout' => static::$timeout,
            'nextID' => $id + 1,
            'url' => $url,
            'proxy' => $proxy,
            'endID' => $endID,
            'TMLog' => $TMLog,
            'end' => true
        ]);
    }

    public function actionTMUpdate($id = 631765) {
        $slash = empty(Yii::$app->request->get('slash')) ? 0 : Yii::$app->request->get('slash');
        $timeout = empty(Yii::$app->request->get('timeout')) ? static::$timeout : Yii::$app->request->get('timeout');

        if ($slash == 1) {
            $trademark = Trademarks::find()
                ->where(['reg_number' => ($id . '/1')])
                ->one();
        } else {
            $trademark = Trademarks::find()
                ->where(['reg_number' => $id])
                ->one();
        }

        $url = Parser::$trademarkUrl . $id;
        if (empty(Yii::$app->request->get('proxy'))) {
            $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
            foreach ($proxyList as $val) {
                $output = $this->parse($url, trim($val));
                if (!empty($output)) {
                    if (strstr($output, 'Слишком быстрый просмотр документов.') ||
                        strstr($output, 'Вы заблокированы до') ||
                        strstr($output, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
                        continue;
                    }
                    $proxy = trim($val);
                    break;
                }
            }
        } else {
            $output = $this->parse($url, Yii::$app->request->get('proxy'));
            $proxy = Yii::$app->request->get('proxy');
        }

        if (empty($trademark)) {
            $trademark = new Trademarks();
        } elseif (!empty($trademark->reg_date)) {
            $date = new DateTime($trademark->reg_date);
            $interval = $date->diff(new DateTime('now'));

            if ($interval->m > 2 || (!empty($trademark->img) && !empty($trademark->rightholder) && !empty($trademark->classes))) {
                return $this->render('trademark', [
                    'result' => "{$trademark->reg_date} {$interval->m} {$id}",
                    'timeout' => $timeout,
                    'proxy' => $proxy,
                    'nextID' => $id - 1,
//                    'nextID' => ($slash ? $id - 1 : $id),
//                    'slash' => $slash ^ 1,
                ]);
            }
        }

        if (!empty($output)) {
            if (strstr($output, 'Слишком быстрый просмотр документов.')) {
                return $this->render('trademark', [
                    'result' => "Слишком быстрый просмотр документов. {$id}",
                    'nextID' => $id,
                    'proxy' => $proxy,
                    'timeout' => $timeout,
//                    'end' => true
                ]);
            }
            if (strstr($output, 'Вы заблокированы до')) {
                return $this->render('trademark', [
                    'result' => "Забанили ip-адрес {$id}",
                    'end' => true
                ]);
            }
            if (strstr($output, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
                return $this->render('trademark', [
                    'result' => "Забанили ip-адрес до конца дня {$id}",
                    'end' => true
                ]);
            }
            if (strstr($output, 'Документ с данным номером отсутствует')) {
                return $this->render('trademark', [
//                    'result' => "Документ с данным номером отсутствует {$id}/" . $slash,
                    'result' => "Документ с данным номером отсутствует {$id}",
                    'timeout' => $timeout,
                    'proxy' => $proxy,
                    'nextID' => $id - 1,
                    'slash' => 0
                ]);
            }

            $reg_number = $this->extractNumber($output, 'Номер государственной регистрации:');
            if ($reg_number == null) {
                $reg_number = $this->extractNumber($output, 'Номер регистрации:');
            }
            if ($reg_number == null) {
                return $this->render('trademark', [
                    'result' => $output,
                    'end' => true
                ]);
            }
            $trademark->reg_number = $reg_number;
            $trademark->request_number = $this->extractNumber($output, 'Номер заявки:');
            $trademark->reg_date = $this->extractDate($output, 'Дата государственной регистрации:');
            if ($trademark->reg_date == null) {
                $trademark->reg_date = $this->extractDate($output, 'Дата регистрации:');
            }
            $trademark->request_date = $this->extractDate($output, 'Дата подачи заявки:');
            $trademark->publish_date = $this->extractDate($output, 'Дата публикации:');
            $trademark->reg_expiry_date = $this->extractDate($output, 'Дата истечения срока действия регистрации:');
            if ($trademark->reg_expiry_date == null) {
                $trademark->reg_expiry_date = $this->extractDate($output, 'Дата истечения срока действия исключительного права:');
            }
            if (($temp = $this->extractLastDate($output, 'Дата, до которой продлен срок действия регистрации:')) != null) {
                $trademark->reg_expiry_date = $temp;
            }
            if (($temp = $this->extractLastDate($output, 'Дата, до которой продлен срок действия исключительного права:')) != null) {
                $trademark->reg_expiry_date = $temp;
            }
            $trademark->publication_pdf = $this->extractPublication($output, 'Дата публикации:');
            $trademark->legal_protection_end_date = $this->extractDate($output, 'Дата прекращения правовой охраны товарного знака:');
            if ($trademark->legal_protection_end_date == null) {
                $trademark->legal_protection_end_date = $this->extractDate($output, 'Дата прекращения правовой охраны:');
            }
            $trademark->priority = $this->extractDate($output, 'Приоритет:');
            if ($trademark->priority == null) {
                $trademark->priority = $this->extractDate($output, 'Дата приоритета:');
            }
            $trademark->rightholder = $this->extractLastString($output, 'Правообладатель:');
            if ($trademark->rightholder == null) {
                $trademark->rightholder = $this->extractLastString($output, 'Имя правообладателя:');
            }
            $trademark->address = $this->extractLastString($output, 'Адрес для переписки:');
            $trademark->first_request_number = $this->extractString($output, 'Номер первой заявки:');
            $trademark->first_request_date = $this->extractDate($output, 'Дата подачи первой заявки:');
            $trademark->first_request_country = $this->extractString($output, 'Код страны или международной организации, куда была подана первая заявка: ');
            $trademark->licensee = $this->extractString($output, 'Наименование лицензиата:');
            $this->extractContract($trademark, $output, 'Дата и номер регистрации договора:');
            $trademark->license_terms = $this->extractString($output, 'Указание условий и/или ограничений лицензии:');
            $trademark->colors = $this->extractString($output, 'Указание цвета или цветового сочетания:');
            $trademark->classes = $this->extractTMClasses($output, 'Классы МКТУ и перечень товаров и/или услуг:');
            $trademark->img = $this->extractTMImg($id, $output, 'Изображение (воспроизведение) товарного знака, знака обслуживания', true);
            if ($trademark->img == null) {
                $trademark->img = $this->extractTMImg($id, $output, 'Изображение товарного знака, знака обслуживания', true);
                if ($trademark->img == null) {
                    $trademark->img = $this->extractTMImg($id, $output, '(540)', true);
                }
            }
            $status = explode('(', $this->extractStatusString($output, 'Статус:'));
            $trademark->status_id = $this->getStatusID($status[0]);
            if (!empty($status[1])) {
                $trademark->status_date = $this->extractStatusDate($status[1]);
            }

            $errorlog = 'OK';
            if (!$trademark->save(false)) {
                $errorlog = '';
                foreach ($trademark->getErrors() as $error) {
                    $errorlog .= "ERROR: $error ";
                }
            }

            if (!empty($trademark->classes)) {
                $this->saveTMClasses(unserialize($trademark->classes), $trademark->id);
            }

            $log = date("Y-m-d H:i:s") . " $id $errorlog\n";
            file_put_contents(Yii::$app->basePath . "/runtime/logs/trademarks_update.log", $log, FILE_APPEND);

            return $this->render('trademark', [
                'result' => $trademark,
                'timeout' => $timeout,
//                'nextID' => ($slash ? $id - 1 : $id),
                'nextID' => $id - 1,
//                'slash' => $slash ^ 1,
                'url' => $url,
                'proxy' => $proxy,
                //'end' => true
            ]);
        } else {
            $errorlog = 'Ошибка чтения документа';
            $log = date("Y-m-d H:i:s") . " $id $errorlog\n";
            file_put_contents(Yii::$app->basePath . "/runtime/logs/trademarks_update.log", $log, FILE_APPEND );

            return $this->render('request', [
                'result' => $errorlog . ' ' . $id,
                'nextID' => $id,
                'proxy' => $proxy,
                'timeout' => $timeout,
//                'end' => true
            ]);
        }
    }

    public function actionTMUpdateSlash($id = 631765) {

        $proxy = empty(Yii::$app->request->get('proxy')) ? static::$proxy : Yii::$app->request->get('proxy');
        $slash = empty(Yii::$app->request->get('slash')) ? 0 : Yii::$app->request->get('slash');
        $error = empty(Yii::$app->request->get('error')) ? 0 : Yii::$app->request->get('error');
        $timeout = empty(Yii::$app->request->get('timeout')) ? static::$timeout : Yii::$app->request->get('timeout');

        $slashExist = false;

        $url = Parser::$trademarkUrl . ($slash ? ($id . '/' . $slash) : $id);
        if (empty(Yii::$app->request->get('proxy'))) {
            $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
            foreach ($proxyList as $val) {
                $output = $this->parse($url, trim($val));
                if (!empty($output)) {
                    if (strstr($output, 'Слишком быстрый просмотр документов.') ||
                        strstr($output, 'Вы заблокированы до') ||
                        strstr($output, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
                        continue;
                    }
                    $proxy = trim($val);
                    break;
                }
            }
        } else {
            $output = $this->parse($url, Yii::$app->request->get('proxy'));
            $proxy = Yii::$app->request->get('proxy');
        }

        if ($slash) {
            $trademark = Trademarks::find()
                ->where(['reg_number' => ($id . '/' . $slash)])
                ->one();
        } else {
            $trademark = Trademarks::find()
                ->where(['reg_number' => $id])
                ->one();
        }

        if (empty($trademark)) {
            $trademark = new Trademarks();
        } elseif (!empty($trademark->reg_date)) {
            $date = new DateTime($trademark->reg_date);
            $interval = $date->diff(new DateTime('now'));

            if (!$slash) {
                for ($i = 1; $i <= 5; $i++) {
                    if (strpos($trademark->rightholder, $trademark->reg_number . '/' . $i) !== false) {
                        $slashExist = true;
                        $slash = 1;
                        break;
                    }
                }
            }

            if ($interval->m > 2 || (!empty($trademark->img) && !empty($trademark->rightholder) && !empty($trademark->classes))) {

                return $this->render('trademark', [
                    'result' => $slash ? "{$trademark->reg_date} {$interval->m} {$id}/" . $slash : "{$trademark->reg_date} {$interval->m} {$id}",
                    'timeout' => $timeout,
                    'proxy' => $proxy,
                    'nextID' => (($slash && $slash < self::MAX_SLASH_NUMBER) ? $id : $id + 1),
                    'slash' => (!$slashExist && $slash) ? $slash < self::MAX_SLASH_NUMBER ? $slash + 1 : 0 : $slash,
                    'error' => 0
                ]);
            }
        }
        
        if (!empty($output)) {
            if (strstr($output, 'Слишком быстрый просмотр документов.')) {
                return $this->render('trademark', [
                    'result' => "Слишком быстрый просмотр документов. {$id}",
                    'end' => true
                ]);
            }
            if (strstr($output, 'Вы заблокированы до')) {
                return $this->render('trademark', [
                    'result' => "Забанили ip-адрес {$id}",
                    'end' => true
                ]);
            }
            if (strstr($output, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
                return $this->render('trademark', [
                    'result' => "Забанили ip-адрес до конца дня {$id}",
                    'end' => true
                ]);
            }
            if (strstr($output, 'Документ с данным номером отсутствует')) {
                return $this->render('trademark', [
                    'result' => "Документ с данным номером отсутствует {$id}/" . $slash,
                    'timeout' => $timeout,
                    'proxy' => $proxy,
                    'nextID' => (($slash && $slash < self::MAX_SLASH_NUMBER) ? $id : $id + 1),
                    'slash' => ($slash < self::MAX_SLASH_NUMBER ? $slash + 1 : 0),
                    'error' => $error + 1,
                    'end' => ($error >= 5) ? true : false
                ]);
            }

            $reg_number = $this->extractNumber($output, 'Номер государственной регистрации:');
            if ($reg_number == null) {
                $reg_number = $this->extractNumber($output, 'Номер регистрации:');
            }
            if ($reg_number == null) {
                return $this->render('trademark', [
                    'result' => $output,
                    'end' => true
                ]);
            }

            $rightholder = $this->extractLastString($output, 'Правообладатель:');
            if ($rightholder == null) {
                $rightholder = $this->extractLastString($output, 'Имя правообладателя:');
            }

            $trademark->reg_number = $reg_number;
            $trademark->request_number = $this->extractNumber($output, 'Номер заявки:');
            $trademark->reg_date = $this->extractDate($output, 'Дата государственной регистрации:');
            if ($trademark->reg_date == null) {
                $trademark->reg_date = $this->extractDate($output, 'Дата регистрации:');
            }
            $trademark->request_date = $this->extractDate($output, 'Дата подачи заявки:');
            $trademark->publish_date = $this->extractDate($output, 'Дата публикации:');
            $trademark->reg_expiry_date = $this->extractDate($output, 'Дата истечения срока действия регистрации:');
            if ($trademark->reg_expiry_date == null) {
                $trademark->reg_expiry_date = $this->extractDate($output, 'Дата истечения срока действия исключительного права:');
            }
            if (($temp = $this->extractLastDate($output, 'Дата, до которой продлен срок действия регистрации:')) != null) {
                $trademark->reg_expiry_date = $temp;
            }
            if (($temp = $this->extractLastDate($output, 'Дата, до которой продлен срок действия исключительного права:')) != null) {
                $trademark->reg_expiry_date = $temp;
            }
            $trademark->publication_pdf = $this->extractPublication($output, 'Дата публикации:');
            $trademark->legal_protection_end_date = $this->extractDate($output, 'Дата прекращения правовой охраны товарного знака:');
            if ($trademark->legal_protection_end_date == null) {
                $trademark->legal_protection_end_date = $this->extractDate($output, 'Дата прекращения правовой охраны:');
            }
            $trademark->priority = $this->extractDate($output, 'Приоритет:');
            if ($trademark->priority == null) {
                $trademark->priority = $this->extractDate($output, 'Дата приоритета:');
            }
            $trademark->rightholder = $rightholder;
            $trademark->address = $this->extractLastString($output, 'Адрес для переписки:');
            $trademark->first_request_number = $this->extractString($output, 'Номер первой заявки:');
            $trademark->first_request_date = $this->extractDate($output, 'Дата подачи первой заявки:');
            $trademark->first_request_country = $this->extractString($output, 'Код страны или международной организации, куда была подана первая заявка: ');
            $trademark->licensee = $this->extractString($output, 'Наименование лицензиата:');
            $this->extractContract($trademark, $output, 'Дата и номер регистрации договора:');
            $trademark->license_terms = $this->extractString($output, 'Указание условий и/или ограничений лицензии:');
            $trademark->colors = $this->extractString($output, 'Указание цвета или цветового сочетания:');
            $trademark->classes = $this->extractTMClasses($output, 'Классы МКТУ и перечень товаров и/или услуг:');
            $trademark->img = $this->extractTMImg($id, $output, 'Изображение (воспроизведение) товарного знака, знака обслуживания', true);
            if ($trademark->img == null) {
                $trademark->img = $this->extractTMImg($id, $output, 'Изображение товарного знака, знака обслуживания', true);
                if ($trademark->img == null) {
                    $trademark->img = $this->extractTMImg($id, $output, '(540)', true);
                }
            }
            $status = explode('(', $this->extractStatusString($output, 'Статус:'));
            $trademark->status_id = $this->getStatusID($status[0]);
            if (!empty($status[1])) {
                $trademark->status_date = $this->extractStatusDate($status[1]);
            }

            $errorlog = 'OK';
            if (!$trademark->save(false)) {
                $errorlog = '';
                foreach ($trademark->getErrors() as $error) {
                    $errorlog .= "ERROR: $error ";
                }
            }

            if (!empty($trademark->classes)) {
                $this->saveTMClasses(unserialize($trademark->classes), $trademark->id);
            }

            $log = date("Y-m-d H:i:s") . " $id $errorlog\n";
            file_put_contents(Yii::$app->basePath . "/runtime/logs/trademarks_update.log", $log, FILE_APPEND);

            if (!$slash) {
                for ($i = 1; $i <= 5; $i++) {
                    if (strpos($rightholder, $reg_number . '/' . $i) !== false) {

                        return $this->render('trademark', [
                            'result' => $trademark,
                            'timeout' => $timeout,
                            'nextID' => $id,
                            'slash' => 1,
                            'url' => $url,
                            'proxy' => $proxy,
                            'error' => 0
                        ]);
                    }
                }
            }

            return $this->render('trademark', [
                'result' => $trademark,
                'timeout' => $timeout,
                'nextID' => (($slash && $slash < self::MAX_SLASH_NUMBER) ? $id : $id + 1),
                'slash' => ($slash < self::MAX_SLASH_NUMBER ? $slash + 1 : 0),
                'url' => $url,
                'proxy' => $proxy,
                'error' => 0
            ]);
        } else {
            $errorlog = 'Ошибка чтения документа';
            $log = date("Y-m-d H:i:s") . " $id $errorlog\n";
            file_put_contents(Yii::$app->basePath . "/runtime/logs/trademarks_update.log", $log, FILE_APPEND );

            return $this->render('trademark', [
                'result' => $errorlog . ' ' . $id,
                'nextID' => $id,
                'slash' => $slash,
                'proxy' => $proxy,
                'timeout' => $timeout,
                'error' => $error + 1,
                'end' => ($error >= 5) ? true : false
            ]);
        }
    }

    public function actionRequestUpdate($id = 2017728638) {
        $error = empty(Yii::$app->request->get('error')) ? 0 : Yii::$app->request->get('error');
        $timeout = empty(Yii::$app->request->get('timeout')) ? static::$timeout : Yii::$app->request->get('timeout');

        $url = Parser::REQUEST_URL . $id;
        if (empty(Yii::$app->request->get('proxy'))) {
            $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
            foreach ($proxyList as $val) {
                $output = $this->parse($url, trim($val));
                if (!empty($output)) {
                    if (strstr($output, 'Слишком быстрый просмотр документов.') ||
                        strstr($output, 'Вы заблокированы до') ||
                        strstr($output, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
                        continue;
                    }
                    $proxy = trim($val);
                    break;
                }
            }
        } else {
            $output = $this->parse($url, Yii::$app->request->get('proxy'));
            $proxy = Yii::$app->request->get('proxy');
        }

        $request = Requests::find()->where(['request_number' => $id])->one();

        if (empty($request)) {
            $request = new Requests();
        } else {
            $date = new DateTime($request->reg_date);
            $interval = $date->diff(new DateTime('now'));

            if ($interval->m > 2 || (!empty($request->img) && !empty($request->rightholder) && !empty($request->classes))) {
                return $this->render('request', [
                    'result' => $request->reg_date . ' ' . $interval->m . ' ' . $id,
                    'timeout' => $timeout,
                    'proxy' => $proxy,
                    'nextID' => $id + 1,
                    'error' => 0
                ]);
            }
        }

        if (!empty($output)) {
            if (strstr($output, 'Слишком быстрый просмотр документов.')) {
                return $this->render('request', [
                    'result' => 'Слишком быстрый просмотр документов. ' . $id,
                    'end' => true
                ]);
            }
            if (strstr($output, 'Вы заблокированы до')) {
                return $this->render('request', [
                    'result' => 'Забанили ip-адрес ' . $id,
                    'end' => true
                ]);
            }
            if (strstr($output, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
                return $this->render('request', [
                    'result' => 'Забанили ip-адрес до конца дня ' . $id,
                    'end' => true
                ]);
            }
            if (strstr($output, 'Документ с данным номером отсутствует')) {
                return $this->render('request', [
                    'result' => 'Документ с данным номером отсутствует ' . $id,
                    'timeout' => $timeout,
                    'proxy' => $proxy,
                    'nextID' => $id + 1,
                    'error' => $error + 1,
                    'end' => ($error >= 5) ? true : false
                ]);
            }

            $reg_number = $this->extractNumber($output, 'Номер государственной регистрации:');
            if (is_null($reg_number)) {
                $reg_number = $this->extractNumber($output, 'Номер регистрации:');
            }

            $reg_date = $this->extractDate($output, 'Дата государственной регистрации:');
            if (is_null($reg_date)) {
                $reg_date = $this->extractDate($output, 'Дата регистрации:');
            }

            $request->reg_number = $reg_number;
            $requestNumber = $this->extractNumber($output, 'Номер заявки:');
            $request->request_number = $requestNumber ? $requestNumber : $id;
            $request->applicant = $this->extractString($output, 'Заявитель:');
            $request->facsimile = $this->extractFacsimile($output, 'Факсимильные изображения');
            $request->reg_date = $reg_date;
            $request->request_date = $this->extractDate($output, 'Дата поступления заявки:');
            $request->address = $this->extractLastString($output, 'Адрес для переписки:');
            $request->first_request_number = $this->extractString($output, 'Номер первой заявки:');
            $request->first_request_date = $this->extractDate($output, 'Дата подачи первой заявки:');
            $request->first_request_country = $this->extractString($output, 'Код страны подачи первой заявки:');
            $request->classes = $this->extractClasses($output, 'Классы МКТУ и перечень товаров и/или услуг:');
            $request->img = $this->extractImgSafe($output, 'Изображение заявляемого обозначения');
            $statusString = explode(':', $this->extractStatusString($output, 'состояние делопроизводства'));
            $status = isset($statusString[1]) ? $this->getStatusID($statusString[1]) : 1;
            $request->status_id = $status;

            $errorlog = 'OK';
            if (!$request->save(false)) {
                $errorlog = '';
                foreach ($request->getErrors() as $error) {
                    $errorlog .= "ERROR: $error ";
                }
            }


            if (!empty($request->classes)) {
                $this->saveRequestClasses(unserialize($request->classes), $request->id);
            }

            $log = date("Y-m-d H:i:s") . " $id $errorlog\n";
            file_put_contents(Yii::$app->basePath . "/runtime/logs/request_update.log", $log, FILE_APPEND );

            return $this->render('request', [
                'result' => $request,
                'timeout' => $timeout,
                'nextID' => $id + 1,
                'url' => $url,
                'proxy' => $proxy,
                'error' => 0
            ]);

        } else {
            $errorlog = 'Ошибка чтения документа';
            $log = date("Y-m-d H:i:s") . " $id $errorlog\n";
            file_put_contents(Yii::$app->basePath . "/runtime/logs/request_update.log", $log, FILE_APPEND );

            return $this->render('request', [
                'result' => $errorlog . ' ' . $id,
                'nextID' => $id,
                'proxy' => $proxy,
                'timeout' => $timeout,
                'error' => $error + 1,
                'end' => ($error >= 5) ? true : false
            ]);
        }
    }

    /**
     * Дописываем значения в табличку класс-знак
     * @return string
     */

    public function actionTMClasses()
    {
        $id = empty(Yii::$app->request->get('id')) ? 0 : Yii::$app->request->get('id');

        $trademarks = Trademarks::find()
            ->select('id, classes')
            ->where(['>', 'id', $id])
            ->andWhere(new Expression('classes IS NOT NULL'))
            ->limit(2000)
            ->orderBy(['id' => SORT_ASC])
            ->all();

        if (empty($trademarks)) {
            echo 'Done.';exit;
        }

        foreach ($trademarks as $trademark) {
            $id = $trademark->id;
            if (empty($trademark->classes)) {
                continue;
            }

            $this->saveTMClasses(unserialize($trademark->classes), $id);
        }

        echo "Последний id: $id";
        echo "<script>
            setTimeout ( function(){
                //window.location.href = window.location.origin + window.location.pathname + '?id=$id';
            }, 1);
        </script>";
    }

    public function actionClasses()
    {
        $id = empty(Yii::$app->request->get('id')) ? 0 : Yii::$app->request->get('id');

        $requests = Requests::find()
            ->select('id, classes')
            ->where(['>', 'id', $id])
            ->andWhere(new Expression('classes IS NOT NULL'))
            ->limit(2000)
            ->orderBy(['id' => SORT_ASC])
            ->all();

        if (empty($requests)) {
            echo 'Done.';exit;
        }

        foreach ($requests as $request) {
            $id = $request->id;
            if (empty($request->classes)) {
                continue;
            }

            $classes = unserialize($request->classes);
            foreach ($classes as $class => $text) {
                $cids = [];
                if (is_string($class)) {
                    if (stripos(',', $class)) {
                        $cids = explode(',', $class);
                    }

                } else {
                    $cids[] = $class;
                }
                foreach ($cids as $cid) {
                    $check = RequestClasses::find()
                        ->where(['request_id' => $request->id])
                        ->andWhere(['class' => intval($cid)])
                        ->limit(1)
                        ->one();

                    if (empty($check)) {
                        $class = new RequestClasses();
                        $class->request_id = $request->id;
                        $class->class = intval($cid);
                        $class->save(false);
                    }
                }
            }
        }

        echo "Последний id: $id";
        echo "<script>
            setTimeout ( function(){
                window.location.href = window.location.origin + window.location.pathname + '?id=$id';
            }, 1);
        </script>";
    }

    public function actionRequestCaptcha () {
        if (Yii::$app->request->isAjax) {
//            $captcha = Yii::$app->decaptcha;
//            var_dump($captcha);
//            $res = $captcha->rucaptcha->recognize('https://monocle.report/resources/images/2016/7/0/1/1/91.jpg');
//            var_dump($res);die;

            $log = null;
            $captchaIDs = [];
            $captchaID = null;

            $requests = Requests::find()
                ->select('id, img')
                ->andWhere(['not', ['img' => null]])
                ->andWhere(['captcha_id' => null])
                ->orderBy(['id' => SORT_ASC])
                ->limit(2)
                ->all();

            if (empty($requests)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'next' => false,
                ];
            }
            foreach ($requests as $request) {
                $log = (date("Y-m-d H:i:s") . " $request->id");
                if (!empty($request->thumb)) {
                    $captchaID = Yii::$app->captcha->run($request->thumb);

                    if (!empty($captchaID)) {
                        $captchaIDs[$request->id] = $captchaID;
                    } elseif (!empty(Yii::$app->captcha->error())) {
                        $log .= (' Error: ' . Yii::$app->captcha->error());
                        $log .= PHP_EOL;
                        file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tag.log',$log, FILE_APPEND );

                        if (Yii::$app->captcha->error() == 'Нулевой либо отрицательный баланс') {
                            $log .= (' Error: ' . Yii::$app->captcha->error());
                            $log .= PHP_EOL;
                            file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tag.log',$log, FILE_APPEND );
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return [
                                'next' => false,
                            ];
                        }
                    }
                }
            }

            if ($this->getAnswers($captchaIDs)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'next' => true,
                ];
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'next' => false,
                ];
            }

//        echo "<script>
//            setTimeout ( function(){
//                window.location.href = window.location.origin + window.location.pathname + '?id=$lastID';
//            }, 3000);
//        </script>";

        }
    }

    public function actionTag () {
        $id = empty(Yii::$app->request->get('id')) ? -1 : Yii::$app->request->get('id');
        $lastID = null;
        $log = null;
        $captchaIDs = [];
        $captchaID = null;

        $requests = Requests::find()
            ->select('id, img')
            ->where(['>', 'id', $id])
            ->andWhere(new Expression('img IS NOT NULL'))
            ->orderBy(['id' => SORT_ASC])
            ->limit(1000)
            ->all();

        if (empty($requests)) {
            echo "Done! {$id}";die;
        }
        foreach ($requests as $request) {
            $log = (date("Y-m-d H:i:s") . " $request->id");
            if (!empty($request->thumb)) {
                $captchaID = Yii::$app->captcha->run($request->thumb);

                if (!empty($captchaID)) {
                    $captchaIDs[$request->id] = $captchaID;
                } elseif (!empty(Yii::$app->captcha->error())) {
                    $log .= (' Error: ' . Yii::$app->captcha->error());
                    $log .= PHP_EOL;
                    file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tag.log',$log, FILE_APPEND );

                    if (Yii::$app->captcha->error() == 'Нулевой либо отрицательный баланс') {
                        echo 'Нулевой либо отрицательный баланс<br>';
                        break;
                    }
                }
            }
            $lastID = $request->id;
        }

        $this->getAnswers($captchaIDs);

        echo "Последний id: $lastID";
        echo "<script>
            setTimeout ( function(){
                window.location.href = window.location.origin + window.location.pathname + '?id=$lastID';
            }, 3000);
        </script>";
    }

    public function actionTagTM () {
        $id = empty(Yii::$app->request->get('id')) ? -1 : Yii::$app->request->get('id');
        $lastID = null;
        $log = null;
        $captchaIDs = [];
        $captchaID = null;

        $trademarks = Trademarks::find()
            ->select('id, img')
            ->where(['>', 'id', $id])
            ->andWhere(new Expression('img IS NOT NULL'))
            ->orderBy(['id' => SORT_ASC])
            ->limit(1000)
            ->all();

        if (empty($trademarks)) {
            echo "Done! {$id}";die;
        }
        foreach ($trademarks as $trademark) {
            $log = (date("Y-m-d H:i:s") . " $trademark->id");
            $lastID = $trademark->id;
            if (!empty($trademark->thumb)) {
                $captchaID = Yii::$app->captcha->run($trademark->thumb);

                if (!empty($captchaID)) {
                    $captchaIDs[$trademark->id] = $captchaID;
                } elseif (!empty(Yii::$app->captcha->error())) {
                    $log .= (' Error: ' . Yii::$app->captcha->error());
                    $log .= PHP_EOL;
                    file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tm_tag.log',$log, FILE_APPEND );

                    if (Yii::$app->captcha->error() == 'Нулевой либо отрицательный баланс') {
                        echo "Нулевой либо отрицательный баланс<br>Последний id: $lastID<br>";
                        ob_flush();
                        flush();

                        $this->getAnswersTM($captchaIDs);

                        exit;
                    }
                }
            }
        }

        $this->getAnswersTM($captchaIDs);

        echo "Последний id: $lastID";
        echo "<script>
            setTimeout ( function(){
                window.location.href = window.location.origin + window.location.pathname + '?id=$lastID';
            }, 3000);
        </script>";
    }

    public function actionTagRequest ($id = -1) {
        $lastID = null;
        $log = null;
        $captchaIDs = [];
        $captchaID = null;

        $request = Requests::find()
            ->select('id, img')
            ->where(['>', 'id', $id])
            ->andWhere(new Expression('img IS NOT NULL'))
            ->orderBy(['id' => SORT_ASC])
//            ->limit(1000)
            ->one();

        if (empty($request)) {
            echo 'Done! ' . $id; die;
        }
//            foreach ($requests as $request) {
        $log = date('Y-m-d H:i:s') . ' ' . $request->id;
        $lastID = $request->id;
        if (!empty($request->img)) {
            $captchaID = Yii::$app->captcha->run($request->img);

            if (!empty($captchaID)) {
                $captchaIDs[$request->id] = $captchaID;
            } elseif (!empty(Yii::$app->captcha->error())) {
                $log .= (' Error: ' . Yii::$app->captcha->error());
                $log .= PHP_EOL;
                file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_request_tag.log', $log, FILE_APPEND );

                if (Yii::$app->captcha->error() == 'Нулевой либо отрицательный баланс') {
                    echo "Нулевой либо отрицательный баланс<br>Последний id: $lastID<br>";
                    ob_flush();
                    flush();

                    $this->getAnswersRequest($captchaIDs);

                    exit;
                }
            }
        }
//            }

        $this->getAnswersRequest($captchaIDs);

        return $this->render('tag_request', [
            'lastID' => $lastID
        ]);
    }

    public function actionBadTag() {
        set_time_limit(20*60);
        $requestIDs = [];
        $captchaIDs = [];

        $handle = @fopen(Yii::$app->basePath . "/runtime/bad_tag_ids.txt", "r");
        if ($handle) {
            if (($buffer = fgets($handle)) !== false) {
                $requestIDs = explode(';', $buffer);
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
                ob_flush();
                flush();
            }
            fclose($handle);
        } else {
            return $this->render('badtag', [
                'result' => "Error: cannot open bad_tag_ids.txt",
                'end' => true
            ]);
        }

        $requestIDs = array_combine($requestIDs, $requestIDs);
        $requests = Requests::find()
            ->select('id, img')
            ->where(['in', 'id', $requestIDs])
            ->orderBy(['id' => SORT_ASC])
            ->limit(1000)
            ->all();

        foreach ($requests as $request) {
            $log = (date("Y-m-d H:i:s") . " $request->id");
            if (!empty($request->thumb)) {
                $captchaID = Yii::$app->captcha->run($request->thumb);

                if (!empty($captchaID)) {
                    $captchaIDs[$request->id] = $captchaID;
                } elseif (!empty(Yii::$app->captcha->error())) {
                    $log .= (' Error: ' . Yii::$app->captcha->error());
                    $log .= PHP_EOL;
                    file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tag.log',$log, FILE_APPEND );
                }
            }
            unset($requestIDs[$request->id]);
        }

        $this->getAnswers($captchaIDs);

        if (count($requestIDs)) {
            $result = implode(';', $requestIDs);
            file_put_contents(Yii::$app->basePath . '/runtime/bad_tag_ids.txt', $result);
            return $this->render('badtag', [
                'result' => ("Осталось: " . count($requestIDs))
            ]);
        } else {
            unlink(Yii::$app->basePath . '/runtime/bad_tag_ids.txt');
            return $this->render('badtag', [
                'result' => "All done.",
                'end' => true
            ]);
        }
    }

    public function actionWrongTags($id, $model) {
        if (empty($id) || empty($model)) {
            return false;
        }
        set_time_limit(200);

        switch ($model) {
            case 'requests':
                $tagModel = 'request_terms';
                break;
            case 'trademarks':
                $tagModel = 'trademark_terms';
                break;
            case 'tmForeign':
                $tagModel = 'tm_foreign_terms';
                break;
            default:
                return false;
        }

        $$model = call_user_func(["app\\models\\".ucfirst($model), 'find'])
            ->where(['id' => $id])
            ->one();

        if (!empty($$model)) {
            if (!empty($$model->img)) {
                $captchaID[$id] = Yii::$app->captcha->run($$model->img);
                switch ($model) {
                    case 'requests':
                        $this->getAnswers($captchaID);
                        break;
                    case 'trademarks':
                        $this->getAnswersTM($captchaID);
                        break;
                    case 'tmForeign':
                        $this->getAnswersTmForeign($captchaID);
                        break;
                }
            }
            return true;
        }

        return false;
    }

    public function actionBadTagPrepare($file)
    {
        $requestIDs = [];
        if (empty($file)) {
            die;
        }

        $handle = @fopen(Yii::$app->basePath . "/runtime/logs/$file", "r");
        if ($handle) {
            while (($buffer = fgets($handle)) !== false) {
                if (stripos($buffer, 'Error') !== false) {
                    preg_match("/^[0-9]+/", substr($buffer, 20), $match);
                    if (!empty($match[0])) {
                        $requestIDs[$match[0]] = $match[0];
                    }
                }
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
                ob_flush();
                flush();
            }
            fclose($handle);
        }

        $result = implode(';', $requestIDs);
        file_put_contents(Yii::$app->basePath . '/runtime/bad_tag_ids.log', $result);
    }


    private function getAnswers($captchaIDs) {
        $timestamp = time();
        while (!empty($captchaIDs)) {
            $captchaIDs =
                array_filter($captchaIDs, function($v, $k) use($timestamp) {
                    if (empty($v) || empty($k)) {
                        return false;
                    }
                    $request = Requests::findOne(['id' => $k]);

                    if (!empty($request)) {
                        $request->captcha_id = $v;
                        if ($request->update() !== false) {
                            $log = (date("Y-m-d H:i:s") . " $k, captchaID: $v");

                            $captcha = Yii::$app->captcha->getAnswer($v);
                            if (!empty($captcha)) {
                                $terms = explode(' ', $captcha);
                                if (!empty($terms)) {
                                    foreach ($terms as $term) {
                                        $term = mb_strtolower($term);
                                        $termModel = Terms::find()
                                            ->where(['term' => $term])
                                            ->one();

                                        if (empty($termModel)) {
                                            $termModel = new Terms();
                                            $termModel->term = $term;
                                            try {
                                                $termModel->save(false);
                                            } catch (Exception $e) {
                                                file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tag.log', $log . " Term: $term Error: {$e->getMessage()}" . PHP_EOL, FILE_APPEND);
                                                continue;
                                            }
                                        }

                                        if (empty(RequestTerms::find()->where(['request_id' => $k, 'term_id' => $termModel->id])->one())) {
                                            $requestTerm = new RequestTerms();
                                            $requestTerm->request_id = $k;
                                            $requestTerm->term_id = $termModel->id;
                                            $requestTerm->save(false);
                                        }
                                    }
                                }
                                $log .= " Response: $captcha";
                            } elseif (empty(Yii::$app->captcha->error())) {
                                if ((time() - $timestamp) < static::$maxTimeout) {
                                    return true;
                                } else {
                                    $log .= (" Error: CAPCHA_NOT_READY");
                                }
                            } else {
                                $log .= (' Error: ' . Yii::$app->captcha->error());
                            }

                            $log .= PHP_EOL;
                            file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tag.log',$log, FILE_APPEND);

                            if (strpos(Yii::$app->captcha->error(), 'CURL вернул ошибку')) {
                                return true;
                            }
                        }
                    }

                }, ARRAY_FILTER_USE_BOTH);
            sleep(5);
        }

        return true;
    }

    private function getAnswersTM($captchaIDs) {
        $timestamp = time();
        while (!empty($captchaIDs)) {
            $captchaIDs =
                array_filter($captchaIDs, function($v, $k) use($timestamp) {
                    if (empty($v) || empty($k)) {
                        return false;
                    }
                    $captcha = Yii::$app->captcha->getAnswer($v);
                    $log = (date("Y-m-d H:i:s") . " $k, captchaID: $v");

                    if (!empty($captcha)) {
                        $terms = explode(' ', $captcha);
                        if (!empty($terms)) {
                            foreach ($terms as $term) {
                                $termModel = Terms::find()
                                    ->where(['term' => $term])
                                    ->one();

                                if (empty($termModel)) {
                                    $termModel = new Terms();
                                    $termModel->term = $term;
                                    try {
                                        $termModel->save(false);
                                    } catch (Exception $e) {
                                        file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tm_tag.log', $log . " Term: $term Error: {$e->getMessage()}" . PHP_EOL, FILE_APPEND);
                                        continue;
                                    }
                                }

                                if (empty(TrademarkTerms::find()->where(['trademark_id' => $k, 'term_id' => $termModel->id])->one())) {
                                    $trademarkTerm = new TrademarkTerms();
                                    $trademarkTerm->trademark_id = $k;
                                    $trademarkTerm->term_id = $termModel->id;
                                    $trademarkTerm->save(false);
                                }
                            }
                        }
                        $log .= " Response: $captcha";
                    } elseif (empty(Yii::$app->captcha->error())) {
                        if ((time() - $timestamp) < static::$maxTimeout) {
                            return true;
                        } else {
                            $log .= (" Error: CAPCHA_NOT_READY");
                        }
                    } else {
                        $log .= (' Error: ' . Yii::$app->captcha->error());
                    }

                    $log .= PHP_EOL;
                    file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tm_tag.log',$log, FILE_APPEND);

                    if (strpos(Yii::$app->captcha->error(), 'CURL вернул ошибку')) {
                        return true;
                    }

                    return false;
                }, ARRAY_FILTER_USE_BOTH);
            sleep(5);
        }

        return true;
    }

    private function getAnswersTmForeign($captchaIDs) {
        $timestamp = time();
        while (!empty($captchaIDs)) {
            $captchaIDs =
                array_filter($captchaIDs, function($v, $k) use($timestamp) {
                    if (empty($v) || empty($k)) {
                        return false;
                    }
                    $captcha = Yii::$app->captcha->getAnswer($v);
                    $log = (date("Y-m-d H:i:s") . " $k, captchaID: $v");

                    if (!empty($captcha)) {
                        $terms = explode(' ', $captcha);
                        if (!empty($terms)) {
                            foreach ($terms as $term) {
                                $term = str_replace(['«','»','"','/', ',,'],'', $term);
                                $termModel = Terms::find()
                                    ->where(['term' => $term])
                                    ->one();

                                if (empty($termModel)) {
                                    $termModel = new Terms();
                                    $termModel->term = $term;
                                    try {
                                        $termModel->save(false);
                                    } catch (Exception $e) {
                                        file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tm_foreign_tag.log', $log . " Term: $term Error: {$e->getMessage()}" . PHP_EOL, FILE_APPEND);
                                        continue;
                                    }
                                }

                                if (empty(TmForeignTerm::find()->where(['tm_id' => $k, 'term_id' => $termModel->id])->one())) {
                                    $trademarkTerm = new TmForeignTerm();
                                    $trademarkTerm->tm_id = $k;
                                    $trademarkTerm->term_id = $termModel->id;
                                    $trademarkTerm->save(false);
                                }
                            }
                        }
                        $log .= " Response: $captcha";
                    } elseif (empty(Yii::$app->captcha->error())) {
                        if ((time() - $timestamp) < static::$maxTimeout) {
                            return true;
                        } else {
                            $log .= (" Error: CAPCHA_NOT_READY");
                        }
                    } else {
                        $log .= (' Error: ' . Yii::$app->captcha->error());
                    }

                    $log .= PHP_EOL;
                    file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_tm_foreign_tag.log',$log, FILE_APPEND);

                    if (strpos(Yii::$app->captcha->error(), 'CURL вернул ошибку')) {
                        return true;
                    }

                    return false;
                }, ARRAY_FILTER_USE_BOTH);
            sleep(5);
        }

        return true;
    }

    private function getAnswersRequest($captchaIDs) {
        $timestamp = time();

        while (!empty($captchaIDs)) {
            $captchaIDs =
                array_filter($captchaIDs, function($v, $k) use($timestamp) {
                    if (empty($v) || empty($k)) {
                        return false;
                    }
                    $captcha = Yii::$app->captcha->getAnswer($v);
                    $log = (date("Y-m-d H:i:s") . " $k, captchaID: $v");

                    if (!empty($captcha)) {
                        $terms = explode(' ', $captcha);
                        if (!empty($terms)) {
                            foreach ($terms as $term) {
                                $termModel = Terms::find()
                                    ->where(['term' => $term])
                                    ->one();

                                if (empty($termModel)) {
                                    $termModel = new Terms();
                                    $termModel->term = $term;
                                    try {
                                        $termModel->save(false);
                                    } catch (Exception $e) {
                                        file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_request_tag.log', $log . " Term: $term Error: {$e->getMessage()}" . PHP_EOL, FILE_APPEND);
                                        continue;
                                    }
                                }

                                if (empty(RequestTerms::find()->where(['request_id' => $k, 'term_id' => $termModel->id])->one())) {
                                    $requestTerm = new RequestTerms();
                                    $requestTerm->request_id = $k;
                                    $requestTerm->term_id = $termModel->id;
                                    $requestTerm->save(false);
                                }
                            }
                        }
                        $log .= " Response: $captcha";
                    } elseif (empty(Yii::$app->captcha->error())) {
                        if ((time() - $timestamp) < static::$maxTimeout) {
                            return true;
                        } else {
                            $log .= (" Error: CAPCHA_NOT_READY");
                        }
                    } else {
                        $log .= (' Error: ' . Yii::$app->captcha->error());
                    }

                    $log .= PHP_EOL;
                    file_put_contents(Yii::$app->basePath . '/runtime/logs/parser_request_tag.log', $log, FILE_APPEND);

                    if (strpos(Yii::$app->captcha->error(), 'CURL вернул ошибку')) {
                        return true;
                    }

                    return false;
                }, ARRAY_FILTER_USE_BOTH);
            sleep(5);
        }

        return true;
    }

    private function parse($url, $proxy) {
        $ua = static::$ua_all[mt_rand(0,14)];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $ua);

        curl_setopt ($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt ($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt ($ch, CURLOPT_PROXYUSERPWD, 'test:smxRDa3i');
        curl_setopt ($ch, CURLOPT_PROXYTYPE, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt ($ch, CURLOPT_FAILONERROR, true);

        $res = curl_exec($ch);
        //var_dump(curl_error($ch)); die;
        curl_close($ch);

        return (iconv('cp1251', 'utf-8', $res));
    }

    private function extractNumber($s, $needle)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</B>', $from);
            $sstring = substr($s, $from, $to - $from);
            $sstring = strip_tags($sstring);
            $res = preg_replace("/[^0-9\/]/", "", $sstring);
        }
        return $res;
    }

    private function extractContract(&$model, $s, $needle) {
        $from = strripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</B>', $from);
            $sstring = substr($s, $from, $to - $from);
            preg_match("/\d{2}.\d{2}.\d{4}/", $sstring, $match);
            $dates = explode('.',empty($match[0]) ? '' : $match[0]);
            if (sizeof($dates) === 3 && $this->validateDate($match[0], 'd.m.Y')) {
                $model->contract_date = $dates[2] . '-' . $dates[1] . '-' . $dates[0];
                $from = stripos($sstring, $match[0]);
                if ($from) {
                    $sstring = substr($sstring, $from + strlen($match[0]));
                    $sstring = trim(strip_tags($sstring));
                    $model->contract_number = $sstring;
                }
            }
            return true;
        }
        return false;
    }

    private function extractDate($s, $needle)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</B>', $from);
            $sstring = substr($s, $from, $to - $from);
            preg_match("/\d{2}.\d{2}.\d{4}/", $sstring, $match);
            $dates = explode('.',empty($match[0]) ? '' : $match[0]);
            if (sizeof($dates) === 3 && $this->validateDate($match[0], 'd.m.Y')) {
                $res = $dates[2] . '-' . $dates[1] . '-' . $dates[0];
            }
        }
        return $res;
    }

    private function extractLastDate($s, $needle)
    {
        $res = null;
        $from = strripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</B>', $from);
            $sstring = substr($s, $from, $to - $from);
            preg_match("/\d{2}.\d{2}.\d{4}/", $sstring, $match);
            $dates = explode('.',empty($match[0]) ? '' : $match[0]);
            if (sizeof($dates) === 3 && $this->validateDate($match[0], 'd.m.Y')) {
                $res = $dates[2] . '-' . $dates[1] . '-' . $dates[0];
            }
        }
        return $res;
    }

    private function extractStatusDate($s)
    {
        $res = null;
        preg_match("/\d{2}.\d{2}.\d{4}/", $s, $match);
        $dates = explode('.',empty($match[0]) ? '' : $match[0]);
        if (sizeof($dates) === 3 && $this->validateDate($match[0], 'd.m.Y')) {
            $res = $dates[2] . '-' . $dates[1] . '-' . $dates[0];
        }

        return $res;
    }

    private function extractString($s, $needle)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</B>', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            $res = addslashes(trim(strip_tags($sstring)));
        }
        return $res;
    }

    private function extractLastString($s, $needle)
    {
        $res = null;
        $from = strripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</B>', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            $res = addslashes(trim(strip_tags($sstring)));
        }
        return $res;
    }

    private function extractStatusString($s, $needle)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</TD>', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            $res = trim(strip_tags($sstring));
        }
        return $res;
    }

    private function extractStatus($s)
    {
        if (strstr($s, 'Документ не найден')) {
            return 11;
        }
        $res = 1;
        $needle = "делопроизводства";
        $from = stripos($s, $needle);
        $to = stripos($s, '</TD>', $from);
        $sstring = substr($s, $from + strlen($needle), $to - $from);
        $term = (trim(strip_tags($sstring),":- \t\n\r\0\x0B"));
        $result = Statuses::find()
            ->select('id')
            ->where(['title' => $term])
            ->one();
        if (!empty($result)) {
            $res = $result['id'];
        }

        return $res;
    }

    private function getStatusID($s)
    {
        $res = 1;
        $term = addslashes(trim(strip_tags($s),":- \t\n\r\0\x0B"));
        $result = Statuses::find()
            ->select('id')
            ->where(['title' => $term])
            ->one();
        if (!empty($result)) {
            $res = $result['id'];
        } elseif (!empty($term)) {
            $status = new Statuses();
            $status->title = $term;
            if ($status->save(false)) {
                $res = $status->id;
            }
        }

        return $res;
    }

    private function extractImgSafe($output, $needle) {
        $imgPath = $this->extractImg($output, $needle);
        if ($imgPath) {
            global $ua;
            $res = false;
            $pathinfo = pathinfo($imgPath);
            //$nOrder = substr($pathinfo[''], 0, stripos($filename,'.'));
            $nOrder = $pathinfo['filename'];
            $realfilename = substr($pathinfo['filename'], 8);
            $year = substr($nOrder, 0, 4);
            $referer = 'http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTMAP&DocNumber=' . $nOrder . '&TypeFile=html';
            $dirname = 'resources/images/' . $year . '/' . $nOrder[4] . '/' . $nOrder[5] . '/' . $nOrder[6] . '/' . $nOrder[7] . '/';
            if (!is_dir($dirname)) {
                mkdir($dirname, 0775, true);
            }
            $to = $dirname . $realfilename . '.' . strtolower($pathinfo['extension']);
            $ch = curl_init($imgPath);
            $fp = fopen($to, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_USERAGENT, $ua);
            curl_setopt($ch, CURLOPT_REFERER, $referer);

            //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            if (curl_exec($ch)) {
                $res = $to;
            };
            curl_close($ch);
            fclose($fp);
            return $res;
        } else {
            return null;
        }
    }

    private function extractImg($output, $needle) {
        $res = null;
        $from = stripos($output, $needle);
        if ($from) {
            $to = stripos($output, '<IMG', $from);
            $sstring = substr($output, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $sstring, $result);
            $res = $result[2];
        }
        return $res;
    }

    private function extractTMImg($id, $s, $needle, $sub = false)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '<IMG', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $sstring, $result);
            $imgpath = $result[2];
            $res = $this->extractTMImgSafe($id, $imgpath, $sub);
        }
        return $res;
    }

    private function extractTMImgSafe($id, $from, $sub = false)
    {
        global $ua;
        $res = false;
        $pathinfo = pathinfo($from);
        preg_match("/\d{4}.\d{2}.\d{2}/", $from, $match);
        $dates = explode('.',empty($match[0]) ? '' : $match[0]);
        $year = '0000';
        $month = '00';
        $day = '01';
        if (sizeof($dates) === 3) {
            $year = $dates[0];
            $month = $dates[1];
            $day = $dates[2];
        }
        if ($sub) {
            $dirname = "resources/images/trademarks/$year/$month/$day/$id/";
            $to = $dirname . '1.' . strtolower($pathinfo['extension']);
            $id .= '/1';
        } else {
            $dirname = "resources/images/trademarks/$year/$month/$day/";
            $to = $dirname . $id . '.' . strtolower($pathinfo['extension']);
        }
        $referer = 'http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTM&DocNumber=' . $id . '&TypeFile=html';
        if (!is_dir($dirname)) {
            mkdir($dirname, 0775, true);
        }
        $ch = curl_init($from);
        $fp = fopen($to, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $ua);
        curl_setopt($ch, CURLOPT_REFERER, $referer);

        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if (curl_exec($ch)) {
            $res = $to;
        };
        curl_close($ch);
        fclose($fp);
        return $res;
    }

    private function extractFax($s, $needle)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '<IMG', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $sstring, $match);
            $res = addslashes(serialize ($match['href']));
        }
        return $res;
    }

    private function extractPublication($s, $needle)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</B>', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $sstring, $match);
            $res = addslashes(serialize ($match['href']));
        }
        return $res;
    }

    private function extractClasses($s, $needle)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '<TABLE>', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            $sarr = explode('.</B>', trim($sstring));
            $res = [];
            foreach ($sarr as $el) {
                $el = trim(strip_tags($el));
                if (!is_null($el) && !empty($el)) {
                    $key = substr($el, 0, 2);
                    $val = substr($el, 2);
                    $res[$key] = trim($val, "- \t\n\r\0\x0B");
                }
            }
            $res = serialize ($res);
        }
        return ($res);
    }

    private function extractTMClasses($s, $needle)
    {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</P>', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            $sarr = explode('</b>', trim($sstring));
            $res = [];
            foreach ($sarr as $el) {
                $el = trim(strip_tags($el));
                if (!empty($el)) {
                    //$key = substr($el, 0, 2);
                    $to = stripos($el, '-');
                    $keys = explode(',', trim(substr($el, 0, $to)));
                    $val = trim(substr($el, $to), "- \t\n\r\0\x0B");
                    foreach ($keys as $key) {
                        $res[$key] = $val;
                    }
                }
            }
            $res = serialize ($res);
        }
        return ($res);
    }

    private function extractFacsimile ($s, $needle) {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = strripos($s, '</a>', $from) + 4; //4 - the number of characters of the tag </a>
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match_all('/<a\s.*?href="(.+?)".*?>(.+?)<\/a>/i', $sstring, $result);

            $facsimile = !empty($result) ? $result[1] : [];
            $res = serialize($facsimile);
        }
        return $res;
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    private function saveTMClasses($classes, $id) {
        foreach ($classes as $class => $text) {
            $cids = [];
            if (is_string($class)) {
                if (stripos(',', $class)) {
                    $cids = explode(',', $class);
                }

            } else {
                $cids[] = $class;
            }
            foreach ($cids as $cid) {
                $check = TrademarkClasses::find()
                    ->where(['trademark_id' => $id])
                    ->andWhere(['class' => intval($cid)])
                    ->limit(1)
                    ->one();

                if (empty($check)) {
                    $class = new TrademarkClasses();
                    $class->trademark_id = $id;
                    $class->class = intval($cid);
                    $class->save(false);
                }
            }
        }
    }

    private function saveRequestClasses($classes, $id) {
        $cids = [];
        foreach ($classes as $class => $text) {
            $cids[] = number_format($class);

            foreach ($cids as $cid) {
                $check = RequestClasses::find()
                    ->where(['request_id' => $id])
                    ->andWhere(['class' => intval($cid)])
                    ->one();

                if (empty($check)) {
                    $class = new RequestClasses();
                    $class->request_id = $id;
                    $class->class = intval($cid);
                    $class->save(false);
                }
            }
        }
    }

}