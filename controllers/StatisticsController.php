<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\AccessRule;


class StatisticsController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['claimsPermission'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionClasses() {
        $tmClasses = new ActiveDataProvider([
            'query' => (new Query())
                ->select(new Expression('class, COUNT(*) AS count'))
                ->from('trademark_classes')
                ->groupBy('class')
                ->orderBy(['count' => SORT_DESC]),
            'pagination' => false,
        ]);
        $rClasses = new ActiveDataProvider([
            'query' => (new Query())
                ->select(new Expression('class, COUNT(*) AS count'))
                ->from('request_classes')
                ->groupBy('class')
                ->orderBy(['count' => SORT_DESC]),
            'pagination' => false,
        ]);

        return $this->render('classes', [
            'tmClasses' => $tmClasses,
            'rClasses' => $rClasses,
        ]);
    }
    
    public function actionRequests() {
        $dayname = new ActiveDataProvider([
            'query' => (new Query())
                ->select(new Expression('dayname(request_date) as dayname,
                count(dayname(request_date)) as count_dayname'))
                ->from('requests')
                ->where(['not', ['request_date' => null]])
                ->groupBy('dayname(request_date)')
                ->orderBy(['count_dayname' => SORT_DESC]),
            'pagination' => false,
        ]);

        $day = new ActiveDataProvider([
            'query' => (new Query())
                ->select(new Expression('day(request_date) as day,
                count(day(request_date)) as count_day'))
                ->from('requests')
                ->where(['not', ['request_date' => null]])
                ->groupBy('day(request_date)')
                ->orderBy(['count_day' => SORT_DESC]),
            'pagination' => false,
        ]);

        $month = new ActiveDataProvider([
            'query' => (new Query())
                ->select(new Expression('month(request_date) as month,
                count(month(request_date)) as count_month'))
                ->from('requests')
                ->where(['not', ['request_date' => null]])
                ->groupBy('month(request_date)')
                ->orderBy(['count_month' => SORT_DESC]),
            'pagination' => false,
        ]);

        $year = new ActiveDataProvider([
            'query' => (new Query())
                ->select(new Expression('year(request_date) as year,
                count(year(request_date)) as count_year'))
                ->from('requests')
                ->where(['not', ['request_date' => null]])
                ->groupBy('year(request_date)')
                ->orderBy(['count_year' => SORT_DESC]),
            'pagination' => false,
        ]);

        return $this->render('requests', [
            'day' => $day,
            'dayname' => $dayname,
            'month' => $month,
            'year' => $year
        ]);
    }

}