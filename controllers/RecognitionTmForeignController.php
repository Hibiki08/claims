<?php

namespace app\controllers;

use app\models\TmForeign;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\AccessRule;


class RecognitionTmForeignController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['claimsPermission'],
                    ],

                ],
            ],
        ];
    }

    public function actionIndex() {
        $unrecognized = TmForeign::getUnrecognized(1000, true);
        $countUnrecognized = TmForeign::getCountUnrecognized();

        return $this->render('index', [
            'unrecognized' => $unrecognized,
            'countUnrecognized' => $countUnrecognized
        ]);
    }

    public function actionEmpties() {
        $empties = TmForeign::getRecognizedAndEmpty(1000, true);

        return $this->render('empties', [
            'empties' => $empties
        ]);
    }

}