<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\Parser;
use app\models\Requests;
use yii\web\Response;
use app\models\Terms;
use app\models\LogType;
use app\models\Log;
use app\models\RequestTerms;
use \Wkhooy\ObsceneCensorRus;


class ParserRequestController extends Controller {

    const MAX_UPDATE_NUMBER = 200;

    public function actionUpdate($id = 2017705528) {
        $contentCorrect = false;
        $result = null;
        $nextID = $id;
        $end = false;
        $output = null;
        $parser = new Parser();
        $newItem = true;

        $error = empty(Yii::$app->request->get('error')) ? 0 : Yii::$app->request->get('error');
        $timeout = empty(Yii::$app->request->get('timeout')) ? Parser::$timeout : Yii::$app->request->get('timeout');
        $update = empty(Yii::$app->request->get('update')) ? 0 : Yii::$app->request->get('update');
        $infinity = empty(Yii::$app->request->get('infinity')) ? 0 : Yii::$app->request->get('infinity');

        if ($update) {
            if (empty($id)) {
                $nextZnak = Requests::find()
                    ->orderBy(['id' => SORT_DESC])
                    ->limit(1)
                    ->one();
                $id = $nextZnak->request_number;
            }
        } else {
            if ($infinity) {
                if (empty($id)) {
                    $nextZnak = Requests::find()
                        ->orderBy(['id' => SORT_ASC])
                        ->limit(1)
                        ->one();

                    $id = $nextZnak->request_number;

                    $logId = LogType::ZNAKI_ALL_UPDATE_ID;
                    $log = new Log();
                    $log->message = 'Начало цикла';
                    $log->number_from = $nextZnak->request_number;
                    $log->log_type_id = $logId;
                    $log->save();
                }
            }
        }

        $url = Parser::REQUEST_URL . $id;

        if (empty(Yii::$app->request->get('proxy'))) {
            $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
            if (!empty($proxyList)) {
                foreach ($proxyList as $val) {
                    $proxy = trim($val);
                    if (!empty($proxy)) {
                        $output = Parser::extract($url, $proxy);
                        if (($contentCorrect = $parser->contentCorrect($output)) || $parser->getErrorCode() == Parser::ERROR_DOC_NOT_EXISTS) {
                            break;
                        }
                    }
                }
            }
        } else {
            $proxy = Yii::$app->request->get('proxy');
            $output = Parser::extract($url, $proxy);
            $contentCorrect = $parser->contentCorrect($output);
        }

        if ($contentCorrect) {
            $request = Requests::find()->where(['request_number' => $id])->one();
            if (empty($request)) {
                $request = new Requests();
            } else {
                $date = new \DateTime($request->reg_date);
                $interval = $date->diff(new \DateTime('now'));

//                if ($interval->y > 0 || ($interval->y == 0 && $interval->m > 2) || (!empty($request->img) && !empty($request->rightholder) && !empty($request->classes))) {
//                    $newItem = false;
//                    $result = $request->reg_date . ' ' . $interval->m . ' ' . $id;
//                    $nextID = $id + 1;
//                }
            }

            if ($newItem) {
                $request_number = Parser::extractNumber($output, 'Номер государственной регистрации:');
                if (is_null($request_number)) {
                    $request_number = Parser::extractNumber($output, 'Номер регистрации:');
                }
                $reg_date = Parser::extractDate($output, 'Дата государственной регистрации:');
                if (is_null($reg_date)) {
                    $reg_date = Parser::extractDate($output, 'Дата регистрации:');
                }
                $request_date = Parser::extractDate($output, 'Дата поступления заявки:');
                if (is_null($request_date)) {
                    $request_date = $reg_date;
                }
                $requestNumber = Parser::extractNumber($output, 'Номер заявки:');
                $statusString = explode(':', Parser::extractStatusString($output, 'состояние делопроизводства'));
                $status = isset($statusString[1]) ? Parser::getStatusID($statusString[1]) : 1;
                $imgPath = Parser::extractImg($output, 'Изображение заявляемого обозначения');
                $classes = Parser::extractClasses($output, 'Классы МКТУ и перечень товаров и/или услуг:');

                $request->request_number = $request_number;
                $request->request_number = $requestNumber ? $requestNumber : $id;
                $request->applicant = Parser::extractString($output, 'Заявитель:');
                $request->facsimile = Parser::extractFacsimile($output, 'Факсимильные изображения');
                $request->reg_date = $reg_date;
                $request->request_date = $request_date;
                $request->address = Parser::extractLastString($output, 'Адрес для переписки:');
                $request->first_request_number = Parser::extractString($output, 'Номер первой заявки:');
                $request->first_request_date = Parser::extractDate($output, 'Дата подачи первой заявки:');
                $request->first_request_country = Parser::extractString($output, 'Код страны подачи первой заявки:');
                $request->classes = serialize($classes);
                $request->img = Parser::saveRequestImage($imgPath);
                $request->status_id = $status;

                if ($request->save(false)) {
                    $result = $request;
                    $error = 0;
                    $errorlog = 'OK';

                    if ($update) {
                        if ($update <= self::MAX_UPDATE_NUMBER) {
                            $nextZnak = Requests::find()
                                ->where(['<', 'id', $request->id])
                                ->orderBy(['id' => SORT_DESC])
                                ->limit(1)
                                ->one();

                            if ($update == self::MAX_UPDATE_NUMBER || $update == 1) {
                                $log = new Log();
                                $from = '';

                                if ($update == 1) {
                                    $message = 'Начало цикла';
                                    $log->number_from = $id;
                                }
                                if ($update == self::MAX_UPDATE_NUMBER) {
                                    $update = 0;
                                    $nextZnak = Requests::find()
                                        ->orderBy(['id' => SORT_DESC])
                                        ->limit(1)
                                        ->one();
                                    $log->number_to = $nextZnak->request_number;

                                    $logFrom = Log::find()
                                        ->where(['is not', 'number_from', null])
                                        ->andWhere(['log_type_id' => LogType::ZNAKI_100_UPDATE_ID])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->limit(1)
                                        ->one();

                                    $message = 'Цикл закончен';
                                    if (!empty($logFrom)) {
                                        $from = '. Обновлено с ' . $logFrom->number_from . ' по ' . $log->number_to . ' номера';
                                    }
                                }
                                $logId = LogType::ZNAKI_100_UPDATE_ID;

                                $log->message = $message . $from;
                                $log->log_type_id = $logId;
                                $log->save();
                            }
                            $update = $update + 1;
                        } else {
                            $update = 1;
                            $nextZnak = Requests::find()
                                ->orderBy(['id' => SORT_DESC])
                                ->limit(1)
                                ->one();
                        }
                        $nextID = $nextZnak->request_number;
                    } else {
                        if ($infinity) {
                            $nextZnak = Requests::find()
                                ->orderBy(['id' => SORT_ASC])
                                ->where(['>', 'id', $request->id])
                                ->limit(1)
                                ->one();

                            if (empty($nextZnak)) {
                                $logId = LogType::ZNAKI_ALL_UPDATE_ID;
                                $from = '';
                                $logFrom = Log::find()
                                    ->where(['is not', 'number_from', null])
                                    ->andWhere(['log_type_id' => $logId])
                                    ->orderBy(['id' => SORT_DESC])
                                    ->limit(1)
                                    ->one();

                                if (!empty($logFrom)) {
                                    $from = '. Обновлено с ' . $logFrom->number_from . ' по ' . $id . ' номера';
                                }
                                $log = new Log();
                                $log->message = 'Цикл закончен' . $from;
                                $log->number_to = $id;
                                $log->log_type_id = $logId;
                                $log->save();

                                $nextID = '';
                            } else {
                                $nextID = $nextZnak->request_number;
                            }
                        } else {
                            $nextID = $id + 1;
                        }
                    }
                    
                    if (!empty($classes)) {
                        Parser::saveRequestClasses($classes, $request->id);
                    }
                } else {
                    $errorlog = '';
                    foreach ($request->getErrors() as $error) {
                        $errorlog .= 'ERROR: ' . $error . ' ';
                    }

                    if ($update || $infinity) {
                        if ($update) {
                            $logId = LogType::ZNAKI_100_UPDATE_ID;
                        }
                        if ($infinity) {
                            $logId = LogType::ZNAKI_ALL_UPDATE_ID;
                        }

                        $log = new Log();
                        $log->message = $errorlog;
                        $log->is_error = 1;
                        $log->log_type_id = $logId;
                        $log->save();
                    }
                }
            } else {
                $errorlog = 'OK';
            }
        } else {
            $parserError = $parser->getErrorCode();
            if ($parserError == Parser::ERROR_FAST_SEEN || $parserError == Parser::ERROR_BAD_REQUEST
                || $parserError == Parser::ERROR_DOC_NOT_EXISTS || $parserError == Parser::ERROR_DOCUMENT
                || $parserError == Parser::ERROR_DOC_NOT_FOUND || Parser::ERROR_BAN) {
                $result = $parser->getErrorText() . ' ' . $id;
            }
            if ($parserError == Parser::ERROR_BAN ||
                $parserError == Parser::ERROR_BAD_REQUEST) {
                $proxy = 0;
            }
            if ($parserError == Parser::ERROR_UNKNOWN) {
                $result = $parser->getErrorText() . ' ' . $id;
                $end = true;
                if ($infinity) {
                    $end = false;
                }
            }
            if ($parserError == Parser::ERROR_LIMIT) {
                $result = 'Забанили ip-адрес до конца дня ' . $id;
                $proxy = 0;
            }
            if ($parserError == Parser::ERROR_DOCUMENT || $parserError == Parser::ERROR_FAST_SEEN) {
                $error = $error + 1;
            }
            if ($parserError == Parser::ERROR_FAST_SEEN) {
                $end = ($error > Parser::LIMIT_ERROR) ? true : false;
                if ($infinity) {
                    $end = false;
                }
            }
            if ($parserError == Parser::ERROR_DOCUMENT) {
                if ($error > Parser::LIMIT_ERROR) {
                    $proxy = 0;
                    $error = 0;
                }
            }
            if ($parserError == Parser::ERROR_DOC_NOT_EXISTS ||
                $parserError == Parser::ERROR_DOC_NOT_FOUND) {
                $nextID = $id + 1;
                $error = $error + 1;
                $end = ($error > Parser::LIMIT_ERROR) ? true : false;

                if ($update || $infinity) {
                    $nextID = $id;
                    if ($infinity) {
                        $end = false;
                    }
                }
            }
            $errorlog = 'ERROR: ' . $parser->getErrorText();

            if ($update || $infinity) {
                if ($update) {
                    $logId = LogType::ZNAKI_100_UPDATE_ID;
                }
                if ($infinity) {
                    $logId = LogType::ZNAKI_ALL_UPDATE_ID;
                }

                $log = new Log();
                $log->message = $errorlog;
                $log->is_error = 1;
                $log->log_type_id = $logId;
                $log->save();
            }
        }

        $log = date('Y-m-d H:i:s') . ' ' . $id . ' ' . $errorlog . "\n";
        $parser->writeLog(Parser::LOG_PARSER_REQUEST, $log);

        return $this->render('index', [
            'result' => $result,
            'update' => $update,
            'infinity' => $infinity,
            'timeout' => $timeout,
            'nextID' => $nextID,
            'url' => $url,
            'proxy' => $proxy,
            'error' => $error,
            'end' => $end
        ]);
    }

    public function actionTagUpdate() {
        if (Yii::$app->request->isAjax) {
            $res = true;
            $requests = Requests::getUnrecognized(1000);
            if (!empty($requests)) {
                $captchaIds = [];
                foreach ($requests as $item) {
                    if (!empty($item->thumb)) {
                        $log = date("Y-m-d H:i:s") . ' ' . $item['id'];
                        $path = $item->thumb;
                        $captchaId = Yii::$app->captcha->run($path);
                        if (!empty($captchaId)) {
                            $captchaIds[$item['id']] = $captchaId;
                        } else {
                            $error = Yii::$app->captcha->error();
                            file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_REQUEST_TAG, $log . ', Error: ' . $error . PHP_EOL, FILE_APPEND);
                        }
                    }
                    sleep(4);
                }
                if (!empty($captchaIds)) {
                    foreach ($captchaIds as $item_id => $captcha_id) {
                        $log = date("Y-m-d H:i:s") . ' ' . $item_id;

                        if (Requests::updateAll(['captcha_id' => $captcha_id], ['id' => $item_id]) !== false) {
                            $result = Yii::$app->captcha->getAnswer($captcha_id);
                            if (!empty($result)) {
                                $words = explode(' ', $result);
                                if (!empty($words)) {
                                    foreach ($words as $word) {
                                        if (!empty($word)) {
                                            $word = mb_strtolower($word);
                                            $term = Terms::findOne(['term' => $word]);
                                            if (empty($term)) {
                                                $allowed = ObsceneCensorRus::isAllowed($term);
                                                if ($allowed) {
                                                    $term = new Terms();
                                                    $term->term = $word;
                                                    if ($term->save() === false) {
                                                        $res = false;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!empty($term)) {
                                                $termId = $term->id;
                                                $requestTerm = RequestTerms::findOne(['request_id' => $item_id, 'term_id' => $termId]);
                                                if (empty($requestTerm)) {
                                                    $requestTerm = new RequestTerms();
                                                    $requestTerm->request_id = $item_id;
                                                    $requestTerm->term_id = $termId;
                                                    if ($requestTerm->save() === false) {
                                                        $res = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_REQUEST_TAG, $log . ', captchaID: ' . $captcha_id . ' Response: ' . $result . PHP_EOL, FILE_APPEND );
                                }
                            } elseif (empty(Yii::$app->captcha->error())) {
                                $error = 'CAPCHA_NOT_READY';
                            } else {
                                $error = Yii::$app->captcha->error();
                            }
                            if (isset($error)) {
                                file_put_contents(Yii::$app->basePath . Parser::LOG_PARSER_REQUEST_TAG, $log . ', captchaID: ' . $captcha_id . ' Error: ' . $error . PHP_EOL, FILE_APPEND );
                            }
                        } else {
                            $res = false;
                            break;
                        }
                    }
                }
                if ($res) {
                    return Yii::createObject([
                        'class' => 'yii\web\Response',
                        'format' => Response::FORMAT_JSON,
                        'data' => [
                            'next' => true,
                            'count' => Requests::getCountUnrecognized()
                        ],
                    ]);
                }
            }
            return Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => Response::FORMAT_JSON,
                'data' => [
                    'next' => false,
                ],
            ]);
        }
    }

}