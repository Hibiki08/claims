<?php

namespace app\controllers;

use app\models\Log;
use yii\web\Controller;
use app\models\Requests;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\data\ArrayDataProvider;
use Zend\Http\PhpEnvironment\Request;


class LogsController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['claimsPermission'],
                    ],

                ],
            ],
        ];
    }

    public function actionIndex() {
        $tmLogs = Log::findTmHundredLogs();

        $provider = new ArrayDataProvider([
            'allModels' => $tmLogs,
            'sort' => [
                'attributes' => ['id', 'log_type_id'],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $tmCycleLogs = Log::findTmCycleLogs();

        $provider2 = new ArrayDataProvider([
            'allModels' => $tmCycleLogs,
            'sort' => [
                'attributes' => ['id', 'log_type_id'],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', [
            'tmLogs' => $provider,
            'tmCycleLogs' => $provider2,
        ]);
    }

    public function actionRequests() {
        $requestLogs = Log::findRequestsHundredLogs();

        $provider = new ArrayDataProvider([
            'allModels' => $requestLogs,
            'sort' => [
                'attributes' => ['id', 'log_type_id'],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $requestsCycleLogs = Log::findRequestsCycleLogs();

        $provider2 = new ArrayDataProvider([
            'allModels' => $requestsCycleLogs,
            'sort' => [
                'attributes' => ['id', 'log_type_id'],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('requests', [
            'requestLogs' => $provider,
            'requestsCycleLogs' => $provider2,
        ]);
    }
    
}