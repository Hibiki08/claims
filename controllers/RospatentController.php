<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Requests;
use app\models\Trademarks;
use yii\web\HttpException;


class RospatentController extends Controller {

    public function actionIndex() {
        
    }

    public function actionZayavka($number) {
        $request = Requests::findOne(['request_number' => $number]);
        $classes = [];

        if (!empty($request)) {
            if (!empty($request->classes)) {
                $classes = unserialize($request->classes);
            }
            return $this->render('zayavka', [
                'request' => $request,
                'classes' => $classes
            ]);
        } else {
            throw new HttpException(404 ,'По данному запросу ничего не найдено!');
        }
    }

    public function actionZnak($number) {
        $slash = false;
        $classes = [];
        $numberArr = explode('/', $number);

        if (!empty($numberArr[1])) {
            $slash = $numberArr[1];
            $number = $numberArr[0];
        }

        if ($slash) {
            $trademark = Trademarks::findOne(['reg_number' => $number . '/' . $slash]);
        } else {
            $trademark = Trademarks::findOne(['reg_number' => $number]);
        }

        if (!empty($trademark)) {
            if (!empty($trademark->classes)) {
                $classes = unserialize($trademark->classes);
            }
            return $this->render('znak', [
                'trademark' => $trademark,
                'classes' => $classes
            ]);
        } else {
            throw new HttpException(404 ,'По данному запросу ничего не найдено!');
        }
    }

}