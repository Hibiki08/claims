<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Requests;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use Zend\Http\PhpEnvironment\Request;


class RecognitionRequestsController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['claimsPermission'],
                    ],

                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $unrecognized = Requests::getUnrecognized(1000, true);
        $countUnrecognized = Requests::getCountUnrecognized();
        
        return $this->render('index', [
            'unrecognized' => $unrecognized,
            'countUnrecognized' => $countUnrecognized
        ]);
    }

    public function actionEmpties() {
        $empties = Requests::getRecognizedAndEmpty(1000, true);
        
        return $this->render('empties', [
            'empties' => $empties
        ]);
    }
    
}