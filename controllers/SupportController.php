<?php

namespace app\controllers;

use app\components\SupportChat;
use app\models\Ticket;
use app\models\TicketType;
use app\models\Users;
use Yii;
use yii\web\Controller;
use app\models\SupportForm;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\filters\VerbFilter;
use Telegram\Bot\Exceptions\TelegramResponseException;


class SupportController  extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['bot-response'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['clientExtendedPermission'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (in_array($action->id, ['bot-response'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex() {
        $form = new SupportForm(['scenario' => SupportForm::NEW_TICKET]);

        if ($form->load(Yii::$app->request->post())) {
            $ticketId = $form->newTicket();
            if (!empty($ticketId)) {
                return Yii::$app->getResponse()->redirect(Url::toRoute(['/profile/support/conversation', 'id' => $ticketId]));
            }
        }
        return $this->render('index', [
            'model' => $form,
        ]);
    }

    public function actionArchive() {
        $tickets = Ticket::getTicketsByUser();
        return $this->render('archive', [
            'dataProvider' => $tickets
        ]);
    }
    
    public function actionChatConversation() {
        if (Yii::$app->request->isAjax) {
            $form = new SupportForm(['scenario' => SupportForm::MESSAGE]);
            
            $ticketId = Yii::$app->request->post('ticket-id');
            $form->message = Yii::$app->request->post('message');
            $form->file = UploadedFile::getInstanceByName('file');
            
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($ticketId) {
                if ($form->sendMessage($ticketId)) {
                    return [
                        'status' => 1,
                    ];
                }
            } else {
                $tick = Ticket::findOne(['user_id' => Yii::$app->user->id, 'closed' => 0]);
                if (!empty($tick)) {
                    if ($form->sendMessage($tick->id)) {
                        return [
                            'status' => 1,
                        ];
                    }
                } else {
                    $form->theme = 'Чат ' . (Yii::$app->user->identity->name ? Yii::$app->user->identity->name : Yii::$app->user->identity->login);
                    if ($form->newTicket()) {
                        return [
                            'status' => 1,
                        ];
                    }
                }
            }

            return [
                'status' => 0,
            ];
        }
        return $this->goBack();
    }

    public function actionConversation() {
        $form = new SupportForm(['scenario' => SupportForm::MESSAGE]);
        $ticketId = empty(Yii::$app->request->get('id')) ? null : Yii::$app->request->get('id');

        $ticket = [];
        if (!empty($ticketId)) {
            $ticket = Ticket::getTicketById($ticketId, ['user_id' => Yii::$app->user->id]);
        }

        if ($form->load(Yii::$app->request->post())) {
            if (!$form->sendMessage($ticketId)) {
                Yii::$app->session->setFlash('ticket_error');
            } 
            return Yii::$app->getResponse()->redirect(Url::toRoute(['/profile/support/conversation', 'id' => $ticketId]));
        }

        return $this->render('conversation', [
            'ticket' => $ticket,
            'model' => $form,
        ]);
    }

    public function actionSeenMessage() {
        if (Yii::$app->request->isPjax) {
            $ticketId = Yii::$app->request->post('ticketId');
            $ticket = Ticket::getTicketById($ticketId);
            if (!empty($ticket)) {
                $ticket->notice = 0;
                $ticket->update();
            }
        }
    }

    public function actionBotResponse() {
        $telegram = Yii::$app->MonocleSupportBot;

        if (!empty($telegram->userId)) {
            if (!empty($telegram->text) || !empty($telegram->photo) || !empty($telegram->callbackData)) {
                if ($telegram->text == '/start') {
                    $telegram->commandStart();
                }
                elseif ($telegram->text == '/help') {
                    $telegram->commandHelp();
                }
                elseif (preg_match('/^Начать чат \(тикет #([\d]+)\)$/', $telegram->text, $matches) ||
                    preg_match('/^\/startticket ([\d]+)/', $telegram->text, $matches) ||
                    preg_match('/^\/startticket/', $telegram->text, $matches)) {
                    $ticketId = !empty($matches[1]) ? (int)$matches[1] : null;
                    $telegram->commandStartChat($ticketId);
                }
                elseif (preg_match('/^\/key/', $telegram->text)) {
                    $telegram->commandKey();
                }
                elseif ($telegram->text == 'Игнорировать') {
                    $telegram->commandRejectChat();
                }
                elseif ($telegram->text == 'Тикеты без ответа') {
                    $telegram->commandTickets();
                }
                elseif ((preg_match('/^Закрыть тикет #([\d]+)$/', $telegram->text, $matches)) ||
                    (preg_match('/^Закрыть тикет #([\d]+)$/', $telegram->callbackData, $matches)) ||
                    preg_match('/^\/closeticket ([\d]+)/', $telegram->text, $matches) ||
                    preg_match('/^\/closeticket/', $telegram->text, $matches)) {
                    $ticketId = !empty($matches[1]) ? (int)$matches[1] : null;
                    $telegram->commandCloseTicket($ticketId);
                } else {
                    $telegram->commandReply();
                }
            }
        } else {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/']));
        }
    }

}