<?php

namespace app\controllers;

use app\models\Trademarks;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\AccessRule;


class RecognitionTrademarksController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['claimsPermission'],
                    ],

                ],
            ],
        ];
    }

    public function actionIndex() {
        $unrecognized = Trademarks::getUnrecognized(1000, true);
        $countUnrecognized = Trademarks::getCountUnrecognized();

        return $this->render('index', [
            'unrecognized' => $unrecognized,
            'countUnrecognized' => $countUnrecognized
        ]);
    }

    public function actionEmpties() {
        $empties = Trademarks::getRecognizedAndEmpty(1000, true);

        return $this->render('empties', [
            'empties' => $empties
        ]);
    }

}