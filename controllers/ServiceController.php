<?php

namespace app\controllers;

use app\models\Colors;
use app\models\CompaniesForm;
use app\models\TmForeign;
use app\models\Trademarks;
use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\db\Expression;
use app\models\ColorsShade;
use yii\web\Response;
use app\models\ColorsAndColorsShade;
use app\models\TrademarkColors;
use app\models\ColorSearch;
use app\models\SearchLogProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessRule;
use yii\sphinx\Query as SphinxQuery;
use app\models\Requests;
use app\models\Users;
use app\models\Classes;
use app\models\RequestSearch;
use app\models\Searchlogs;


class ServiceController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['claimsPermission'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return false;
    }
    
    public function actionColors() {
        $colorsBase = Colors::getAll();

        $searchModel = new ColorSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('colors', [
            'dataProvider' => $dataProvider,
            'colorsBase' => $colorsBase,
            'searchModel' => $searchModel
        ]);
    }

    public function actionColorsAssign() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $shade_color_id = Yii::$app->request->post('shade_color_id');
            $color_ids = Yii::$app->request->post('color_ids');

            $query = 'insert into ' . ColorsAndColorsShade::tableName() . ' (color_shade_id, color_id) values (' . $shade_color_id . ', ' . implode('), (' . $shade_color_id . ', ' , $color_ids) . ') on duplicate key update color_shade_id=color_shade_id, color_id=color_id';
            if (Yii::$app->db->createCommand($query)->execute()) {
                $res = (new TrademarkColors)->assignColor($shade_color_id);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'status' => $res,
                ];
            }
            Yii::$app->end();
        }
    }

    public function actionColorDelete() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $shade_color_id = Yii::$app->request->post('shade_color_id');
            $color_id = Yii::$app->request->post('color_id');

            if ((new TrademarkColors)->deleteColor($shade_color_id, $color_id) >= 0) {
                $colorsAndColorsShade = ColorsAndColorsShade::findOne(['color_shade_id' => $shade_color_id, 'color_id' => $color_id]);
                $res = $colorsAndColorsShade->delete();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'status' => $res,
                ];
            }
            Yii::$app->end();
        }
    }
    
    public function actionBadRequests() {
        $rows = (new SphinxQuery())
            ->select('id')
            ->from('requests')
            ->where(['status_id' => 1])
            ->showMeta(true)
            ->options([
                'max_matches' => 1000,
            ])
            ->limit(1000)
            ->search();

        $ids = [];
        foreach ($rows['hits'] as $row) {
            $ids[] = $row['id'];
        }

        $query = Requests::find()
            ->where(['in','id',$ids]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        return $this->render('bad-requests', [
            'dataProvider' => $dataProvider,
            'meta' => $rows['meta']
        ]);
    }

    public function actionBadTrademarks() {
        $rows = (new SphinxQuery())
            ->select('id')
            ->from('trademarks')
            ->where(['status_id' => 1])
            ->showMeta(true)
            ->options([
                'max_matches' => 1000,
            ])
            ->limit(1000)
            ->search();

        $ids = [];
        foreach ($rows['hits'] as $row) {
            $ids[] = $row['id'];
        }

        $query = Trademarks::find()
            ->where(['in','id',$ids]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        return $this->render('bad-trademarks', [
            'dataProvider' => $dataProvider,
            'meta' => $rows['meta']
        ]);
    }
    
//    public function actionClassesStat() {
//        $tmClasses = new ActiveDataProvider([
//            'query' => (new Query())
//                ->select(new Expression('class, COUNT(*) AS count'))
//                ->from('trademark_classes')
//                ->groupBy('class')
//                ->orderBy(['count' => SORT_DESC]),
//            'pagination' => false,
//        ]);
//        $rClasses = new ActiveDataProvider([
//            'query' => (new Query())
//                ->select(new Expression('class, COUNT(*) AS count'))
//                ->from('request_classes')
//                ->groupBy('class')
//                ->orderBy(['count' => SORT_DESC]),
//            'pagination' => false,
//        ]);
//
//        return $this->render('classes-stat', [
//            'tmClasses' => $tmClasses,
//            'rClasses' => $rClasses,
//        ]);
//    }
    
    public function actionRecognition() {
        $requestCount = Requests::getCountUnrecognized();
        $trademarkCount = Trademarks::getCountUnrecognized();
        $tmForeignCount = TmForeign::getCountUnrecognized();
        
        $requestCountEmpty = Requests::getCountRecognizedAndEmpty();
        $trademarkCountEmpty = Trademarks::getCountRecognizedAndEmpty();
        $tmForeignCountEmpty = TmForeign::getCountRecognizedAndEmpty();
        
        return $this->render('recognition', [
            'requestCount' => $requestCount,
            'trademarkCount' => $trademarkCount,
            'tmForeignCount' => $tmForeignCount,
            'requestCountEmpty' => $requestCountEmpty,
            'trademarkCountEmpty' => $trademarkCountEmpty,
            'tmForeignCountEmpty' => $tmForeignCountEmpty,
        ]);
    }

    public function actionSearchLogs() {
        $users = Users::find()->where(['status' => 1])->orderBy('id')->all();
        $usersArr = [];
        if (!empty($users)) {
            $usersArr['guest'] = 'Гость';

            foreach ($users as $val) {
                $usersArr[$val['id']] = $val['login'];
            }
        }
        $searchModel = new SearchLogProvider();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $count = $dataProvider->getTotalCount();

        return $this->render('search-logs', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'users' => $usersArr,
            'count' => $count
        ]);
    }
    
    public function actionCompanies() {
        $form = new CompaniesForm(['scenario' => 'companies']);
        $trademarkDataProvider = $form->search(Yii::$app->request->queryParams);
        
        $classes = Classes::getAll();
        $classesCount = Classes::getCount();
        
        return $this->render('companies', [
            'trademarkDataProvider' => $trademarkDataProvider,
            'searchModel' => $form,
            'classes' => $classes,
            'classesCount' => $classesCount,
        ]);
    }

}