<?php

namespace app\controllers;

use app\models\RequestTracking;
use app\models\TrademarkTracking;
use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\db\Expression;
use yii\web\Response;
use app\models\SearchTracking;
use yii\filters\AccessControl;
use app\models\Users;
use yii\filters\AccessRule;
use yii\data\ActiveDataProvider;
use app\models\TrackingForm;
use yii\helpers\Url;
use yii\widgets\Pjax;


class TrackingController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['bot-response'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['clientExtendedPermission'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (in_array($action->id, ['bot-response'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex() {
        $querySearch = SearchTracking::getByUser(true);

        $queryTrademarks = TrademarkTracking::getByUser(true);
        
        $searchRequests = new ActiveDataProvider([
            'query' => $querySearch,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        $trademarks = new ActiveDataProvider([
            'query' => $queryTrademarks,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        $tracking = new TrackingForm();

        if ($tracking->load(Yii::$app->request->post())) {
            $tracking->add();
        }

        return $this->render('index', [
            'searchRequests' => $searchRequests,
            'trademarks' => $trademarks,
            'tracking' => $tracking
        ]);
    }

    public function actionRequests() {
        $queryRequests = RequestTracking::getByUser(true);

        $requests = new ActiveDataProvider([
            'query' => $queryRequests,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        return $this->render('requests', [
            'requests' => $requests,
        ]);
    }

    public function actionTrademarks() {
        $queryTrademarks = TrademarkTracking::getByUser(true);

        $trademarks = new ActiveDataProvider([
            'query' => $queryTrademarks,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
        
        return $this->render('trademarks', [
            'trademarks' => $trademarks,
        ]);
    }
    
    public function actionAddItem() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $res = false;
            $message = 'Что-то пошло не так';

            $id = Yii::$app->request->post('id');
            $model = Yii::$app->request->post('model');
            
            if (!empty($id) && !empty($model)) {
                $tracking = new TrackingForm();
                switch ($model) {
                    case 'requests':
                        $tracking->request_id = $id;
                        $res = $tracking->addRequest();
                        if ($res) {
                            $message = 'Заявка добавлена';
                        }
                        break;
                    case 'trademarks':
                        $tracking->trademark_id = $id;
                        $res = $tracking->addTrademark();
                        if ($res) {
                            $message = 'Товарный знак добавлен';
                        }
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $res,
                'message' => $message
            ];
        }
    }
    
    public function actionSaveTitle() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $value = Yii::$app->request->post('value');
            $id = Yii::$app->request->post('id');
            $model = Yii::$app->request->post('model');

            if (!empty($value) && !empty($id) && !empty($model)) {
                switch ($model) {
                    case 'search_tracking':
                        $track = SearchTracking::findOne(['id' => $id]);
                        break;
                    case 'request_tracking':
                        $track = RequestTracking::findOne(['id' => $id]);
                        break;
                    case 'trademark_tracking':
                        $track = TrademarkTracking::findOne(['id' => $id]);
                        break;
                }

                if (!empty($track)) {
                    $track->title = $value;
                    if ($track->update() !== false) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status' => true,
                        ];
                    }
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => false,
            ];
        }
        return false;
    }

    public function actionActive() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $active = Yii::$app->request->post('active');
            $id = Yii::$app->request->post('id');
            $model = Yii::$app->request->post('model');

            if (!empty($id) && !empty($model)) {
                switch ($model) {
                    case 'search_tracking':
                        $track = SearchTracking::findOne(['id' => $id]);
                        break;
                    case 'request_tracking':
                        $track = RequestTracking::findOne(['id' => $id]);
                        break;
                    case 'trademark_tracking':
                        $track = TrademarkTracking::findOne(['id' => $id]);
                        break;
                }

                if (!empty($track)) {
                    $track->active = $active;
                    if ($track->update() !== false) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status' => true,
                        ];
                    }
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => false,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $response = false;
            $id = (int)Yii::$app->request->post('id');
            $model = Yii::$app->request->post('model');

            if (!empty($id) && !empty($model)) {
                switch ($model) {
                    case 'search_tracking':
                        $track = SearchTracking::findOne(['id' => $id]);
                        break;
                    case 'request_tracking':
                        $track = RequestTracking::findOne(['id' => $id]);
                        break;
                    case 'trademark_tracking':
                        $track = TrademarkTracking::findOne(['id' => $id]);
                        break;
                }

                if (!empty($track)) {
                    if ($track->delete() !== false) {
                        $response = true;
                    }
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }

    public function actionBotResponse() {
        $telegram = Yii::$app->MonocleMentionBot;

        if (!empty($telegram->userId)) {
            if (!empty($telegram->text)) {
                if ($telegram->text == '/start') {
                    $telegram->commandStart();
                }
                if ($telegram->text == '/stop') {
                    $telegram->commandStop();
                }
                if (preg_match('/^\/key/', $telegram->text)) {
                    $telegram->commandKey();
                }
            }
        } else {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/']));
        }
    }

    public function actionInstruction() {
        return $this->render('instruction');
    }

}