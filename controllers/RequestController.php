<?php

namespace app\controllers;

use app\models\Classes;
use app\models\RequestTerms;
use app\models\RequestTracking;
use app\models\Searchlogs;
use app\models\Statuses;
use app\models\Terms;
use app\models\TrademarkClasses;
use app\models\Trademarks;
use app\models\TrademarkTracking;
use app\models\Users;
use Yii;
use app\models\Requests;
use app\models\Colors;
use app\models\RequestSearch;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\sphinx\MatchExpression;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use app\models\TrackingForm;
use yii\web\Response;
use app\models\SearchTracking;
use yii\helpers\Json;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use \Wkhooy\ObsceneCensorRus;
use yii\web\Cookie;
use app\models\TrademarkTerms;
use app\models\TmForeignTerm;


/**
 * RequestController implements the CRUD actions for Requests model.
 */
class RequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'query-list'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['export'],
                        'allow' => true,
                        'roles' => ['clientExtendedPermission'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['claimsPermission'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'verify' => [
                'class' => 'app\components\Verify',
                'actions' => [
                    'classes',
                    'error',
                ]
            ]
        ];
    }

    /**
     * Lists all Requests models.
     * @return mixed
     */
    public function actionIndex() {
        $session = Yii::$app->session;

        $amountExportItems = 0;
        $currentItems = [];

        if (isset($session['exp'])) {
            $requestsCount = isset($session['exp']['requests']) ? count($session['exp']['requests']) : 0;
            $trademarksCount = isset($session['exp']['trademarks']) ? count($session['exp']['trademarks']) : 0;
            $amountExportItems = $requestsCount + $trademarksCount;
            $currentItems = $session['exp'];
        }
        
        $searchArea = [];
        if (Yii::$app->user->isGuest || !Yii::$app->user->can('clientExtendedPermission')) {
            $searchArea = [
                'term' => 'По номеру или информации на изображении'
            ];
        } else {
            $searchArea = [
                'term' => 'По номеру или информации на изображении',
                'all' => 'По всему',
                'applicant' => 'По заявителю/правообладателю',
                'address' => 'По адресу для переписки'
            ];
        }
        
        $queryParams = Yii::$app->request->queryParams;
        $searchModel = new RequestSearch();

//        $searchModel->load($queryParams);
//        $google = Yii::$app->google;
//        $google->query = !isset($queryParams['RequestSearch']['string']) ? !isset($queryParams['string']) ? '' : $queryParams['string'] : $queryParams['RequestSearch']['string'];
//        $queryCorrected = $google->getQueryCorrected();

        if (isset($queryParams['search-notice'])) {
            $tracking = SearchTracking::findOne(['id' => $queryParams['search-notice']]);
            $tracking->notice = 0;
            $tracking->update();
        }
        if (isset($queryParams['request-notice'])) {
            $tracking = RequestTracking::findOne(['id' => $queryParams['request-notice']]);
            $tracking->notice = 0;
            $tracking->update();
        }
        if (isset($queryParams['trademark-notice'])) {
            $tracking = TrademarkTracking::findOne(['id' => $queryParams['trademark-notice']]);
            $tracking->notice = 0;
            $tracking->update();
        }

        $colors = Colors::getAll();
        $colorsCount = Colors::getCount();
        $classes = Classes::getAll();
        $classesCount = Classes::getCount();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $lastUpdateRequests = Requests::getLastUpdate();
        $countRequests = Requests::getCount();
        $lastUpdateTrademarks = Trademarks::getLastUpdate();
        $countTrademarks = Trademarks::getCount();

        $hasAccess = 0;
        if (Yii::$app->user->can('clientExtendedPermission')) {
            $hasAccess = 1;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'lastUpdateRequests' => $lastUpdateRequests,
            'countRequests' => $countRequests,
            'lastUpdateTrademarks' => $lastUpdateTrademarks,
            'countTrademarks' => $countTrademarks,
            'colors' => $colors,
            'colorsCount' => $colorsCount,
            'hasAccess' => $hasAccess,
            'itemClasses' => $classes,
            'classesCount' => $classesCount,
            'amountExportItems' => $amountExportItems,
            'searchArea' => $searchArea,
            'currentItems' => $currentItems
//            'queryCorrected' => $queryCorrected
        ]);
    }

    public function actionQueryList($string = null) {
        if ($string) {
            $searchLog = Searchlogs::getUniqQuery(['like', 'query', $string]);
            $searchQuery = [];
            if (!empty($searchLog)) {
                foreach ($searchLog as $item) {
                    $searchQuery[] = ['value' => $item->query];
                }
            }
            return Json::encode($searchQuery);
        }
        return $this->redirect(['/request']);
    }

    public function actionAddRequest() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $res = false;

            $url = Yii::$app->request->post('url');

            if (!empty($url)) {
                $tracking = new TrackingForm();
                $tracking->search_request = $url;

                $res = $tracking->add();
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $res,
            ];
        }
    }

    public function actionReg()
    {
        $users = [
//            'nikolay.shevchenko@claims.ru' => 'nNOJTmEY',
//            'anton.endresyak@claims.ru' => 'KodkUExo',
//            'ekaterina.pronicheva@claims.ru' => 'GpxBxoGM',
//            'alyona.reshetova@claims.ru' => 'e9Ft6ubg',
//            'alexandra.burushkina@claims.ru' => 'uROQSVdK',
//            'alexey.petrov@claims.ru' => '8I0l4yux',
//            'anastasia.kuznetsova@claims.ru' => 'CuzhAHA0',
//            'igor.nevzorov@claims.ru' => 'kaoSJSEM',
//            'paster_v@mail.ru' => 'vAPN9mQe'
//            'nikita.petrov@heineken.com' => '4h4eBcPf'
//            'nataliya.goloskova@heineken.com' => 'j8Lb6Xdj'
//	        'ILoginov@ntv.ru' => 'R477k8v4'
//	        'a.mirgorodskaya@victorpasternak.ru' => 'Sm0FerLM'
//	        'e.neshina@victorpasternak.ru' => 'bXzD45Qs'
//	        'yura@chetverikov.org' => '12345678'
//            'mikhail.s.kozin@gmail.com' => 'sj4L09MaHEz45n'
//            'klimina.ks@gazprom-neft.ru' => 'mSpwLqiR',
//            'Sedova.MV@gazprom-neft.ru' => 'vnotJfAq',
//            'NYUNYAEV.VO@gazprom-neft.ru' => 'mApd4Kny',
//            'maksim.paramonov@sns.ru' => 'pt7MaqrG',
//            'Marina.Pozdeeva@gfdrinks.ch' => 'vdhjYue9',
//            'Aleksey.Sdobnov@sns.ru' => 'zpemBd67'
//            'EBourliand@kpmg.ru' => 'paSe6Mck',
//            'ETsybikova@kpmg.ru' => 'wzXo902B'
//            'victor.gm@claims.ru' => 'sgfU76nZ',
//            'maria.lavrenova@claims.ru' => 'naqOr49M'
        ];

        foreach ($users as $login => $password) {
            $user = new Users();
            $user->login = $login;
            $user->setPassword($password);
            $user->status = 1;
            $user->telegram_key = md5(time() . 'DLfljD9^ns,A[p');
            $user->save(false);

            $lasId = $user->id;
            $userRole = Yii::$app->authManager->getRole('client');
            Yii::$app->authManager->assign($userRole, $lasId);
        }
    }

    /**
     * Displays a single Requests model.
     * @param integer $id
     * @return mixed
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Requests model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
    /*public function actionCreate()
    {
        $model = new Requests();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Requests model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Requests model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Requests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Requests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Requests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionSetCstatus($id)
    {
        $model = $this->findModel($id);
        $model->attributes = [
            'cstatus_id' => Yii::$app->request->post('cstatus_id')
        ];
        $model->save();
    }

    public function actionTmSetCstatus($id) {
        $trademark = Trademarks::findOne($id);
        if (!empty($trademark)) {
            $trademark->attributes = [
                'cstatus_id' => Yii::$app->request->post('cstatus_id')
            ];
            $trademark->save();
        }
    }

    public function actionAmountExportItems() {
        if (Yii::$app->request->isAjax) {
            $session = Yii::$app->session;

            $amountExportItems = 0;
            if ($session->get('exp')) {
                $requestsCount = isset($session['exp']['requests']) ? count($session['exp']['requests']) : 0;
                $trademarksCount = isset($session['exp']['trademarks']) ? count($session['exp']['trademarks']) : 0;
                $amountExportItems = $requestsCount + $trademarksCount;

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'amount' => $amountExportItems,
                ];
            }
        }
    }
    
    public function actionAddTerm() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $noModel = false;
            $res = true;

            if (!empty($post['val'])) {
                $val = mb_strtolower(trim($post['val']));
                $term = Terms::findOne(['term' => $val]);
                if (empty($term)) {
                    $allowed = ObsceneCensorRus::isAllowed($term);
                    if ($allowed) {
                        $term = new Terms();
                        $term->term = $val;
                        if ($term->save() === false) {
                            $res = false;
                        }
                    }
                }

                $termId = $term->id;

                switch ($post['type']) {
                    case 'trademarks':
                        $term = TrademarkTerms::findOne(['trademark_id' => $post['id'], 'term_id' => $termId]);
                        $newTerm = new TrademarkTerms();
                        $newTerm->trademark_id = $post['id'];
                        break;
                    case 'requests':
                        $term = RequestTerms::findOne(['request_id' => $post['id'], 'term_id' => $termId]);
                        $newTerm = new RequestTerms();
                        $newTerm->request_id = $post['id'];
                        break;
                    case 'tm_foreign':
                        $term = TmForeignTerm::findOne(['tm_id' =>  $post['id'], 'term_id' => $termId]);
                        $newTerm = new TmForeignTerm();
                        $newTerm->tm_id = $post['id'];
                        break;
                    default:
                        $noModel = true;
                }

                    if (!$noModel) {
                        $newTerm->term_id = $termId;
                        if ($newTerm->save() === false) {
                            $res = false;
                        }

                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'res' => $res,
                            'term_id' => $termId
                        ];
                    }

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'res' => false,
                ];
            }
        }
        return false;
    }

    public function actionDeleteTerm() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $noModel = false;
            $res = true;

            if (!empty($post['term_id'])) {
                $termId = $post['term_id'];

                switch ($post['type']) {
                    case 'trademarks':
                        $termRelation = TrademarkTerms::findOne(['term_id' => $termId, 'trademark_id' => $post['id']]);
                        break;
                    case 'requests':
                        $termRelation = RequestTerms::findOne(['term_id' => $termId, 'request_id' => $post['id']]);
                        break;
                    case 'tm_foreign':
                        $termRelation = TmForeignTerm::findOne(['term_id' => $termId, 'tm_id' => $post['id']]);
                        break;
                    default:
                        $noModel = true;
                }

                if (!$noModel) {
                    if (!empty($termRelation)) {
                        if ($termRelation->delete() !== false) {
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return [
                                'res' => $res,
                            ];
                        }
                    }
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'res' => false,
                ];
            }
        }
        return false;
    }

    public function actionExportPrepare() {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $data = [];
            $session = Yii::$app->session;

            if (!empty($post)) {
                $currentRequests = [];
                $currentTrademarks = [];

                if (isset($session['exp']['requests'])) {
                    $currentRequests = $session['exp']['requests'];
                    $data['requests'] = $currentRequests;
                }
                if (isset($session['exp']['trademarks'])) {
                    $currentTrademarks = $session['exp']['trademarks'];
                    $data['trademarks'] = $currentTrademarks;
                }

                if (isset($post['ids'])) {
                    if (!isset($session['exp']) || (isset($session['exp']) && empty($session['exp']))) {
//                        Yii::$app->response->cookies->add(new Cookie([
//                            'name' => 'exp',
//                            'value' => $post['ids']
//                        ]));
                        $session->set('exp', $post['ids']);

                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'amount' => (isset($post['ids']['requests']) ? count($post['ids']['requests']) : 0) + (isset($post['ids']['trademarks']) ? count($post['ids']['trademarks']) : 0),
                        ];
                    } else {
                        if (!empty($currentRequests)) {
                            if (isset($post['ids']['requests'])) {
                                if (isset($post['check']) && ($post['check'] == 'unchecked')) {
                                    $data['requests'] = $post['ids']['requests'];
                                } else {
                                    foreach ($post['ids']['requests'] as $item) {
                                        if (!in_array($item, $data['requests'])) {
                                            $data['requests'][] = $item;
                                        }
                                    }
                                }
                            } else {
                                $data['requests'] = [];
                            }
                        } else {
                            if (isset($post['ids']['requests'])) {
                                $data['requests'] = $post['ids']['requests'];
                            }
                        }

                        if (!empty($currentTrademarks)) {
                            if (isset($post['ids']['trademarks'])) {
                                if (isset($post['check']) && ($post['check'] == 'unchecked')) {
                                    $data['trademarks'] = $post['ids']['trademarks'];
                                } else {
                                    foreach ($post['ids']['trademarks'] as $item) {
                                        if (!in_array($item, $data['trademarks'])) {
                                            $data['trademarks'][] = $item;
                                        }
                                    }
                                }
                            } else {
                                $data['trademarks'] = [];
                            }
                        } else {
                            if (isset($post['ids']['trademarks'])) {
                                $data['trademarks'] = $post['ids']['trademarks'];
                            }
                        }

                        $session->set('exp', $data);
                        
//                        Yii::$app->response->cookies->add(new Cookie([
//                            'name' => 'exp',
//                            'value' => $data
//                        ]));

                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'amount' => (isset($data['requests']) ? count($data['requests']) : 0) + (isset($data['trademarks']) ? count($data['trademarks']) : 0),
                        ];
                    }
                }

                if (isset($post['id']) && isset($post['model'])) {
                    foreach ($data[$post['model']] as $key => $val) {
                        if ($val == $post['id']) {
                            unset($data[$post['model']][$key]);
                            break;
                        }
                    }

                    $session->set('exp', $data);
//                    Yii::$app->response->cookies->add(new Cookie([
//                        'name' => 'exp',
//                        'value' => $data
//                    ]));

                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'amount' => (isset($data['requests']) ? count($data['requests']) : 0) + (isset($data['trademarks']) ? count($data['trademarks']) : 0),
                    ];
                }

                $session->remove('exp');
                $session->destroy();

//                $cookies = Yii::$app->response->cookies;
//                $cookies->remove('exp');
//                unset($cookies['exp']);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'amount' => (isset($data['requests']) ? count($data['requests']) : 0) + (isset($data['trademarks']) ? count($data['trademarks']) : 0),
                ];

            } else {
                $session->remove('exp');
                $session->destroy();
//                $cookies = Yii::$app->response->cookies;
//                $cookies->remove('exp');
//                unset($cookies['exp']);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'amount' => (isset($data['requests']) ? count($data['requests']) : 0) + (isset($data['trademarks']) ? count($data['trademarks']) : 0),
                ];
            }
        }
    }

    public function actionExport($export_type) {
        $session = Yii::$app->session;
        if (isset($session['exp'])) {
            $requests = isset($session['exp']['requests']) ? $session['exp']['requests'] : [];
            $trademarks = isset($session['exp']['trademarks']) ? $session['exp']['trademarks'] : [];

            $dataArr = [];
            $dataPdf = [];
            $requstArr = [];
            $trademarksArr = [];

            if (!empty($requests)) {
                $requstArr = Requests::find()->where(['in', 'id', $requests])->all();
            }
            if (!empty($trademarks)) {
                $trademarksArr = Trademarks::find()->where(['in', 'id', $trademarks])->all();
            }

            $data['requests'] = $requstArr;
            $data['trademarks'] = $trademarksArr;

            if (!empty($data)) {
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();

                $i = 3;
                foreach ($data as $key => $item) {
                    if (!empty($item)) {
                        foreach ($item as $val) {

                            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                            $drawing->setName('Paid');
                            $drawing->setDescription('Paid');
                            $drawing->setPath($val->thumb);
                            $drawing->setCoordinates('A' . $i);
                            $drawing->setWidthAndHeight(110, 40);
                            $drawing->setResizeProportional(true);
                            $drawing->setWorksheet($spreadsheet->getActiveSheet());
                            $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(35);

                            $dataArr[] = [
                                'type' => $key == 'requests' ? 'Заявка' : 'Товарный знак',
                                'classes' => implode(', ', array_keys(unserialize($val->classes))),
                                'req' => (isset($val->reg_number) ? $val->reg_number : $val->request_number) . ' - ' . $val->request_date,
                                'reg' => $val->reg_number . ' - ' . $val->reg_date,
                                'right' => (isset($val->applicant) ? $val->applicant : $val->rightholder),
                                'status' => Statuses::getStatusById($val->status_id)->title,
                            ];
                            $dataPdf[] = [
                                'type' => $key == 'requests' ? 'Заявка' : 'Товарный знак',
                                'classes' => implode(', ', array_keys(unserialize($val->classes))),
                                'req' => (isset($val->reg_number) ? $val->reg_number : $val->request_number) . ' - ' . $val->request_date,
                                'reg' => $val->reg_number . ' - ' . $val->reg_date,
                                'right' => (isset($val->applicant) ? $val->applicant : $val->rightholder),
                                'status' => Statuses::getStatusById($val->status_id)->title,
                                'thumb' => $val->thumb
                            ];
                            $i++;
                        }
                    }
                }

                $sheet->setCellValue('A1', 'Экспорт по запросу: ');
                $sheet->setCellValue('A2', 'Картинка');
                $sheet->setCellValue('B2', 'Тип');
                $sheet->setCellValue('C2', 'Классы');
                $sheet->setCellValue('D2', 'Номер,' . "\n" . ' дата подачи заявки');
                $sheet->setCellValue('E2', 'Номер' . "\n" . ' и дата регистрации');
                $sheet->setCellValue('F2', 'Заявитель / Правообладатель,' . "\n" . 'Адрес для переписки');
                $sheet->setCellValue('G2', 'Статус');
                $spreadsheet->getActiveSheet()->fromArray($dataArr, null, 'B3');

                $spreadsheet->getActiveSheet()->mergeCells('A1:G1');
                $spreadsheet->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
                $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $spreadsheet->getActiveSheet()->getRowDimension(2)->setRowHeight(30);
                $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(40);

                $date = date('d_m_Y__H_i_s');

                if ($export_type == 'excel') {
                    $excelWriter = IOFactory::createWriter($spreadsheet, 'Xlsx');

                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="export_requests_tm_' . $date . '.xlsx"');
                    header('Cache-Control: max-age=0');

                    $excelWriter->save('php://output');
                }

                if ($export_type == 'pdf') {
                    $mpdf = new \Mpdf\Mpdf();
                    $tr = '';
                    foreach ($dataPdf as $item) {

                        $tr .= '<tr>' .
                            '<td><img src="' . $item['thumb'] . '"></td>'.
                            '<td>' . $item['type'] . '</td>'.
                            '<td>' . $item['classes'] . '</td>'.
                            '<td>' . $item['req'] . '</td>'.
                            '<td>' . $item['reg'] . '</td>'.
                            '<td>' . $item['right'] . '</td>'.
                            '<td>' . $item['status'] . '</td>'.
                            '</tr>';
                    }

                    $table =  '<table><tr><td>Картинка</td><td>Тип</td><td>Классы</td><td>Номер, дата подачи заявки</td><td>Номер и дата регистрации</td><td>Заявитель / Правообладатель, Адрес для переписки</td><td>Статус</td></tr>' . $tr . '</table>';
                    $html = '<h1>Экспорт по запросу: </h1>' . $table;
                    $mpdf->WriteHTML($html);
                    $mpdf->Output();
                }

                if ($export_type == 'docx') {
                    $docx = new PhpWord();
                    $section = $docx->addSection();
                    $fontStyle = ['color'=> '000000', 'size' => 16, 'bold' => true];
                    $section->addText('Экспорт по запросу: ', $fontStyle);
                    $tableStyle = [
                        'borderColor' => '006699',
                        'borderSize'  => 6,
                        'cellMargin'  => 50
                    ];
                    $table = $section->addTable([$tableStyle]);
                    $table->addRow();
                    $cell = $table->addCell(15, 10);
                    $cell->addText('Картинка');
                    $cell = $table->addCell(25, 10);
                    $cell->addText('Тип');
                    $cell = $table->addCell(30, 10);
                    $cell->addText('Классы');
                    $cell = $table->addCell(30, 10);
                    $cell->addText('Номер, дата подачи заявки');
                    $cell = $table->addCell(30, 10);
                    $cell->addText('Номер и дата регистрации');
                    $cell = $table->addCell(30, 10);
                    $cell->addText('Заявитель / Правообладатель, Адрес для переписки');
                    $cell = $table->addCell(25, 10);
                    $cell->addText('Статус');

                    foreach ($dataPdf as $item) {
                        $table->addRow();
                        $cell = $table->addCell(15, 10);
                        $cell->addImage($item['thumb']);
                        $cell = $table->addCell(25, 10);
                        $cell->addText($item['type']);
                        $cell = $table->addCell(30, 10);
                        $cell->addText($item['classes']);
                        $cell = $table->addCell(30, 10);
                        $cell->addText($item['req']);
                        $cell = $table->addCell(30, 10);
                        $cell->addText($item['reg']);
                        $cell = $table->addCell(30, 10);
                        $cell->addText($item['right']);
                        $cell = $table->addCell(25, 10);
                        $cell->addText($item['status']);
                    }
                    $docxWriter = \PhpOffice\PhpWord\IOFactory::createWriter($docx, 'Word2007');
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="export_requests_tm_' . $date . '.docx"');
                    header('Cache-Control: max-age=0');

                    $docxWriter->save('php://output');
                }
            }
        }
        exit;
    }

}
