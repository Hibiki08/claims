<?php

namespace app\controllers;

use app\models\AuthAssignment;
use Yii;
use yii\web\Controller;
use yii\filters\AccessRule;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\db\Expression;
use app\models\Users;
use app\models\UserSearch;
use app\models\AuthItem;
use yii\web\Response;


class UsersController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['claimsPermission'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $roles = AuthItem::getRoles();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'roles' => $roles
        ]);
    }

    public function actionActive() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $active = Yii::$app->request->post('active');
            $id = (int)Yii::$app->request->post('id');

            if (!empty($id)) {
                $user = Users::findOne(['id' => $id]);

                if (!empty($user)) {
                    $user->status = $active;
                    if ($user->update() !== false) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status' => true,
                        ];
                    }
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => false,
            ];
        }
        Yii::$app->end();
    }

    public function actionDelete() {
        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $response = false;
            $id = (int)Yii::$app->request->post('id');

            if (!empty($id)) {
                $user = Users::findOne(['id' => $id]);

                if (!empty($user)) {
                    if ($user->delete() !== false) {
                        $response = true;
                    }
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        Yii::$app->end();
    }
    
    public function actionSetRole($id) {
        $oldRole = Yii::$app->request->post('oldRole');
        $role = Yii::$app->request->post('role');
        $oldAssignment = AuthAssignment::find()
            ->where(['and', ['item_name' => $oldRole], ['user_id' => $id]])
        ->one();
        if (!empty($oldAssignment)) {
            if ($oldAssignment->delete() !== false) {
                $userRole = Yii::$app->authManager->getRole($role);
                Yii::$app->authManager->assign($userRole, $id);
            }
        }
    }

    public function actionSetSupport() {
        if (Yii::$app->request->post()) {
            $checked = Yii::$app->request->post('checked');
            $id = Yii::$app->request->post('id');
            if ($checked) {
                $userRole = Yii::$app->authManager->getRole('support');
                Yii::$app->authManager->assign($userRole, $id);
            } else {
                $assignment = AuthAssignment::find()
                    ->where(['and', ['item_name' => 'support'], ['user_id' => $id]])
                ->one();
                if (!empty($assignment)) {
                    $assignment->delete();
                }
            }
        }
    }

    public function actionSubscribeDate() {
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $date = !empty(Yii::$app->request->post('date')) ? Yii::$app->request->post('date') : 0;

            if (!empty($id)) {
                $user = Users::findOne(['id' => $id]);

                if (!empty($user)) {
                    $user->subscription_date = !empty($date) ? (new \DateTime($date))->format('Y-m-d') : null;
                    if ($user->update() !== false) {
                        $premium = AuthAssignment::find()
                            ->where(['and', ['item_name' => 'premium'], ['user_id' => $id]])
                            ->one();

                        if (strtotime('now') < strtotime($date)) {
                            if (empty($premium)) {
                                $userRole = Yii::$app->authManager->getRole('premium');
                                Yii::$app->authManager->assign($userRole, $id);
                            }
                        } else {
                            if (!empty($premium)) {
                                $premium->delete();
                            }
                        }
                    }
                }
            }
        }
    }

}