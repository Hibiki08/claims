<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ProfileResetPassForm;
use app\models\ProfileForm;
use app\models\Users;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use app\models\Searchlogs;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\AccessRule;


class ProfileController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['notifications'],
                        'allow' => false,
                        'roles' => ['client']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['clientPermission'],
                    ],

                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $form = new ProfileResetPassForm();
        $profileForm = new ProfileForm();
        $reset = false;
        $profile = false;
        $user = Users::findIdentity(Yii::$app->user->id);

        if ($reset = ($form->load(Yii::$app->request->post()) && $form->validate())) {
            $user->setPassword($form->new_pass);
            $res = $user->update();
            if ($res) {
                Yii::$app->session->set('success', true);
            }
        }

        if ($profile = ($profileForm->load(Yii::$app->request->post()) && $profileForm->validate())) {
            $user->name = $profileForm->name;
            $res = $user->update();
            if ($res) {
                Yii::$app->session->set('profile_success', true);
            }
        }
        
        if (($reset || $profile) || ($reset && $profile)) {
            return Yii::$app->getResponse()->redirect(Url::toRoute(['/profile']));
        }
        
        $roles = Yii::$app->authManager->getRolesByUser($user->id);

        return $this->render('index', [
            'model' => $form,
            'profileModel' => $profileForm,
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function actionSearchlog() {
        $dataProvider = new ActiveDataProvider([
            'query' => Searchlogs::find()->where(['user_id' => Yii::$app->user->id]),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        return $this->render('searchlog', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionNotifications() {
        $user = Users::findOne(['id' => Yii::$app->user->id]);

        if (Yii::$app->request->isAjax && Yii::$app->request->post()) {
            $response = 0;

            $checked = Yii::$app->request->post('checked');

            $user = Users::findOne(['id' => Yii::$app->user->id]);
            if (!empty($user)) {
                $user->telegram_notice = $checked;
                if ($user->update() !== false) {
                    $response = 1;
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => $response,
            ];
        }
        
        return $this->render('notifications', [
            'user' => $user
        ]);
    }
    
}