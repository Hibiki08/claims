<?php

namespace app\components;

use yii\base\Widget;

class TrackingMenu extends Widget {

    public function run() {
        return $this->render('tracking_menu');
    }

}