<?php

namespace app\components;

use Yii;
use yii\base\Component;
use Telegram\Bot\Api;
use app\models\Users;


class MonocleMentionBot extends Component {

    private $api;
    public $userId = null;
    public $text = null;

    public function __construct() {
        parent::__construct();
        $this->api = new Api(Yii::$app->params['telegramBotToken']);
        $webhook = $this->api->getWebhookUpdates();
        $this->userId = $webhook->getMessage() ? $webhook->getMessage()->getChat()->getId() : null;
        $this->text = $webhook->getMessage() ? $webhook->getMessage()->getText() : null;
    }

    public function commandStart() {
        $this->api->sendMessage([ 'chat_id' => $this->userId, 'text' => Yii::t('app', 'start')]);
    }

    public function commandStop() {
        $user = Users::findOne(['telegram_id' => $this->userId]);
        if (!empty($user)) {
            $user->telegram_notice = 0;
            if ($user->update() !== false) {
                $this->api->sendMessage([ 'chat_id' => $this->userId, 'text' => Yii::t('app', 'stop')]);
            }
        }
    }

    public function commandKey() {
        $key = trim(str_replace('/key', '', $this->text));
        if (!empty($key)) {
            $user = Users::findOne(['telegram_key' => $key]);
            if (!empty($user)) {
                if (!empty($user->telegram_id)) {
                    $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_exist')]);
                } else {
                    $user->telegram_id = $this->userId;
                    if ($user->update() !== false) {
                        $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_ok')]);
                    } else {
                        $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_error_save')]);
                    }
                }
            } else {
                $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_wrong')]);
            }
        } else {
            $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_empty')]);
        }
    }

}