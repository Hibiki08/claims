<?php

namespace app\components;

use yii\base\Widget;

class ProfileMenu extends Widget {

    public function run() {
        return $this->render('profile_menu');
    }

}