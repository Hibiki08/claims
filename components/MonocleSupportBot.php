<?php

namespace app\components;

use app\models\SupportMessage;
use app\models\Ticket;
use Yii;
use yii\base\Component;
use Telegram;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\File;
use Telegram\Bot\Keyboard\Keyboard;
use app\models\Users;
use Telegram\Bot\Exceptions\TelegramSDKException;


class MonocleSupportBot extends Component {

    public $api;
    public $userId = null;
    public $text = null;
    public $photo = null;
    public $callbackData = null;

    const API_URL = 'https://api.telegram.org/';
    const IMG_PATH = 'resources/images/tickets/';

    public function __construct() {
        parent::__construct();
        $token = Yii::$app->params['monocleSupportBotToken'];
        $this->api = new Api($token);
        $webhook = $this->api->getWebhookUpdates();
        $message = $webhook->getMessage();
        $this->userId = $message ? $message->getChat()->getId() : null;
        $this->text = $message ? $message->getText() : null;
        $this->photo = ($message && $message->has('photo')) ? $message->getPhoto() : null;
        if ($webhook->isType('callback_query')) {
            $query = $webhook->getCallbackQuery();
            $this->userId = $query->getFrom()->getId();
            $this->callbackData = $query->getData();
        }
    }

    public function commandStart() {
        $this->api->sendMessage([ 'chat_id' => $this->userId, 'text' => Yii::t('app', 'start')]);
    }

    public function keyboardStartChat($ticketId, $chatId, $text) {
        $keyboard = [
            ['Начать чат (тикет #' . $ticketId . ')', 'Игнорировать'],
            ['Закрыть тикет #' . $ticketId, 'Тикеты без ответа'],
        ];

        $reply_markup = $this->api->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        $this->api->sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'html'
        ]);
    }

    public function commandStartChat($ticketId) {
        if (!empty($ticketId)) {
            $admin = Users::findOne(['telegram_id' => $this->userId]);
            if (!empty($admin)) {
                $openTicket = Ticket::getOpenTicketByAdmin($admin->id);
                if (!empty($openTicket)) {
                    $keyboard = Keyboard::make()
                        ->inline()
                        ->row(
                            Keyboard::inlineButton(['text' => 'Закрыть тикет #' . $openTicket->id, 'callback_data' => 'Закрыть тикет #' . $openTicket->id])
                        );
                    $this->api->sendMessage([
                        'chat_id' => $this->userId,
                        'text' => 'Вы не можете начать чат. У вас есть не закрытый тикет #' . $openTicket->id,
                        'reply_markup' => $keyboard
                    ]);
                } else {
                    $ticket = Ticket::findOne(['id' => $ticketId, 'closed' => 0]);
                    if (!empty($ticket)) {
                        if (empty($ticket->admin_id)) {
                            $ticket->admin_id = $admin->id;
                            if ($ticket->update() !== false) {
                                $this->api->sendMessage([
                                    'chat_id' => $this->userId,
                                    'text' => 'Теперь вы можете написать ответ.'
                                ]);

                                $supportUsers = Users::getUsersWithSupportRole();
                                if (!empty($supportUsers)) {
                                    foreach ($supportUsers as $supportUser) {
                                        if (!empty($supportUser->telegram_id)) {
                                            try {
                                                $text = 'Тикет #' . $ticketId . ' взят в обработку' . "\r\n" .
                                                    'Оператор: ' . (!empty(trim($admin->name)) ? $admin->name : $admin->login);
                                                $this->api->sendMessage([
                                                    'chat_id' => $supportUser->telegram_id,
                                                    'text' => $text
                                                ]);
                                            } catch (TelegramSDKException $e) {

                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $this->api->sendMessage([
                                'chat_id' => $this->userId,
                                'text' => 'Другой оператор уже ведёт этот тикет.',
                                'reply_markup' => $this->api->replyKeyboardHide()
                            ]);
                        }
                    } else {
                        $this->api->sendMessage([
                            'chat_id' => $this->userId,
                            'text' => 'Этот тикет уже закрыт.',
                            'reply_markup' => $this->api->replyKeyboardHide()
                        ]);
                    }
                }
            } else {
                $this->api->sendMessage([
                    'chat_id' => $this->userId,
                    'text' => 'В базе нет вашего id'
                ]);
            }
        } else {
            $this->api->sendMessage([
                'chat_id' => $this->userId,
                'text' => 'Вы не ввели номер тикета'
            ]);
        }
    }

    public function commandRejectChat() {
        $this->api->sendMessage([
            'chat_id' => $this->userId,
            'text' => 'Тикет отклонён.',
            'reply_markup' => $this->api->replyKeyboardHide()
        ]);
    }

    public function commandCloseTicket($ticketId) {
        if (!empty($ticketId)) {
            $admin = Users::findOne(['telegram_id' => $this->userId]);

            if (!empty($admin)) {
                $ticket = Ticket::getTicketById($ticketId, ['closed' => 0]);

                if (!empty($ticket)) {
                    if (!empty($ticket->admin_id)) {
                        if ($ticket->admin->id == $admin->id) {
                            $ticket->closed = 1;
                            if ($ticket->update() !== false) {
                                $this->api->sendMessage([
                                    'chat_id' => $this->userId,
                                    'text' => 'Тикет успешно закрыт.'
                                ]);
                            }
                        } else {
                            $this->api->sendMessage([
                                'chat_id' => $this->userId,
                                'text' => 'Вы не можете закрыть этот тикет. Его ведёт другой оператор.'
                            ]);
                        }
                    } else {
                        $this->api->sendMessage([
                            'chat_id' => $this->userId,
                            'text' => 'Вы не можете закрыть этот тикет. Сначала его нужно взять в обработку.'
                        ]);
                    }
                } else {
                    $this->api->sendMessage([
                        'chat_id' => $this->userId,
                        'text' => 'Тикет уже закрыт.'
                    ]);
                }
            } else {
                $this->api->sendMessage([
                    'chat_id' => $this->userId,
                    'text' => 'В базе нет вашего id'
                ]);
            }
        } else {
            $this->api->sendMessage([
                'chat_id' => $this->userId,
                'text' => 'Вы не ввели номер тикета'
            ]);
        }
    }
    
    public function commandHelp() {
        $text = '<b>Перечень доступных команд:</b>' . "\r\n" . "\r\n" .
            '<a href="/key">/key [ключ]</a> - начать получать уведомления' . "\r\n" .
            '<a href="/startticket">/startticket [номер]</a> - начать тикет (номер)' . "\r\n" .
            '<a href="/closeticket">/closeticket [номер]</a> - закрыть тикет (номер)';

        $this->api->sendMessage([
            'chat_id' => $this->userId,
            'text' => $text,
            'parse_mode' => 'html'
        ]);
    }

    public function commandReply() {
        $admin = Users::findByTelegramId($this->userId);
        $openTicket = Ticket::getOpenTicketByAdmin($admin->id);
        if (!empty($openTicket)) {
            $message = new SupportMessage();
            $message->support_message = $this->text;
            if (!empty($this->photo)) {
                $imgPath = $this->saveImage($openTicket->id);
                $message->support_img = $imgPath;
            }
            $message->ticket_id = $openTicket->id;
            if ($message->save()) {
                $openTicket->notice = 1;
                $openTicket->update();
            }
        } else {
            $this->api->sendMessage([
                'chat_id' => $this->userId,
                'text' => 'У вас нет открытых тикетов.'
            ]);
        }
    }

    public function commandTickets() {
        $tickets = Ticket::getTicketsWithoutReply();
        $admin = Users::findByTelegramId($this->userId);

        if (!empty($tickets)) {
            $text = '<i>Список доступных тикетов:</i>' . "\r\n" . "\r\n";
            foreach ($tickets as $ticket) {
                $user = (new Users())->findIdentity($ticket->user_id);
                $message = SupportMessage::getFirstTicketMessageById($ticket->id);

                $text .= '<b>Тикет #' . $ticket->id . '</b>' . "\r\n" .
                    'Тема: ' . $ticket->theme . "\r\n" .
                    'Сообщение: ' . $message->user_message . "\r\n" .
                    'Имя: ' . $user->name . "\r\n" .
                    'Email: ' . $user->login . "\r\n" . '---------------------' . "\r\n";
            }
            $this->api->sendMessage([
                'chat_id' => $admin->telegram_id,
                'text' => $text,
                'parse_mode' => 'html'
            ]);
        } else {
            $this->api->sendMessage([
                'chat_id' => $admin->telegram_id,
                'text' => 'Нет доступных тикетов',
                'parse_mode' => 'html'
            ]);
        }
    }

    public function commandStop() {}

    public function commandKey() {
        $key = trim(str_replace('/key', '', $this->text));
        if (!empty($key)) {
            $user = Users::findOne(['telegram_key' => $key]);
            if (!empty($user)) {
                if (!empty($user->telegram_id)) {
                    $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_exist')]);
                } else {
                    $user->telegram_id = $this->userId;
                    if ($user->update() !== false) {
                        $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_ok')]);
                    } else {
                        $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_error_save')]);
                    }
                }
            } else {
                $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_wrong')]);
            }
        } else {
            $this->api->sendMessage(['chat_id' => $this->userId, 'text' => Yii::t('app', 'key_empty')]);
        }
    }

    private function saveImage($ticketId) {
        $res = null;
        $bigSize = null;

        foreach ($this->photo as $photo) {
            $bigSize = $photo['file_id'];
        }
        if (!empty($bigSize)) {
            $img = $this->api->getFile([
                'file_id' => $bigSize
            ]);

            $url = self::API_URL . '/file/bot' . Yii::$app->params['monocleSupportBotToken'] . '/' . $img['file_path'];
            $pathinfo = pathinfo($url);
            $dirname = self::IMG_PATH . $ticketId . '/';

            if (!is_dir($dirname)) {
                mkdir($dirname, 0775, true);
            }

            $to = $dirname . $pathinfo['filename'] . '-' . time() . '.' . strtolower($pathinfo['extension']);
            $ch = curl_init($url);
            $fp = fopen($to, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_REFERER, $url);

            if (curl_exec($ch)) {
                $res = $to;
            }
            curl_close($ch);
            fclose($fp);
        }
        return $res;
    }

}