<?php
use yii\helpers\Url;

?>
<div class="sidebar-menu col-lg-3">
    <nav class="navbar navbar-default sidebar" >
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo Yii::$app->controller->route == 'recognition-requests/index' ? 'active' : ''; ?>"><a><span class="hidden-xs glyphicon glyphicon-list-alt"></span>Заявки</a>
                        <ul>
                            <li class="<?php echo Yii::$app->controller->route == 'recognition-requests/index' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/service/recognition/requests']); ?>">Нераспознанные</a></li>
                            <li class="<?php echo Yii::$app->controller->route == 'recognition-requests/empties' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/service/recognition/requests/empties']); ?>">Пустые</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo Yii::$app->controller->route == 'recognition-trademarks/index' ? 'active' : ''; ?>"><a><span class="hidden-xs glyphicon glyphicon-registration-mark"></span>Товарные знаки</a>
                        <ul>
                            <li class="<?php echo Yii::$app->controller->route == 'recognition-trademarks/index' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/service/recognition/trademarks']); ?>">Нераспознанные</a></li>
                            <li class="<?php echo Yii::$app->controller->route == 'recognition-trademarks/empties' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/service/recognition/trademarks/empties']); ?>">Пустые</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo Yii::$app->controller->route == 'recognition-tm-foreign/index' ? 'active' : ''; ?>"><a><span class="hidden-xs glyphicon glyphicon-list-alt"></span>ТЗ WIPO</a>
                        <ul>
                            <li class="<?php echo Yii::$app->controller->route == 'recognition-tm-foreign/index' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/service/recognition/tm-foreign']); ?>">Нераспознанные</a></li>
                            <li class="<?php echo Yii::$app->controller->route == 'recognition-tm-foreign/empties' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/service/recognition/tm-foreign/empties']); ?>">Пустые</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>