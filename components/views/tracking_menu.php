<?php
use yii\helpers\Url;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
?>
<div class="sidebar-menu col-lg-3">
    <nav class="navbar navbar-default sidebar" >
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($controller == 'tracking' && $action == 'index') ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/tracking']); ?>"><span class="hidden-xs showopacity glyphicon glyphicon-search"></span>Запросы</a></li>
                    <li class="<?php echo ($controller == 'tracking' && $action == 'requests') ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/tracking/requests']); ?>"><span class="hidden-xs glyphicon glyphicon-list-alt"></span>Заявки</a></li>
                    <li class="<?php echo ($controller == 'tracking' && $action == 'trademarks') ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/tracking/trademarks']); ?>"><span class="hidden-xs glyphicon glyphicon-registration-mark"></span>Товарные знаки</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>