<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;

?>
    <div class="parent-chat-widget" data-ticket-id="<?php echo !empty($ticket) ? $ticket->id : ''; ?>">
        <div class="chat-widget col-md-5">
            <div class="panel panel-primary">
                <div class="panel-heading" id="accordion">
                    <span class="glyphicon glyphicon-comment"></span> <?php echo !empty($ticket) ? 'Тикет #' . $ticket->id : 'Помощь патентного поверенного'; ?>
                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-default btn-xs arrow" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <span class="glyphicon glyphicon-chevron-up"></span>
                        </a>
                    </div>
                </div>
                <div class="panel-collapse collapse" id="collapseOne">
                    <div class="panel-body" >
                        <?php Pjax::begin([
                            'id' => 'pjax-chat',
                            'timeout' => false,
                            'scrollTo' => false,
                            'clientOptions' => ['backdrop' => false]
                        ]); ?>
                        <ul class="chat" data-user_name="<?php echo Yii::$app->user->identity->name; ?>">
                            <?php if (!empty($ticket->messages)) {
                                foreach ($ticket->messages as $message) {
                                    if (!empty($message->user_message)) { ?>
                                        <li class="left clearfix">
                                            <span class="chat-img pull-left">
                                                <img src="http://placehold.it/50/dff0d8/797979&text=ME" alt="User Avatar" class="img-circle" />
                                            </span>
                                            <div class="chat-body clearfix">
                                                <div class="header">
                                                    <strong class="primary-font"><?php echo Yii::$app->user->identity->name; ?></strong> <small class="pull-right text-muted">
                                                        <span class="glyphicon glyphicon-time"></span><?php echo Yii::$app->formatter->asDate($message->time, 'dd.MM.yyyy H:m:s'); ?></small>
                                                </div>
                                                <p><?php echo $message->user_message; ?></p>
                                                <?php if (!empty($message->user_file)) {?>
                                                    <?php $fileName = explode('/', $message->user_file); ?>
                                                    <div class="file">
                                                        <a data-pjax=0 href="/<?php echo $message->user_file; ?>"><span class="glyphicon glyphicon-file"></span><?php echo array_pop($fileName); ?></a>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </li>
                                    <?php } ?>
                                    <?php if (!empty($message->user_img)) { ?>
                                        <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="http://placehold.it/50/dff0d8/797979&text=ME" alt="User Avatar" class="img-circle" />
                                    </span>
                                            <div class="chat-body clearfix">
                                                <div class="header">
                                                    <strong class="primary-font"><?php echo Yii::$app->user->identity->name; ?></strong> <small class="pull-right text-muted">
                                                        <span class="glyphicon glyphicon-time"></span><?php echo Yii::$app->formatter->asDate($message->time, 'dd.MM.yyyy H:m:s'); ?></small>
                                                </div>
                                                <div class="user-img img-thumbnail">
                                                    <a data-fancybox="gallery" href="/<?php echo $message->user_img; ?>"><img src="/<?php echo $message->user_img; ?>"></a>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                    <?php if (!empty($message->support_message)) { ?>
                                        <li class="right clearfix">
                                            <span class="chat-img pull-right support"></span>
                                            <div class="chat-body clearfix">
                                                <div class="header">
                                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo Yii::$app->formatter->asDate($message->time, 'dd.MM.yyyy H:m:s'); ?></small>
                                                    <strong class="pull-right primary-font">Помощник Монокля</strong>
                                                </div>
                                                <p><?php echo $message->support_message; ?></p>
                                            </div>
                                        </li>
                                    <?php } ?>
                                    <?php if (!empty($message->support_img)) { ?>
                                        <li class="right clearfix">
                                    <span class="chat-img pull-right">
                                        <img src="http://placehold.it/50/d9edf7/797979&text=S" alt="User Avatar" class="img-circle" />
                                    </span>
                                            <div class="chat-body clearfix">
                                                <div class="header">
                                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo Yii::$app->formatter->asDate($message->time, 'dd.MM.yyyy H:m:s'); ?></small>
                                                    <strong class="pull-right primary-font">Помощник Монокля</strong>
                                                </div>
                                                <div class="support-img img-thumbnail">
                                                    <a data-fancybox="gallery" href="/<?php echo $message->support_img; ?>"><img src="/<?php echo $message->support_img; ?>"></a>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            <?php } else { ?>
                                <p class="empty-chat">Отправьте сообщение, чтобы начать чат.</p>
                            <?php } ?>
                        </ul>
                        <?php if (!empty($ticket)) { ?>
                            <?php if ($ticket->notice) {?>
                            <script type="text/javascript">
                                $('#collapseOne').addClass('show');
                                $('.chat-widget a.arrow > span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                                var ticketId = $('.parent-chat-widget ul.chat').data('ticket-id');
                                $('input[name=ticket-id]').val(ticketId);
                            </script>
                        <?php } ?>
                            <script type="text/javascript">
                                var ticketId = $('.parent-chat-widget ul.chat').data('ticket-id');
                                $('input[name=ticket-id]').val(ticketId);
                            </script>
                        <?php } ?>
                        <?php Pjax::end(); ?>
                    </div>
                    <div class="panel-footer">
                        <!--                    --><?php //Pjax::begin([
                        //                        'id' => 'pjax-form',
                        //                        'timeout' => false,
                        //                        'scrollTo' => false,
                        //                        'enablePushState' => false
                        //                    ]); ?>
                        <?php echo Html::beginForm('', 'post', ['accept-charset' => 'UTF-8', 'enctype' => 'multipart/form-data', 'data-pjax' => true, 'id' => 'chat-form']); ?>
                        <div class="input-group">
                            <?php echo Html::input('text', 'message', '', [
                                'class' => 'form-control input-sm',
                                'id' => 'supportform-message',
                                'placeholder' => 'Введите текст здесь...',
                                'autocomplete' => 'off'
                            ]); ?>
                            <div class="upload-file">
                                <div class="form-group">
                                    <span class="glyphicon glyphicon-download-alt"></span>
                                    <label class="control-label" for="supportform-file">Файл или скриншот</label>
                                    <?php echo Html::fileInput('file', '', [
                                        'id' => 'supportform-file',
                                        'aria-invalid' => false,
                                        'disabled' => (!isset($ticket->messages) ? true : false)
                                    ]); ?>
                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                            <?php echo Html::hiddenInput('ticket-id', (!empty($ticket) ? $ticket->id : '')); ?>
                            <?php echo Html :: hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []); ?>
                            <span class="input-group-btn">
                            <?php echo Html::submitButton('Отправить', [
                                'class' => 'btn btn-warning btn-sm',
                                'id' => 'btn-chat'
                            ]); ?>
                        </span>
                        </div>
                        <?php echo Html::endForm(); ?>
                        <!--                    --><?php //Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php //} ?>