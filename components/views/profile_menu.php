<?php
use yii\helpers\Url;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
?>
<div class="sidebar-menu col-lg-3">
    <nav class="navbar navbar-default sidebar" >
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($controller == 'profile' && $action == 'index') ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/profile']); ?>"><span class="hidden-xs showopacity glyphicon glyphicon-user"></span>Профиль</a></li>
                    <li class="<?php echo ($controller == 'subscription' && $action == 'index') ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/profile/subscription/']); ?>"><span class="hidden-xs glyphicon glyphicon-rub"></span>Подписка</a>
                        <ul>
                            <li class="<?php echo ($controller == 'subscription' && $action == 'payments') ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/profile/subscription/payments']); ?>">История платежей</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo $action == 'searchlog' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/profile/searchlog']); ?>"><span class="hidden-xs showopacity glyphicon glyphicon-search"></span>История поиска</a></li>
                    <?php  if (Yii::$app->user->can('clientExtendedPermission')) { ?>
                    <li class="<?php echo $action == 'notifications' ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/profile/notifications']); ?>"><span class="hidden-xs glyphicon glyphicon-bell"></span>Уведомления</a></li>
                    <?php } ?>
                    <?php if (Yii::$app->user->can('clientExtendedPermission') || Yii::$app->user->id == 36 || Yii::$app->user->id == 37 || Yii::$app->user->id == 38) {?>
                    <li class="<?php echo ($controller == 'support' && $action == 'index') ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/profile/support']); ?>"><span class="hidden-xs glyphicon glyphicon-envelope"></span>Поддержка</a>
                        <ul>
                            <li class="<?php echo ($controller == 'support' && $action == 'archive') ? 'active' : ''; ?>"><a href="<?php echo Url::to(['/profile/support/archive']); ?>">Архив тикетов</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
</div>