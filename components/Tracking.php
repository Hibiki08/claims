<?php

namespace app\components;

use app\models\RequestTracking;
use app\models\TrademarkTracking;
use Yii;
use yii\base\Widget;
use app\models\SearchTracking;

class Tracking extends Widget {

    public function run() {
        $searchCount = SearchTracking::getCount();
        $requestCount = RequestTracking::getCount();
        $trademarkCount = TrademarkTracking::getCount();

        $count = $searchCount + $requestCount + $trademarkCount;

        return $this->render('tracking', [
            'count' => $count
        ]);
    }

}