<?php

namespace app\components;

use yii\base\Component;


class GoogleApi extends Component {

    public $url = 'https://www.googleapis.com/customsearch/v1';
    public $apiKey = '';
    public $cx = '';
    public $query = '';

    private function execute($request) {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $request);
        return $res->getBody();
    }

    private function request($params = []) {
        $request = $this->url . '?' . 'key=' . $this->apiKey . '&cx=' . $this->cx . '&q=' . $this->query;
        foreach ($params as $key => $param) {
            if (is_array($param)) {
                $request .= '&' . $key . '=';
                foreach ($param as $v) {
                    $request .= $v . ',';
                }
                $request = substr($request, 0, -1);
            } else {
                $request .= '&' . $key . '=' . $param;
            }
        }
        return $this->execute($request);
    }

    public function getQueryCorrected() {
        $query = \GuzzleHttp\json_decode($this->request());
        if (isset($query->spelling->correctedQuery)) {
            return $query->spelling->correctedQuery;
        }
        return null;
    }

}