<?php

namespace app\components;

use yii\base\Widget;

class LogMenu extends Widget {

    public function run() {
        return $this->render('log_menu');
    }

}