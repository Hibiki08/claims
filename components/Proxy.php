<?php

namespace app\components;

use yii\base\Component;


class Proxy extends Component {

    public $url = 'http://api.best-proxies.ru/proxylist.';
    public $key;
    public $format = 'txt';
    public $speed = [1, 2];
    public $type = [
        'http',
        'https'
    ];
    public $level = [1, 2];
    public $countries = [
        'cn'
    ];
    public $uptime = 2;
    public $unique = 1;
    public $limit = 0;
    public $exclude = 1;

    public function setFormat($format) {
        $this->format = $format;
    }

    private function request($params) {
        $request = $this->url . $this->format . '?' . 'key=' . $this->key;
        foreach ($params as $key => $param) {
            if (is_array($param)) {
                $request .= '&' . $key . '=';
                foreach ($param as $v) {
                    $request .= $v . ',';
                }
                $request = substr($request, 0, -1);
            } else {
                $request .= '&' . $key . '=' . $param;
            }
        }
        return $this->execute($request);
    }

    private function execute($request) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $request);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_FAILONERROR, 1);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function getProxyList() {
        $params = [
            'speed' => $this->speed,
            'type' => $this->type,
            'level' => $this->level,
            'limit' => $this->limit,
            'country' => $this->countries,
            'exclude' => ($this->exclude ? 1 : 0),
            'uptime' => $this->uptime,
            'unique' => $this->unique
        ];
        return $this->request($params);
    }

}