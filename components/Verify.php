<?php
namespace app\components;

use Yii;
use yii\base\Behavior;
use yii\web\ForbiddenHttpException;
use yii\web\Controller;

class Verify extends Behavior {

    public $actions = null;

    public function events() {
        return [
            Controller::EVENT_BEFORE_ACTION => 'access'
        ];
    }

    public function access() {
        foreach($this->actions as $action){
            if($this->owner->action->id == $action){
                if (!\Yii::$app->user->can('adminPermission')) {
                    throw new ForbiddenHttpException('Доступ закрыт.');
                }
            }
        }
    }

}