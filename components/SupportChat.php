<?php

namespace app\components;

use Yii;
use app\models\Ticket;
use yii\base\Widget;


class SupportChat extends Widget {

    public function run() {
        $ticket = [];
        if (!Yii::$app->user->isGuest) {
            $ticket = Ticket::getLastUsersTicket();
        }

        return $this->render('support_chat', [
            'ticket' => $ticket
        ]);
    }

}