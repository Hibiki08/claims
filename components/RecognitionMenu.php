<?php

namespace app\components;

use yii\base\Widget;

class RecognitionMenu extends Widget {

    public function run() {
        return $this->render('recognition_menu');
    }

}