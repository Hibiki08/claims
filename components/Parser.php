<?php

namespace app\components;

use app\models\Trademarks;
use Mpdf\Tag\Em;
use Yii;
use yii\base\Component;
use app\models\Requests;
use app\models\Statuses;
use app\models\prom\Status;
use app\models\TrademarkClasses;
use app\models\RequestClasses;
use app\models\prom\PatentImage;
use app\models\prom\Classes;
use app\models\TmForeignClass;
use app\models\Country;
use app\models\TmForeignCountry;
use app\models\prom\PatentClass;
use app\models\ColorsShade;
use app\models\ColorsAndColorsShade;
use app\models\TrademarkColors;
use app\models\TmType;
use app\models\KindOfMark;
use app\models\ForeignStatus;


class Parser extends Component {

    static $browsers = [
        'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0',
        'Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko',
        'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)',
        'Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.9.0.16) Gecko/2009122206 Firefox/3.0.16 Flock/2.5.6',
        'Mozilla/5.0 (X11; U; Linux x86_64; es-AR; rv:1.9.0.2) Gecko/2008091920 Firefox/3.0.2 Flock/2.0b3',
        'Mozilla/5.0 (compatible; Konqueror/4.2) KHTML/4.2.4 (like Gecko) Fedora/4.2.4-2.fc11',
        'Mozilla/5.0 (X11) KHTML/4.9.1 (like Gecko) Konqueror/4.9',
        'Mozilla/4.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0)',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; chromeframe/12.0.742.100)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36'
    ];
    public static $proxy;
    public static $timeout = 4500;
//    public static $error = 0;
    private $errorText;
    private $errorCode;

    const REQUEST_URL = 'http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTMAP&rn=6932&TypeFile=html&DocNumber=';
    const TRADEMARK_URL = 'http://www1.fips.ru/fips_servl/fips_servlet?DB=RUTM&TypeFile=html&DocNumber=';
    const WELLKNOWN_TM_URL = 'http://www1.fips.ru/fips_servl/fips_servlet?DB=WKTM&rn=405&TypeFile=html&DocNumber=';
    const PATENT_URL = 'http://www1.fips.ru/fips_servl/fips_servlet?DB=RUDE&rn=1065&TypeFile=html&DocNumber=';
    const TM_FOREIGN_PAGE_URL = 'https://www.tmdn.org/tmview/search-tmv?_search=false&rows=100&sidx=ad&sord=asc&q=tp%3ARU+AND+oc%3AWO&fq=%5B%5D&pageSize=100&facetQueryType=2&selectedRowRefNumber=null&providerList=null&expandedOffices=null&page=';
    const TM_FOREIGN_URL = 'https://www.tmdn.org/tmview/get-detail?st13=';
    
    const LOG_PARSER_REQUEST_TAG = '/runtime/logs/parser_request_tag.log';
    const LOG_PARSER_TRADEMARK_TAG = '/runtime/logs/parser_tm_tag.log';
    const LOG_PARSER_TM_FOREIGN_TAG = '/runtime/logs/parser_tm_foreign_tag.log';
    const LOG_PARSER_PATENT = '/runtime/logs/parser_patent.log';
    const LOG_PARSER_REQUEST = '/runtime/logs/parser_request.log';
    const LOG_PARSER_TRADEMARK = '/runtime/logs/parser_trademark.log';
    const LOG_PARSER_TRADEMARK_MISSING = '/runtime/logs/parser_trademark_missing.log';
    const LOG_PARSER_TM_FOREIGN = '/runtime/logs/tm_foreign.log';
    const IMG_PATCH = 'resources/images/';
    const ERROR_FAST_SEEN = 1;
    const ERROR_BAN = 2;
    const ERROR_LIMIT = 3;
    const ERROR_DOC_NOT_EXISTS = 4;
    const ERROR_BAD_REQUEST = 5;
    const ERROR_DOCUMENT = 6;
    const ERROR_DOC_NOT_FOUND = 7;
    const ERROR_UNKNOWN = 8;
    const LIMIT_ERROR = 5;
    const FOREIGN_STATUS_ERROR = 5;

    public function contentCorrect($content) {
        if (!empty($content)) {
            if (strstr($content, 'Слишком быстрый просмотр документов.')) {
                $this->errorText = 'Слишком быстрый просмотр документов.';
                $this->errorCode = self::ERROR_FAST_SEEN;
            }
            elseif (strstr($content, 'Вы заблокированы до')) {
                $this->errorText = 'Забанили ip-адрес.';
                $this->errorCode = self::ERROR_BAN;
            }
            elseif (strstr($content, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
                $this->errorText = 'Забанили ip-адрес до конца дня';
                $this->errorCode = self::ERROR_LIMIT;
            }
            elseif (strstr($content, 'Документ с данным номером отсутствует')) {
                $this->errorText = 'Документ с данным номером отсутствует';
                $this->errorCode = self::ERROR_DOC_NOT_EXISTS;
            }
            elseif (strstr($content, 'Документ не найден')) {
                $this->errorText = 'Документ не найден';
                $this->errorCode = self::ERROR_DOC_NOT_FOUND;
            }
            elseif (strstr($content, 'Bad Request')) {
                $this->errorText = 'Bad Request';
                $this->errorCode = self::ERROR_BAD_REQUEST;
            }
            elseif (strlen($content) < 150){
                $this->errorText = 'Неизвестная ошибка';
                $this->errorCode = self::ERROR_UNKNOWN;
            }

            if (!empty($this->errorCode)) {
                return false;
            }
            return true;
        } else {
            $this->errorText = 'Ошибка чтения документа';
            $this->errorCode = self::ERROR_DOCUMENT;
        }
        return false;
    }

    public function getContent($url) {
        $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
        foreach ($proxyList as $val) {
            $proxy = trim($val);
            if (!empty($proxy)) {
                $extraction = self::extract($url, $proxy);
                if ($contentCorrect = $this->contentCorrect($extraction)) {
                    Parser::$proxy = $proxy;
                    return $extraction;
                }
            }
        }
        return null;
    }

    public function getErrorText() {
        return $this->errorText;
    }

    public function getErrorCode() {
        return $this->errorCode;
    }
    
    public function writeLog($logPath, $text) {
        file_put_contents(Yii::$app->basePath . $logPath, $text, FILE_APPEND );        
    }

    public static function extract($url, $proxy) {
        $browsers = static::$browsers[mt_rand(0, 14)];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $browsers);

        curl_setopt ($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt ($ch, CURLOPT_PROXY, $proxy);
        curl_setopt ($ch, CURLOPT_PROXYTYPE, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt ($ch, CURLOPT_FAILONERROR, true);

        $res = curl_exec($ch);
        curl_close($ch);

        return (mb_convert_encoding($res, 'UTF-8', 'cp1251'));
    }

    public static function extractStatusString($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);
        if ($from) {
            $to = stripos($extraction, '</TD>', $from);
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $res = trim(strip_tags($string));
        }
        return $res;
    }

    public static function extractPatentStatusString($extraction) {
        $res = null;
        $from = stripos($extraction, '<td id="StatusR">');
        if ($from) {
            $to = stripos($extraction, '(последнее изменение статуса:', $from);
            $string = substr($extraction, $from, $to - $from);
            $res = trim(strip_tags($string));
        }
        return $res;
    }

    public static function getStatusID($string) {
        $title = addslashes(trim(strip_tags($string), ":- \t\n\r\0\x0B"));
        return (new Statuses())->getStatusID($title);
    }

    public static function getPatentStatusID($string) {
        $title = addslashes(trim(strip_tags($string), ":- \t\n\r\0\x0B"));
        return (new Status())->getStatusID($title);
    }

    public static function extractStatusDate($string) {
        $res = null;
        preg_match('/\d{2}.\d{2}.\d{4}/', $string, $match);
        $dates = explode('.', empty($match[0]) ? '' : $match[0]);
        if (count($dates) === 3 && self::validateDate($match[0], 'd.m.Y')) {
            $res = $dates[2] . '-' . $dates[1] . '-' . $dates[0];
        }

        return $res;
    }

    public static function assignColorById($tm_id) {
        $tm = Trademarks::findOne($tm_id);
        if (!empty($tm)) {
            if (!empty($tm->colors)) {
                $arrColors = explode(',', $tm->colors);

                foreach ($arrColors as $colorName) {
                    $shadeId = ColorsShade::findOne(['name' => trim($colorName)]);
                    if (!empty($shadeId)) {
                        $mainColors = ColorsAndColorsShade::find()
                            ->where(['color_shade_id' => $shadeId])
                            ->all();

                        if (!empty($mainColors)) {
                            $values = '';
                            foreach ($mainColors as $mainColor) {
                                $values .= '(' . $tm_id . ', ' . $mainColor['color_id'] . '),';
                            }
                            $values = substr($values, 0, -1);
                            $query = 'insert into ' . TrademarkColors::tableName() . ' (trademark_id, color_id) values ' . $values . ' on duplicate key update trademark_id=trademark_id, color_id=color_id';
                            if (Yii::$app->db->createCommand($query)->execute() === false) {
                                break;
                            }
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    public static function extractNumber($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);
        if ($from) {
            $to = stripos($extraction, '</B>', $from);
            $sstring = substr($extraction, $from, $to - $from);
            $sstring = strip_tags($sstring);
            $res = preg_replace('/[^0-9\/]/', '', $sstring);
        }
        return $res;
    }

    public static function extractDate($extraction, $needle, $last = false) {
        $res = null;
        if ($last) {
            $from = strripos($extraction, $needle);
        } else {
            $from = stripos($extraction, $needle);
        }
        if ($from) {
            $to = stripos($extraction, '</B>', $from);
            $string = substr($extraction, $from, $to - $from);
            preg_match('/\d{2}\.\d{2}\.\d{4}/', $string, $match);
            $dates = explode('.', empty($match[0]) ? '' : $match[0]);
            if (sizeof($dates) === 3 && self::validateDate($match[0], 'd.m.Y')) {
                $res = $dates[2] . '-' . $dates[1] . '-' . $dates[0];
            }
        }
        return $res;
    }

    public static function extractPublication($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);
        if ($from) {
            $to = stripos($extraction, '</B>', $from);
            $sstring = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $sstring, $match);
            $res = addslashes(serialize ($match['href']));
        }
        return $res;
    }

    public static function extractString($extraction, $needle, $last = false) {
        $res = null;
        if ($last) {
            $from = strripos($extraction, $needle);
        } else {
            $from = stripos($extraction, $needle);
        }
        if ($from) {
            $to = stripos($extraction, '</B>', $from);
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $res = addslashes(trim(strip_tags($string)));
        }
        return $res;
    }

    public static function extractForeignString($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);

        if ($from) {
            $to = stripos($extraction, '</td>', $from + strlen($needle));
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $res = addslashes(trim(strip_tags($string)));
        }
        return $res;
    }

    public static function extractForeignDate($extraction, $needle) {
        $from = strripos($extraction, $needle);
        if ($from) {
            $to = stripos($extraction, '</td>', $from + strlen($needle));
            $string = substr($extraction, $from, $to - $from);
            preg_match('/\d{4}-\d{2}-\d{2}/', $string, $match);
            $dates = explode('-' , empty($match[0]) ? '' : $match[0]);
            if (count($dates) === 3 && self::validateDate($match[0], 'Y-m-d')) {
                $contractDate = $dates[0] . '-' . $dates[1] . '-' . $dates[2];
                return $contractDate;
            }
        }
        return null;
    }

    public static function extractForeignAddress($extraction, $needle) {
        $from = strripos($extraction, $needle);
        $addressAr = [];
        if ($from !== false) {
            $result = mb_substr($extraction, $from);
            if ($result !== false) {
                $addressAr['id'] = Parser::extractForeignString($result, 'Applicant identifier</td>');
                $addressAr['name'] = self::extractForeignString($result, '<span class="FreeFormatNameLine">');
                $addressAr['nation'] = Parser::extractForeignString($result, '(811)</span>Entitlement nationality</td>');
                $addressAr['country'] = Parser::extractForeignString($result, 'Address country</td>');
                $addressAr['address'] = Parser::extractForeignString($result, '<td class="size25 bold">Address</td>');

                return implode(' ', $addressAr);
            }
        }
        return null;
    }

    public static function extractCountries($extraction, $needle) {
        $res = [];
        $from = stripos($extraction, $needle);

        if ($from) {
            $to = stripos($extraction, '</td>', $from + strlen($needle));
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $resString = addslashes(trim(strip_tags($string)));
            $res = explode('-', $resString);
        }
        return $res;
    }
    
    public static function extractTypeId($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);

        if ($from) {
            $to = stripos($extraction, '</td>', $from + strlen($needle));
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $resString = addslashes(trim(strip_tags($string)));
            $type = TmType::findOne(['name' => $resString]);
            if (empty($type)) {
                $type = new TmType();
                $type->name = $resString;
                $type->save();
            } 
            $res = $type->id;
        }
        return $res;
    }

    public static function extractKindId($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);

        if ($from) {
            $to = stripos($extraction, '</td>', $from + strlen($needle));
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $resString = addslashes(trim(strip_tags($string)));
            $kind = KindOfMark::findOne(['name' => $resString]);
            if (empty($kind)) {
                $kind = new KindOfMark();
                $kind->name = $resString;
                $kind->save();
            }
            $res = $kind->id;
        }
        return $res;
    }
    
    public static function extractForeignClasses($extraction, $needle) {
        $res = [];
        $from = stripos($extraction, $needle);

        if ($from) {
            $to = stripos($extraction, '</td>', $from + strlen($needle));
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $resString = addslashes(trim(strip_tags($string)));
            $res = explode(',', $resString);
        }
        return $res;
    }

    public static function extractForeignStatusId($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);

        if ($from) {
            $to = stripos($extraction, '</td>', $from + strlen($needle));
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $resString = addslashes(trim(strip_tags($string)));
            $status = ForeignStatus::findOne(['title' => $resString]);
            if (!empty($status)) {
                $res = $status->id;
            } else {
                $res = self::FOREIGN_STATUS_ERROR;
            }
        }
        return $res;
    }

    public static function extractForeignImg($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);
        if ($from) {
            $to = stripos($extraction, '</tbody>', $from + strlen($needle));
            $sstring = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match('/<img[^>]*?src=(["\']?(?<src>[^"\'\s>]+)["\'][^>]*?)>/is', $sstring, $result);
            if (isset($result['src'])) {
                $res = $result['src'];
            }
        }
        return $res;
    }

    public static function saveForeignImage($imgPath, $id) {
        if ($imgPath) {
            $dirname = self::IMG_PATCH . 'tm_foreign/';
            $to = dirname(__DIR__) . '/web/' . $dirname . $id . '.jpg';

            if (!is_dir($dirname)) {
                mkdir($dirname, 0775, true);
            }

            exec(dirname(__DIR__) . '/bin/phantomjs/bin/phantomjs ' . dirname(__DIR__) . '/web/js/get-img.js \'' . $imgPath . '\' \'' . $to . '\' \'' . $browsers = Parser::$browsers[mt_rand(0, 14)] . '\' 2>&1', $error);
            if (empty($error)) {
                return $dirname . $id . '.jpg';
            }
        }
        return null;
    }
    
    public static function saveForeignClasses($classes, $tm_id) {
        foreach ($classes as $class) {
            $tm_class = TmForeignClass::find()
                ->where(['tm_id' => $tm_id])
                ->andWhere(['class_id' => intval($class)])
                ->one();

            if (empty($tm_class)) {
                $tmClass = new TmForeignClass();
                $tmClass->tm_id = $tm_id;
                $tmClass->class_id = intval($class);
                $tmClass->save(false);
            }
        }
    }

    public static function saveForeignCountries($countries, $tm_id) {
        foreach ($countries as $item) {
            $country = Country::findOne(['abbr' => $item]);

            if (empty($country)) {
                $country = new Country();
                $country->abbr = $item;
                $country->save();
            }
            
            $tm_country = TmForeignCountry::find()
                ->where(['tm_id' => $tm_id])
                ->andWhere(['country_id' => intval($country->id)])
                ->one();

            if (empty($tm_country)) {
                $tmCountry = new TmForeignCountry();
                $tmCountry->tm_id = $tm_id;
                $tmCountry->country_id = intval($country->id);
                $tmCountry->save(false);
            }
        }
    }

    public static function extractLastString($extraction, $needle) {
        $res = null;
        $from = strripos($extraction, $needle);
        if ($from) {
            $to = stripos($extraction, '</B>', $from);
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $res = trim(strip_tags($string));
        }
        return $res;
    }

    public static function extractFacsimile ($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);
        if ($from) {
            $to = strripos($extraction, '</A>', $from) + 4; //4 - the number of characters of the tag </a>
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match_all('/<a\s.*?href="(.+?)".*?>(.+?)<\/a>/i', $string, $result);

            $facsimile = !empty($result) ? $result[1] : [];
            $res = serialize($facsimile);
        }
        return $res;
    }

    public static function extractClasses($extraction, $needle) {
        $res = null;
        $from = stripos($extraction, $needle);
        if ($from) {
            $to = stripos($extraction, '<TABLE>', $from);
            $string = substr($extraction, $from + strlen($needle), $to - $from - strlen($needle));
            $sarr = explode('.</B>', trim($string));
            $res = [];
            foreach ($sarr as $el) {
                $el = trim(strip_tags($el));
                if (!is_null($el) && !empty($el)) {
                    $key = substr($el, 0, 2);
                    $val = substr($el, 2);
                    $res[$key] = trim($val, "- \t\n\r\0\x0B");
                }
            }
        }
        return $res;
    }

    public static function extractPatentClasses($extraction) {
        $res = null;
        $from = stripos($extraction, '<td id="td3">');
        if ($from) {
            $to = stripos($extraction, '</td>', $from);
            $string = substr($extraction, $from, $to - $from + strlen('</td>'));
            $str = preg_replace('#<br.*?>#s', '', $string);

            $classes = new \SimpleXMLElement($str);
            if (!empty($classes->a)) {
                $i = 0;
                foreach ($classes->a as $class) {
                    $classArr = explode('-', $class);
                    $res[$i]['parent_class'] = intval($classArr[0]);
                    $res[$i]['class'] = intval($classArr[1]);
                    $i++;
                }
            }
        }
        return $res;
    }

    public static function savePatentClasses($classes, $patent_id) {
        foreach ($classes as $class) {
            $classItem = Classes::getClass($class['parent_class'], $class['class']);
            if (!empty($classItem)) {
                $parent_id = $classItem->id;
                $exists = PatentClass::findOne(['patent_id' => $patent_id, 'class_id' => $parent_id]);
                if (empty($exists)) {
                    $patentClass = new PatentClass();
                    $patentClass->patent_id = $patent_id;
                    $patentClass->class_id = $parent_id;
                    if (!$patentClass->save()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static function extractTMClasses($s, $needle) {
        $res = null;
        $from = stripos($s, $needle);
        if ($from) {
            $to = stripos($s, '</P>', $from);
            $sstring = substr($s, $from + strlen($needle), $to - $from - strlen($needle));
            $sarr = explode('</b>', trim($sstring));
            $res = [];
            foreach ($sarr as $el) {
                $el = trim(strip_tags($el));
                if (!empty($el)) {
                    $to = stripos($el, '-');
                    $keys = explode(',', trim(substr($el, 0, $to)));
                    $val = trim(substr($el, $to), "- \t\n\r\0\x0B");
//                    $val = trim($val, "\xD0");
                    foreach ($keys as $key) {
                        $res[$key] = $val;
                    }
                }
            }
        }
        return $res;
    }

    public static function saveTMClasses($classes, $id) {
        foreach ($classes as $class => $text) {
//            $cids = [];
//            if (is_string($class)) {
//                $cids = explode(',', $class);
//            } else {
//                $cids[] = $class;
//            }
//            foreach ($cids as $cid) {
                $check = TrademarkClasses::find()
                    ->where(['trademark_id' => $id])
                    ->andWhere(['class' => intval($class)])
                    ->one();

                if (empty($check)) {
                    $tmClass = new TrademarkClasses();
                    $tmClass->trademark_id = $id;
                    $tmClass->class = intval($class);
                    $tmClass->save(false);
                }
//            }
        }
    }

    public static function saveRequestClasses($classes, $id) {
        $cids = [];
        foreach ($classes as $class => $text) {
            $cids[] = number_format($class);

            foreach ($cids as $cid) {
                $check = RequestClasses::find()
                    ->where(['request_id' => $id])
                    ->andWhere(['class' => intval($cid)])
                    ->one();

                if (empty($check)) {
                    $class = new RequestClasses();
                    $class->request_id = $id;
                    $class->class = intval($cid);
                    $class->save(false);
                }
            }
        }
    }

    public static function saveRequestImage($imgPath) {
        $res = null;
        if ($imgPath) {
            $browsers = static::$browsers[mt_rand(0, 14)];
            $pathinfo = pathinfo($imgPath);
            $requestNumber = $pathinfo['filename'];
            $imgName = substr($pathinfo['filename'], 8);
            $year = substr($requestNumber, 0, 4);

            $dirname = self::IMG_PATCH . $year . '/' . $requestNumber[4] . '/' . $requestNumber[5] . '/' . $requestNumber[6] . '/' . $requestNumber[7] . '/';

            if (!is_dir($dirname)) {
                mkdir($dirname, 0775, true);
            }
            $url = self::REQUEST_URL . $requestNumber;

            $to = $dirname . $imgName . '.' . strtolower($pathinfo['extension']);
            $ch = curl_init($imgPath);
            $fp = fopen($to, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_USERAGENT, $browsers);
            curl_setopt($ch, CURLOPT_REFERER, $url);

            if (curl_exec($ch)) {
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if($httpCode != 404) {
                    $res = $to;
                }
            };
            curl_close($ch);
            fclose($fp);
            return $res;
        }
        return null;
    }

    public static function saveTrademarkImage($imgPath, $regNumber) {
        $res = null;
        if ($imgPath) {
            $browsers = static::$browsers[mt_rand(0, 14)];
            $pathinfo = pathinfo($imgPath);
            preg_match('/\d{4}.\d{2}.\d{2}/', $imgPath, $match);
            $dates = explode('.', (empty($match[0]) ? '' : $match[0]));
            $year = '0000';
            $month = '00';
            $day = '01';
            if (count($dates) === 3) {
                $year = $dates[0];
                $month = $dates[1];
                $day = $dates[2];
            }
            $dirname = self::IMG_PATCH . 'trademarks/' . $year . '/' . $month . '/' . $day . '/' . $regNumber . '/';
            $to = $dirname . '1.' . strtolower($pathinfo['extension']);
            $url = self::TRADEMARK_URL . $regNumber;
            if (!is_dir($dirname)) {
                mkdir($dirname, 0775, true);
            }
            $ch = curl_init($imgPath);
            $fp = fopen($to, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_USERAGENT, $browsers);
            curl_setopt($ch, CURLOPT_REFERER, $url);

            if (curl_exec($ch)) {
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if($httpCode != 404) {
                    $res = $to;
                }
            };
            curl_close($ch);
            fclose($fp);
            return $res;
        }
        return null;
    }

    public static function savePatentImages(array $images, $id) {
        foreach ($images as $key => $image) {
            $browsers = static::$browsers[mt_rand(0, 14)];
            if (!empty($image['images'])) {
                foreach ($image['images'] as $k => $imgPath) {
                    $pathinfo = pathinfo($imgPath);
                    $dirname = $pathinfo['dirname'];
                    $imgName = $pathinfo['filename'];
                    $dirArr = explode('/', $dirname);
//                    $number = intval(array_pop($dirArr));

                    $dirImg = self::IMG_PATCH . 'patents/' . $id . '/';

                    if (!is_dir($dirImg)) {
                        mkdir($dirImg, 0775, true);
                    }
                    $url = self::PATENT_URL . $id;

                    $to = $dirImg . $imgName . '.' . strtolower($pathinfo['extension']);
                    $ch = curl_init($imgPath);
                    $fp = fopen($to, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_USERAGENT, $browsers);
                    curl_setopt($ch, CURLOPT_REFERER, $url);

                    if (curl_exec($ch)) {
                        $imgExists = PatentImage::findOne(['patent_id' => $id, 'path' => $to]);
                        if (empty($imgExists)) {
                            $patentImage = new PatentImage();
                            if ($k == 0) {
                                if (isset($image['description'])) {
                                    $patentImage->description = $image['description'];
                                }
                            }

                            $patentImage->path = $to;
                            $patentImage->patent_id = $id;
                            if (!$patentImage->save()) {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    };
                    curl_close($ch);
                    fclose($fp);
                }
            }
        }
        return true;
    }

    public static function extractImg($output, $needle) {
        $res = null;
        $from = stripos($output, $needle);
        if ($from) {
            $to = stripos($output, '<IMG', $from);
            $sstring = substr($output, $from + strlen($needle), $to - $from - strlen($needle));
            preg_match('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $sstring, $result);
            $res = $result[2];
        }
        return $res;
    }

    public static function extractPatentImages($output) {
        $images = [];
        $from = stripos($output, '<p id="B542">');
        if ($from) {
            $titAbs = strrpos($output, '<p class="TitAbs">', $from);
            if ($titAbs) {
                $to = stripos($output, '</p>', $titAbs);
            } else {
                $imgFrom = strrpos($output, '<img src', $from);
                if ($imgFrom) {
                    $to = stripos($output, '</a>', $imgFrom);
                } else {
                    $to = false;
                }
            }
            $string = substr($output, $from, $to - $from);

            if ($string) {
                $lines = preg_split('/<p/', $string);
                if (!empty($lines)) {
                    $i = 0;
                    foreach ($lines as $line) {
                        $res = stripos($line, 'class="izv">');
                        if ($res) {
                            $res_len = $res + strlen('class="izv">') ;
                            $descTo = stripos($line, '</p>', $res);
                            $description = trim(str_replace('(55) (57)', '', substr($line, $res + $res_len, $descTo - $res)));
                            $images[$i]['description'] = $description;
                            preg_match_all('/<img[^>]+src=([\'"])(?<src>.+?)\1[^>]*>/i', $line, $result);
                            if (!empty($result['src'])) {
                                $images[$i]['images'] = $result['src'];
                            }
                        } else {
                            $tit_abs = stripos($line, 'class="TitAbs">');
                            if ($tit_abs) {
                                $tit_len = $tit_abs + strlen('class="TitAbs">');
                                $descTo = stripos($line, '</p>', $tit_abs);
                                $description = isset($images[$i]['description']) ? $images[$i]['description'] : '';
                                $images[$i]['description'] = trim($description . substr($line, $tit_abs + $tit_len, $descTo - $tit_abs));
                                $i++;
                            } else {
                                preg_match_all('/<img[^>]+src=([\'"])(?<src>.+?)\1[^>]*>/i', $line, $result);
                                if (!empty($result['src'])) {
                                    $images[$i]['images'] = $result['src'];
                                }
                            }
                        }

                    }
                }
            }
        }
        return $images;
    }

    public static function extractContractDate($output, $needle) {
        $from = strripos($output, $needle);
        if ($from) {
            $to = stripos($output, '</B>', $from);
            $string = substr($output, $from, $to - $from);
            preg_match('/\d{2}.\d{2}.\d{4}/', $string, $match);
            $dates = explode('.' , empty($match[0]) ? '' : $match[0]);
            if (count($dates) === 3 && self::validateDate($match[0], 'd.m.Y')) {
                $contractDate = $dates[2] . '-' . $dates[1] . '-' . $dates[0];
                return $contractDate;
            }
        }
        return null;
    }

    public static function extractContractNumber($output, $needle) {
        $from = strripos($output, $needle);
        if ($from) {
            $to = stripos($output, '</B>', $from);
            $string = substr($output, $from, $to - $from);
            preg_match('/\d{2}.\d{2}.\d{4}/', $string, $match);
            $dates = explode('.' , empty($match[0]) ? '' : $match[0]);
            if (count($dates) === 3 && self::validateDate($match[0], 'd.m.Y')) {
                $from = stripos($string, $match[0]);
                if ($from) {
                    $sstring = substr($string, $from + strlen($match[0]));
                    $sstring = trim(strip_tags($sstring));
                    return $sstring;
                }
            }
        }
        return null;
    }

    private static function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

}