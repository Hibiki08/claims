<?php

namespace app\components;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;


abstract class RequestAndTMModel extends ActiveRecord {

    public function getAll($order = ['id' => SORT_ASC]) {
        return self::find()->orderBy($order)->all();
    }

    public static function getLastUpdate() {
        $lastDate = self::find()->max('update_time');
        return Yii::$app->formatter->asDate($lastDate, 'd.MM.yyyy');
    }

    public static function getCount() {
        $count = self::find()->count();
        return $count;
    }

    public static function getUnrecognized($limit = false, $dataProvider = false) {
        $query =  self::find()
            ->where(['and', ['not', ['img' => null]], ['captcha_id' => null]])
            ->orderBy(['id' => SORT_DESC]);
        
        if ($limit) {
            $query->limit($limit);
        }

        if ($dataProvider) {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
            return $dataProvider;
        }

        return $query->all();
    }

    public static function getCountUnrecognized() {
        return self::find()
            ->where(['and', ['not', ['img' => null]], ['captcha_id' => null]])
            ->count();
    }

    public static function getReqNumbers($ids) {
        return self::find()->select('request_number')->where(['in', 'id', $ids])->all();
    }
    
}