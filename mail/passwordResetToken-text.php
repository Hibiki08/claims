<?php

/* @var $this yii\web\View */
/* @var $user app\models\Users */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['reset-password', 'token' => $user->password_reset_token]);
?>

Follow the link below to reset your password:

<?= $resetLink ?>
