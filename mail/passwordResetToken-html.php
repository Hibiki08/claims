<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\Users */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    
    <p>Перейдите по ссылке, чтобы сбросить пароль:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
