<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $user_message
 * @property string $user_img
 * @property string $user_file
 * @property string $file_size
 * @property string $support_message
 * @property string $support_img
 * @property integer $admin_id
 * @property integer $ticket_id
 * @property string $time
 *
 */
class SupportMessage extends ActiveRecord {

    public static function tableName() {
        return 'support_messages';
    }
    
    public static function getFirstTicketMessageById($id) {
        return self::find()->where(['ticket_id' => $id])->orderBy('time')
            ->one();
    } 

}