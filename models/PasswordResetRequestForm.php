<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model {

    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\models\Users',
                'targetAttribute' => 'login',
                'filter' => ['status' => Users::STATUS_ACTIVE],
                'message' => 'Пользователь с указанным почтовым адресом не найден'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail() {
        $user = Users::findOne([
            'status' => Users::STATUS_ACTIVE,
            'login' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        if (!Users::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Сброс пароля для ' . Yii::$app->name)
            ->send();
    }

    public function attributeLabels(){
        return [
            'email' => 'Email',
        ];
    }

}
