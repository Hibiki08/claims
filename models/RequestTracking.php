<?php

namespace app\models;

use Yii;

/**
 *
 * @property integer $id
 * @property integer $title
 * @property string $request_id
 * @property string $user_id
 * @property string $notice
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 *
 */
class RequestTracking extends SearchTracking {

    public static function tableName() {
        return 'request_tracking';
    }
    
    public function getRequest() {
        return $this->hasOne(Requests::className(), ['id' => 'request_id']);
    }

    public function getHistory() {
        return $this->hasMany(RequestTrackingHistory::className(), ['request_tracking_id' => 'id']);
    }

    public static function getAllWithUser() {
        return self::find()
            ->joinWith('user')
            ->joinWith('history')
            ->joinWith('request')
            ->where(['active' => 1])
            ->all();
    }

    public static function getByUser($dataProvider = false) {
        $query = self::find()
            ->distinct()
            ->joinWith('history')
            ->joinWith('request')
            ->andWhere(['user_id' => Yii::$app->user->id])
            ->orderBy(['notice' => SORT_DESC, 'id' => SORT_DESC]);

        if ($dataProvider) {
            return $query;
        }
        return $query->all();
    }

}