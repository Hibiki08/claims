<?php

namespace app\models;

use Yii;

/**
 *
 * @property integer $id
 * @property integer $title
 * @property string $trademark_id
 * @property string $user_id
 * @property string $notice
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 *
 */
class TrademarkTracking extends SearchTracking {

    public static function tableName() {
        return 'trademark_tracking';
    }

    public function getTrademark() {
        return $this->hasOne(Trademarks::className(), ['id' => 'trademark_id']);
    }

    public function getHistory() {
        return $this->hasMany(TrademarkTrackingHistory::className(), ['trademark_tracking_id' => 'id']);
    }

    public static function getByUser($dataProvider = false) {
        $query = self::find()
            ->distinct()
            ->joinWith('history')
            ->joinWith('trademark')
            ->andWhere(['user_id' => Yii::$app->user->id])
            ->orderBy(['notice' => SORT_DESC, 'id' => SORT_DESC]);

        if ($dataProvider) {
            return $query;
        }
        return $query->all();
    }

}