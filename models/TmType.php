<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $rus_name
 *
 */
class TmType extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'tm_types';
    }

}
