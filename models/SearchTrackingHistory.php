<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $event
 * @property string $count
 * @property string $requests_id
 * @property string $trademarks_id
 * @property string $search_tracking_id
 * @property string $requests_diff_ids
 * @property string $trademarks_diff_ids
 * @property string $date
 *
 */
class SearchTrackingHistory extends ActiveRecord {

    public static function tableName() {
        return 'search_tracking_history';
    }

}