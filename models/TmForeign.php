<?php

namespace app\models;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Exception\RuntimeException;
use app\components\RequestAndTMModel;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tm_foreign".
 *
 * @property integer $id
 * @property string $name
 * @property string $st13
 * @property integer $app_number
 * @property string $app_language
 * @property string $app_date
 * @property string $reg_office
 * @property integer $reg_number
 * @property string $img
 * @property integer $captcha_id
 * @property string $reg_date
 * @property string $expiry_date
 * @property integer $base_app_number
 * @property integer $base_reg_number
 * @property string $base_app_date
 * @property string $base_reg_date
 * @property string $owner
 * @property string $address
 * @property integer $type_id
 * @property integer $kind_id
 * @property integer $status_id
 * @property string $update_time
 *
 */
class TmForeign extends RequestAndTMModel {
    
    public $rank;

    public static function tableName() {
        return 'tm_foreign';
    }

    public function getCountries() {
        return $this->hasMany(TmForeignCountry::className(), ['tm_id' => 'id']);
    }

    public function getStatus() {
        return $this->hasOne(ForeignStatus::className(), ['id' => 'status_id']);
    }

    public function getType() {
        return $this->hasOne(TmType::className(), ['id' => 'type_id']);
    }

    public function getKind() {
        return $this->hasOne(KindOfMark::className(), ['id' => 'kind_id']);
    }

    public function getClasses() {
        return $this->hasMany(TmForeignClass::className(), ['tm_id' => 'id'])
            ->with('class');
    }

    public function getThumb() {
        if (file_exists($this->img) && exif_imagetype($this->img)) {
            $tmbPath = substr($this->img, 0, strpos($this->img, '.'));
            $ext = substr($this->img, strpos($this->img, '.'));
            $tmbPath .= "_tmb$ext";
            if (!file_exists($tmbPath)) {
                try {
                    Image::getImagine()->open($this->img)->thumbnail(new Box(130, 100))->save($tmbPath , ['quality' => 90]);
                } catch (RuntimeException $e) {

                }
            }
            return $tmbPath;
        }
        return NULL;
    }

    public static function getRecognizedAndEmpty($limit = false, $dataProvider = false) {
        $requestTerms = TmForeignTerm::find()
            ->select('tm_id')->groupBy('tm_id');

        $query = self::find()
            ->where(['and',
                ['not', ['img' => null]],
                ['>', 'captcha_id', 0],
                ['not in', 'id', $requestTerms]
            ])->orderBy(['id' => SORT_DESC]);

        if ($limit) {
            $query->limit($limit);
        }

        if ($dataProvider) {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
            return $dataProvider;
        }
        return $query->all();
    }

    public static function getCountRecognizedAndEmpty() {
        $trademarkTerms = TmForeignTerm::find()
            ->select('tm_id')->groupBy('tm_id');

        return self::find()
            ->where(['and',
                ['not', ['img' => null]],
                ['>', 'captcha_id', 0],
                ['not in', 'id', $trademarkTerms]
            ])
            ->count();
    }

}
