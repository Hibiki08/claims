<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $id
 * @property string $abbr
 *
 */
class Country extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'countries';
    }

}
