<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "searchlogs".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $query
 * @property string $area
 * @property string $ip
 * @property string $date
 *
 * @property Users $user
 */
class Searchlogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'searchlogs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'query'], 'required'],
            [['user_id'], 'integer'],
            [['query'], 'string'],
            [['date', 'area'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'query' => 'Запрос',
            'area' => 'Область поиска',
            'ip' => 'IP',
            'date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public static function getUniqQuery($where = false) {
        $query = self::find()
            ->select('query')
            ->groupBy('query');
        
        if ($where) {
            $query->where($where);
        }
        return $query->all();
    }

}
