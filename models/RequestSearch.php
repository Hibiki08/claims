<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\data\SqlDataProvider;
use yii\sphinx\Query as SphinxQuery;
use yii\sphinx\MatchExpression;
use yii\db\Query;
use yii\db\Expression;
use yii\db\ActiveQuery;
use app\models\Requests;
use yii\helpers\Html;
use app\components\SearchQueryCorrect;
use \Wkhooy\ObsceneCensorRus;

/**
 * RequestSearch represents the model behind the search form about `app\models\Requests`.
 */
class RequestSearch extends Requests
{
    public $type = 'requests.trademarks';
    public $area = 'term';
    public $status = 'all';
    public $string = null;
    public $classes;
    public $similarity = 1.0;
    public $yearMin;
    public $yearMax;
    public $colors;
    public $colorCondition;
    public static $count = 0;
    public static $googleResult = null;
    public static $ids = [];

    public static $statuses_active = [7, 8, 14];
    public static $statuses_invalid = [4, 5, 6, 13, 15];

    public static $foreign_statuses_active = [2];
    public static $foreign_statuses_invalid = [3, 4, 5];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'request_number', 'reg_number', 'status_id'], 'integer'],
            [['similarity'], 'double'],
            [['colors', 'classes', 'request_date', 'reg_date', 'applicant',
                'address', 'first_request_number', 'first_request_date',
                'first_request_country', 'classes', 'img', 'facsimile', 'update_time', 'status'], 'safe'],
            [['colorCondition', 'area', 'string', 'classes', 'type', 'yearMax', 'yearMin'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params, $logWrite = true)
    {
        $this->load($params, '');
        $log = '';
        $searchString = Html::encode($this->string);
        $allowed = ObsceneCensorRus::isAllowed($searchString);
        $totalFound = 0;

        if (!$allowed) {
            $searchString = trim(preg_replace('/\*+/', '', ObsceneCensorRus::getFiltered($searchString)));
        }

        if ($this->similarity < 1) {
            preg_match_all('/[^\-\ "\\\|\/\&\(\)\!\^\$\.0-9]+/', $searchString, $words);
            if (!empty($words[0])) {
                foreach ($words[0] as $word) {
                    $len = mb_strlen($word);
                    $query = Dictionary::makeTrigrams($word);
                    $max = ceil((1 - $this->similarity) * 10);
                    $from = stripos($searchString, $word);

                    if ($from !== false) {
                        $rows = (new SphinxQuery())
                            ->select(new Expression('id, WEIGHT() AS rank'))
//                            ->select(new Expression('id'))
                            ->from('dictionary')
                            ->showMeta(true)
                            ->options([
                                'max_matches' => $max,
                                'ranker' => new Expression("expr('sum(word_count) + 10 - abs(len - $len)')")
                            ])
                            ->limit($max)
                            ->match(new MatchExpression(new Expression("\"$query\"/{$this->similarity}")))
                            ->search();

                        if (empty($rows['hits'])) {
                            continue;
                        }

                        $ranks = [];
                        foreach ($rows['hits'] as $row) {
                            $ranks[$row['id']] = $row['rank'];
                        }

                        $dict = Dictionary::find()
                            ->select('id, word')
                            ->where(['in', 'id', array_keys($ranks)])
                            ->orderBy(new Expression('FIELD (id, ' . implode(',', array_keys($ranks)) . ')'))
                            ->asArray()
                            ->all();

                        $result = [];
                        foreach ($dict as $item) {
                            $result[] = "{$item['word']}^{$ranks[$item['id']]}";
                        }

                        if (!empty($result)) {
                            $searchString = str_replace($word, implode('|', $result), $searchString);
                        }
                    }
                }
            }
        }


        $last = 0;
        while(($slash = stripos($searchString, '/', $last)) !== false) {
            $from = $last;
            while(($quote = stripos($searchString, '"', $from)) !== false) {
                $from = $quote + 1;

                if ($quote > $slash) {
                    $searchString = substr_replace($searchString, '"/"', $slash, 1);
                    $last = $slash + 3;
                    continue 2;
                } elseif (($quote = stripos($searchString, '"', $from)) !== false && $quote > $slash) {
                    $last = $quote + 1;
                    continue 2;
                }
            }

            $searchString = substr_replace($searchString, '"/"', $slash, 1);
            $last = $slash + 3;
        }

        switch ($this->area) {
            case 'all':
                $log .= 'по всему';
                break;
            case 'applicant':
                $log .= 'по заявителю';
                break;
            case 'address':
                $log .= 'по адресу для переписки';
                break;
            case 'term':
                $log .= 'по тегам изображений';
                break;
        }

        if (!empty($this->classes)) {
            $classes = explode('.', $this->classes);
            $log .= '; классы: ';
            $log .= implode(', ', $classes);
        }

        $models = [];
        $types = explode('.', $this->type);
        foreach ($types as $type) {
            switch ($type) {
                case 'trademarks':
                    $applicant = 'rightholder';
                    $number = 'reg_number';
                    $table = 'trademarks';
                    break;
                case 'requests';
                    $applicant = 'applicant';
                    $number = 'request_number';
                    $table = 'requests';
                    break;
                case 'wipo';
                    $applicant = 'owner';
                    $number = 'reg_number';
                    $table = 'tm_foreign';
                    break;
                default:
                    continue 2;
            }

            if (!empty($searchString)) {
                $match = new MatchExpression();

                switch ($this->area) {
                    case 'all':
                        $match->match(new Expression("'" . $searchString . "'"));
                        break;
                    case 'applicant':
                        $match->match(new Expression("'@" . $applicant . " " . $searchString . "'"));
                        break;
                    case 'address':
                        $match->match(new Expression("'@address " . $searchString . "'"));
                        break;
                    case 'term':
//                    if ($searchString) {
                        $match->match(new Expression("@term_name " . $searchString));
                        $match->orMatch(new Expression("(@term_name " . $searchString . ") | (@" . $number . " " . $searchString . ")"));
//                    }
                        break;
                }
//            }

                $rows = (new SphinxQuery())
                    ->select(new Expression('id, WEIGHT() AS rank'))
                    ->from($table)
                    ->showMeta(true)
                    ->options([
                        'max_matches' => 1000,
                    ])
                    ->limit(1000)
                    ->match($match);


                if (isset($params['add'])) {
                    $rows->limit(1000000)
                        ->options([
                            'max_matches' => 1000000,
                        ]);
                }

                if (!empty($this->classes)) {
                    $classes = explode('.', $this->classes);
                    $rows->where(['in', 'class_id', $classes]);
                }

                if (!empty($this->yearMin) && !empty($this->yearMax)) {
                    switch ($type) {
                        case 'trademarks':
                            $rows->andWhere(['between', 'reg_date', $this->yearMin, $this->yearMax]);
                            break;
                        case 'requests';
                            $rows->andWhere(['between', 'request_date', $this->yearMin, $this->yearMax]);
                            break;
                        case 'wipo':
                            $rows->andWhere(['between', 'reg_date', $this->yearMin, $this->yearMax]);
                            break;
                        default:
                            continue 2;
                    }
                }

                if (!empty($this->status)) {
                    if ($type == 'wipo') {
                        switch ($this->status) {
                            case 'active':
                                $rows->andWhere(['in', 'status_id', self::$foreign_statuses_active]);
                                break;
                            case 'invalid':
                                $rows->andWhere(['in', 'status_id', self::$foreign_statuses_invalid]);
                                break;
                        }
                    } else {
                        switch ($this->status) {
                            case 'active':
                                break;
                            case 'invalid':
                                $rows->andWhere(['in', 'status_id', self::$statuses_invalid]);
                                break;
                        }
                    }
                }

                if (!empty($this->colors)) {
                    if ($type == 'trademarks') {
                        $colors = explode('.', $this->colors);

                        switch ($this->colorCondition) {
                            case 'any':
                                $colors = array_map('intval', $colors);
                                $rows->andWhere(['in', 'color_id', $colors]);
                                break;
                            case 'all':
                                $colorCond[] = 'and';
                                foreach ($colors as $color) {
                                    $colorCond[] = ['color_id' => (int)$color];
                                }
                                $rows->andWhere($colorCond);
                                break;
                        }
                    }
                }

                $rows = $rows->search();

                $totalFound += $rows['meta']['total_found'];
            } else {
                $rows = [];
            }

//            var_dump($rows);die;

            if (!empty($rows['hits'])) {
                if (isset($params['add'])) {
                    static::$ids[$type] = [];
                    foreach ($rows['hits'] as $row) {
                        static::$ids[$type][] = $row['id'];
                    }
                }
                switch ($type) {
                    case 'trademarks':
                        $cache = Trademarks::getDb();
                        $query = Trademarks::find();
                        break;
                    case 'requests';
                        $cache = Requests::getDb();
                        $query = Requests::find();
                        break;
                    case 'wipo';
                        $cache = TmForeign::getDb();
                        $query = TmForeign::find();
                        break;
                    default:
                        continue 2;
                }


                $ranks = [];
                foreach ($rows['hits'] as $row) {
                    $ranks[$row['id']] = $row['rank'];
                }


//                $dataCacheKey = md5($type . implode('.', $ranks));
//                $data = Yii::$app->cache->get($dataCacheKey);
//
//                if ($data !== false) {
//                    $model = $data;
//                } else {
//                    $model = $query->where(['in', 'id', array_keys($ranks)])
//                            ->orderBy(new Expression('FIELD (id, ' . implode(',', array_keys($ranks)) . ')'))
//                        ->limit(1000)->all();
//                    Yii::$app->cache->add($dataCacheKey, $model, 86400);
//                }

                $model = $query->where(['in', 'id', array_keys($ranks)])
                    ->orderBy(new Expression('FIELD (id, ' . implode(',', array_keys($ranks)) . ')'))
                    ->limit(1000)->all();

                foreach ($model as $key => $row) {
                    if (isset($model[$key]->rank)) {
                        $model[$key]->rank = $ranks[$row->id];
                    }
                }

                $models[] = $model;
            }
        }

        if (count($models) > 1) {
            $models = array_merge(...$models);
        } elseif (count($models) == 1) {
            $models = array_shift($models);
        }

        if (!empty($models)) {
            usort($models, function ($a, $b) {
                if ($a->rank < $b->rank) return 1;
                elseif ($a->rank > $b->rank) return -1;
                elseif (!empty($a->request_date) && !empty($b->request_date)) {
                    if (strtotime($a->request_date) < strtotime($b->request_date)) return 1;
                    elseif (strtotime($a->request_date) > strtotime($b->request_date)) return -1;
                } elseif (!empty($a->reg_date) && !empty($b->reg_date)) {
                    if (strtotime($a->reg_date) < strtotime($b->reg_date)) return 1;
                    elseif (strtotime($a->reg_date) > strtotime($b->reg_date)) return -1;
                }
                return 0;
            });

            if (count($models) > 1000000) {
                $models = array_slice($models, 0, 1000000);
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
//            'totalCount' => $totalFound,
            'pagination' => [
                'pageSizeLimit' => [20, 50, 100, 300],
            ],
        ]);

        if (!empty($searchString) && $logWrite) {
            $searchLog = new Searchlogs();
            $searchLog->query = $searchString;
            $searchLog->area = $log;
            $searchLog->user_id = Yii::$app->user->getId();
            $searchLog->ip = Yii::$app->request->userIP;
            $searchLog->save(false);
        }


        self::$count = $totalFound;
        return $dataProvider;
    }
}
