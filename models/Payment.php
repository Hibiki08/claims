<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $description
 * @property string $pay_status
 * @property string $user_id
 * @property string $wallet_data
 * @property string $date
 */
class Payment extends ActiveRecord {

    public static function tableName() {
        return 'payments';
    }

    public static function getHistory($dataProvider = false) {
        $query = self::find()->where(['user_id' => Yii::$app->user->id]);

        if ($dataProvider) {
            return $query;
        }
        return $query->all();
    }

    public function attributeLabels() {
        return [
            'description' => 'Описание',
            'pay_status' => 'Статус платежа',
            'date' => 'Дата'
        ];
    }

}
