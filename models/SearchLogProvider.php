<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class SearchLogProvider extends Searchlogs {

    public function rules() {
        return [
            [['id'], 'integer'],
            [['query', 'date', 'user_id'], 'safe'],
        ];
    }

    public function search($params) {
        $query = Searchlogs::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [20, 50, 100, 300],
            ],
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }


        if ($this->user_id == 'guest') {
            $query->andWhere(['user_id' => null]);
        } else {
            $query->andFilterWhere(['user_id' => $this->user_id]);
        }

        if (!empty($this->date)) {
            $query->andFilterWhere(['like', 'date', Yii::$app->formatter->asDate($this->date, 'yyyy-MM-dd')]);
        }

        return $dataProvider;
    }

}
