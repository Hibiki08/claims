<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $description
 *
 */
class Classes extends ActiveRecord {

    public static function tableName() {
        return 'classes';
    }

    public static function getAll() {
        return self::find()->all();
    }

    public static function getCount() {
        return self::find()->count();
    }

}
