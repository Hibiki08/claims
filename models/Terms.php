<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "terms".
 *
 * @property integer $id
 * @property string $term
 *
 * @property RequestTerms[] $requestTerms
 */
class Terms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'terms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['term'], 'required'],
            [['term'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'term' => 'Term',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestTerms()
    {
        return $this->hasMany(RequestTerms::className(), ['term_id' => 'id']);
    }
}
