<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $type
 * @property string $name
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property string $created_at
 * @property string $updated_at
 *
 */
class AuthItem extends ActiveRecord {

    public static function tableName() {
        return 'auth_item';
    }

    public static function getRoles($where = false) {
        $query =  self::find()
            ->where(['and', ['type' => 1], ['not', ['name' => ['support', 'premium']]]]);
        
        if ($where) {
            $query->andWhere($where);
        }
        
        return $query->all();
    }

}
