<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $password
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $access_token
 * @property string $company
 * @property string $domain
 * @property integer $status
 * @property string $login
 * @property string $name
 * @property string $subscription_date
 * @property string $telegram_key
 * @property string $telegram_id
 * @property string $telegram_notice
 * @property integer $sub_expired_sent
 * @property integer $left_few_days_sent
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'login'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 300],
            [['password', 'password_reset_token', 'auth_key', 'access_token', 'company', 'login'], 'string', 'max' => 100],
            [['domain'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'password' => 'Password',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'company' => 'Company',
            'domain' => 'Domain',
            'status' => 'Status',
            'login' => 'Login',
            'name' => 'ФИО',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $userType = null) {

        $user = Users::find()
            ->where(['access_token' => $token])
            ->one();
        if (!count($user)) {
            return null;
        }
        return new static($user);
    }

    /**
     * Finds user by login
     *
     * @param  string      $login
     * @param  bool      $new
     * @return static|null
     */
    public static function findByLogin($login, $status = Users::STATUS_ACTIVE) {
        $user = Users::find()
            ->where([
                'login' => $login,
                'status' => $status
            ])
            ->one();
        if (!count($user)) {
            return null;
        }
        return new static($user);
    }
    
    public static function findByTelegramId($telegramId) {
        return self::findOne(['telegram_id' => $telegramId]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    public static function getUsersWithSupportRole() {
        $supportUsers = [];
        $users = self::find()->where(['status' => 1])
            ->andWhere(['is not', 'telegram_id', null])->all();
        if (!empty($users)) {
            foreach ($users as $val) {
                $rolesArr = Yii::$app->authManager->getRolesByUser($val->id);
                if (array_key_exists('support', $rolesArr)) {
                    $supportUsers[] = $val;
                }
            }
        }
        return $supportUsers;
    }

    public static function getUsersWithAdminRole() {
        $adminUsers = [];
        $users = self::find()->where(['status' => 1])
            ->andWhere(['is not', 'telegram_id', null])->all();
        if (!empty($users)) {
            foreach ($users as $val) {
                $rolesArr = Yii::$app->authManager->getPermissionsByUser($val->id);
                if (array_key_exists('claimsPermission', $rolesArr)) {
                    $adminUsers[] = $val;
                }
            }
        }
        return $adminUsers;
    }

    public static function hasSupportRole($id) {
        $rolesArr = Yii::$app->authManager->getRolesByUser($id);
        if (array_key_exists('support', $rolesArr)) {
            return true;
        }
        return false;
    }

    public static function hasPremiumRole($id) {
        $rolesArr = Yii::$app->authManager->getRolesByUser($id);
        if (array_key_exists('premium', $rolesArr)) {
            return true;
        }
        return false;
    }
    
    public static function getUserWithRole($id) {
        return self::find()
            ->with('role')
            ->where(['id' => $id])
            ->one();
    } 
    
    public function getRole() {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id'])
            ->where(['not', ['item_name' => ['support', 'premium']]])
            ->alias('role');
    }
    
    public static function getUsersWithRoles($dataProvider = false, $where = false) {
        $query = self::find()
            ->joinWith('role');
        
        if ($where) {
            $query->andWhere($where);
        }
        
        if ($dataProvider) {
            return $query;
        }
        
        return $query->all();
    }

}
