<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\data\ActiveDataProvider;


class UserSearch extends Users {

    public $role;

    public function rules() {
        return [
            [['name', 'login', 'role', 'subscription_date'], 'string'],
        ];
    }

    public function search($params) {
        $this->load($params);

        if (!empty($this->role)) {
            $query = UserSearch::getUsersWithRoles(true, ['role.item_name' => $this->role]);
        } else {
            $query = UserSearch::getUsersWithRoles(true);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', UserSearch::tableName() . '.login', $this->login]);
//        $query->andFilterWhere([UserSearch::tableName() . '.login', $this->login]);

        return $dataProvider;
    }

}
