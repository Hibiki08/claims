<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $type
 *
 */
class LogType extends ActiveRecord {

    const TM_100_UPDATE_ID = 1;
    const TM_ALL_UPDATE_ID = 2;
    const ZNAKI_100_UPDATE_ID = 3;
    const ZNAKI_ALL_UPDATE_ID = 4;


    public static function tableName() {
        return 'log_type';
    }
    
}