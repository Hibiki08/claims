<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trademark_classes".
 *
 * @property integer $trademark_id
 * @property integer $class
 *
 * @property Trademarks $trademark
 */
class TrademarkClasses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trademark_classes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trademark_id', 'class'], 'required'],
            [['trademark_id', 'class'], 'integer'],
            [['trademark_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trademarks::className(), 'targetAttribute' => ['trademark_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trademark_id' => 'Trademark ID',
            'class' => 'Class',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrademark()
    {
        return $this->hasOne(Trademarks::className(), ['id' => 'trademark_id']);
    }
}
