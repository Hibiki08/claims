<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "request_terms".
 *
 * @property integer $request_id
 * @property integer $term_id
 * @property integer $captcha_id
 *
 * @property Requests $request
 * @property Terms $term
 */
class RequestTerms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_terms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'term_id'], 'required'],
            [['request_id', 'term_id', 'captcha_id'], 'integer'],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Requests::className(), 'targetAttribute' => ['request_id' => 'id']],
            [['term_id'], 'exist', 'skipOnError' => true, 'targetClass' => Terms::className(), 'targetAttribute' => ['term_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'term_id' => 'Term ID',
            'captcha_id' => 'Captcha ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Requests::className(), ['id' => 'request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerm()
    {
        return $this->hasOne(Terms::className(), ['id' => 'term_id']);
    }
}
