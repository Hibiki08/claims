<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Users;
use yii\helpers\Html;
use yii\helpers\StringHelper;


class TrackingForm extends Model {

    public $search_request;
    public $request_id;
    public $trademark_id;
//    public $add;

    public function rules() {
        return [
            ['search_request', 'trim'],
//            ['search_request', 'required'],
            [['request', 'trademark'], 'safe'],
        ];
    }

    public function add() {
        $res = false;

        if (!$this->validate()) {
            return false;
        }

        if (!empty($this->search_request)) {
            $searchModel = new RequestSearch();

            $url = parse_url($this->search_request);

            if (isset($url['query']) && !empty($url['query'])) {
                parse_str($url['query'], $params);

                $params['add'] = 1;
                $searchModel->search($params, false);
                $matches = RequestSearch::$count;
                $ids = RequestSearch::$ids;
                unset($params['add']);

                $searchTracking = new SearchTracking();
                $searchTracking->title = StringHelper::truncate($params['string'], 30, '...') . ' от ' . date('d.m.Y H:i', time());
                $searchTracking->request = serialize($params);
                $searchTracking->matches = $matches;
                if (isset($ids['requests'])) {
                    $searchTracking->request_ids = serialize($ids['requests']);
                }
                if (isset($ids['trademarks'])) {
                    $searchTracking->trademark_ids = serialize($ids['trademarks']);
                }
                $searchTracking->user_id = Yii::$app->user->id;
                $res = $searchTracking->save();
                if ($res) {
                    $history = new SearchTrackingHistory();
                    $history->event = 'Запрос создан';
                    $history->search_tracking_id = $searchTracking->id;
                    $res = $history->save();

                    if ($res) {
                        Yii::$app->session->setFlash('success', 'Запрос добавлен');
                    }
                }
            } else {
                Yii::$app->session->setFlash('request_empty', 'Нужно ввести запрос');
            }
        }

        return $res;
    }

    public function addRequest() {
        $res = false;

        if (!$this->validate()) {
            return false;
        }
        if (!empty($this->request_id)) {
            $request = Requests::findOne(['id' => $this->request_id]);
            if (!empty($request)) {
                $requestHistory = new RequestTracking();
                $requestHistory->title = $request->request_number . ' от ' . date('d.m.Y H:i', time());
                $requestHistory->user_id = Yii::$app->user->id;
                $requestHistory->request_id = $this->request_id;
                $res = $requestHistory->save();
                if ($res) {
                    $history = new RequestTrackingHistory();
                    $history->event = 'Заявка добавлена';
                    $history->request_tracking_id = $requestHistory->id;
                    $res = $history->save();
                }
            }

        }
        return $res;
    }

    public function addTrademark() {
        $res = false;

        if (!$this->validate()) {
            return false;
        }
        if (!empty($this->trademark_id)) {
            $trademark = Trademarks::findOne(['id' => $this->trademark_id]);
            if (!empty($trademark)) {
                $trademarkHistory = new TrademarkTracking();
                $trademarkHistory->title = $trademark->reg_number . ' от ' . date('d.m.Y H:i', time());
                $trademarkHistory->user_id = Yii::$app->user->id;
                $trademarkHistory->trademark_id = $this->trademark_id;
                $res = $trademarkHistory->save();
                if ($res) {
                    $history = new TrademarkTrackingHistory();
                    $history->event = 'Тованый знак добавлен';
                    $history->trademark_tracking_id = $trademarkHistory->id;
                    $res = $history->save();
                }
            }
        }
        return $res;
    }

    public function attributeLabels(){
        return [
            'search_request' => 'Поисковой запрос',
            'request' => 'Заявка',
            'trademark' => 'Товарный знак',
        ];
    }

}
