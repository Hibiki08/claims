<?php

namespace app\models\prom;

use Yii;

/**
 *
 * @property integer $id
 * @property integer $number
 * @property integer $request_number
 * @property string $request_date
 * @property string $reg_date
 * @property string $address
 * @property string $actual_at
 * @property string $not_valid_date
 * @property string $publish_date
 * @property integer $status_id
 * @property integer $last_update_status
 * @property string $owner
 * @property string $name
 *
 */
class Patent extends Prom {

    public static function tableName() {
        return 'patents';
    }

    public function getClasses() {
        return $this->hasMany(PatentClass::className(), ['patent_id' => 'id'])
            ->joinWith('class');
    }

    public function getImages() {
        return $this->hasMany(PatentImage::className(), ['patent_id' => 'id']);
    }

    public function getStatus() {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
    
    public static function getAll($dataProvider = false) {
        $query = self::find()
            ->with('classes')
            ->with('images')
            ->with('status');
        
        if ($dataProvider) {
            return $query;
        }
        return $query->all();
    }

    public static function getByCondition($where, $dataProvider = false) {
        $query = self::find()
            ->with('classes')
            ->with('images')
            ->with('status')
            ->where($where)
        ->orderBy(['request_date' => SORT_DESC]);

        if ($dataProvider) {
            return $query;
        }
        return $query->all();
    }

    public static function getLastUpdate() {
        $lastDate = self::find()->max('update_time');
        return Yii::$app->formatter->asDate($lastDate, 'd.MM.yyyy');
    }

    public static function getCount() {
        return self::find()->count();
    }

}