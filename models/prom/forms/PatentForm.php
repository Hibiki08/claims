<?php

namespace app\models\prom\forms;

use Yii;
//use yii\base\Model;
use yii\sphinx\Query as SphinxQuery;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\sphinx\MatchExpression;
use yii\helpers\Html;
use app\models\Dictionary;
//use yii\data\ArrayDataProvider;

use app\models\prom\Patent;


class PatentForm extends Patent {

    public $string;
    public $area = 'all';
    public $yearMin;
    public $yearMax;
    public $patentClasses;
    public $stat = 'all';
    public $similarity = 1.0;

    public static $count = 0;

    const STATUS_ACTIVE = 2;
    const STATUS_INVALID = 3;

    public function rules(){
        return [
            [['similarity'], 'double'],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['patent'] = ['owner', 'patentClasses', 'string', 'area', 'yearMin', 'yearMax', 'stat', 'similarity'];
        return $scenarios;
    }

    public function search($params) {
        $this->load($params, '');
        $searchString = Html::encode($this->string);

        $totalFound = 0;

        if ($this->similarity < 1) {
            preg_match_all('/[^\-\ "\\\|\/\&\(\)\!\^\$\.0-9]+/', $searchString, $words);
            if (!empty($words[0])) {
                foreach ($words[0] as $word) {
                    $len = mb_strlen($word);
                    $query = Dictionary::makeTrigrams($word);
                    $max = ceil((1 - $this->similarity) * 10);
                    $from = stripos($searchString, $word);

                    if ($from !== false) {
                        $rows = (new SphinxQuery())
                            ->select(new Expression('id, WEIGHT() AS rank'))
//                            ->select(new Expression('id'))
                            ->from('dictionary')
                            ->showMeta(true)
                            ->options([
                                'max_matches' => $max,
                                'ranker' => new Expression("expr('sum(word_count) + 10 - abs(len - $len)')")
                            ])
                            ->limit($max)
                            ->match(new MatchExpression(new Expression("\"$query\"/{$this->similarity}")))
                            ->search();

                        if (empty($rows['hits'])) {
                            continue;
                        }

                        $ranks = [];
                        foreach ($rows['hits'] as $row) {
                            $ranks[$row['id']] = $row['rank'];
                        }

                        $dict = Dictionary::find()
                            ->select('id, word')
                            ->where(['in', 'id', array_keys($ranks)])
                            ->orderBy(new Expression('FIELD (id, ' . implode(',', array_keys($ranks)) . ')'))
                            ->asArray()
                            ->all();

                        $result = [];
                        foreach ($dict as $item) {
                            $result[] = "{$item['word']}^{$ranks[$item['id']]}";
                        }

                        if (!empty($result)) {
                            $searchString = str_replace($word, implode('|', $result), $searchString);
                        }
                    }
                }
            }
        }
        
        $match = new MatchExpression();

        if (!empty($searchString)) {
            $searchString = Html::encode($searchString);
        switch ($this->area) {
            case 'all':
                $match->match(new Expression($searchString));
                break;
            case 'name':
                $match->match(new Expression('@name ' . $searchString));
                break;
            case 'owner':
                $match->match(new Expression('@owner ' . $searchString));
                break;
            case 'address':
                $match->match(new Expression('@address ' . $searchString));
                break;
            case 'img_description':
                $match->match(new Expression('@img_description ' . $searchString));
                break;
        }
        }

        $query = (new SphinxQuery())
            ->select('id')
            ->from(Patent::tableName())
            ->showMeta(true)
            ->limit(10000)
            ->options([
                'max_matches' => 10000,
            ])->match($match)
            ->orderBy(['request_date' => SORT_DESC]);

        if (!empty($this->patentClasses)) {
            $classes = explode('.', $this->patentClasses);
            $query->andWhere(['in', 'class_id', $classes]);
        }

        if (!empty($this->stat)) {
            switch ($this->stat) {
                case 'active':
                    $query->andWhere(['status_id' => self::STATUS_ACTIVE]);
                    break;
                case 'invalid':
                    $query->andWhere(['status_id' => self::STATUS_INVALID]);
                    break;
            }
        }

        if (!empty($this->yearMin) && !empty($this->yearMax)) {
            $query->andWhere(['between', 'request_date', $this->yearMin, $this->yearMax]);
        }
        $result = $query->search();

        $totalFound += $result['meta']['total_found'];

        $ids = [];
        foreach ($result['hits'] as $id) {
            $ids[] = $id['id'];
        }

        $model = self::getByCondition(['in', 'id', $ids], true);


        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSizeLimit' => [20, 50, 100, 300],
            ],
        ]);

//        $dataProvider->setSort([
//            'attributes' => [
//                'count_rightholder'
//            ]
//        ]);


        self::$count = $totalFound;
        return $dataProvider;
    }

}
