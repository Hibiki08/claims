<?php

namespace app\models\prom;

use Yii;

/**
 *
 * @property integer $id
 * @property integer $patent_id
 * @property integer $class_id
 *
 */
class PatentClass extends Prom {

    public static function tableName() {
        return 'patents_classes';
    }

    public function getClass() {
        return $this->hasOne(Classes::className(), ['id' => 'class_id'])
            ->joinWith('parentClass');
    }

}
