<?php

namespace app\models\prom;

use Yii;
use yii\db\ActiveRecord;

class Prom extends ActiveRecord {

    public static function getDb() {
        return Yii::$app->dbProm;
    }
    
}