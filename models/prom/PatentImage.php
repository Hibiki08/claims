<?php

namespace app\models\prom;

use Yii;

/**
 *
 * @property integer $id
 * @property string $description
 * @property string $path
 * @property integer $patent_id
 *
 */
class PatentImage extends Prom {

    public static function tableName() {
        return 'patents_images';
    }

}