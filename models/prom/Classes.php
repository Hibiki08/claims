<?php

namespace app\models\prom;

use Yii;

/**
 *
 * @property integer $id
 * @property integer $class_key
 * @property string $description
 * @property integer $parent_id
 *
 */
class Classes extends Prom {

    public static function tableName() {
        return 'classes';
    }

    public function getParentClass() {
        return $this->hasOne(self::className(), ['id' => 'parent_id'])
            ->alias('parent_class');
    }

    public static function getClass($parent_class, $class) {
        $parentClass = self::findOne(['class_key' => $parent_class, 'parent_id' => null]);
        if (!empty($parentClass)) {
            $parent_id = $parentClass->id;
            return self::findOne(['class_key' => $class, 'parent_id' => $parent_id]);
        }
        return null;
    }

    public static function getCount() {
        return self::find()
            ->where(['is not', 'parent_id', null])->count();
    }

    public static function getAllClasses() {
        return self::find()
            ->with('parentClass')
            ->all();
    }

}
