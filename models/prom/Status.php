<?php

namespace app\models\prom;

use Yii;

/**
 *
 * @property integer $id
 * @property string $title
 *
 */
class Status extends Prom {

    public static function tableName() {
        return 'statuses';
    }

    public function getStatusID($title) {
        $res = 1;
        $result = self::findOne(['title' => $title]);
        if (!empty($result)) {
            return $result['id'];
        } 
        return $res;
    }

}
