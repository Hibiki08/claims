<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property integer $title
 * @property string $request
 * @property string $matches
 * @property string $notice
 * @property string $user_id
 * @property string $request_ids
 * @property string $trademark_ids
 * @property string $created_at
 * @property string $updated_at
 *
 */
class SearchTracking extends ActiveRecord {

    public static function tableName() {
        return 'search_tracking';
    }

    public function getUser() {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function getHistory() {
        return $this->hasMany(SearchTrackingHistory::className(), ['search_tracking_id' => 'id'])
            ->alias('history');
    }

//    public static function getAll() {
//        return self::find()->all();
//    }
//
    public static function getCount() {
        $query = self::find()
        ->where(['notice' => 1, 'user_id' => Yii::$app->user->id]);

        return $query->count();
    }

    public static function getAllWithUser() {
        return self::find()
            ->joinWith('user')
            ->joinWith('history')
            ->where(['active' => 1])
            ->all();
    }
    
    public static function getByUser($dataProvider = false) {
        $query = self::find()
            ->distinct()
            ->joinWith('history')
            ->andWhere(['user_id' => Yii::$app->user->id])
            ->orderBy(['notice' => SORT_DESC, 'id' => SORT_DESC]);

        if ($dataProvider) {
            return $query;
        }
        return $query->all();
    }
    
}