<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_foreign_terms".
 *
 * @property integer $tm_id
 * @property integer $term_id
 *
 */

class TmForeignTerm extends \yii\db\ActiveRecord {
    
    public static function tableName() {
        return 'tm_foreign_terms';
    }

    public function getTrademark() {
        return $this->hasOne(TmForeign::className(), ['id' => 'tm_id']);
    }

    public function getTerm() {
        return $this->hasOne(Terms::className(), ['id' => 'term_id']);
    }
    
}
