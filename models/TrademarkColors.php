<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Trademarks;

/**
 *
 * @property integer $trademark_id
 * @property string $color_id
 *
 */
class TrademarkColors extends ActiveRecord {

    public static function tableName() {
        return 'trademark_colors';
    }

    public function assignColor($color_id) {
        $trademarkIds = Trademarks::getTMByColor($color_id);

        if (!empty($trademarkIds)) {
            $colorIds = ColorsAndColorsShade::find()
                ->select('color_id')
                ->where(['color_shade_id' => $color_id])
                ->all();
            
            if (!empty($colorIds)) {
                $values = '';
                foreach ($trademarkIds as $id) {
                    foreach ($colorIds as $colId) {
                        $values .= '(' . $id . ', ' . $colId['color_id'] . '),';
                    }
                }
                $values = substr($values, 0, -1);
                $query = 'insert into ' . TrademarkColors::tableName() . ' (trademark_id, color_id) values ' . $values . ' on duplicate key update trademark_id=trademark_id, color_id=color_id';
                return Yii::$app->db->createCommand($query)->execute();
            }
        }
        return -1;
    }

    public function deleteColor($shade_color_id, $color_id) {
        $trademarkIds = Trademarks::getTMByColor($shade_color_id);

        if (!empty($trademarkIds)) {
            $values = '';
            foreach ($trademarkIds as $id) {
                $values .= '(trademark_id=' . $id . ' AND color_id=' . $color_id . ') OR ' ;
            }
            $values = substr($values, 0, -3);

            return TrademarkColors::deleteAll($values);
        }
        return -1;
    }
    
}