<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $event
 * @property string $trademark_tracking_id
 * @property string $date
 *
 */
class TrademarkTrackingHistory extends ActiveRecord {

    public static function tableName() {
        return 'trademark_tracking_history';
    }

}