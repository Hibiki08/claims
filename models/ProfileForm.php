<?php

namespace app\models;

use Yii;
use yii\base\Model;


class ProfileForm extends Model {

    public $name;

    public function rules() {
        return [
            [['name'], 'string', 'max' => 300],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => 'Имя',
        ];
    }

}
