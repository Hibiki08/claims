<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $message
 * @property string $number_from
 * @property string $number_to
 * @property integer $is_error
 * @property integer $log_type_id
 * @property string $created_at
 *
 */
class Log extends ActiveRecord {

    public static function tableName() {
        return 'logs';
    }

    public static function findTmHundredLogs() {
        return self::find()
            ->where(['log_type_id' => LogType::TM_100_UPDATE_ID])
            ->all();
    }

    public static function findTmCycleLogs() {
        return self::find()
            ->where(['log_type_id' => LogType::TM_ALL_UPDATE_ID])
            ->all();
    }

    public static function findRequestsHundredLogs() {
        return self::find()
            ->where(['log_type_id' => LogType::ZNAKI_100_UPDATE_ID])
            ->all();
    }

    public static function findRequestsCycleLogs() {
        return self::find()
            ->where(['log_type_id' => LogType::ZNAKI_ALL_UPDATE_ID])
            ->all();
    }

}