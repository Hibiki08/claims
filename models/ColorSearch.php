<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\data\ActiveDataProvider;


class ColorSearch extends ColorsShade {

    public function rules() {
        return [
            [['id'], 'integer'],
            [['name'], 'string'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $this->load($params);

        $query = ColorsShade::find()
            ->distinct()
            ->joinWith('colorShade');

        if (isset($params['type'])) {
            if ($params['type'] == 'unsorted') {
                $query->where(['colors_and_colors_shade.color_id' => null]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', ColorsShade::tableName() . '.name', $this->name]);

        return $dataProvider;
    }

}
