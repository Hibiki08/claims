<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\sphinx\Query as SphinxQuery;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use yii\sphinx\MatchExpression;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;


class CompaniesForm extends Trademarks {

    public $string;
    public $rightholder;
    public $classes;

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['companies'] = ['rightholder', 'classes', 'string'];
        return $scenarios;
    }

    public function search($params) {
        $this->load($params, '');

        $this->string = Html::encode($this->string);

        $match = new MatchExpression();
        if (!empty($this->string)) {
            $match->match(new Expression("'@rightholder  $this->string'"));
        }

        $query = (new SphinxQuery())
            ->select('id')
            ->from(Trademarks::tableName())
            ->showMeta(true)
            ->limit(1000000)
            ->options([
                'max_matches' => '1000000',
            ])->match($match);

        if (!empty($this->classes)) {
            $classes = explode('.', $this->classes);
            $query->andWhere(['in', 'class_id', $classes]);
        }

        $result = $query->search();

        $model = (new \yii\db\Query())
            ->select(new Expression('replace(replace(rightholder, "  ", " "), "  ", " ") as rightholder, count(*) as count_rightholder'))
            ->from(self::tableName())
            ->groupBy(new Expression('replace(replace(rightholder, "  ", " "), "  ", " ")'))
            ->having('count(*) > 1')
            ->where(['not', ['rightholder' => null]])
            ->orderBy(['count_rightholder' => SORT_DESC]);

        $ids = [];
        foreach ($result['hits'] as $id) {
            $ids[] = $id['id'];
        }

        $model->andWhere(['in', 'id', $ids]);

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
//            'totalCount' => $totalFound,
            'pagination' => [
                'pageSizeLimit' => [20, 50, 100, 300],
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'count_rightholder'
            ]
        ]);

        $model->andFilterWhere(['like', 'rightholder', $this->rightholder]);

        return $dataProvider;
    }

}
