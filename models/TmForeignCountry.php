<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_foreign_country".
 *
 * @property integer $tm_id
 * @property integer $country_id
 *
 */
class TmForeignCountry extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'tm_foreign_country';
    }

    public function getCountry() {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

}
