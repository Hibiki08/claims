<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $name
 *
 */
class ColorsShade extends ActiveRecord {

    public static function tableName() {
        return 'colors_shade';
    }
    
    public function getColorShade() {
        return $this->hasMany(ColorsAndColorsShade::className(), ['color_shade_id' => 'id'])
            ->joinWith('color');
    }

}