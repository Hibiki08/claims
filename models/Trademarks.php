<?php

namespace app\models;

use app\models\TrademarkColors;
use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\sphinx\Query as SphinxQuery;
use app\models\ColorsShade;
use app\models\ColorsAndColorsShade;
use yii\db\Expression;
use app\components\RequestAndTMModel;
use yii\sphinx\MatchExpression;
use yii\data\ActiveDataProvider;
use Imagine\Exception\RuntimeException;
//use app\models\TrademarkColors;

/**
 * This is the model class for table "trademarks".
 *
 * @property integer $id
 * @property string $reg_number
 * @property integer $request_number
 * @property string $reg_date
 * @property string $request_date
 * @property string $reg_expiry_date
 * @property string $publish_date
 * @property string $publication_pdf
 * @property string $legal_protection_end_date
 * @property string $rightholder
 * @property string $address
 * @property string $first_request_number
 * @property string $first_request_date
 * @property string $first_request_country
 * @property string $colors
 * @property string $classes
 * @property string $priority
 * @property string $licensee
 * @property string $contract_number
 * @property string $contract_date
 * @property string $license_terms
 * @property string $img
 * @property integer $wellknown
 * @property integer $status_id
 * @property string $status_date
 * @property string $update_time
 * @property integer $rank
 * @property integer $cstatus_id
 *
 * @property TrademarkClasses[] $trademarkClasses
 * @property TrademarkTerms[] $trademarkTerms
 * @property Statuses $status
 */
class Trademarks extends RequestAndTMModel
{
    public $rank;
//    public $string;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trademarks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_number', 'status_id'], 'required'],
            [['request_number', 'status_id', 'cstatus_id'], 'integer'],
            [['reg_date', 'request_date', 'reg_expiry_date', 'publish_date', 'legal_protection_end_date', 'first_request_date', 'priority', 'contract_date', 'status_date', 'update_time'], 'safe'],
            [['reg_number', 'publication_pdf', 'rightholder', 'address', 'first_request_number', 'first_request_country', 'colors', 'classes', 'licensee', 'contract_number', 'license_terms', 'img'], 'string'],
            [['status_id', 'cstatus_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
//        $scenarios['companies'] = ['rightholder', 'classes'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reg_number' => 'Номер регистрации',
            'request_number' => 'Номер заявки',
            'reg_date' => 'Дата государственной регистрации',
            'request_date' => 'Дата подачи заявки',
            'reg_expiry_date' => 'Дата истечения срока действия регистрации',
            'publish_date' => 'Дата публикации',
            'publication_pdf' => 'Официальная публикация в формате PDF',
            'legal_protection_end_date' => 'Дата прекращения правовой охраны товарного знака',
            'rightholder' => 'Правообладатель',
            'address' => 'Адрес для переписки',
            'first_request_number' => 'Номер первой заявки',
            'first_request_date' => 'Дата подачи первой заявки',
            'first_request_country' => 'Код страны подачи первой заявки',
            'colors' => 'Указание цвета или цветового сочетания',
            'classes' => 'Классы МКТУ и перечень товаров и/или услуг',
            'priority' => 'Приоритет',
            'licensee' => 'Наименование лицензиата',
            'contract_number' => 'Номер регистрации договора',
            'contract_date' => 'Дата регистрации договора',
            'license_terms' => 'Указание условий и/или ограничений лицензии',
            'img' => 'Изображение (воспроизведение) товарного знака, знака обслуживания ',
            'status_id' => 'Статус',
            'cstatus_id' => 'Статус заявки',
            'status_date' => 'Последнее изменение статуса',
            'update_time' => 'Update Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrademarkClasses()
    {
        return $this->hasMany(TrademarkClasses::className(), ['trademark_id' => 'id']);
    }

    public function getTrademarkColors() {
        return $this->hasMany(TrademarkColors::className(), ['trademark_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrademarkTerms()
    {
        return $this->hasMany(TrademarkTerms::className(), ['trademark_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'status_id']);
    }

    public function getThumb() {
        if (file_exists($this->img) && exif_imagetype($this->img)) {
            $tmbPath = substr($this->img, 0, strpos($this->img, '.'));
            $ext = substr($this->img, strpos($this->img, '.'));
            $tmbPath .= "_tmb$ext";
//            if (!file_exists($tmbPath)) {
            try {
                Image::getImagine()->open($this->img)->thumbnail(new Box(130, 100))->save($tmbPath , ['quality' => 90]);
            } catch (RuntimeException $e) {

            }

//            }
            return $tmbPath;
        }
        return NULL;
    }

    public static function getTMByColor($color_id) {
        $ids = [];
        $color = ColorsShade::find()->where(['id' => $color_id])->one();

        if (!empty($color)) {
            $trademarks = self::find()
                ->select('id')
                ->where(['like', 'colors', $color->name])
                ->all();

            if (!empty($trademarks)) {
                foreach ($trademarks as $trademark) {
                    $ids[] = $trademark->id;
                }
            }
        }

        return $ids;
    }

    public static function getRecognizedAndEmpty($limit = false, $dataProvider = false) {
        $requestTerms = TrademarkTerms::find()
            ->select('trademark_id')->groupBy('trademark_id');

        $query = self::find()
            ->where(['and',
                ['not', ['img' => null]],
                ['>', 'captcha_id', 0],
                ['not in', 'id', $requestTerms]
            ])->orderBy(['id' => SORT_DESC]);

        if ($limit) {
            $query->limit($limit);
        }

        if ($dataProvider) {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
            return $dataProvider;
        }
        return $query->all();
    }

    public static function getCountRecognizedAndEmpty() {
        $trademarkTerms = TrademarkTerms::find()
            ->select('trademark_id')->groupBy('trademark_id');

        return self::find()
            ->where(['and',
                ['not', ['img' => null]],
                ['>', 'captcha_id', 0],
                ['not in', 'id', $trademarkTerms]
            ])
            ->count();
    }

}
