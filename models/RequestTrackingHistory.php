<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $event
 * @property string $request_tracking_id
 * @property string $date
 *
 */
class RequestTrackingHistory extends ActiveRecord {

    public static function tableName() {
        return 'request_tracking_history';
    }

}