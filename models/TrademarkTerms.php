<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trademark_terms".
 *
 * @property integer $trademark_id
 * @property integer $term_id
 *
 * @property Trademarks $trademark
 * @property Terms $term
 */
class TrademarkTerms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trademark_terms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trademark_id', 'term_id'], 'required'],
            [['trademark_id', 'term_id'], 'integer'],
            [['trademark_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trademarks::className(), 'targetAttribute' => ['trademark_id' => 'id']],
            [['term_id'], 'exist', 'skipOnError' => true, 'targetClass' => Terms::className(), 'targetAttribute' => ['term_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trademark_id' => 'Trademark ID',
            'term_id' => 'Term ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrademark()
    {
        return $this->hasOne(Trademarks::className(), ['id' => 'trademark_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerm()
    {
        return $this->hasOne(Terms::className(), ['id' => 'term_id']);
    }
}
