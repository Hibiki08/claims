<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tm_foreign_classes".
 *
 * @property integer $tm_id
 * @property integer $class_id
 *
 */
class TmForeignClass extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'tm_foreign_classes';
    }

    public function getClass() {
        return $this->hasOne(Classes::className(), ['id' => 'class_id']);
    }

}
