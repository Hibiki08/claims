<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $user_id
 * @property string $item_name
 * @property string $created_at
 *
 */
class AuthAssignment extends ActiveRecord {

    public static function tableName() {
        return 'auth_assignment';
    }
    
}
