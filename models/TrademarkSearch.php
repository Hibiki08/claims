<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\sphinx\Query as SphinxQuery;
use yii\sphinx\MatchExpression;
use yii\db\Query;
use yii\db\Expression;
use yii\db\ActiveQuery;
use app\models\Trademarks;

/**
 * RequestSearch represents the model behind the search form about `app\models\Requests`.
 */
class TrademarkSearch extends Trademarks
{
    public $database = 'trademarks';
    public $searchArea = 'all';
    public $searchString;
    public $searchClasses;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reg_number', 'request_number', 'status_id'], 'integer'],
            [['searchClasses', 'reg_date', 'request_date', 'reg_expiry_date', 'publish_date', 'publication_pdf', 'legal_protection_end_date', 'rightholder', 'address', 'first_request_number', 'first_request_date', 'first_request_country', 'colors', 'classes', 'priority', 'licensee', 'contract_number', 'contract_date', 'license_terms', 'img', 'status_date', 'update_time'], 'safe'],
            [['searchArea', 'searchString', 'database'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $match = new MatchExpression();
        $log = $this->searchString;

        switch ($this->searchArea) {
            case 'all':
                $match->match('"'.$this->searchString.'"');
                $log .= ' (Область поиска: по товарным знакам, по всему';
                break;
            case 'applicant':
                $match->match(['rightholder' => $this->searchString]);
                $log .= ' (Область поиска: по товарным знакам, по правообладателю';
                break;
            case 'address':
                $match->match(['address' => $this->searchString]);
                $log .= ' (Область поиска: по товарным знакам, по адресу для переписки';
                break;
        }

        if (!empty($this->searchClasses)) {
            $log .= ', классы: ';
            $log .= implode(', ', array_keys($this->searchClasses));
        }

        $rows = (new SphinxQuery())
            ->select('id')
            ->from('trademarks')
            ->showMeta(true)
            ->options([
                'max_matches' => 1000,
            ])
            ->limit(1000)
            ->match($match);

        if (!empty($this->searchClasses)) {
            $rows->where(['in', 'class_id', array_keys($this->searchClasses)]);
        }

        $rows = $rows->search();

        $ids = [];
        foreach ($rows['hits'] as $row) {
            $ids[] = $row['id'];
        }

        $query = Trademarks::find()
            ->where(['in','id',$ids]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $log .= ')';
        if (!empty($this->searchString)) {
            $searchLog = new Searchlogs();
            $searchLog->query = $log;
            $searchLog->user_id = Yii::$app->user->getId();
            $searchLog->save(false);
        }

        return $dataProvider;
    }
}
