<?php

namespace app\models;

use Yii;
use yii\base\Model;
use Telegram\Bot\Keyboard\Keyboard;
use yii\web\UploadedFile;
use app\components\Translate;
use yii\helpers\FileHelper;
use Telegram\Bot\Exceptions\TelegramResponseException;


class SupportForm extends Model {

    public $name;
    public $theme;
    public $message;
    public $file;

    const MESSAGE = 'message';
    const NEW_TICKET = 'new_ticket';

    public function rules() {
        return [
            [['name', 'theme', 'message'], 'required'],
            [['message', 'name'], 'string'],
            [['file'], 'file',
                'extensions' => ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'txt', 'docx',
                    'pdf', 'doc', 'rtf', 'xls', 'xlsx', 'tiff'],
                'skipOnEmpty' => true]
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::NEW_TICKET] = ['name','theme', 'message', 'file'];
        $scenarios[self::MESSAGE] = ['message', 'file'];
        return $scenarios;
    }

    public function attributeLabels() {
        return [
            'name' => 'Имя',
            'theme' => 'Тема запроса',
            'message' => 'Сообщение',
            'file' => 'Файл или скриншот'
        ];
    }

    public function upload($ticketId) {
        if (isset($this->file->name) && !is_null($this->file->name)) {
            $translate = new Translate();
            $this->file->name = $translate->translate($this->file->name);
            $path = 'resources/images/tickets/' . $ticketId . '/';
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
            }
            if ($this->file->saveAs($path . $this->file->name)) {
                return $path . $this->file->name;
            }
        }
        return false;
    }

    public static function getFileSize($path){
        $fileSize = filesize($path) ;
        return Yii::$app->formatter->asShortSize($fileSize) ;
    }

    public function sendMessage($ticketId) {
        if ($this->validate()) {
            $ticket = Ticket::getTicketById($ticketId, ['closed' => 0, 'user_id' => Yii::$app->user->id]);

            if (!empty($ticket)) {
                $message = new SupportMessage();
                $message->user_message = trim(strip_tags($this->message));

                if ($path = $this->upload($ticketId)) {
                    if (stristr($this->file->type, 'image')) {
                        $message->user_img = $path;
                    } else {
                        $message->user_file = $path;
                    }
                    $message->file_size = $this->file->size;
                }
                $message->ticket_id = $ticketId;
                if ($message->save()) {
                    $telegram = Yii::$app->MonocleSupportBot;

                    if (!is_null($ticket->admin_id)) {
                        try {
                            $telegram->api->sendMessage([
                                'chat_id' => $ticket->admin->telegram_id,
                                'text' => trim(strip_tags($this->message))
                            ]);
                            if (!empty($message->user_img)) {
                                $telegram->api->sendPhoto([
                                    'chat_id' => $ticket->admin->telegram_id,
                                    'photo' => Yii::$app->request->baseUrl . $message->user_img
                                ]);
                            }

                            if (!empty($message->user_file)) {
                                $telegram->api->sendDocument([
                                    'chat_id' => $ticket->admin->telegram_id,
                                    'document' => Yii::$app->urlManager->createAbsoluteUrl(['/']) . $message->user_file,
                                ]);
                            }
                        } catch (TelegramResponseException $e) {
                        }
                    } else {
                        $supportUsers = Users::getUsersWithSupportRole();
                        if (!empty($supportUsers)) {
                            foreach ($supportUsers as $supportUser) {
                                try {
                                    $text = 'Тикет #' . $ticket->id . "\r\n" .
                                        '<b>Тема:</b> ' . $ticket->theme . "\r\n" .
                                        '<b>Сообщение:</b> ' . trim(strip_tags($this->message)) . "\r\n" .
                                        '<b>Имя:</b> ' . Yii::$app->user->identity->name . "\r\n" .
                                        '<b>Email:</b> ' . Yii::$app->user->identity->login . "\r\n" .
                                        '<b>user-agent:</b> ' . Yii::$app->request->getUserAgent();
                                    $telegram->keyboardStartChat($ticket->id, $supportUser->telegram_id, $text);

                                    if (!empty($message->user_img)) {
                                        $telegram->api->sendPhoto([
                                            'chat_id' => $supportUser->telegram_id,
                                            'photo' => Yii::$app->request->baseUrl . $message->user_img,
                                            'caption' => 'Тикет #' . $ticket->id
                                        ]);
                                    }

                                    if (!empty($message->user_file)) {
                                        $telegram->api->sendDocument([
                                            'chat_id' => $supportUser->telegram_id,
                                            'document' => Yii::$app->urlManager->createAbsoluteUrl(['/']) . $message->user_file,
                                            'caption' => 'Тикет #' . $ticket->id
                                        ]);
                                    }
                                } catch (TelegramResponseException $e) {
                                    continue;
                                }
                            }
                        }
                    }
                    return true;
                }
            } else {
                Yii::$app->session->setFlash('error', 'Вы не можете писать в закрытый тикет. Если у вас остались вопросы, создайте новый запрос.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Вы не ввели текст');
        }
        return false;
    }

    public function newTicket () {
        if ($this->validate()) {
            $ticket = new Ticket();
            $ticket->theme = trim(strip_tags($this->theme));
            $ticket->user_id = Yii::$app->user->id;
            if ($ticket->save()) {
                $message = new SupportMessage();
                $message->user_message = trim(strip_tags($this->message));
                $message->ticket_id = $ticket->id;

                $this->file = UploadedFile::getInstance($this, 'file');
                if ($path = $this->upload($ticket->id)) {
                    if (stristr($this->file->type, 'image')) {
                        $message->user_img = $path;
                    } else {
                        $message->user_file = $path;
                    }
                    $message->file_size = $this->file->size;
                }

                if ($message->save()) {
                    $user = Users::findIdentity(Yii::$app->user->id);
                    $user->name = trim(strip_tags($this->name));
                    $user->update();

                    $supportUsers = Users::getUsersWithSupportRole();
                    if (!empty($supportUsers)) {
                        $telegram = Yii::$app->MonocleSupportBot;

                        foreach ($supportUsers as $supportUser) {
                            try {
                                $text = 'Тикет #' . $ticket->id . "\r\n" .
                                    'Тема: ' . trim(strip_tags($this->theme)) . "\r\n" .
                                    'Сообщение: ' . trim(strip_tags($this->message)) . "\r\n" .
                                    'Имя: ' . $this->name . "\r\n" .
                                    'Email: ' . Yii::$app->user->identity->login . "\r\n" .
                                    'user-agent: ' . Yii::$app->request->getUserAgent();
                                $telegram->keyboardStartChat($ticket->id, $supportUser['telegram_id'], $text);

                                if (!empty($message->user_img)) {
                                    $telegram->api->sendPhoto([
                                        'chat_id' => $supportUser['telegram_id'],
                                        'photo' => Yii::$app->request->baseUrl . $message->user_img,
                                        'caption' => 'Тикет #' . $ticket->id
                                    ]);
                                }

                                if (!empty($message->user_file)) {
                                    $telegram->api->sendDocument([
                                        'chat_id' => $supportUser['telegram_id'],
                                        'document' => Yii::$app->urlManager->createAbsoluteUrl(['/']) . $message->user_file,
                                        'caption' => 'Тикет #' . $ticket->id
                                    ]);
                                }
                            } catch (TelegramResponseException $e) {
                                continue;
                            }
                        }
                    }

                    return $ticket->id;
                }
            }
        }
        return null;
    }

}
