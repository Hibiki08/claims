<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Users;
use yii\helpers\Html;


class SignupForm extends Model {

    public $email;
    public $password;
    public $password_repeat;

    public function rules() {
        return [
            ['email', 'trim'],
            [['email', 'password', 'password_repeat'], 'required'],
            [['password', 'password_repeat'], 'string', 'min' => 6, 'max' => 255],
            ['password', 'compare', 'compareAttribute' => 'password_repeat', 'message' => 'Пароли не совпадают'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique',
                'targetClass' => '\app\models\Users',
                'targetAttribute' => 'login',
                'message' => 'Пользователь с указанным почтовым адресом уже зарегистрирован'
            ],
        ];
    }

    public function signup() {

        if (!$this->validate()) {
            return null;
        }

        $email = Html::encode($this->email);

        $user = new Users();
        $user->login = $email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->status = $user::STATUS_DELETED;
        $user->telegram_key = md5(time() . 'DLfljD9^ns,A[p');

        if ($user->save()) {
            Yii::$app->mailer->compose('emailConfirm', ['user' => $user])
                ->setFrom([Yii::$app->params['projectEmail'] => Yii::$app->name . ' robot'])
                ->setTo($this->email)
                ->setSubject('Подтверждение регистрации на ' . Yii::$app->name)
                ->send();


            return $user;
        }
        return null;
    }

    public function attributeLabels(){
        return [
            'email' => 'Ваш email',
            'password' => 'Ваш пароль',
            'password_repeat' => 'Повторите пароль',
        ];
    }

}
