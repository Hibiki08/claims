<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foreign_statuses".
 *
 * @property integer $id
 * @property string $title
 *
 */
class ForeignStatus extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'foreign_statuses';
    }
    
}
