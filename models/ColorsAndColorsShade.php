<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $color_shade_id
 * @property string $color_id
 *
 */
class ColorsAndColorsShade extends ActiveRecord {

    public static function tableName() {
        return 'colors_and_colors_shade';
    }

    public function getColor() {
        return $this->hasOne(Colors::className(), ['id' => 'color_id']);
    }

    

}