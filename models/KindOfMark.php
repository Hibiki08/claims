<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kind_of_mark".
 *
 * @property integer $id
 * @property string $name
 *
 */
class KindOfMark extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'kind_of_mark';
    }
    
}
