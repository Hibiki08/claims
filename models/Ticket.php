<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 *
 * @property integer $id
 * @property string $theme
 * @property integer $user_id
 * @property integer $admin_id
 * @property integer $notice
 * @property integer $closed
 * @property string $date
 *
 */
class Ticket extends ActiveRecord {

    public static function tableName() {
        return 'tickets';
    }

    public function getMessages() {
        return $this->hasMany(SupportMessage::className(), ['ticket_id' => 'id'])
            ->orderBy('time');
    }

    public function getAdmin() {
        return $this->hasOne(Users::className(), ['id' => 'admin_id'])
            ->alias('admin');
    }

    public static function getTicketById($id, $where = false) {
        $query = self::find()
            ->where([self::tableName() . '.id' => $id])
            ->joinWith('messages')
            ->joinWith('admin');
        
        if ($where) {
            $query->andWhere($where);
        }
        return $query->one();
    }
    
    public static function getOpenTicketByAdmin($adminId) {
        return self::findOne(['admin_id' => $adminId, 'closed' => 0]);
    }

    public static function getTicketsWithoutReply() {
        return self::find()->where(['admin_id' => null, 'closed' => 0])
            ->all();
    }

    public static function getTicketsByUser() {
        $query = self::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->orderBy(['id' => SORT_DESC]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    public static function getLastUsersTicket() {
        return self::find()
            ->where(['and', [self::tableName() . '.user_id' => Yii::$app->user->id], ['closed' => 0]])
            ->orderBy(['date' => SORT_DESC])
            ->limit(1)
            ->one();
    }

}