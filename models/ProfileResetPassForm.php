<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Users;


class ProfileResetPassForm extends Model {

    public $new_pass;
    public $repeat_pass;
    public $password;
    public $email;
    private $_user;

    public function rules() {
        return [
            [['password', 'new_pass', 'repeat_pass'], 'required'],
            ['new_pass', 'compare', 'compareAttribute' => 'repeat_pass', 'message' => 'Пароли не совпадают'],
            ['password', 'validatePassword'],
            [['new_pass', 'repeat_pass', 'email'], 'string'],
        ];
    }

    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный пароль!');
            }
        }
    }

    protected function getUser() {
        if ($this->_user === null) {
            $this->_user = Users::findByLogin($this->email);
        }

        return $this->_user;
    }

    public function attributeLabels() {
        return [
            'new_pass' => 'Новый пароль',
            'repeat_pass' => 'Повторите пароль',
            'password' => 'Старый пароль',
        ];
    }

}