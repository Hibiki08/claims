<?php

namespace app\models;

use Yii;
use yii\db\Command;
use yii\imagine\Image;
use Imagine\Image\Box;
use app\components\RequestAndTMModel;
use yii\sphinx\Query as SphinxQuery;
use yii\db\Expression;
use Zend\Http\PhpEnvironment\Request;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "requests".
 *
 * @property integer $id
 * @property integer $request_number
 * @property integer $reg_number
 * @property string $request_date
 * @property string $reg_date
 * @property string $applicant
 * @property string $address
 * @property string $first_request_number
 * @property string $first_request_date
 * @property string $first_request_country
 * @property string $classes
 * @property string $img
 * @property string $captcha_id
 * @property string $facsimile
 * @property integer $status_id
 * @property integer $cstatus_id
 * @property string $update_time
 * @property integer $rank
 *
 * @property RequestClasses[] $requestClasses
 * @property RequestTerms[] $requestTerms
 * @property Statuses $status
 */
class Requests extends RequestAndTMModel
{
    public $rank;
    public $count_request;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_number', 'status_id'], 'required'],
            [['request_number', 'reg_number', 'status_id', 'cstatus_id'], 'integer'],
            [['count_request', 'request_date', 'reg_date', 'first_request_date', 'update_time'], 'safe'],
            [['applicant', 'address', 'first_request_number', 'first_request_country', 'classes', 'img', 'facsimile'], 'string'],
            [['request_number'], 'unique'],
            [['status_id', 'cstatus_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_number' => 'Номер заявки',
            'reg_number' => 'Номер регистрации',
            'request_date' => 'Дата поступления заявки',
            'reg_date' => 'Дата публикации',
            'applicant' => 'Заявитель',
            'address' => 'Адрес для переписки',
            'first_request_number' => 'Номер первой заявки',
            'first_request_date' => 'Дата подачи первой заявки',
            'first_request_country' => 'Код страны подачи первой заявки',
            'classes' => 'Классы МКТУ и перечень товаров и/или услуг',
            'img' => 'Изображение заявляемого обозначения',
            'facsimile' => 'Факсимильные изображения',
            'status_id' => 'Статус заявки',
            'cstatus_id' => 'Статус заявки',
            'update_time' => 'Update Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestClasses()
    {
        return $this->hasMany(RequestClasses::className(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestTerms()
    {
        return $this->hasMany(RequestTerms::className(), ['request_id' => 'id']);
    }

//    public function getCommonCompanies() {
//        return $this->hasMany(Trademarks::className(), ['rightholder' => 'applicant']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'status_id']);
    }
    public function getCstatus()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'cstatus_id']);
    }

    public function getThumb() {
        if (file_exists($this->img) && exif_imagetype($this->img)) {
            $tmbPath = substr($this->img, 0, strpos($this->img, '.'));
            $ext = substr($this->img, strpos($this->img, '.'));
            $tmbPath .= "_tmb$ext";
//            if (!file_exists($tmbPath)) {
                Image::getImagine()->open($this->img)->thumbnail(new Box(130, 100))->save($tmbPath , ['quality' => 90]);
//            }
            return $tmbPath;
        }
        return NULL;
    }

    public static function getRecognizedAndEmpty($limit = false, $dataProvider = false) {
        $requestTerms = RequestTerms::find()
            ->select('request_id')->groupBy('request_id');

        $query = self::find()
            ->where(['and',
                ['not', ['img' => null]],
                ['>', 'captcha_id', 0],
                ['not in', 'id', $requestTerms]
            ]);

        if ($limit) {
            $query->limit($limit);
        }

        if ($dataProvider) {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
            return $dataProvider;
        }
        return $query->all();
    }

    public static function getCountRecognizedAndEmpty() {
        $requestTerms = RequestTerms::find()
            ->select('request_id')->groupBy('request_id');

        return self::find()
            ->where(['and',
                ['not', ['img' => null]],
                ['>', 'captcha_id', 0],
                ['not in', 'id', $requestTerms]
            ])
            ->count();
    }
    
    public function searchApplicant($params, $limit = 50) {
        $query = (new \yii\db\Query())
            ->select(new Expression('replace(replace(applicant, "  ", " "), "  ", " ") as applicant, count(*) as count_request'))
            ->from(self::tableName())
            ->groupBy(new Expression('replace(replace(applicant, "  ", " "), "  ", " ")'))
            ->having('count(*) > 1')
            ->where(['not', ['applicant' => null]])
            ->orderBy(['count_request' => SORT_DESC])
            ->limit($limit);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'count_request'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'applicant', $this->applicant]);
        
        return $dataProvider;
    }
    
}
