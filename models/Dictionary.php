<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dictionary".
 *
 * @property integer $id
 * @property integer $len
 * @property string $word
 * @property string $trigrams
 */
class Dictionary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['len'], 'integer'],
            [['word'], 'required'],
            [['word', 'trigrams'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'len' => 'Length',
            'word' => 'Word',
            'trigrams' => 'Trigrams',
        ];
    }

    public static function makeTrigrams($word) {
        $chars = static::mbStringToArray($word);
        $len=count($chars);
        $trigrams = [];
        switch (true) {
            case ($len > 1):
                $trigrams[0] = "__{$chars[0]}";
                $trigrams[1] = "_{$chars[0]}{$chars[1]}";
                break;
            case ($len == 1):
                $trigrams[0] = $chars[0];
                break;
        }

        for ($i = 2; $i < $len; $i++) {
            $trigrams[$i]="{$chars[$i - 2]}{$chars[$i - 1]}{$chars[$i]}";
        }

        if ($len > 1) {
            $trigrams[] = "{$chars[$len-2]}{$chars[$len-1]}_";
            $trigrams[] = "{$chars[$len-1]}__";
        }

        return implode(' ', $trigrams);
    }

    private static function mbStringToArray ($string) {
        $strlen = mb_strlen($string);
        $array = [];
        while ($strlen) {
            $array[] = mb_substr($string,0,1,"UTF-8");
            $string = mb_substr($string,1,$strlen,"UTF-8");
            $strlen = mb_strlen($string);
        }
        return $array;
    }
}
