<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statuses".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Requests[] $requests
 */
class Statuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Requests::className(), ['status_id' => 'id']);
    }

    public function getStatusID($title) {
        $res = 1;
        $result = Statuses::find()
            ->select('id')
            ->where(['title' => $title])
            ->one();
        if (!empty($result)) {
            $res = $result['id'];
        } elseif (!empty($title)) {
            $status = new Statuses();
            $status->title = $title;
            if ($status->save(false)) {
                $res = $status->id;
            }
        }
        return $res;
    }
    
    public static function getStatusById($id) {
        return Statuses::find()
            ->select('title')
            ->where(['id' => $id])
            ->one();
    } 
    
}
