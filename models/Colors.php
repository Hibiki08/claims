<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer $id
 * @property string $name
 * @property string $hex_bg
 * @property string $hex_text
 *
 */
class Colors extends ActiveRecord {

    public static function tableName() {
        return 'colors';
    }

    public static function getAll() {
        return self::find()->all();
    }

    public static function getCount() {
        return self::find()->count();
    }

}