var page = new WebPage();
var system = require('system');

var address = system.args[1];
var timout = system.args[2];
var browser = system.args[3];

page.settings.userAgent = browser;
// page.setProxy("http://80.211.189.165:3128/");

page.open(address, function (status) {
    just_wait();
});

function just_wait() {
    setTimeout(function() {
        var content = page.content;
        console.log(content);
        phantom.exit();
    }, timout);
}
