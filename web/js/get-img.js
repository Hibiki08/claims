var page = new WebPage();
var system = require('system');
var address = system.args[1];
var path = system.args[2];
var browser = system.args[3];

page.settings.userAgent = browser;

page.open(address, function (status) {
    if (status === 'success') {
        setTimeout(function() {
            page.clipRect = page.evaluate(function () {
                var element = document.getElementsByTagName('img')[0];
                var rect = element.getBoundingClientRect();

                return {
                    top: rect.top,
                    left: rect.left,
                    width: rect.width,
                    height: rect.height
                };
            });

            page.render(path);
            // console.log(JSON.stringify(page.clipRect));
            phantom.exit();
        }, 2000);
    } else {
        phantom.exit();
    }
});