var Xvfb = require('xvfb');
var Nightmare = require('nightmare');


var xvfb = new Xvfb({
    silent: true
});

var nightmare = Nightmare({
    show: false,
    webPreferences: {
        partition: 'custom'
    }
});


try {
    xvfb.start(function() {
        nightmare
            .goto('https://www.tmdn.org/tmview/search-tmv?_search=false&nd=1530104528502&rows=100&sidx=ad&sord=asc&q=tp%3ARU+AND+oc%3AWO&fq=%5B%5D&pageSize=100&facetQueryType=2&selectedRowRefNumber=null&providerList=null&expandedOffices=null&page=1')
            .wait('pre')
            .evaluate(function (e) {
                return document.querySelector('pre').innerHTML
            })
            .end()
            .then(function (result) {
                console.log(result);
                xvfb.stop();
            });
    });
} catch (e) {
    xvfb.stop();
}