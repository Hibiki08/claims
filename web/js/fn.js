//chat support
function chatReload() {
    $.pjax.reload({
        container:"#pjax-chat",
        enablePushState: false,
        timeout: false
    });
}

interval = setInterval(chatReload, 7000);

$(document).ready(function () {
    hideClasses();

    $('.js-search-form-classes_selectall').click(function (e) {
        e.preventDefault();
        $('.js-search-form-classes_checkbox').prop('checked', true);
    });
    $('.js-search-form-classes_deselectall').click(function (e) {
        e.preventDefault();
        $('.js-search-form-classes_checkbox').prop('checked', false);
    });

    if (document.documentElement.clientWidth < 500) {

        $('.notice-bell a').addClass('notice-bell-link').insertAfter( $( ".navbar-brand" ) );
        $('<div class="tarif-link"></div>').insertAfter( $( ".notice-bell-link" ) );
        $('<div class="login-link"></div>').insertAfter( $( ".tarif-link" ) );


        var tarifInfo = $('.tarif.prem').html();
        $('<div class="tarif-info">' + tarifInfo + '</div>').insertAfter( $( ".tarif-link" ) );
        $('.tarif-link').on('click', function(e) {

            $('.tarif-info').toggleClass('active');
            e.stopPropagation();
        });

        $(document).click(function() {
            $('.tarif-info').removeClass('active');
            /*$('.user-info').removeClass('active');*/
        });

        /*var userInfo = $('#w4').html();
         $('<div class="user-info"><ul id="w4">' + userInfo + '</ul></div>').insertAfter( $( ".login-link" ) );
         $('.login-link').on('click', function(e) {

         $('.user-info').toggleClass('active');
         e.stopPropagation();
         });*/

    }

    $('body').on('click', '.js-wrong_tags', function (e) {
        e.preventDefault();
        var container = $(this).data('container');
//        $('body').fadeTo('slow', 0.3);
        $(this).html('Распознается&hellip;').removeClass('btn-primary').attr('disabled', 'disabled');
        $.ajax({
            type: "GET",
            timeout: 200000,
            url: $(this).attr('href'),
            success: function (data) {
                if (data) {
                    $.pjax.reload({container:"#pjax-grid"});
                }
            }
        });
    })
        .on('input', '#similarity', function(){
            $('.js_similarity')[0].innerHTML = $(this).val()
        })
        .on('change', '.js-err_trademark_update', function () {
            $.post('/request/tm-set-cstatus?id='+$(this).data('id'), {
                'cstatus_id': $(this).val()
            }, function (data) {
                console.log(data);
            });
        }).on('click', '.colors-wrapper .sortable2 li .glyphicon-remove', function () {
        var agree = confirm('Вы действительно хотите удалить эту связь?');
        if (agree) {
            // $('.colors-wrapper .progress').show();
            $('body').fadeTo(100, 0.3);
            var $thisColor = $(this).parent('li');
            var shade_color_id = $(this).parents('tr').data('key');
            var color_id = $(this).parent('li').data('color_id');

            $.ajax({
                url: '/service/color-delete',
                type: 'post',
                data: {
                    shade_color_id: shade_color_id,
                    color_id: color_id,
                    _csrf: yii.getCsrfToken()
                },
                timeout: 600000,
                success: function (data) {
                    console.log('ok');
                    console.log(data);
                    if (data.status > 0) {
                        $.pjax.reload({container: '#pjax-grid'});
                        $thisColor.remove();
                    }
                },
                error: function (data) {
                    console.log('bad');
                    console.log(data);
                }
            });
        }
    }).on('click', '.tracking table .delete', function () {     // Tracking delete
        var agree = confirm('Вы действительно хотите удалить этот пункт из отслеживания?');
        if (agree) {
            var $this = $(this);
            var id = $this.parents('tr').data('key');
            var model = $this.data('model');
            $('body').fadeTo(100, 0.3);

            $.ajax({
                url: '/tracking/delete',
                type: 'post',
                data: {
                    id: id,
                    model: model,
                    _csrf: yii.getCsrfToken()
                },
                timeout: 600000,
                success: function (data) {
                    if (data.status > 0) {
                        $this.parents('tr').remove();
                        $('body').fadeTo(100, 1);
                    }
                },
                error: function (data) {
                    console.log('bad');
                    $('body').fadeTo(100, 1);
                    console.log(data);
                }
            });
        }
    }).on('click', '.users table .delete', function () {     // users delete
        var agree = confirm('Вы действительно хотите удалить этот пункт из отслеживания?');
        if (agree) {
            var $this = $(this);
            var id = $this.parents('tr').data('id');
            $('body').fadeTo(100, 0.3);

            $.ajax({
                url: '/service/users/delete',
                type: 'post',
                data: {
                    id: id,
                    _csrf: yii.getCsrfToken()
                },
                timeout: 600000,
                success: function (data) {
                    if (data.status > 0) {
                        $this.parents('tr').remove();
                        $('body').fadeTo(100, 1);
                    }
                },
                error: function (data) {
                    console.log('bad');
                    $('body').fadeTo(100, 1);
                    console.log(data);
                }
            });
        }
    }).on('change', '.select-role', function () {
        $('body').fadeTo(100, 0.3);
        var $this = $(this);
        $.post('/service/users/set-role?id=' + $this.parents('tr').data('id'), {
            'role' : $this.val(),
            'oldRole' : $this.attr('data-role')
        }, function (e) {
            $this.attr('data-role', $this.val());
            $('body').fadeTo(100, 1);
            console.log(e);
        })
    }).on('click', '.users .checkbox input', function () {
        $('body').fadeTo(100, 0.3);
        var checked;
        var id = $(this).parents('tr').data('id');
        if ($(this).prop('checked')) {
            checked = 1;
        } else {
            checked = 0;
        }

        $.post('/service/users/set-support', {
            'checked': checked,
            'id': id
        }, function (data) {
            $('body').fadeTo(100, 1);
            console.log(data);
        });

    }).on('change', '.users .datepicker', function(){
        $('body').fadeTo(100, 0.3);
        var id = $(this).parents('tr').data('id');

        $.post('/service/users/subscribe-date', {
            'id': id,
            'date': $(this).val()
        }, function (data) {
            $('body').fadeTo(100, 1);
            console.log(data);
        });
    }).on('click', '.tracking table .switch input', function () {     // Tracking active
        var $this = $(this);
        var active = $this.prop('checked') ? 1 : 0;
        var id = $this.parents('tr').data('key');
        var model = $this.data('model');

        $('body').fadeTo(100, 0.3);
        $.ajax({
            url: '/tracking/active',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                active: active,
                model: model,
                _csrf: yii.getCsrfToken()
            },
            timeout: 600000,
            success: function (data) {
                if (data.status !== false) {
                    if (active) {
                        $this.parents('.switch').attr('title', 'Деактивировать');
                    } else {
                        $this.parents('.switch').attr('title', 'Активировать');
                    }
                    $('body').fadeTo(100, 1);
                }
            },
            error: function (data) {
                console.log('bad');
                console.log(data);
                $('body').fadeTo(100, 1);
            }
        });
    }).on('click', '.users table .switch input', function () {     // users active
        var $this = $(this);
        var active = $this.prop('checked') ? 1 : 0;
        var id = $this.parents('tr').data('id');

        $('body').fadeTo(100, 0.3);
        $.ajax({
            url: '/service/users/active',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                active: active,
                _csrf: yii.getCsrfToken()
            },
            timeout: 600000,
            success: function (data) {
                if (data.status !== false) {
                    if (active) {
                        $this.parents('.switch').attr('title', 'Деактивировать');
                    } else {
                        $this.parents('.switch').attr('title', 'Активировать');
                    }
                    $('body').fadeTo(100, 1);
                }
            },
            error: function (data) {
                console.log('bad');
                console.log(data);
                $('body').fadeTo(100, 1);
            }
        });
    }).on('click', '.tracking th.seen input', function () {    // Tracking check
        if ($(this).prop("checked")) {
            $('.tracking table tr.success .seen-check input').prop('checked', 'checked');
        } else {
            $('.tracking table tr.success .seen-check input').prop('checked', false);
        }
    }).on('click', '.tracking #save', function () {
        $('body').fadeTo(100, 0.3);
        var tracks = {};
        var i = 0;
        $('.tracking table tbody tr').each(function () {
            tracks[i] = {};
            tracks[i].id = $(this).data('id');
            if ($(this).hasClass('success')) {
                if ($(this).find('td.seen-check input').prop('checked')) {
                    tracks[i].notice = 0;
                } else {
                    tracks[i].notice = 1;
                }
            }

            if ($(this).find('td.active input').prop('checked')) {
                tracks[i].active = 1;
            } else {
                tracks[i].active = 0;
            }
            i++;
        });

        $.ajax({
            url: '/tracking/save',
            type: 'post',
            dataType: 'json',
            data: {
                tracks: tracks,
                _csrf: yii.getCsrfToken()
            },
            timeout: 600000,
            success: function (data) {
                if (data.status !== false) {
                    // $.pjax.reload({container: '#pjax-tracking'});
                    location.reload();
                }
            },
            error: function (data) {
                console.log('bad');
                console.log(data);
            }
        });
    }).on('click', '.tags-wrap .fa-plus', function () {
        $(this).before('<div class="add-term"><input type="text" data-term="add"><i class="glyphicon glyphicon-ok agree"></i></div>');
    }).on('keyup', '.tags-wrap .add-term input', function (e) {
        var val = $(this).val();
        $(this).val($.trim(val));
        if(e.keyCode == 13) {
            $('.tags-wrap .add-term .agree').click();
        }
    }).on('click', '.tags-wrap .add-term .agree', function () {
        var $this = $(this);
        var val = $this.parents('.add-term').find('input').val();
        var type = $this.parents('tr').data('model');
        var id = $this.parents('tr').data('id');

        $.ajax({
            url: '/request/add-term',
            type: 'post',
            dataType: 'json',
            data: {
                val: val,
                type: type,
                id: id,
                _csrf: yii.getCsrfToken()
            },
            timeout: 600000,
            success: function (data) {
                if (data.res !== false) {
                    var string = '<p><small>' + val.toLowerCase() + ' <i class="fa fa-times delete-term" aria-hidden="true" data-term_id="' + data.term_id + '"></i></small></p>';
                    $this.parents('.add-term').before(string);
                    $this.parents('.add-term').remove();
                }
            },
            error: function (data) {
                console.log('bad');
                console.log(data);
            }
        });
    })
        .on('click', '.tags-wrap .delete-term', function () {
            var conf = confirm('Вы действительно хотите удалить этот тэг?');
            if (conf) {
                var $this = $(this);
                var term_id = $this.data('term_id');
                var type = $this.parents('tr').data('model');
                var id = $this.parents('tr').data('id');

                $.ajax({
                    url: '/request/delete-term',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        term_id: term_id,
                        type: type,
                        id: id,
                        _csrf: yii.getCsrfToken()
                    },
                    timeout: 600000,
                    success: function (data) {
                        if (data.res !== false) {
                            $this.parents('p').remove();
                        }
                    },
                    error: function (data) {
                        console.log('bad');
                        console.log(data);
                    }
                });
            }
        })
        .on('click', '.requests-index #add-to-track', function (e) {
            e.preventDefault();
            $('body').fadeTo(100, 0.3);
            var url = window.location.href;

            $.ajax({
                url: '/request/add-request',
                type: 'post',
                dataType: 'json',
                data: {
                    url: url,
                    _csrf: yii.getCsrfToken()
                },
                timeout: 600000,
                success: function (data) {
                    console.log(data);
                    if (data.status) {
                        location.reload();
                    } else {
                        location.reload();
                    }
                },
                error: function (data) {
                    console.log('bad');
                    console.log(data);
                    location.reload();
                }
            });
        }).on('click', '.requests-index .add-item', function (e) {
        var $this = $(this);
        var id = $this.data('id');
        var model = $this.data('model');
        $('body').fadeTo(100, 0.3);

        $.ajax({
            url: '/tracking/add-item',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                model: model,
                _csrf: yii.getCsrfToken()
            },
            timeout: 600000,
            success: function (data) {
                $('body').fadeTo(100, 1);
                if (data.status) {
                    $this.parents('tr').find('.helper p').addClass('bg-success').text(data.message);
                } else {
                    $this.parents('tr').find('.helper p').addClass('bg-error').text(data.message);
                }
            },
            error: function (data) {
                $('body').fadeTo(100, 1);
                console.log('bad');
                console.log(data);
                // location.reload();
            }
        });
    }).on('click', '.notifications #telegram-notice', function () {
        $('body').fadeTo(100, 0.3);
        var checked;
        if ($(this).prop('checked')) {
            checked = 1;
        } else {
            checked = 0;
        }

        $.ajax({
            url: '/profile/notifications',
            type: 'post',
            dataType: 'json',
            data: {
                checked: checked,
                _csrf: yii.getCsrfToken()
            },
            timeout: 600000,
            success: function (data) {
                console.log(data);
                if (data.status) {
                    $('body').fadeTo(100, 1);
                }
            },
            error: function (data) {
                $('body').fadeTo(100, 1);
            }
        });
    }).on('click', '.start-content .tabs > div', function () {
        $('.start-content .tabs > div').removeClass('active');
        $('.start-content form').removeClass('active');

        $(this).addClass('active');
        if ($(this).hasClass('login')) {
            $('#login-form').addClass('active');
        }
        if ($(this).hasClass('signup')) {
            $('#form-signup').addClass('active');
        }
    }).on('click', '#request-recognize', function () {
        $('#request-recognize .spinner').addClass('active');
        $.ajax({
            url: '/parser-request/tag-update',
            type: 'post',
            dataType: 'json',
            data: {
                _csrf: yii.getCsrfToken(),
            },
            timeout: 9000000,
            success: function (data) {
                console.log(data.next);
                if (data.next) {
                    $('#request-recognize').click();
                    $('#request-recognize .current-count span').html(data.count);
                } else {
                    $('#request-recognize .spinner').removeClass('active');
                    location.reload();
                }
            },
            error: function(xhr, textStatus) {
                alert([xhr.status, textStatus]);
            }
        });
    }).on('click', '#trademark-recognize', function () {
        $('#trademark-recognize .spinner').addClass('active');
        $.ajax({
            url: '/parser-trademark/tag-update',
            type: 'post',
            dataType: 'json',
            data: {
                _csrf: yii.getCsrfToken(),
            },
            timeout: 9000000,
            success: function (data) {
                console.log(data);
                if (data.next) {
                    $('#trademark-recognize').click();
                    $('#trademark-recognize .current-count span').html(data.count);
                } else {
                    $('#trademark-recognize .spinner').removeClass('active');
                    location.reload();
                }
            },
            error: function(xhr, textStatus) {
                alert([xhr.status, textStatus]);
            }

        });
    }).on('click', '#tm-foreign-recognize', function () {
        $('#tm-foreign-recognize .spinner').addClass('active');
        $.ajax({
            url: '/parser-tm-foreign/tag-update',
            type: 'post',
            dataType: 'json',
            data: {
                _csrf: yii.getCsrfToken(),
            },
            timeout: 9000000,
            success: function (data) {
                console.log(data);
                if (data.next) {
                    $('#tm-foreign-recognize').click();
                    $('#tm-foreign-recognize .current-count span').html(data.count);
                } else {
                    $('#tm-foreign-recognizee .spinner').removeClass('active');
                    location.reload();
                }
            },
            error: function(xhr, textStatus) {
                alert([xhr.status, textStatus]);
            }

        });
    }).on('click', '.tracking table .edit', function () {
        var title = $(this).parents('tr').find('h4.title');
        if (title.text().length != 0) {
            title.html('<input value="' + title.text() + '"><i class="glyphicon glyphicon-ok agree"></i>');
        }
    }).on('click', '.tracking table .agree', function () {
        var value = $(this).parents('h4.title').find('input').val();
        var model = $(this).parents('h4.title').data('model');
        var id = $(this).parents('tr').data('key');

        $('body').fadeTo(100, 0.3);
        $.ajax({
            url: '/tracking/save-title',
            type: 'post',
            dataType: 'json',
            data: {
                _csrf: yii.getCsrfToken(),
                value: value,
                id: id,
                model: model
            },
            timeout: 600000,
            success: function (data) {
                if (data.status) {
                    var title = $('tr' + '[data-key=' + id + ']').find('h4.title');
                    title.text(value);
                    $('body').fadeTo(100, 1);
                } else {
                    $('body').fadeTo(100, 1);
                }
            },
            error: function(data, textStatus) {
                alert([data.status, textStatus]);
                $('body').fadeTo(100, 1);
            }
        });
    })
        .on('click', '.chat-widget .btn-group.pull-right', function () {
            var $this = $(this);
            if ($this.parents('.chat-widget').find('#collapseOne').hasClass('show')) {
                $this.parents('.chat-widget').find('a.arrow span').removeClass('glyphicon-chevron-down');
                $this.parents('.chat-widget').find('a.arrow span').addClass('glyphicon-chevron-up');

            } else {
                $this.parents('.chat-widget').find('a.arrow span').removeClass('glyphicon-chevron-up');
                $this.parents('.chat-widget').find('a.arrow span').addClass('glyphicon-chevron-down');
            }
            $.post({
                url: '/profile/support/seen-message',
                data: {
                    _csrf: yii.getCsrfToken(),
                    ticketId: $this.parents('.parent-chat-widget').data('ticket-id'),
                    success: function() {
                        // $this.parents('.chat-widget').find('#collapseOne').removeClass('show');
                    }
                }
            });
        })
        .on('click', '.requests-index .mobile-btn', function () {
            $('#request-form').submit();
        })
        .on('click', '.checkbox-true', function () {
            $(this).parents('tr').toggleClass('active');
        })
        .on('click', '.more_options', function() {
            $('.rest_options').slideToggle( "slow" );
            if ($(this).hasClass('active')) {
                $(this).find('.more_options-text').text('Показать дополнительные опции').css('color', '#b5b5b5');
            } else {
                $(this).find('.more_options-text').text('Скрыть дополнительные опции').css('color', '#000');
            }
            $(this).toggleClass('active');
            $('.row-status').css('border-bottom', '1px solid #e9e9e9');
        })

        .on('click', '.collapse-search', function() {
            $('.common-form-wrapper').slideToggle( "slow" );
            $(this).toggleClass('passive');

            var svgElemValue = iconCollapse.getAttribute('fill');
            if(svgElemValue === "#b5b5b5") {
                iconCollapse.setAttribute('fill', '#333333');
            } else {
                iconCollapse.setAttribute('fill', '#b5b5b5');
            }
        })

        .on('click', '.page-size-limit .options', function (e) {
            if (!$(this).hasClass('active')) {
                e.preventDefault();
                $(this).addClass('active');
            }
        })
        .on('click', '*', function (e) {
            var elem = $('.page-size-limit .options');
            if (e.target != elem[0] && !elem.has(e.target).length) {
                $('.page-size-limit .options').removeClass('active');
            }
        })
        .on('submit', '#chat-form', function () {
            var $this = $(this);
            var formData = new FormData($this[0]);
            var text = $this.find('#supportform-message').val();
            var name = $this.parents('.chat-widget').find('ul.chat').data('user_name');

            var today = new Date();
            var day = today.getDate();
            var month = today.getMonth() + 1;
            var year = today.getFullYear();
            var hours = today.getHours();
            var minutes = today.getMinutes();
            var seconds = today.getSeconds();
            var date = ((day < 10) ? day = '0' + day : day) + '.' + ((month < 10) ? month = '0' + month : month) + '.' + year + ' ' + ((hours < 10) ? hours = '0' + hours : hours) + ':' + ((minutes < 10) ? minutes = '0' + minutes : minutes) + ':' + ((seconds < 10) ? seconds = '0' + seconds : seconds);

            $.ajax({
                url: '/profile/support/chat-conversation',
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function(data){
                    if (data.status) {
                        if (text.length > 0) {
                            $('.chat-widget .empty-chat').hide();
                            $this.parents('.chat-widget').find('.chat').append('<li class="left clearfix">' +
                                '<span class="chat-img pull-left">' +
                                '<img src="http://placehold.it/50/dff0d8/797979&text=ME" alt="User Avatar" class="img-circle" />' +
                                '</span>' +
                                '<div class="chat-body clearfix">' +
                                '<div class="header">' +
                                '<strong class="primary-font">' + name + '</strong> <small class="pull-right text-muted">' +
                                '<span class="glyphicon glyphicon-time"></span>' + date + '</small>' +
                                '</div>' +
                                '<p>' + text + '</p>' +
                                '</div>' +
                                '</li>')
                        }
                        chatReload();
                    }
                    $this.find('#supportform-message').val('');
                    $this.find('#supportform-file').val('').attr('disabled', false);
                }
            });

            return false;
        });

    if ($('div').is('.support')) {
        var formSubmit = true;
        document.onkeyup = function (e) {
            if ($('div.support').find('form textarea').is(':focus')) {
                if (e.ctrlKey && e.keyCode == 13) {
                    if (formSubmit) {
                        $('div.support').find('form').submit();
                        formSubmit = false;
                        $('div.support').find('form').find('button[type=submit]').removeAttr('enabled').attr('disabled', 'disabled');
                    } else {
                        return false;
                    }
                }
            }
        };
    }

    //subscription

    // var subscriprionVal = $('.profile-wrapper .profile-content div.radio input:checked').val();
    var subscriprionVal = $('.profile-wrapper .profile-content .subscription-list__block.selected').data('val');
    $('.profile-wrapper .profile-content #subscription-' + subscriprionVal + ' button[type=submit]').addClass('active-button');
    $('.profile-wrapper .profile-content .subscription-list__block').on('click', function () {
        $('.profile-wrapper .profile-content .subscription-list__block').removeClass('selected');
        $('.profile-wrapper .profile-content .form-subscription button[type=submit]').removeClass('active-button');
        var subscriprionVal = $(this).data('val');
        $('.profile-wrapper .profile-content #subscription-' + subscriprionVal + ' button[type=submit]').addClass('active-button');
        $(this).addClass('selected');
    });

    var mh = 0;
    $(".table-bordered tbody td").each(function () {
        var h_block = parseInt($(this).height());
        if(h_block > mh) {
            mh = h_block;
        }
    });
    $(".table-bordered tbody tr > td").height(mh);

    $(document).on('click', '.export_btn', function() {
        if ($(this).hasClass('active')) {
            $(this).text('Экспорт');
            $(this).removeClass('active');
            $('.export-amount label, .export label').css('opacity', '0');
            $('.table-striped .checkbox-true').prop('checked', false);
            $('.table-striped > tbody > tr').removeClass('active').find('.checkbox-true');

            $('.export-links-text-bold').text('0 позиций');
            $('.export-links').css('display', 'none');

            $.ajax({
                type: 'post',
                timeout: 200000,
                url: '/request/export-prepare',
                data: [],
                success: function (data) {
                    // $.pjax.reload({container:"#pjax-grid"});
                    console.log('success');
                    // $('.export-links-text-bold').text('0 позиций');
                    // $('.export-links').css('display', 'none');
                }
            });

        } else {
            $(this).addClass('active');
            $(this).text('Отменить экспорт');
            $('.export-amount label, .export label').css('opacity', '1');
            $('.export-links').css('display', 'block');

            // $.ajax({
            //     type: 'post',
            //     timeout: 200000,
            //     url: '/request/amount-export-items',
            //     // data: [],
            //     success: function (data) {
            //         // if (typeof data.amount !== 'undefined') {
            //         //     var result = num(data.amount, 'позицию', 'позиции', 'позиций');
            //         //     $('.export-links-text').html('Экспортировать <span class="export-links-text-bold"></span> в формате:');
            //         //     $('.export-links-text-bold').text(data.amount + " " + result);
            //         // } else {
            //         //     $('.export-links-text').html('Экспортировать <span class="export-links-text-bold">0 позиций</span> в формате:');
            //         // }
            //     }
            // });
        }
    });
});
$(function () {
    $('[data-toggle="popover"]').popover();
});

function checkItems(currentExportAmount, exportItems, elem) {
    var $this = elem || null;
    var amount = currentExportAmount;
    var requests = exportItems.requests;
    var trademarks = exportItems.trademarks;
    var ids = {};
    var tm = 0;
    var r = 0;
    var expCheck = null;
    var check = 'unchecked';

    if (!$this) {
        expCheck = $('.exp-check');

        if (expCheck.find('input').prop("checked")) {
            expCheck.parents('table').find('tbody td.export input').prop("checked", true);
            expCheck.parents('table').find('tbody tr').addClass('active');
            check = 'checked';
        } else {
            expCheck.parents('table').find('tbody td.export input').prop("checked", false);
            expCheck.parents('table').find('tbody tr').removeClass('active');
            check = 'unchecked';
        }
    } else {
        $this.toggleClass('active');

        if ($this.hasClass('active')) {
            $this.find('td.export input').prop('checked', true);
        } else {
            $this.find('td.export input').prop('checked', false);
        }
    }

    $('table').find('tbody tr').each(function () {
        var id = $(this).data('id');
        var model = $(this).data('model');

        if ($(this).find('td.export input').prop("checked")) {

            if (model == 'requests') {
                if (requests.indexOf(id) == -1) {
                    requests.push(id);
                }
            }
            if (model == 'trademarks') {
                if (trademarks.indexOf(id) == -1) {
                    trademarks.push(id);
                }
            }
        } else {
            if (model == 'requests') {
                var indexReq = requests.indexOf(id);
                if (indexReq > -1) {
                    requests.splice(indexReq, 1);
                }
            }
            if (model == 'trademarks') {
                var indexTrad = trademarks.indexOf(id);
                if (indexTrad > -1) {
                    trademarks.splice(indexTrad, 1);
                }
            }
        }
    });

    ids.requests = requests;
    ids.trademarks = trademarks;

    amount = ids.requests.length + ids.trademarks.length;

    if (amount >=1) {
        $('.export-links').css('display', 'block');
        var result = num(amount, 'позицию', 'позиции', 'позиций');
        $('.export-links-text').html('Экспортировать <span class="export-links-text-bold"></span> в формате:');
        $('.export-links-text-bold').text(amount + " " + result);
    } else {
        $('.export-links').css('display', 'none');
    }

    if ($this) {
        if ($this.hasClass('active')) {

            $.ajax({
                type: 'post',
                timeout: 200000,
                url: '/request/export-prepare',
                data: {
                    ids: ids
                },
                success: function (data) {
                    console.log('success');
                }
            });
        }
        else {
            var id = $this.data('id');
            var model = $this.data('model');

            $.ajax({
                type: 'post',
                timeout: 200000,
                url: '/request/export-prepare',
                data: {
                    id: id,
                    model: model
                },
                success: function (data) {
                    console.log('success');
                }
            });
        }
    }
    if (expCheck) {
        console.log(check);
        $.ajax({
            type: 'post',
            timeout: 200000,
            url: '/request/export-prepare',
            data: {
                ids: ids,
                check: check
            },
            success: function (data) {
                console.log('success');
            }
        });
    }

    return false;
}

//Type-list (request/trademarks)

$('body').on('click', '#type-list label', function () {
    var value = '';
    $('#type-list label').each(function () {
        if ($(this).find('input').prop("checked")) {
            value += '.' + $(this).find('input').val();
        }

    });
    value = value.slice(1);
    $('#requestsearch-type').val(value);
});



//Class-list show/hide

$('body').on('click', '#classValue', function () {
    $(this).parent('.classes').find('.checkbox-list').toggle();
});
$(document).click(function(e){
    if (e.target.id != 'classValue') {
        var elem = $('.class-list .classes .checkbox-list');
        if (e.target != elem[0] && !elem.has(e.target).length) {
            elem.hide();
        }
    }
});

//Classes select

function classesSearch(event) {
    var $this = $(this);
    var value = $this.find('input').val();
    var textVal = '';
    var classVal = '';
    if (value == 'all') {
        classVal = '';
        if ($this.find('input[value="all"]').prop("checked")) {
            $('.classes .checkbox-list label input').prop("checked", true);
            textVal = 'Все классы';
            $('.classes .checkbox-list li').each(function () {
                if ($(this).find('input').length > 0) {
                    if ($(this).find('input').val() != 'all') {
                        classVal += '.' + $(this).find('input').val();
                    }
                }
            });
            classVal = classVal.slice(1);
        } else {
            $('.classes .checkbox-list label input').prop("checked", false);
            textVal = '';
        }
    } else {
        classVal = '';
        $('.classes .checkbox-list li').each(function () {
            $(this).find('input[value="all"]').prop("checked", false);
            if ($(this).find('input').prop("checked")) {
                classVal += '.' + $(this).find('input').val();
            }
        });
        classVal = classVal.slice(1);
        textVal = classVal.split('.').join(', ');
    }
    $('.class-list .classes input[type=hidden]').val(classVal);
    var valArr = classVal.split('.');
    if (valArr.length > 3 && valArr.length != event.data.classCount) {
        textVal = 'Выбрано ' + valArr.length + ' из ' + event.data.classCount ;
    }
    $('.classes .value').text(textVal);
    console.log(classVal);
}


//Colors-list show/hide

$('body').on('click', '#colorValue', function () {
    $(this).parent('.colors').find('.checkbox-list').toggle();
});
$(document).click(function(e){
    if (e.target.id != 'colorValue') {
        var elem = $('.colors-list .colors .checkbox-list');
        if (e.target != elem[0] && !elem.has(e.target).length) {
            elem.hide();
        }
    }
});

//Colors select

function colorsSearch(event) {
    var $this = $(this);
    var value = $this.find('input').val();
    var textVal = '';
    var colorVal = '';

    $('#requestsearch-colorcondition').find('label input[value="all"]').prop("checked", true);

    if (value == 'all') {
        colorVal = '';
        if ($this.find('input[value="all"]').prop("checked")) {
            $('.colors .checkbox-list label input').prop("checked", true);
            textVal = 'Все цвета';
            $('.colors .checkbox-list li').each(function () {
                if ($(this).find('input').val() != 'all') {
                    colorVal += '.' + $(this).find('input').val();
                }
            });
            colorVal = colorVal.slice(1);
        } else {
            $('.colors .checkbox-list label input').prop("checked", false);
            textVal = '';
        }
    } else {
        colorVal = '';
        textVal = '';
        $('.colors .checkbox-list li').each(function () {
            $(this).find('input[value="all"]').prop("checked", false);
            if ($(this).find('input').prop("checked")) {
                colorVal += '.' + $(this).find('input').val();
                textVal += ', <span class="color-circle" style="background: ' + $(this).find('.color-circle').css('background-color') + '"></span>';
            }
        });
        colorVal = colorVal.slice(1);
        textVal = textVal.slice(2);
    }
    $('#requestsearch-colors').val(colorVal);
    var valArr = colorVal.split('.');
    if (valArr.length > 5 && valArr.length != event.data.colorCount) {
        textVal = 'Выбрано ' + valArr.length + ' из ' + event.data.colorCount ;
    }
    $('.colors .value').html(textVal);
}

function hideClasses() {
    var classesCount;
    if ($('.js-classes-wrap').length) {
        $('.js-classes-wrap').each(function(ind, el) {
            classesCount = 0;
            classesCount = $(el).find('a').length;
            if (classesCount > 5) {
                $(el).addClass('half-hide');
                $(el).append('<div class="show-trigger"></div>');
            }
        });
    }
    $(document).on('click', '.show-trigger', function() {
        $(this).hide();
        $(this).parents('.js-classes-wrap').removeClass('half-hide');
    });
}

$(document).on('pjax:success', '#pjax-grid', function () {
    $('html, body').animate({
        scrollTop: $('.grid-view').offset().top - $('.navbar-fixed-top').height() - 10
    }, 300);
    $('body').fadeTo(200,1);
    $('[data-toggle="popover"]').popover();
    hideClasses();
    interval = setInterval(chatReload, 7000);
});

$(document).on('pjax:start', '#pjax-grid', function () {
    clearInterval(interval);
    $('body').fadeTo(100, 0.3);
    $('.btn.btn-raised.btn-primary.btn-violet').attr('data-loading-text', '<div class="spin-loader"><span class="spin-for-input"></span></div>');
    $('.btn.btn-raised.btn-primary.btn-violet').button('loading');
    $('#requestsearch-string').attr('readonly', 'readonly');
});

$(document).on('pjax:end', '#pjax-form', function () {
    chatReload();
});


function navbarPosition() {
    if ($(window).width() < 768) {
        $('.wrap > .navbar-inverse').removeClass('navbar-fixed-top');
    } else {
        $('.wrap > .navbar-inverse').addClass('navbar-fixed-top');
    }
}

navbarPosition();

$(window).resize(function () {
    navbarPosition();
});

function num(n, one, two, five) {
    if ((n = Math.abs(n) % 100) > 4 && n < 21 || (n %= 10) > 4 || n === 0 ) return five;
    if (n > 1) return two;
    return one;
}

