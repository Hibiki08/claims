
$(document).ready(function(){

 	var shrinkHeader = 105;
		$(window).scroll(function() {
			var scroll = getCurrentScroll();
	  		if ( scroll >= shrinkHeader ) {
	       		$('.header').addClass('fix');
	    	}
	    	else {
	        	$('.header').removeClass('fix');
	    	}
		});
	function getCurrentScroll() {
    	return window.pageYOffset || document.documentElement.scrollTop;
    }


	$('.advantages-list__item8').click(function(e) {
		$('.advantages-tooltip').toggleClass('active');
		e.stopPropagation();
	});

	$('.advantages-tooltip').click(function(e) {
	  e.stopPropagation();
	});

	$(document).click(function() {
	  $('.advantages-tooltip').removeClass('active');
	});

	autosize(document.querySelector('textarea'));


	 $("#demo01").animatedModal({
	 	modalTarget:'modal-01',
        animatedIn:'lightSpeedIn',
        animatedOut:'bounceOutDown',
        color:'#957da2'
	 });


	$("#demo02").animatedModal({
        modalTarget:'modal-02',
        animatedIn:'lightSpeedIn',
        animatedOut:'bounceOutDown',
        color:'#957da2'
    });

    $("#demo03").animatedModal({
        modalTarget:'modal-03',
        animatedIn:'lightSpeedIn',
        animatedOut:'bounceOutDown',
        color:'#957da2',
        beforeOpen: function() {

            $(".m-menu").css('display', 'block');

        },
        afterClose: function() {
            $(".m-menu").hide();
        }
    });

	setTimeout ("$('.main .wow-content').fadeIn('slow');",300);
    setTimeout ("$('.main .main-form').fadeIn('slow');",900);
    setTimeout ("$('.main .mouse-link').addClass('active').fadeIn('slow');",1500);


    $(".main .mouse-link, .header__menu ul li .common-link, .opportunities-list__link, .subscription .common-text a").on("click", function (event) {

        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 500);
    });

    var wow = new WOW(
	  {
	    boxClass:     'wow',     
	    animateClass: 'animated',
	    offset:       0,          
	    mobile:       true,       
	    live:         true,       
	    scrollContainer: null,   
	    resetAnimation: true,    
	  }
	);
	wow.init();

	if (document.documentElement.clientWidth < 600) {
		$('.main-content-description').text('Поиск по номерам, словам и изображениям товарных знаков и заявок');
	    $('.main-form input').attr('placeholder', 'Что ищем?');
	    $('.header__logo img').attr('src', '/images/logo-small-index.png');
	    $('.footer .logo img').attr('src', '/images/logo-small-index.png');
	    $('.advantages-list-img .responsive').attr('src', 'img/adv-small-bg.png');

	    $('.wrapper-box__item').wrapAll("<div class='wrapper-box'></div>");
	    $('.free-list__block').addClass('wow bounceInLeft');
	    $('.free-list__block2').attr('data-wow-delay', '0.5s');
	    $('.free-list__block3').attr('data-wow-delay', '1s');
	    $('.free-list__block4').attr('data-wow-delay', '1.5s');
	    $('.advantages-list__left').removeClass('wow bounceInLeft');
	    $('.advantages-list__right').removeClass('wow bounceInRight');
	}

	function afterStringTiped() {
	    $('#elem1').next().css('opacity', '0');
	}

	/*var typed = new Typed('#elem1', {
	    strings: ["Аналитическая система поиска"],
	    typeSpeed: 40,
	    loop: false,
	    onComplete: afterStringTiped,
	});*/

	var typed2 = new Typed('#elem2', {
	    strings: ["по товарным знакам и заявкам", "по промышленным образцам", "с онлайн-консультацией поверенного", "с отслеживанием заявок и товарных знаков"],
	    typeSpeed: 70,
	    backSpeed: 20,
	    loop: true,
	    loopCount: Infinity,
	    startDelay: 2000,
	    backDelay: 1000,

	});

	$('.opportunities-widget__block').click(function() {

		$('.opportunities-widget__block').removeClass('active');
		$(this).addClass('active');

		var dataItem = $(this).data('item');
		console.log(dataItem);


		if ( $('.wrapper-items').hasClass(dataItem) ) {

			if (dataItem === "wrapper-items3") {
				$('.wrapper-items').not('.wrapper-items1, .wrapper-items2').removeClass('active');
			} else {
				$('.wrapper-items').not('.wrapper-items1').removeClass('active');
			}
			
			$('.opportunities-widget__info .wrapper-items.' + dataItem).addClass('active');
		}

		

	});

	$('.user-offer .hvr-pulse').click(function () {
		var $this = $(this);
		var offer = $('#autosize-example').val();

		$.ajax({
			url: '/site/offer',
			type: 'post',
			data: {
				offer: offer,
				_csrf: yii.getCsrfToken()
			},
			timeout: 600000,
			success: function (data) {
				if (data.status) {
					$this.parents('.advantages-tooltip').removeClass('active');
				}
			}
		});
	});
});



