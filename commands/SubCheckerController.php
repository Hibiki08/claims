<?php

namespace app\commands;

use PhpOffice\PhpSpreadsheet\Calculation\DateTime;
use Yii;
use yii\console\Controller;
use app\models\RequestSearch;
use app\models\Users;
use yii\helpers\Url;
use app\models\AuthAssignment;
use yii\db\Expression;


class SubCheckerController extends Controller {

    public function actionStart() {
        $res = true;
        $currentData = date('Y-m-d');
        $currentDataTime = date('Y-m-d H:m:s');

        $usersFirstNotice = Users::find()->where(['and', ['status' => 1], ['=', 'DATE_ADD(DATE(subscription_date), INTERVAL -3 DAY)', $currentData]])->andWhere(['left_few_days_sent' => 0])->all();
        $users = Users::find()->where(['and', ['status' => 1], ['<=', 'subscription_date', $currentDataTime]])->andWhere(['sub_expired_sent' => 0])->all();

        if (!empty($usersFirstNotice)) {
            foreach ($usersFirstNotice as $userFirstNotice) {

                $currentUser = Users::findOne(['id' => $userFirstNotice->id]);
                $currentUser->left_few_days_sent = 1;
                if ($currentUser->update() !== false) {
                    Yii::$app->mailer
                        ->compose()
                        ->setFrom(['mention@monocle.report' => Yii::$app->name . ' robot'])
                        ->setTo($currentUser->login)
                        ->setHtmlBody('Подписка на Monocle истекает через три дня: ' . date('d.m.Y', strtotime($currentUser->subscription_date)))
                        ->setSubject('Подписка истекает через три дня')
                        ->send();
                }
            }
        }

        if (!empty($users)) {
            foreach ($users as $user) {
                $expired[] = $user['id'];

                Yii::$app->mailer
                    ->compose()
                    ->setFrom(['mention@monocle.report' => Yii::$app->name . ' robot'])
                    ->setTo($user->login)
                    ->setHtmlBody('Период подписки на Monocle подошёл к концу.')
                    ->setSubject('Период подписки подошёл к концу')
                    ->send();
            }

            if (!empty($expired)) {
                if (Users::updateAll(['sub_expired_sent' => 1], ['in', 'id', $expired]) !== false) {
                    if (AuthAssignment::deleteAll(['and', ['item_name' => 'premium'], ['in', 'user_id', $expired]]) === false) {
                        $res = false;
                    }
                }
            }
        }
        if ($res) {
            echo 'OK';
        }
    }

}