<?php

namespace app\commands;

use app\models\RequestTracking;
use app\models\RequestTrackingHistory;
use app\models\SearchTrackingHistory;
use app\models\Trademarks;
use app\models\TrademarkTracking;
use app\models\TrademarkTrackingHistory;
use Yii;
use yii\console\Controller;
use app\models\SearchTracking;
use app\models\RequestSearch;
use app\models\Users;
use app\components\Parser;
use yii\helpers\Url;
use Telegram\Bot\Api;
use app\models\Requests;


class TrackingController extends Controller {

    public function actionStart() {
        $sent = false;
        $url = Yii::$app->urlManager->createAbsoluteUrl(['/tracking']);
        $telegram = new Api(Yii::$app->params['telegramBotToken']);
        $requestTracking = $this->requestTracking();
        $trademarkTracking = $this->trademarkTracking();
        $searchTracking = $this->searchTracking();

        if (!empty($requestTracking)) {
            foreach ($requestTracking as $track) {
                $sent = Yii::$app->mailer
                    ->compose()
                    ->setFrom(['mention@monocle.report' => Yii::$app->name . ' robot'])
                    ->setTo($track['email'])
                    ->setHtmlBody(($track['img'] ? '<img src="' . Yii::$app->urlManager->createAbsoluteUrl(['/']) . $track['img'] . '"><br>' : '') . 'В отслеживаемой вами заявке есть изменения: ' . implode(', ', $track['fields']) . '<br><a href="' . $url . '">Посмотреть</a>')
                    ->setSubject('В отслеживаемой вами заявке есть изменения')
                    ->send();

                if ($sent) {
                    if (isset($track['telegram_id'])) {
                        if (!empty($track['img'])) {
                            $telegram->sendPhoto(['chat_id' => $track['telegram_id'], Yii::$app->urlManager->createAbsoluteUrl(['/']) . $track['img']]);
                        }
                        $telegram->sendMessage(['chat_id' => $track['telegram_id'], 'text' => ($track['img'] ? Yii::$app->urlManager->createAbsoluteUrl(['/']) . $track['img'] . "\r\n" : '') . 'В отслеживаемой вами заявке есть изменения: ' . implode(', ', $track['fields']) . "\r\n" . $url]);
//                        $telegram->sendPhoto(['chat_id' => 385526136, ($track['img'] ? Yii::$app->urlManager->createAbsoluteUrl(['/']) . $track['img'] : '')]);
//                        $telegram->sendMessage(['chat_id' => 385526136, 'text' => 'В отслеживаемой вами заявке есть изменения: ' . implode(', ', $track['fields']) . "\r\n" . $url]);
                    }
                }
            }
        }

        if (!empty($trademarkTracking)) {
            foreach ($trademarkTracking as $track) {
                $sent = Yii::$app->mailer
                    ->compose()
                    ->setFrom(['mention@monocle.report' => Yii::$app->name . ' robot'])
                    ->setTo($track['email'])
                    ->setHtmlBody(($track['img'] ? '<img src="' . Yii::$app->urlManager->createAbsoluteUrl(['/']) . $track['img'] . '"><br>' : '') . 'В отслеживаемом вами товарном знаке есть изменения: ' . implode(', ', $track['fields']) . '<br><a href="' . $url . '">Посмотреть</a>')
                    ->setSubject('В отслеживаемом вами товарном знаке есть изменения')
                    ->send();

                if ($sent) {
                    if (isset($track['telegram_id'])) {
                        if (!empty($track['img'])) {
                            $telegram->sendPhoto(['chat_id' => $track['telegram_id'], Yii::$app->urlManager->createAbsoluteUrl(['/']) . $track['img']]);
                        }
                        $telegram->sendMessage(['chat_id' => $track['telegram_id'], 'text' => 'В отслеживаемом вами товарном знаке есть изменения: ' . implode(', ', $track['fields']) . "\r\n" . $url]);
                    }
                }
            }
        }

        if (!empty($searchTracking)) {
            foreach ($searchTracking as $message) {
                $sent = Yii::$app->mailer
                    ->compose()
                    ->setFrom(['mention@monocle.report' => Yii::$app->name . ' robot'])
                    ->setTo($message['email'])
                    ->setHtmlBody($message['event'] . ': <a href="' . $url . '">Посмотреть</a>')
                    ->setSubject('В отслеживаемом вами запросе есть изменения')
                    ->send();

                if ($sent) {
                    if (isset($message['chat_id'])) {
                        $telegram->sendMessage([ 'chat_id' => $message['chat_id'], 'text' => $message['event'] . ': ' . $url]);
                    }
                }
            }
        }

        echo 'OK';
    }

    private function requestTracking() {
        $tracks = RequestTracking::getAllWithUser();
        $i = 0;
        $messages = [];
        $parser = new Parser();
        $extraction = null;
        $contentCorrect = false;
//        Parser::$proxy = '188.128.1.37:8080';

        if (!empty($tracks)) {
            foreach ($tracks as $track) {
                if (!empty($track->request)) {
                    $date = new \DateTime($track->request->reg_date);
                    $interval = $date->diff(new \DateTime('now'));
                    $url = Parser::REQUEST_URL . $track->request->request_number;
                    if (!($interval->m > 2)) {
                        if (empty(Parser::$proxy)) {
//                            $extraction = $parser->getContent($url);
                            $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
                            foreach ($proxyList as $val) {
                                $proxy = trim($val);
                                $extraction = Parser::extract($url, $proxy);
                                $contentCorrect = $parser->contentCorrect($extraction);
                                if (!$contentCorrect) {
                                    continue;
                                } else {
                                    Parser::$proxy = trim($val);
                                    break;
                                }
//                                if (!empty($extraction)) {
//                                    if (strstr($extraction, 'Слишком быстрый просмотр документов.') ||
//                                        strstr($extraction, 'Вы заблокированы до') ||
//                                        strstr($extraction, 'Вы заблокированы до') ||
//                                        strstr($extraction, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
//                                        continue;
//                                    } else {
//                                        Parser::$proxy = trim($val);
//                                        break;
//                                    }
//                                } else {
//                                    break;
//                                }
                            }
                        } else {
                            $extraction = Parser::extract($url, Parser::$proxy);
                            $contentCorrect = $parser->contentCorrect($extraction);
                        }
                        
                        $changedValues = [];
                        $fields = [];
                        if ($contentCorrect) {
                            echo 'прокси для заявок подобран' . "\n\r";
                            if (strstr($extraction, 'Документ с данным номером отсутствует')) {
                                echo 'Документ с данным номером отсутствует ' . $track->request->request_number;
                                return false;
                            }

                            $regNumber = Parser::extractNumber($extraction, 'Номер государственной регистрации:');
                            if (is_null($regNumber)) {
                                $regNumber = Parser::extractNumber($extraction, 'Номер регистрации:');
                            }
                            $regDate =Parser::extractDate($extraction, 'Дата государственной регистрации:');
                            if (is_null($regDate)) {
                                $regDate = Parser::extractDate($extraction, 'Дата регистрации:');
                            }
                            $applicant = Parser::extractString($extraction, 'Заявитель:');
                            $facsimile = Parser::extractFacsimile($extraction, 'Факсимильные изображения');
                            $requestDate = Parser::extractDate($extraction, 'Дата поступления заявки:');
                            $address = Parser::extractLastString($extraction, 'Адрес для переписки:');
                            $firstRequestNumber = Parser::extractString($extraction, 'Номер первой заявки:');
                            $firstRequestDate = Parser::extractDate($extraction, 'Дата подачи первой заявки:');
                            $firstRequestCountry = Parser::extractString($extraction, 'Код страны подачи первой заявки:');
                            $classes = Parser::extractClasses($extraction, 'Классы МКТУ и перечень товаров и/или услуг:');
                            $img = Parser::extractImg($extraction, 'Изображение заявляемого обозначения');
                            $statusString = explode(':', Parser::extractStatusString($extraction, 'состояние делопроизводства'));
                            $status = isset($statusString[1]) ? Parser::getStatusID($statusString[1]) : 1;

                            $request = new Requests();
                            if ($track->request->reg_number != $regNumber) {
                                $changedValues['reg_number'] = $regNumber;
                                $fields[] = $request->getAttributeLabel('reg_number');
                            }
                            if ($track->request->reg_date != $regDate) {
                                $changedValues['reg_date'] = $regDate;
                                $fields[] = $request->getAttributeLabel('reg_date');
                            }
                            if ($track->request->applicant != $applicant) {
                                $changedValues['applicant'] = $applicant;
                                $fields[] = $request->getAttributeLabel('applicant');
                            }
                            if ($track->request->facsimile != $facsimile) {
                                $changedValues['facsimile'] = $facsimile;
                                $fields[] = $request->getAttributeLabel('facsimile');
                            }
                            if ($track->request->request_date != $requestDate) {
                                $changedValues['request_date'] = $requestDate;
                                $fields[] = $request->getAttributeLabel('request_date');
                            }
                            if ($track->request->address != $address) {
                                $changedValues['address'] = $address;
                                $fields[] = $request->getAttributeLabel('address');
                            }
                            if ($track->request->first_request_number != $firstRequestNumber) {
                                $changedValues['first_request_number'] = $firstRequestNumber;
                                $fields[] = $request->getAttributeLabel('first_request_number');
                            }
                            if ($track->request->first_request_date != $firstRequestDate) {
                                $changedValues['first_request_date'] = $firstRequestDate;
                                $fields[] = $request->getAttributeLabel('first_request_date');
                            }
                            if ($track->request->first_request_country != $firstRequestCountry) {
                                $changedValues['first_request_country'] = $firstRequestCountry;
                                $fields[] = $request->getAttributeLabel('first_request_country');
                            }

                            if (!empty($track->request->classes)) {
                                $diffClasses = array_diff((is_array($classes) ? $classes : []), unserialize($track->request->classes));
                                if (!empty($diffClasses)) {
                                    $changedValues['classes'] = serialize($classes);
                                    $fields[] = $request->getAttributeLabel('classes');
                                }
                            } else {
                                if (!empty($classes)) {
                                    $changedValues['classes'] = serialize($classes);
                                    $fields[] = $request->getAttributeLabel('classes');
                                }
                            }

                            $hash = !empty($track->request->img) ? hash_file('md5', Yii::$app->urlManager->createAbsoluteUrl([$track->request->img])) : null;
                            if (!empty($img) && !empty($hash)) {
                                if ($hash != hash_file('md5', $img)) {
                                    if ($path = Parser::saveRequestImage($img)) {
                                        $changedValues['img'] = $path;
                                        $fields[] = $request->getAttributeLabel('img');
                                    }
                                }
                            }

                            if ($track->request->status_id != $status) {
                                $changedValues['status_id'] = $status;
                                $fields[] = $request->getAttributeLabel('status_id');
                            }

                            if (!empty($changedValues)) {
                                $res = Requests::updateAll($changedValues, ['request_number' => $track->request->request_number]);
                                if (isset($changedValues['classes'])) {
                                    Parser::saveRequestClasses($classes, $track->request->id);
                                }
                                if ($res !== false) {
                                    $history = new RequestTrackingHistory();
                                    $history->event = 'Изменилось: ' . implode(', ', $fields);
                                    $history->request_tracking_id = $track->id;
                                    if ($history->save()) {
                                        $messages[$i]['title'] = $track->title;
                                        $messages[$i]['request_number'] = $track->request->request_number;
                                        $messages[$i]['img'] = $track->request->thumb;
                                        $messages[$i]['email'] = $track->user->login;
                                        $messages[$i]['fields'] = $fields;
                                        if ($track->user->telegram_notice) {
                                            $messages[$i]['telegram_id'] = $track->user->telegram_id;
                                        }
                                        $i++;

                                        $trackRequest = RequestTracking::findOne(['id' => $track->id]);
                                        $trackRequest->notice = 1;
                                        $trackRequest->update();
                                    }
                                }
                            }
                        }
                    }
                }
                sleep(3);
            }
        }
        return $messages;
    }

    private function trademarkTracking() {
        $tracks = TrademarkTracking::getAllWithUser();
        $i = 0;
        $messages = [];
        $contentCorrect = false;
        $parser = new Parser();
        $extraction = null;
        
        if (!empty($tracks)) {
            foreach ($tracks as $track) {
                if (!empty($track->trademark)) {
                    $date = new \DateTime($track->trademark->reg_date);
                    $interval = $date->diff(new \DateTime('now'));
                    $url = Parser::TRADEMARK_URL . $track->trademark->reg_number;
                    if (!($interval->m > 2)) {
                        if (empty(Parser::$proxy)) {
                            $proxyList = explode("\n", Yii::$app->proxy->getProxyList());
                            foreach ($proxyList as $val) {
                                $proxy = trim($val);
                                $extraction = Parser::extract($url, $proxy);
                                $contentCorrect = $parser->contentCorrect($extraction);
                                if (!$contentCorrect) {
                                    continue;
                                } else {
                                    Parser::$proxy = trim($val);
                                    break;
                                }
//                                if (!empty($extraction)) {
//                                    if (strstr($extraction, 'Слишком быстрый просмотр документов.') ||
//                                        strstr($extraction, 'Вы заблокированы до') ||
//                                        strstr($extraction, 'Превышен допустимый предел количества просмотров документов из реестра в день.')) {
//                                        continue;
//                                    } else {
//                                        Parser::$proxy = trim($val);
//                                        break;
//                                    }
//                                }
                            }
                        } else {
                            $extraction = Parser::extract($url, Parser::$proxy);
                            $contentCorrect = $parser->contentCorrect($extraction);
                        }

                        $changedValues = [];
                        $fields = [];
                        if ($contentCorrect) {
                            echo 'прокси для ТЗ подобран' . "\n\r";
                            if (strstr($extraction, 'Документ с данным номером отсутствует')) {
                                echo 'Документ с данным номером отсутствует ' . $track->trademark->reg_number;
                                return false;
                            }

                            $requestNumber = Parser::extractNumber($extraction, 'Номер заявки:');
                            $regDate = Parser::extractDate($extraction, 'Дата государственной регистрации:');
                            if (is_null($regDate)) {
                                $regDate = Parser::extractDate($extraction, 'Дата регистрации:');
                            }
                            $requestDate = Parser::extractDate($extraction, 'Дата подачи заявки:');
                            $regExpiryDate = Parser::extractDate($extraction, 'Дата истечения срока действия регистрации:');
                            if (is_null($regExpiryDate)) {
                                $regExpiryDate = Parser::extractDate($extraction, 'Дата истечения срока действия исключительного права:');
                            }
                            if (($temp = Parser::extractDate($extraction, 'Дата, до которой продлен срок действия регистрации:', true)) != null) {
                                $regExpiryDate = $temp;
                            }
                            if (($temp = Parser::extractDate($extraction, 'Дата, до которой продлен срок действия исключительного права:', true)) != null) {
                                $regExpiryDate = $temp;
                            }
                            $rightholder = Parser::extractLastString($extraction, 'Правообладатель:');
                            if (is_null($rightholder)) {
                                $rightholder = Parser::extractLastString($extraction, 'Имя правообладателя:');
                            }
                            $publishDate = Parser::extractDate($extraction, 'Дата публикации:');
                            $publicationPdf = Parser::extractPublication($extraction, 'Дата публикации:');
                            $legalProtectionEndDate = Parser::extractDate($extraction, 'Дата прекращения правовой охраны товарного знака:');
                            if (is_null($legalProtectionEndDate)) {
                                $legalProtectionEndDate = Parser::extractDate($extraction, 'Дата прекращения правовой охраны:');
                            }
                            $address = Parser::extractLastString($extraction, 'Адрес для переписки:');
                            $firstRequestNumber = Parser::extractString($extraction, 'Номер первой заявки:');
                            $firstRequestDate = Parser::extractDate($extraction, 'Дата подачи первой заявки:');
                            $firstRequestCountry = Parser::extractString($extraction, 'Код страны или международной организации, куда была подана первая заявка: ');
                            $colors = Parser::extractString($extraction, 'Указание цвета или цветового сочетания:');
                            $classes = Parser::extractTMClasses($extraction, 'Классы МКТУ и перечень товаров и/или услуг:');
                            $priority = Parser::extractDate($extraction, 'Приоритет:');
                            if (is_null($priority)) {
                                $priority = Parser::extractDate($extraction, 'Дата приоритета:');
                            }
                            $licensee = Parser::extractString($extraction, 'Наименование лицензиата:');
                            $contractDate = Parser::extractContractDate($extraction, 'Дата и номер регистрации договора:');
                            $contractNumber = Parser::extractContractNumber($extraction, 'Дата и номер регистрации договора:');
                            $licenseTerms = Parser::extractString($extraction, 'Указание условий и/или ограничений лицензии:');
                            $img = Parser::extractImg($extraction, 'Изображение (воспроизведение) товарного знака, знака обслуживания');
                            if (is_null($img)) {
                                $img = Parser::extractImg($extraction, 'Изображение товарного знака, знака обслуживания');
                                if (is_null($img)) {
                                    $img = Parser::extractImg($extraction, '(540)');
                                }
                            }
                            Parser::saveTrademarkImage($img, $track->trademark->reg_number);
                            $statusString = explode('(', Parser::extractStatusString($extraction, 'Статус:'));
                            $status = Parser::getStatusID($statusString[0]);
                            $statusDate = !empty($statusString[1]) ? Parser::extractStatusDate($status[1]) : null;

                            $trademark = new Trademarks();
                            if ($track->trademark->request_number != $requestNumber) {
                                $changedValues['request_number'] = $requestNumber;
                                $fields[] = $trademark->getAttributeLabel('request_number');
                            }
                            if ($track->trademark->reg_date != $regDate) {
                                $changedValues['reg_date'] = $regDate;
                                $fields[] = $trademark->getAttributeLabel('reg_date');
                            }
                            if ($track->trademark->request_date != $requestDate) {
                                $changedValues['request_date'] = $requestDate;
                                $fields[] = $trademark->getAttributeLabel('request_date');
                            }
                            if ($track->trademark->reg_expiry_date != $regExpiryDate) {
                                $changedValues['reg_expiry_date'] = $regExpiryDate;
                                $fields[] = $trademark->getAttributeLabel('reg_expiry_date');
                            }
                            if ($track->trademark->rightholder != $rightholder) {
                                $changedValues['rightholder'] = $rightholder;
                                $fields[] = $trademark->getAttributeLabel('rightholder');
                            }
                            if ($track->trademark->publish_date != $publishDate) {
                                $changedValues['publish_date'] = $publishDate;
                                $fields[] = $trademark->getAttributeLabel('publish_date');
                            }
                            if ($track->trademark->publication_pdf != $publicationPdf) {
                                $changedValues['publication_pdf'] = $publicationPdf;
                                $fields[] = $trademark->getAttributeLabel('publication_pdf');
                            }
                            if ($track->trademark->legal_protection_end_date != $legalProtectionEndDate) {
                                $changedValues['legal_protection_end_date'] = $legalProtectionEndDate;
                                $fields[] = $trademark->getAttributeLabel('legal_protection_end_date');
                            }
                            if ($track->trademark->address != $address) {
                                $changedValues['address'] = $address;
                                $fields[] = $trademark->getAttributeLabel('address');
                            }
                            if ($track->trademark->first_request_number != $firstRequestNumber) {
                                $changedValues['first_request_number'] = $firstRequestNumber;
                                $fields[] = $trademark->getAttributeLabel('first_request_number');
                            }
                            if ($track->trademark->first_request_date != $firstRequestDate) {
                                $changedValues['first_request_date'] = $firstRequestDate;
                                $fields[] = $trademark->getAttributeLabel('first_request_date');
                            }
                            if ($track->trademark->first_request_country != $firstRequestCountry) {
                                $changedValues['first_request_country'] = $firstRequestCountry;
                                $fields[] = $trademark->getAttributeLabel('first_request_country');
                            }
                            if ($track->trademark->colors != $colors) {
                                $changedValues['colors'] = $colors;
                                $fields[] = $trademark->getAttributeLabel('colors');
                            }
                            if (!empty($track->trademark->classes)) {
                                $diffClasses = array_diff((is_array($classes) ? $classes : []), unserialize($track->trademark->classes));
                                if (!empty($diffClasses)) {
                                    $changedValues['classes'] = serialize($classes);
                                    $fields[] = $trademark->getAttributeLabel('classes');
                                }
                            } else {
                                if (!empty($classes)) {
                                    $changedValues['classes'] = serialize($classes);
                                    $fields[] = $trademark->getAttributeLabel('classes');
                                }
                            }
                            if ($track->trademark->priority != $priority) {
                                $changedValues['priority'] = $priority;
                                $fields[] = $trademark->getAttributeLabel('priority');
                            }
                            if ($track->trademark->licensee != $licensee) {
                                $changedValues['licensee'] = $licensee;
                                $fields[] = $trademark->getAttributeLabel('licensee');
                            }
                            if ($track->trademark->contract_date != $contractDate) {
                                $changedValues['contract_date'] = $contractDate;
                                $fields[] = $trademark->getAttributeLabel('contract_date');
                            }
                            if ($track->trademark->contract_number != $contractNumber) {
                                $changedValues['contract_number'] = $contractNumber;
                                $fields[] = $trademark->getAttributeLabel('contract_number');
                            }
                            if ($track->trademark->license_terms != $licenseTerms) {
                                $changedValues['license_terms'] = $licenseTerms;
                                $fields[] = $trademark->getAttributeLabel('license_terms');
                            }
                            $hash = !empty($track->trademark->img) ? hash_file('md5', Yii::$app->urlManager->createAbsoluteUrl([$track->trademark->img])) : null;
                            if (!empty($img) && !empty($hash)) {
                                if ($hash != hash_file('md5', $img)) {
                                    if ($path = Parser::saveTrademarkImage($img, $track->trademark->reg_number)) {
                                        $changedValues['img'] = $path;
                                        $fields[] = $trademark->getAttributeLabel('img');
                                    }
                                }
                            }
                            if ($track->trademark->status_id != $status) {
                                $changedValues['status_id'] = $status;
                                $fields[] = $trademark->getAttributeLabel('status_id');
                            }
                            if ($track->trademark->status_date != $statusDate) {
                                $changedValues['status_date'] = $statusDate;
                                $fields[] = $trademark->getAttributeLabel('status_date');
                            }

                            if (!empty($changedValues)) {
                                $res = Trademarks::updateAll($changedValues, ['reg_number' => $track->trademark->reg_number]);
                                if (isset($changedValues['classes'])) {
                                    Parser::saveTMClasses($classes, $track->trademark->id);
                                }
                                if ($res !== false) {
                                    $history = new TrademarkTrackingHistory();
                                    $history->event = 'Изменилось: ' . implode(', ', $fields);
                                    $history->trademark_tracking_id = $track->id;
                                    if ($history->save()) {
                                        $messages[$i]['title'] = $track->title;
                                        $messages[$i]['reg_number'] = $track->trademark->reg_number;
                                        $messages[$i]['email'] = $track->user->login;
                                        $messages[$i]['img'] = $track->trademark->thumb;
                                        $messages[$i]['fields'] = $fields;
                                        if ($track->user->telegram_notice) {
                                            $messages[$i]['telegram_id'] = $track->user->telegram_id;
                                        }
                                        $i++;

                                        $trackTrademark = TrademarkTracking::findOne(['id' => $track->id]);
                                        $trackTrademark->notice = 1;
                                        $trackTrademark->update();
                                    }
                                }
                            }
                        }
                    }
                }
                sleep(3);
            }
        }
        return $messages;
    }

    private function searchTracking() {
        $tracks = SearchTracking::getAllWithUser();
        $messages = [];

        if (!empty($tracks)) {
            $i = 0;
            foreach ($tracks as $track) {
                $request = unserialize($track['request']);
                $request['add'] = 1;
                $searchModel = new RequestSearch();
                $searchModel->search($request, false);
                $matches = RequestSearch::$count;
                $ids = RequestSearch::$ids;
                unset($request['add']);

                if ($matches != $track['matches']) {
                    $tracking = SearchTracking::findOne(['id' => $track['id']]);
                    $tracking->notice = 1;
                    $tracking->matches = $matches;
                    if (!empty($tracking->request_ids)) {
                        $oldRequestIds = unserialize($tracking->request_ids);
                        if (isset($ids['requests'])) {
                            $newRequestIds = $ids['requests'];
                        } else {
                            $newRequestIds = [];
                        }
//                        $newRequestIds = isset($ids['requests']) ? $ids['requests'] : [];

                        $diffOldRequestid = array_diff($oldRequestIds, $newRequestIds);
                        $diffNewRequestid = array_diff($newRequestIds, $oldRequestIds);

                        $tracking->request_ids = serialize($newRequestIds);
                    }
                    if (!empty($tracking->trademark_ids)) {
                        $oldTrademarkIds = unserialize($tracking->trademark_ids);
                        if (isset($ids['trademarks'])) {
                            $newTrademarkIds = $ids['trademarks'];
                        } else {
                            $newTrademarkIds = [];
                        }
//                        $newTrademarkIds = isset($ids['trademarks']) ? $ids['trademarks'] : [];

                        $diffOldTrademarkid = array_diff($oldTrademarkIds, $newTrademarkIds);
                        $diffNewTrademarkid = array_diff($newTrademarkIds, $oldTrademarkIds);

                        $tracking->trademark_ids = serialize($newTrademarkIds);
                    }
                    $tracking->update();

                    if (!empty($diffOldRequestid) || !empty($diffOldTrademarkid)) {
                        $history = new SearchTrackingHistory();
                        if (!empty($diffOldRequestid) && !empty($diffOldTrademarkid)) {
                            $history->event = 'Число совпадений уменьшилось';
                            $history->requests_diff_ids = serialize($diffOldRequestid);
                            $history->trademarks_diff_ids = serialize($diffOldTrademarkid);
                            $history->count = count($diffOldRequestid) + count($diffOldTrademarkid);
                        } else {
                            if (!empty($diffOldRequestid) && empty($diffOldTrademarkid)) {
                                $history->event = 'Число заявок уменьшилось';
                                $history->requests_diff_ids = serialize($diffOldRequestid);
                                $history->count = count($diffOldRequestid);
                            }
                            if (empty($diffOldRequestid) && !empty($diffOldTrademarkid)) {
                                $history->event = 'Число товарных знаков уменьшилось';
                                $history->trademarks_diff_ids = serialize($diffOldTrademarkid);
                                $history->count = count($diffOldTrademarkid);
                            }
                        }
                        if ($track->user['telegram_notice']) {
                            $messages[$i]['chat_id'] = $track->user['telegram_id'];
                        }
                        $messages[$i]['event'] = $history->event . ' на ' . $history->count;
                        $messages[$i]['email'] = $track->user['login'];
                        $history->search_tracking_id = $tracking->id;
                        $history->save();
                    }

                    if (!empty($diffNewRequestid) || !empty($diffNewTrademarkid)) {
                        $history = new SearchTrackingHistory();
                        if (!empty($diffNewRequestid) && !empty($diffNewTrademarkid)) {
                            $history->event = 'Число совпадений увеличилось';
                            $history->requests_diff_ids = serialize($diffNewRequestid);
                            $history->trademarks_diff_ids = serialize($diffNewTrademarkid);
                            $history->count = count($diffNewRequestid) + count($diffNewTrademarkid);
                        } else {
                            if (!empty($diffNewRequestid) && empty($diffNewTrademarkid)) {
                                $history->event = 'Число заявок увеличилось';
                                $history->requests_diff_ids = serialize($diffNewRequestid);
                                $history->count = count($diffNewRequestid);
                            }
                            if (empty($diffNewRequestid) && !empty($diffNewTrademarkid)) {
                                $history->event = 'Число товарных знаков увеличилось';
                                $history->trademarks_diff_ids = serialize($diffNewTrademarkid);
                                $history->count = count($diffNewTrademarkid);
                            }
                        }
                        if ($track->user['telegram_notice']) {
                            $messages[$i+1]['chat_id'] = $track->user['telegram_id'];
                        }
                        $messages[$i+1]['event'] = $history->event . ' на ' . $history->count;
                        $messages[$i+1]['email'] = $track->user['login'];
                        $history->search_tracking_id = $tracking->id;
                        $history->save();
                        $i++;
                    }

                    $i++;
                }
            }

        }
        return $messages;
    }

}