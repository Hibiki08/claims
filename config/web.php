<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$dbProm = require(__DIR__ . '/db_prom.php');

$config = [
    'id' => 'basic',
    'name' => 'Monocle',
    'sourceLanguage' => 'en-US',
    'language' => 'rus-RUS',
    'timezone' => 'UTC',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
//    'defaultRoute' => 'site/login',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'K7ee3m2YNmDJC22eTuhfmpFZUxFUuKoi',
            'baseUrl' => '',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'MonocleMentionBot' => [
            'class' => 'app\components\MonocleMentionBot',
        ],
        'MonocleSupportBot' => [
            'class' => 'app\components\MonocleSupportBot',
        ],
        'cache' => [
            'class' => 'yii\caching\DbCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [ // The pattern app* indicates that all message categories whose names start with app should be translated using this message source.
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // \messages\en\app.php
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/auth' => 'auth.php'
                    ]
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
            'viewPath' => '@app/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'mention@monocle.report',
                'password' => 'h1na9SYHeQZIaLxvZvys',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 0 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'dbProm' => $dbProm,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'service/recognition/tm-foreign' => 'recognition-tm-foreign/index',
                'service/recognition/tm-foreign/<action>' => 'recognition-tm-foreign/<action>',
                'service/recognition/requests' => 'recognition-requests/index',
                'service/recognition/requests/<action>' => 'recognition-requests/<action>',
                'service/recognition/trademarks' => 'recognition-trademarks/index',
                'service/recognition/trademarks/<action>' => 'recognition-trademarks/<action>',
                'service/<controller:(users|statistics|logs)>' => '<controller>/index',
                'service/<controller:(users|statistics|logs)>/<action>' => '<controller>/<action>',
                'profile/<controller:(support|subscription)>' => '<controller>/index',
                'profile/<controller:(support|subscription)>/<action>' => '<controller>/<action>',
                '<controller:(rospatent)>/<action:(zayavka)>/<number:\d+>.html' => '<controller>/<action>',
                '<controller:(rospatent)>/<action:(znak)>/<number:\d+(\/\d+)?>.html' => '<controller>/<action>',
                '<action:(request-password-reset|reset-password|confirm-email|login|logout)>' => 'site/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>/<params:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller>' => '<controller>/index',
            ],
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=monocle.report;port=9306;',
            'username' => '',
            'password' => '',
        ],
        'captcha' => [
            'class' => 'jumper423\Captcha',
            'pathTmp' => '@app/captcha',
            'apiKey' => '249a4ae8473452a90a9e6639836765a2',
            'minLen' => 3,
            'maxTimeout' => 180,
//            'textinstructions' => 'Напишите текст, который видите на логотипе'
        ],
        'proxy' => [
            'class' => 'app\components\Proxy',
            'key' => '9ff64c9cfc59d33fb21c7a8a2d4428e4',
        ],
        'google' => [
            'class' => 'app\components\GoogleApi',
            'apiKey' => 'AIzaSyAW62HlGWlXxUDvcTbZoepJ-27AYP63OHg',
            'cx' => '002032871122813009730:jnraegpetq8',
        ],
        'walletone' => [
            'class' => 'bookin\walletone\WalletOne',
            'secretKey' => '744a76506a4a575441354f73684149587770793344534e47735675',
            'signatureMethod' => 'sha1',
            'buttonLabel' => 'Оплатить',
            'walletOptions' => [
                'WMI_MERCHANT_ID' => '168292426232',
                'WMI_CURRENCY_ID' => '643',
                'WMI_SUCCESS_URL' => ['profile/subscription/success'],
                'WMI_FAIL_URL' => ['profile/subscription/fail'],
            ]
        ],
    ],
    'on beforeAction' => function ($event) {
//        if (Yii::$app->user->isGuest) {
//            if (Yii::$app->controller->route != 'site/login') {
//                echo '<div style="width: 100%; height: 100vh; background: url(/images/works.jpg) no-repeat center; "></div>';
//                die;
//            }
//        } else {
//            if (!Yii::$app->user->can('superAdminPermission')) {
//                echo '<div style="width: 100%; height: 100vh; background: url(/images/works.jpg) no-repeat center; "></div>';
//                die;
//            }
//        }
    },
    'modules' => [
        'gridview' => ['class' => 'kartik\grid\Module']
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    /*$config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['*'],
    ];*/

    /*$config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];*/
}

return $config;
