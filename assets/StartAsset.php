<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StartAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/lib/jquery-ui-1.12.1.custom/jquery-ui.min.css',
        'css/add-style.css?r6',
    ];
    public $js = [
        '/lib/jquery-ui-1.12.1.custom/jquery-ui.min.js',
        'js/fn.js?v=0.39'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
