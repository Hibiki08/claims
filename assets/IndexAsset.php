<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=cyrillic',
        'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
        'css/normalize.css',
        'css/hover.css',
        'css/animate.min.css',
        'css/style.css?r=1',
        'css/media.css',
    ];
    public $js = [
        'https://code.jquery.com/jquery-3.3.1.min.js',
        'js/index/autosize.min.js',
        'js/index/animatedModal.js',
        'js/index/wow.min.js',
        'js/index/typed.min.js',
        'js/index/script.js?v=1'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
