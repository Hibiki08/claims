<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/lib/jquery-ui-1.12.1.custom/jquery-ui.min.css',
        'css/multiple-select.css',
        '/lib/fancybox-master/dist/jquery.fancybox.min.css',
        'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
        '/lib/slick/dist/slick.css',
        '/lib/slick/dist/slick-theme.css',
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
//        'css/style.css?v=0.1',
        'css/site.css?v=1.4',
        'css/media.css?v=0.1',
    ];
    public $js = [
        '/lib/jquery-ui-1.12.1.custom/jquery-ui.min.js',
        'https://rawgit.com/tuupola/jquery_lazyload/2.x/lazyload.js',
        'js/multiple-select.js?v=0.23',
        '/lib/fancybox-master/dist/jquery.fancybox.min.js',
        '/lib/slick/dist/slick.min.js',
        'js/fn.js?v=0.87',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
